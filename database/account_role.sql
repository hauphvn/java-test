/*
 Navicat Premium Data Transfer

 Source Server         : GP-mysql
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : 192.168.64.2:3306
 Source Schema         : kinglogi

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 02/07/2020 00:34:48
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for account_role
-- ----------------------------
DROP TABLE IF EXISTS `account_role`;
CREATE TABLE `account_role` (
  `role_id` bigint(20) NOT NULL,
  `account_id` bigint(20) NOT NULL,
  PRIMARY KEY (`role_id`,`account_id`),
  KEY `fk_account_role_account1` (`account_id`),
  CONSTRAINT `fk_account_role_account1` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_account_role_role1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of account_role
-- ----------------------------
BEGIN;
INSERT INTO `account_role` VALUES (1, 1);
INSERT INTO `account_role` VALUES (1, 7);
INSERT INTO `account_role` VALUES (2, 4);
INSERT INTO `account_role` VALUES (3, 5);
INSERT INTO `account_role` VALUES (4, 6);
INSERT INTO `account_role` VALUES (5, 3);
INSERT INTO `account_role` VALUES (6, 2);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
