/*
 Navicat Premium Data Transfer

 Source Server         : GP-mysql
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : 192.168.64.2:3306
 Source Schema         : kinglogi

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 02/07/2020 00:28:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for feature
-- ----------------------------
DROP TABLE IF EXISTS `feature`;
CREATE TABLE `feature` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` bigint(20) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` bigint(20) DEFAULT NULL,
  `api_path` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKlqtnexxep9h8avuryfstf6eqk` (`code`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of feature
-- ----------------------------
BEGIN;
INSERT INTO `feature` VALUES (1, '2020-07-01 14:41:13', 1, '2020-06-30 17:07:47', 1, '/trip-catalogs', 'Danh mục tuyến', 'TRIP_CATALOGS', 'MASTER', 1);
INSERT INTO `feature` VALUES (2, '2020-07-01 14:41:15', 1, '2020-06-30 17:07:47', 1, '/fixed-trips', 'Quản lý tuyến', 'FIXED_TRIPS', 'FIXED_TRIP', 1);
INSERT INTO `feature` VALUES (3, '2020-07-01 17:25:10', 1, '2020-06-30 17:07:47', 1, '/fixed-trip-auctions', 'Đấu giá', 'FIXED_TRIP_AUCTIONS', 'FIXED_TRIP', 2);
INSERT INTO `feature` VALUES (4, '2020-07-01 17:25:42', 1, '2020-06-30 17:07:47', 1, '/fiexed-trip-orders', 'Đơn hàng', 'FIXED_TRIP_ORDERS', 'FIXED_TRIP', 3);
INSERT INTO `feature` VALUES (5, '2020-07-01 14:41:19', 1, '2020-06-30 17:07:47', 1, '/flexible-trips', 'Quản lý tuyến', 'FLEXIBLE_TRIPS', 'FLEXIBLE_TRIP', 1);
INSERT INTO `feature` VALUES (6, '2020-07-01 17:25:08', 1, '2020-06-30 17:07:47', 1, '/flexible-trip-auctions', 'Đấu giá', 'FLEXIBLE_TRIP_AUCTIONS', 'FLEXIBLE_TRIP', 2);
INSERT INTO `feature` VALUES (7, '2020-07-01 17:25:41', 1, '2020-06-30 17:07:47', 1, '/flexible-trip-orders', 'Đơn hàng', 'FLEXIBLE_TRIP_ORDERS', 'FLEXIBLE_TRIP', 3);
INSERT INTO `feature` VALUES (8, '2020-07-01 14:41:47', 1, '2020-06-30 17:07:47', 1, '/admin-accounts', 'Quản lý Admin', 'ADMIN_ACCOUNTS', 'SYSTEM', 1);
INSERT INTO `feature` VALUES (9, '2020-07-01 14:41:49', 1, '2020-06-30 17:07:47', 1, '/partner-accounts', 'Đối tác', 'PARTNER_ACCOUNTS', 'SYSTEM', 2);
INSERT INTO `feature` VALUES (10, '2020-07-01 14:41:50', 1, '2020-06-30 17:07:47', 1, '/collaborator-accounts', 'Cộng tác viên', 'COLLABORATOR_ACCOUNTS', 'SYSTEM', 3);
INSERT INTO `feature` VALUES (11, '2020-07-01 14:41:52', 1, '2020-06-30 17:07:47', 1, '/members-accounts', 'Thành viên', 'MEMBER_ACCOUNTS', 'SYSTEM', 4);
INSERT INTO `feature` VALUES (12, '2020-07-01 14:41:53', 1, '2020-06-30 17:07:47', 1, '/banks', 'Tài khoản ngân hàng', 'BANKS', 'SYSTEM', 5);
INSERT INTO `feature` VALUES (13, '2020-07-01 14:41:54', 1, '2020-06-30 17:07:47', 1, '/contacts', 'Thông tin liên hệ', 'CONTACTS', 'SYSTEM', 6);
INSERT INTO `feature` VALUES (14, '2020-07-01 17:18:46', 1, '2020-06-30 17:07:47', 1, '/mail-templates', 'Template gửi mail', 'MAIL_TEMPLATES', 'SYSTEM', 7);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
