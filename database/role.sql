/*
 Navicat Premium Data Transfer

 Source Server         : GP-mysql
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : 192.168.64.2:3306
 Source Schema         : kinglogi

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 02/07/2020 00:28:32
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_UNIQUE` (`code`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `UKc36say97xydpmgigg38qv5l2p` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of role
-- ----------------------------
BEGIN;
INSERT INTO `role` VALUES (1, 'Super Admin', 'SUPER_ADMIN', NULL, NULL, NULL, NULL);
INSERT INTO `role` VALUES (2, 'Admin tuyến cố định', 'FIXED_TRIP_ADMIN', NULL, NULL, NULL, NULL);
INSERT INTO `role` VALUES (3, 'Admin tuyến không cố định', 'FLEXIBLE_TRIP_ADMIN', NULL, NULL, NULL, NULL);
INSERT INTO `role` VALUES (4, 'Cộng tác viên', 'COLLABORATOR', NULL, NULL, NULL, NULL);
INSERT INTO `role` VALUES (5, 'Thành viên', 'MEMBER', NULL, NULL, NULL, NULL);
INSERT INTO `role` VALUES (6, 'Đối tác', 'PARTNER', NULL, NULL, NULL, NULL);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
