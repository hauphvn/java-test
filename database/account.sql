/*
 Navicat Premium Data Transfer

 Source Server         : GP-mysql
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : 192.168.64.2:3306
 Source Schema         : kinglogi

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 02/07/2020 00:34:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for account
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `full_name` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `email` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `gender` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_name_UNIQUE` (`username`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `UKgex1lmaqpg0ir5g1f5eftyaa1` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of account
-- ----------------------------
BEGIN;
INSERT INTO `account` VALUES (1, 'ptgiang56it@gmail.com', '$2a$10$SQHLKFEu5x5cRIquwVbmk.S98Th.8DEsfVcmlg7Gx1C0JA.t1hlSK', NULL, NULL, 'Giang Phan', '12112121212', 1, NULL, 'ptgiang56it@gmail.com', NULL, NULL, 'M');
INSERT INTO `account` VALUES (2, 'partner1@gmail.com', '$2a$10$SQHLKFEu5x5cRIquwVbmk.S98Th.8DEsfVcmlg7Gx1C0JA.t1hlSK', NULL, NULL, 'User partner', '12112121212', 1, NULL, 'ptgiang56it@gmail.com', NULL, NULL, 'M');
INSERT INTO `account` VALUES (3, 'member1@gmail.com', '$2a$10$SQHLKFEu5x5cRIquwVbmk.S98Th.8DEsfVcmlg7Gx1C0JA.t1hlSK', NULL, NULL, 'User Member', '12112121212', 1, NULL, 'ptgiang56it@gmail.com', NULL, NULL, 'F');
INSERT INTO `account` VALUES (4, 'fixed-admin1@gmail.com', '$2a$10$SQHLKFEu5x5cRIquwVbmk.S98Th.8DEsfVcmlg7Gx1C0JA.t1hlSK', NULL, NULL, 'User fixed admin', '12112121212', 1, NULL, 'ptgiang56it@gmail.com', NULL, NULL, 'F');
INSERT INTO `account` VALUES (5, 'flexible-admin1@gmail.com', '$2a$10$SQHLKFEu5x5cRIquwVbmk.S98Th.8DEsfVcmlg7Gx1C0JA.t1hlSK', NULL, NULL, 'User flexible admin', '12112121212', 1, NULL, 'ptgiang56it@gmail.com', NULL, NULL, 'F');
INSERT INTO `account` VALUES (6, 'collaborator1@gmail.com', '$2a$10$SQHLKFEu5x5cRIquwVbmk.S98Th.8DEsfVcmlg7Gx1C0JA.t1hlSK', NULL, NULL, 'User collaborator', '12112121212', 1, NULL, 'ptgiang56it@gmail.com', NULL, NULL, 'F');
INSERT INTO `account` VALUES (7, 'superadmin1@gmail.com', '$2a$10$SQHLKFEu5x5cRIquwVbmk.S98Th.8DEsfVcmlg7Gx1C0JA.t1hlSK', NULL, NULL, 'User Super Admin', '12112121212', 1, NULL, 'ptgiang56it@gmail.com', NULL, NULL, 'F');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
