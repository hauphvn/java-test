/*
 Navicat Premium Data Transfer

 Source Server         : GP-mysql
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : 192.168.64.2:3306
 Source Schema         : kinglogi

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 03/07/2020 00:09:28
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for feature_role
-- ----------------------------
DROP TABLE IF EXISTS `feature_role`;
CREATE TABLE `feature_role` (
  `feature_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`feature_id`,`role_id`),
  KEY `FK54h9lrovttmh2j50nmkk6xoxc` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of feature_role
-- ----------------------------
BEGIN;
INSERT INTO `feature_role` VALUES (1, 1);
INSERT INTO `feature_role` VALUES (1, 2);
INSERT INTO `feature_role` VALUES (1, 3);
INSERT INTO `feature_role` VALUES (2, 1);
INSERT INTO `feature_role` VALUES (2, 2);
INSERT INTO `feature_role` VALUES (3, 1);
INSERT INTO `feature_role` VALUES (3, 2);
INSERT INTO `feature_role` VALUES (4, 1);
INSERT INTO `feature_role` VALUES (4, 2);
INSERT INTO `feature_role` VALUES (5, 1);
INSERT INTO `feature_role` VALUES (5, 3);
INSERT INTO `feature_role` VALUES (6, 1);
INSERT INTO `feature_role` VALUES (6, 3);
INSERT INTO `feature_role` VALUES (7, 1);
INSERT INTO `feature_role` VALUES (7, 3);
INSERT INTO `feature_role` VALUES (8, 1);
INSERT INTO `feature_role` VALUES (9, 1);
INSERT INTO `feature_role` VALUES (10, 1);
INSERT INTO `feature_role` VALUES (11, 1);
INSERT INTO `feature_role` VALUES (11, 2);
INSERT INTO `feature_role` VALUES (11, 3);
INSERT INTO `feature_role` VALUES (12, 1);
INSERT INTO `feature_role` VALUES (13, 1);
INSERT INTO `feature_role` VALUES (14, 1);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
