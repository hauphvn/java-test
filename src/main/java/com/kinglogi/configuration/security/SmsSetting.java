package com.kinglogi.configuration.security;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "sms")
@Data
public class SmsSetting {

    private String user;

    private String pass;

    private String brandName;

    private String urlApi;

    private boolean ownTemplate;

    private boolean enableSendSms;

}
