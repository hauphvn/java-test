package com.kinglogi.configuration.security;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.entity.Feature;
import com.kinglogi.entity.Role;
import com.kinglogi.repository.FeatureRepository;
import com.kinglogi.service.impl.UserDetailsServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        securedEnabled = true,
        jsr250Enabled = true,
        prePostEnabled = true
)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	private static final String[] AUTH_WHITELIST = {
			// -- swagger ui
			"/v3/api-docs/**",
			"/swagger-config**",
			"/swagger-resources",
			"/swagger-resources/**",
			"/configuration/ui",
			"/configuration/security",
			"/swagger-ui.html**",
			"/swagger-ui/**",
			"/webjars/**",
			"favicon.ico",
			// other public endpoints of your API may be appended to this array
			ApiVersion.API_V1 + "/auth/**",
			ApiVersion.API_V1 + "/auth/**/**",
			ApiVersion.API_V1 + "/publish/**",
			ApiVersion.API_V1 + "/payments/**",
			ApiVersion.API_V1 + "/masters/**",
			ApiVersion.API_V1 + "/visitor-orders/**",
			ApiVersion.API_V1 + "/supports/**",
			"/ping/**",
			ApiVersion.API_V2 + "/auth/**",
			ApiVersion.API_V2 + "/auth/**/**",
			ApiVersion.API_V2 + "/publish/**",
			ApiVersion.API_V2 + "/payments/**",
			ApiVersion.API_V2 + "/masters/**",
			ApiVersion.API_V2 + "/visitor-orders/**",
			ApiVersion.API_V2 + "/supports/**",
	};

	@Autowired
	private UserDetailsServiceImpl userDetailsService;

	@Autowired
	private JwtAuthenticationEntryPoint unauthorizedHandler;

	@Autowired
	private FeatureRepository featureRepository;

	@Bean
	public JwtAuthenticationFilter jwtAuthenticationFilter() {
		return new JwtAuthenticationFilter();
	}

	@Override
	public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
		authenticationManagerBuilder
				.userDetailsService(userDetailsService)
				.passwordEncoder(passwordEncoder());
	}

	@Bean(BeanIds.AUTHENTICATION_MANAGER)
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry registry = http
				// .anonymous().disable()
				.cors()
				.and()
				.csrf()
				.disable()
				.exceptionHandling()
				.authenticationEntryPoint(unauthorizedHandler)
				.and()
				.sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				.and()
				.authorizeRequests()
				// No need authentication.
				.antMatchers(AUTH_WHITELIST).permitAll();

		registry = registry.antMatchers(HttpMethod.POST,ApiVersion.API_V1 + "/partner-orders").permitAll();
		registry = registry.antMatchers(HttpMethod.GET, ApiVersion.API_V2 + "/complaints/{id}/files/{fileName}").permitAll();
		registry = registry.antMatchers(HttpMethod.POST, ApiVersion.API_V2 + "/complaints").permitAll();
		registry = registry.antMatchers(HttpMethod.GET, ApiVersion.API_V2 + "/complaints/{id}").permitAll();
		registry = registry.antMatchers(HttpMethod.POST, ApiVersion.API_V2 + "/complaints/{id}/files").permitAll();
		registry = registry.antMatchers(HttpMethod.GET, ApiVersion.API_V2 + "/car-groups/{id}/images/{fileName}").permitAll();
		registry = registry.antMatchers(HttpMethod.GET, ApiVersion.API_V2 + "/services/{id}/images/{fileName}").permitAll();
		registry = registry.antMatchers(HttpMethod.GET, ApiVersion.API_V2 + "/partner-links/{id}/images/{fileName}").permitAll();
		registry = registry.antMatchers(HttpMethod.GET, ApiVersion.API_V2 + "/cars/{imageId}/images/{fileName}").permitAll();
		registry = registry.antMatchers(HttpMethod.GET, ApiVersion.API_V2 + "/profile/avatars/{id}").permitAll();
		registry = registry.antMatchers(HttpMethod.GET, ApiVersion.API_V2 + "/inventories/validTrip").permitAll();
		registry = registry.antMatchers(HttpMethod.GET, ApiVersion.API_V2 + "/partner-links").permitAll();
		registry = registry.antMatchers(HttpMethod.GET, ApiVersion.API_V2 + "/partner-links/ads").permitAll();
		registry = registry.antMatchers(HttpMethod.GET, ApiVersion.API_V2 + "/car-groups").permitAll();
		registry = registry.antMatchers(HttpMethod.GET, ApiVersion.API_V2 + "/services").permitAll();
		registry = registry.antMatchers(HttpMethod.GET, ApiVersion.API_V2 + "/trip-catalogs").hasAnyRole(
				com.kinglogi.constant.Role.AGENT_ADMIN.getCode(),
				com.kinglogi.constant.Role.SUB_AGENT_ADMIN.getCode(),
				com.kinglogi.constant.Role.SUPER_ADMIN.getCode(),
				com.kinglogi.constant.Role.FIXED_TRIP_ADMIN.getCode(),
				com.kinglogi.constant.Role.FLEXIBLE_TRIP_ADMIN.getCode());
		registry = registry.antMatchers(HttpMethod.GET, ApiVersion.API_V2 + "/price-ranges/{priceRangeId}").permitAll();
		registry = registry.antMatchers(HttpMethod.GET, ApiVersion.API_V2 + "/price-ranges").permitAll();
		registry = registry.antMatchers(HttpMethod.GET, ApiVersion.API_V2 + "/trips/picking-up-locations").permitAll();
		registry = registry.antMatchers(HttpMethod.GET, ApiVersion.API_V2 + "/trips/dropping-off-locations").permitAll();
		registry = registry.antMatchers(HttpMethod.GET, ApiVersion.API_V2 + "/trips").permitAll();
		registry = registry.antMatchers(ApiVersion.API_V2 + "/report/**").hasAnyRole(com.kinglogi.constant.Role.SUPER_ADMIN.getCode());
		registry = registry.antMatchers(HttpMethod.GET, ApiVersion.API_V2 + "/devs/**").permitAll();
		registry = registry.antMatchers(ApiVersion.API_V2 + "/partner-orders/order").permitAll();
		registry = registry.antMatchers(ApiVersion.API_V2 + "/partner-orders/order/**").permitAll();
		registry = registry.antMatchers(HttpMethod.POST, ApiVersion.API_V2 + "/partner-orders").permitAll();

		// Need authentication.
		registry = registry.antMatchers(ApiVersion.API_V1 + "/transactions/**").hasAnyRole(
				com.kinglogi.constant.Role.SUPER_ADMIN.getCode(),
				com.kinglogi.constant.Role.FIXED_TRIP_ADMIN.getCode(),
				com.kinglogi.constant.Role.FLEXIBLE_TRIP_ADMIN.getCode());
		registry = buildDynamicMatchers(registry);
		registry.anyRequest().authenticated() // require authentication for any endpoint that's not whitelisted
		;

		// Add our custom JWT security filter
		http.addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
	}

	private ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry buildDynamicMatchers(
			ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry registry) {
		List<Feature> features = featureRepository.findAll();
		for (Feature feature : features) {
			List<String> roles = Optional.ofNullable(feature.getRoles())
					.orElseGet(HashSet::new)
					.stream()
					.map(Role::getCode)
					.collect(Collectors.toList());
			registry = registry.antMatchers(ApiVersion.API_V2 + feature.getApiPath() + "/**").hasAnyRole(roles.toArray(new String[0]));
		}
		return registry;
	}
}
