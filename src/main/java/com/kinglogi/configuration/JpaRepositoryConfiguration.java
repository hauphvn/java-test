package com.kinglogi.configuration;

import com.kinglogi.KinglogiApiApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackageClasses = KinglogiApiApplication.class, repositoryFactoryBeanClass = BaseRepositoryFactoryBean.class)
public class JpaRepositoryConfiguration {

}
