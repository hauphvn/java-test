package com.kinglogi.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = {"classpath:core.properties", "classpath:notification.properties", "classpath:jwt.properties"})
public class KlgResourceConfig {
}
