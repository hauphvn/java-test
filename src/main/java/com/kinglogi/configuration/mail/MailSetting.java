package com.kinglogi.configuration.mail;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Data
public class MailSetting {

    @Value("${spring.mail.additional.sendFrom}")
    private String sendFrom;

    @Value("${spring.mail.additional.replyTo}")
    private String replyTo;

}
