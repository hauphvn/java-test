package com.kinglogi.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Role {

	SUPER_ADMIN("SUPER_ADMIN"),
	FIXED_TRIP_ADMIN("FIXED_TRIP_ADMIN"),
	FLEXIBLE_TRIP_ADMIN("FLEXIBLE_TRIP_ADMIN"),
	MEMBER("MEMBER"),
	PARTNER("PARTNER"),
	COLLABORATOR("COLLABORATOR"),
	AGENT_ADMIN("AGENT_ADMIN"),
	SUB_AGENT_ADMIN("SUB_AGENT_ADMIN");

	@Getter
	private String code;
}
