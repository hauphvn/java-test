package com.kinglogi.constant;

public enum EmailTemplateType {
    FORGOT_PASSWORD,
    CONTACT,
    WELCOME,
    PAID,
    UNPAID,
    START_AUCTION,
    FINISH_AUCTION,

    INVOICE,
    NEW_ORDER,
    COLLABORATOR_CANCEL_ORDER,
    ORDER_CANCELED_BY_COLLABORATOR,
}
