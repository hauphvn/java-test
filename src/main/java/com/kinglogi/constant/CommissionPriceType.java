package com.kinglogi.constant;

public enum CommissionPriceType {
    AMOUNT, PERCENT
}
