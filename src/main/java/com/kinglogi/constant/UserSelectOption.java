package com.kinglogi.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum UserSelectOption {

    OPTIONAL("OPTIONAL"),
    ALL("ALL"),
    MEMBER("MEMBER"),
    COLLABORATOR("COLLABORATOR"),
    PARTNER("PARTNER");

    @Getter
    private String code;

}
