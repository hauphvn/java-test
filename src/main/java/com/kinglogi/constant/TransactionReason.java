package com.kinglogi.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum TransactionReason {
    PAY_IN(0),
    PAYMENT_ORDER(1);

    @Getter
    private Integer type;
}
