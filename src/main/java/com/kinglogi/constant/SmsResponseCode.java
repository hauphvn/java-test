package com.kinglogi.constant;


import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@Getter
public enum SmsResponseCode {

    SUCCESS(1, "Thành Công"),

    ERROR_01(-1, "Lỗi nội bộ"),
    ERROR_00(0, "Gửi tin lỗi"),
    ERROR_02(2, "IP không được phép gửi tin"),
    ERROR_03(3, "Lỗi dịch vụ"),
    ERROR_04(4, "Lỗi dịch vụ"),
    ERROR_05(5, "Số điện thoại gửi không đúng"),
    ERROR_06(6, "Thông tin user/password sai"),

    ERROR_07(7, "Brandname chưa kích hoạt"),
    ERROR_08(8, "Nhà mạng gửi tin không được cấp"),
    ERROR_10(10, "Độ dài tin nhắn không hợp lệ"),
    ERROR_11(11, "Tin nhắn chứa ký tự Unicode"),
    ERROR_12(12, "Tin nhắn không có nội dung"),
    ERROR_13(13, "Tài khoản bị khóa"),
    ERROR_15(15, "Cùng nội dung gửi đến 1 số ĐT trong thời gian ngắn"),
    ERROR_16(16, "Trùng Trand_id trong 5 phút"),
    ERROR_17(17, "Cấu trúc dữ liệu XML không đúng"),
    ERROR_18(18, "Cấu trúc dữ liệu Json không đúng"),
    ERROR_19(19, "Quá số lượng tin nhắn gửi trong 1 giây (Default 50sms/s)"),
    ERROR_20(20, "Nội dung chưa từ khóa bị cấm"),
    ERROR_21(21, "Thời gian cho Quảng cáo chưa set"),
    ERROR_22(22, "Thời gian QC trước ngày hiện tại"),
    ERROR_23(23, "Thời gian quảng cáo ngoài khung giờ 8:00 – 20:00"),
    ERROR_24(24, "Cần set thời gian QC sau thời điểm hiện tại 3h"),
    ERROR_25(25, "Chiến dịch chưa được duyệt"),
    ERROR_26(26, "Chiến dịch không tìm thấy bởi Campaign ID"),
    ERROR_29(29, "Người dùng xoá chiến dịch"),
    ERROR_30(30, "Quán hạn mức"),
    ERROR_31(31, "Số điện thoại trong Blacklist"),
    ERROR_32(32, "Số điện thoại đã chuyển mạng giữ số"),
    ERROR_33(33, "Nhà mạng không đúng"),
    ERROR_35(35, "Tài khoản hết tiền hoặc không cho phép"),
    ERROR_36(36, "Nội dung sai temp"),
    ERROR_37(37, "Không tìm thấy hướng gửi"),
    ERROR_38(38, "Brandname hết hạn"),
    ERROR_39(39, "Brandname bị khoá bởi nhà mạng"),
    ERROR_40(40, "Chưa set giá cho tài khoản"),
    ERROR_41(41, "Lỗi hệ thống tính cước"),
    ERROR_98(98, "Bảo Trì hệ Thống"),
    ERROR_99(99, "Đã nhận tin")

    ;

    private static final Map<Integer, String> BY_CODE = new HashMap<>();

    static {
        for (SmsResponseCode e : values()) {
            BY_CODE.put(e.code, e.message);
        }
    }

    private int code;

    private String message;

    public static String getMessageByCode(int code){
        return BY_CODE.get(code);
    }

}
