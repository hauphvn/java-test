package com.kinglogi.constant;

public final class Auth {

    public static final String JSON_STRING = "application/json";
    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String BEARER_PREFIX = "Bearer";
    public static final String GOOGLE_PASSWORD_TOKEN = "random-for-oauth2";

    private Auth() {

    }
}