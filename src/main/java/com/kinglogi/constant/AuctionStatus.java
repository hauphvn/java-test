package com.kinglogi.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum AuctionStatus {

	INIT(1),
	START(10),
	STOP(90),
	FINISH(100);

	@Getter
	private Integer status;
}
