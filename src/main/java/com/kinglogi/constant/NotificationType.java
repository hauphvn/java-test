package com.kinglogi.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Optional;
import java.util.stream.Stream;

@AllArgsConstructor
public enum NotificationType {

    // Notify to web user (admin) using firebase
    NEW_ORDER(10, NotificationProvider.FIREBASE),
    CANCEL_ORDER(11, NotificationProvider.FIREBASE),
    CTV_CANCEL_ORDER(12, NotificationProvider.FIREBASE),
    JOIN_A_BID(30,  NotificationProvider.FIREBASE),
    WINNING_A_BID(32,  NotificationProvider.FIREBASE),

    LOSING_A_BID(33,  NotificationProvider.FIREBASE),

    // Notify to mobile user (collaborator) using expo push
    CONFIRM_A_BID(31, NotificationProvider.FIREBASE),
    AUCTION_STARTED(20, NotificationProvider.FIREBASE),

    AUCTION_END(24, NotificationProvider.FIREBASE),

    AUCTION_CANCEL(25, NotificationProvider.FIREBASE),
    PROMOTION_STARTED(21, NotificationProvider.FIREBASE),
    NEW_TRANSACTION(22, NotificationProvider.FIREBASE),
    LOYALTY_POINT(25, NotificationProvider.FIREBASE);

    @Getter
    private Integer type;

    @Getter
    private NotificationProvider provider;

    public static Optional<NotificationType> getByType(Integer type) {
        return Stream.of(NotificationType.values()).filter(notificationType -> notificationType.getType().equals(type)).findAny();
    }
}
