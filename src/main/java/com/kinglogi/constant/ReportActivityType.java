package com.kinglogi.constant;

public enum ReportActivityType {

    TOPUP, CREATE, DEDUCT, UPDATE, BID, WINNER, START, FINISH;

}
