package com.kinglogi.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum PromotionStatus {

    ACTIVE("ACTIVE"),
    EXPIRE("EXPIRE");

    @Getter
    private String status;

}
