package com.kinglogi.constant;

public enum AuthRole {
    SUPER_ADMIN, FIXED_TRIP_ADMIN, FLEXIBLE_TRIP_ADMIN, BUSINESS_PARTNER, PARTNER, MEMBER, VISITOR, AGENT_ADMIN, SUB_AGENT_ADMIN, COLLABORATOR;

    public String authority() {
        return "ROLE_" + this.name();
    }
}
