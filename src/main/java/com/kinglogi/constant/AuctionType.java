package com.kinglogi.constant;

public enum AuctionType {
    BUY, RESERVE
}
