package com.kinglogi.constant;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum ComplaintConstant {
    NEW, REPLIED, SPAM
}
