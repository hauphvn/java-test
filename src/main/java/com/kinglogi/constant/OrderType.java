package com.kinglogi.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

@AllArgsConstructor
public enum OrderType {

    NONE(0),
    FIXED_TRIP(1),
    FLEXIBLE_TRIP(2),
    ALL(3);

    @Getter
    private Integer type;

    public static OrderType getOrderType(Integer orderType) {
        return Stream.of(OrderType.values()).filter(type -> type.getType().equals(orderType)).findFirst().orElse(OrderType.NONE);
    }
}
