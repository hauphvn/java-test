package com.kinglogi.constant;

public final class AppConstant {

    public static final String BASE_PACKAGE = "com.kinglogi";

    public static final String TRANSACTION_ORDER_INFO = "Dat xe King Logi [{orderCode}], {transactionAt}";

    private AppConstant() {
        super();
    }
}
