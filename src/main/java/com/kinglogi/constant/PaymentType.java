package com.kinglogi.constant;

import java.util.Optional;
import java.util.stream.Stream;

public enum PaymentType {
    VNPAY,
    BANK_ACCOUNT,
    BALANCE,
    DIRECT,
    APPOTAPAY,
    MBBANK_MINI_APP,
    MANUAL,
    TRANSFER,
    PAYNET_ONE;

    public static Optional<PaymentType> getPaymentType(String type) {
        return Stream.of(PaymentType.values()).filter(paymentType -> paymentType.name().equals(type)).findAny();
    }
}
