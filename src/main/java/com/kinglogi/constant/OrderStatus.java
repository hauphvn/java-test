package com.kinglogi.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Optional;
import java.util.stream.Stream;

@AllArgsConstructor
public enum OrderStatus {

    WAITING_FOR_PAY_IN(10),
    PAID(20),
    READY_FOR_MOVING(21),
    ON_GOING(22),
    REJECTED(40),
    CANCELLED(50),
    FINISH(60);

    @Getter
    private Integer status;

    public static Optional<OrderStatus> getOrderStatus(int status) {
        return Stream.of(OrderStatus.values()).filter(orderStatus -> orderStatus.status == status).findAny();
    }
}
