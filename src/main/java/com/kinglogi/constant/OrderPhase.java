package com.kinglogi.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum OrderPhase {

    PAY_IN("pay_in"),
    REDIRECT_TO_PAYMENT("redirect_payment"),
    FINISH("finish")
    ;

    @Getter
    private String phase;
}
