package com.kinglogi.constant;

public enum RankPoint {
    STANDARD, SILVER, GOLD
}
