package com.kinglogi.token.factory;

import com.kinglogi.configuration.security.JwtSetting;
import com.kinglogi.constant.Scope;
import com.kinglogi.converter.UserCredentialConverter;
import com.kinglogi.dto.AuthUserDetails;
import com.kinglogi.dto.response.UserCredential;
import com.kinglogi.token.AccessToken;
import com.kinglogi.token.RefreshToken;
import com.kinglogi.utils.JsonUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Factory class that should be always used to create {@link com.kinglogi.token.JwtToken}.
 */
@Component
public class JwtTokenFactory {

    public static final String TOKEN_SCOPES_KEY = "scopes";

    private final JwtSetting setting;

    @Autowired
    public JwtTokenFactory(JwtSetting setting) {
        this.setting = setting;
    }

    /**
     * Factory method for issuing new access JWT Tokens.
     */
    public AccessToken createAccessToken(AuthUserDetails userDetail) {
        if (userDetail == null) {
            throw new IllegalArgumentException("Cannot create JWT Token without auth user details");
        }

        UserCredential subject = new UserCredentialConverter().convert(userDetail);
        UserCredential userCredential = new UserCredential();
        userCredential.setRoles(subject.getRoles());
        userCredential.setId(subject.getId());
        List<String> scopes = userDetail.getAuthorities().stream().map(Object::toString).collect(Collectors.toList());

        Claims claims = Jwts.claims().setSubject(JsonUtils.toJson(userCredential));
        claims.put(TOKEN_SCOPES_KEY, scopes);

        String token = createToken(claims, setting.getAccessTokenExpirationInMs());
        return new AccessToken(token, subject);
    }

    /**
     * Factory method for issuing new refresh JWT Tokens.
     */
    public RefreshToken createRefreshToken(AuthUserDetails userDetail) {
        if (userDetail == null) {
            throw new IllegalArgumentException("Cannot create JWT Token without auth user details");
        }

        UserCredential subject = UserCredential.builder().id(userDetail.getId()).build();

        List<String> scopes = Collections.singletonList(Scope.REFRESH_TOKEN.authority());

        Claims claims = Jwts.claims().setSubject(JsonUtils.toJson(subject));
        claims.put(TOKEN_SCOPES_KEY, scopes);

        String token = createToken(claims, setting.getRefreshTokenExpirationInMs());
        return new RefreshToken(token);
    }

    private String createToken(Claims claims, int expirationInMs) {
        LocalDateTime currentTime = LocalDateTime.now();
        Date issueAt = Date.from(currentTime.atZone(ZoneId.systemDefault()).toInstant());
        Date expirationAt = Date.from(currentTime
                .plusMinutes(expirationInMs)
                .atZone(ZoneId.systemDefault()).toInstant());

        return Jwts.builder()
                .setId(UUID.randomUUID().toString())
                .setClaims(claims)
                .setIssuer(setting.getTokenIssuer())
                .setIssuedAt(issueAt)
                .setExpiration(expirationAt)
                .signWith(SignatureAlgorithm.HS512, setting.getTokenSigningKey())
                .compact();
    }
}
