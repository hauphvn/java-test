package com.kinglogi.token;

/**
 * The jwt token
 */
public interface JwtToken {

    String getToken();
}
