package com.kinglogi.token.verifier;

import com.kinglogi.configuration.security.JwtSetting;
import com.kinglogi.token.parser.TokenParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ExpirationTokenVerifier implements TokenVerifier {

    @Autowired
    private JwtSetting setting;

    @Autowired
    private TokenParser tokenParser;

    @Override
    public boolean verify(String token) {
        return tokenParser.parseClaims(token, setting.getTokenSigningKey()) != null;
    }
}
