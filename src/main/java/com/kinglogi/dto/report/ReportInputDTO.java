package com.kinglogi.dto.report;

import com.kinglogi.entity.Account;
import lombok.Data;

import java.time.Instant;

@Data
public class ReportInputDTO {

    private String date;

    private String user;

    private Long agent;

    private String activity;

    private String code;

    private String pickUp;

    private String dropOff;

    private Long price;

    private String car;

    private String currency;

    private Long tax;

    private Long totalCharged;

    private Long klgRevenue;

    private Long driverRevenue;

    private Account customer;

    private String driverPhone;

    private Long loyalty;

    private Long amount;

}
