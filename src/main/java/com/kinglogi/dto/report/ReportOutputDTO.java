package com.kinglogi.dto.report;

import com.kinglogi.constant.ReportActivityType;
import com.kinglogi.entity.Report;
import lombok.Data;

import java.time.Instant;

@Data
public class ReportOutputDTO {

    private Instant date;

    private String account;

    private Long agent;

    private String code;

    private ReportActivityType activity;

    private String pickUp;

    private String dropOff;

    private Long price;

    private String car;

    private String currency;

    private Long totalCharged;

    private Long klgRevenue;

    private Long driverRevenue;

    private String customer;

    private String customerPhone;

    private String driverPhone;

    private Long loyaltyNumber;

    public ReportOutputDTO(Report report) {
        this.date = report.getDate();
        if (report.getAccount() != null){
            this.account = report.getAccount().getFullName();
            this.agent = report.getAccount().getAgentId();
        }
        this.activity = report.getActivity();
        this.code = report.getCode();
        this.pickUp = report.getPickUp();
        this.dropOff = report.getDropOff();
        this.price = report.getPrice();
        this.car = report.getCar();
        this.currency = report.getCurrency();
        this.totalCharged = report.getTotalCharged();
        this.klgRevenue = report.getKlgRevenue();
        this.driverRevenue = report.getDriverRevenue();
        this.customer = report.getCustomer();
        this.customerPhone = report.getCustomerPhone();
        this.driverPhone = report.getDriverPhone();
        this.loyaltyNumber = report.getLoyaltyNumber();
    }
}
