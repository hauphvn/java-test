package com.kinglogi.dto.filter;

import com.kinglogi.dto.filter.base.BaseFilter;

import lombok.Data;

@Data
public class TripFilter extends BaseFilter {

    public TripFilter() {
        setSorts(new String[]{ "pickingUpLocation" });
    }

	private String language;
	
    @Override
    public String[] getSearchableFields() {
        return new String[] {
                "pickingUpLocation",
                "droppingOffLocation",
        };
    }

    @Override
    public String[] getSortableFields() {
        return new String[] {
                "pickingUpLocation",
                "droppingOffLocation",
                "distanceInKm",
                "totalPrice",
                "brand",
                "numberOfSeat",
                "createdAt",
        };
    }
}