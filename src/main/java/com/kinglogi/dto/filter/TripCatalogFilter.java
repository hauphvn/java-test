package com.kinglogi.dto.filter;

import com.kinglogi.dto.filter.base.BaseFilter;

public class TripCatalogFilter extends BaseFilter {

    @Override
    public String[] getSearchableFields() {
        return new String[] {
                "pickingUpLocation",
                "droppingOffLocation",
        };
    }

    @Override
    public String[] getSortableFields() {
        return new String[] {
                "pickingUpLocation",
                "droppingOffLocation",
                "distanceInKm",
                "pricePerKm",
                "createdAt",
        };
    }
}