package com.kinglogi.dto.filter;

import com.kinglogi.dto.filter.base.BaseFilter;
import lombok.*;

@Data
@EqualsAndHashCode(callSuper = true)
public class ComplaintFilter extends BaseFilter {
    String type;

    Long userId;

    @Override
    public String[] getSearchableFields() {
        return new String[] {
                "title",
                "description",
                "email",
        };
    }

    @Override
    public String[] getSortableFields() {
        return new String[] {
                "title",
                "description",
                "email",
                "phoneNumber",
                "createdAt"
        };
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
