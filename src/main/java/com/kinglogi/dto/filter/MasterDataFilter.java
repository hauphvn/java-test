package com.kinglogi.dto.filter;

import com.kinglogi.dto.filter.base.BaseFilter;

import lombok.Data;

@Data
public class MasterDataFilter extends BaseFilter {

	private String language;

	@Override
	public String[] getSearchableFields() {
		return null;
	}

	@Override
	public String[] getSortableFields() {
		return null;
	}

}
