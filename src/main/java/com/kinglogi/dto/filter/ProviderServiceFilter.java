package com.kinglogi.dto.filter;

import com.kinglogi.dto.filter.base.BaseFilter;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ProviderServiceFilter extends BaseFilter {

    @Override
    public String[] getSearchableFields() {
        return new String[] {
                "title",
                "description",
        };
    }

    @Override
    public String[] getSortableFields() {
        return new String[] {
                "title",
                "description",
        };
    }
}
