package com.kinglogi.dto.filter;

import com.kinglogi.dto.filter.base.BaseFilter;
import lombok.Data;

@Data
public class PartnerFilter extends BaseFilter {
    
    @Override
    public String[] getSearchableFields() {
        return new String[] {
                "name",
                "description"
        };
    }

    @Override
    public String[] getSortableFields() {
        return new String[] {
                "name",
                "description"
        };
    }
}