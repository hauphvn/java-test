package com.kinglogi.dto.filter;

import com.kinglogi.dto.filter.base.BaseFilter;

public class CommissionFilter extends BaseFilter {
    @Override
    public String[] getSearchableFields() {
        return new String[] {
            "title", "description"
        };
    }

    @Override
    public String[] getSortableFields() {
        return new String[] {
            "title", "description", "priceType", "priceValue"
        };
    }
}
