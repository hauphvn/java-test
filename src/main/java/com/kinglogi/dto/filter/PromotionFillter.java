package com.kinglogi.dto.filter;

import com.kinglogi.dto.filter.base.BaseFilter;

public class PromotionFillter extends BaseFilter {



    @Override
    public String[] getSearchableFields() {
        return new String[] {
                "name",
                "code",
                "description"
        };
    }

    @Override
    public String[] getSortableFields() {
        return new String[] {
                "name",
                "code",
                "description",
                "expireTime",
                "numberOfUsage",
                "createdAt"
        };
    }
}
