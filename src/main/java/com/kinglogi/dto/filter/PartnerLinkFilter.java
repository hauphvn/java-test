package com.kinglogi.dto.filter;

import com.kinglogi.dto.filter.base.BaseFilter;

public class PartnerLinkFilter extends BaseFilter {
    @Override
    public String[] getSearchableFields() {
        return new String[] {
            "name", "url"
        };
    }

    @Override
    public String[] getSortableFields() {
        return new String[] {
            "name", "position", "url"
        };
    }
}
