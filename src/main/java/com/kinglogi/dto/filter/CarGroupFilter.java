package com.kinglogi.dto.filter;

import com.kinglogi.dto.filter.base.BaseFilter;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class CarGroupFilter extends BaseFilter {

    int numberOfSeat = 0;

    @Override
    public String[] getSearchableFields() {
        return new String[] {
                "title",
        };
    }

    @Override
    public String[] getSortableFields() {
        return new String[] {
                "title",
                "price",
                "numberOfSeat",
        };
    }
}