package com.kinglogi.dto.filter;

import com.kinglogi.constant.OrderStatus;
import com.kinglogi.dto.filter.base.BaseFilter;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderFilter extends BaseFilter {

    private Integer orderType;
    private Long createdBy;
    private Long createdUser;
    private Integer orderStatus;
    private List<Long> collaboratorIds = new ArrayList<>();
    private List<Integer> orderStatuses = new ArrayList<>();
    private boolean ofAuction = true;
    private Long agentId;

    @Override
    public String[] getSearchableFields() {
        return new String[] {
            "orderCode",
            "fullName",
            "phoneNumber",
            "pickingUpLocation",
            "droppingOffLocation",
        };
    }

    @Override
    public String[] getSortableFields() {
        return new String[] {
            "orderCode",
            "fullName",
            "phoneNumber",
            "pickingUpDatetime",
            "pickingUpLocation",
            "droppingOffLocation",
            "distanceInKm",
            "totalPrice",
            "brand",
            "numberOfSeat",
            "createdAt",
            "status"
        };
    }
}
