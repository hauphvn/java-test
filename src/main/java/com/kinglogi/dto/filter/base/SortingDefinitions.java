package com.kinglogi.dto.filter.base;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Define fields is allowed sorting
 */
public interface SortingDefinitions {

    @JsonIgnore
    String[] getSortableFields();
}
