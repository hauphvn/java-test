package com.kinglogi.dto.filter.base;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Define fields is allowed searching
 */
public interface SearchingDefinitions {

    @JsonIgnore
    String[] getSearchableFields();
}
