package com.kinglogi.dto.filter.base;

import lombok.Data;

/**
 * The base class for search with keyword, paging and sorting
 */
@Data
public abstract class BaseFilter extends PagingAndSortingParam implements SearchingDefinitions {

    private String keyword;

}
