package com.kinglogi.dto.filter;

import com.kinglogi.dto.filter.base.BaseFilter;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class AuctionFilter extends BaseFilter {

	private Integer status;

	private Boolean isInBidding;

	@Override
	public String[] getSearchableFields() {
		return new String[] {
                "name"
        };
	}

	@Override
	public String[] getSortableFields() {
		return new String[] {
				"name",
				"startAt",
				"endAt",
				"totalPrice",
				"status",
				"createdAt"
		};
	}

}
