package com.kinglogi.dto.filter;

import com.kinglogi.dto.filter.base.BaseFilter;

public class NotificationFilter extends BaseFilter {

    @Override
    public String[] getSearchableFields() {
        return new String[] {
                "title",
                "content",
        };
    }

    @Override
    public String[] getSortableFields() {
        return new String[] {
                "title",
                "content",
                "createdAt",
        };
    }
}