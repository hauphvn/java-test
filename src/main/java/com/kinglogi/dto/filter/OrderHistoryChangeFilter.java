package com.kinglogi.dto.filter;

import com.kinglogi.dto.filter.base.BaseFilter;

public class OrderHistoryChangeFilter extends BaseFilter {
    @Override
    public String[] getSearchableFields() {
        return new String[] {
                "orderCode"
        };
    }

    @Override
    public String[] getSortableFields() {
        return new String[] {
                "orderCode",
                "createdBy",
                "createdAt",
        };
    }
}
