package com.kinglogi.dto.filter;

import com.kinglogi.dto.filter.base.BaseFilter;

public class AccountLoginFilter extends BaseFilter {
    @Override
    public String[] getSearchableFields() {
        return new String[]{
                "username"
        };
    }

    @Override
    public String[] getSortableFields() {
        return new String[]{
                "username", "lastLogin"
        };
    }
}
