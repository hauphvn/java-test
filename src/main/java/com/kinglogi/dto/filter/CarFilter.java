package com.kinglogi.dto.filter;

import com.kinglogi.dto.filter.base.BaseFilter;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class CarFilter extends BaseFilter {

    private Long accountId;

    @Override
    public String[] getSearchableFields() {
        return new String[] {
                "driverName",
                "brand",
                "driverPhoneNumber",
        };
    }

    @Override
    public String[] getSortableFields() {
        return new String[] {
                "driverName",
                "brand",
                "driverPhoneNumber",
                "numberOfSeat",
                "description",
                "updatedAt",
        };
    }
}