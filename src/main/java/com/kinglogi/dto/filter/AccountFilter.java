package com.kinglogi.dto.filter;

import java.util.ArrayList;
import java.util.List;

import com.kinglogi.dto.filter.base.BaseFilter;

import lombok.Data;

@Data
public class AccountFilter extends BaseFilter {

	List<String> roleCodes = new ArrayList<>();
	String email;
    String username;
    boolean inGroup = false;

    @Override
    public String[] getSearchableFields() {
        return new String[] {
                "username",
                "fullName",
                "phoneNumber",
                "email"
        };
    }

    @Override
    public String[] getSortableFields() {
        return new String[] {
                "username",
                "fullName",
                "gender",
                "phoneNumber",
                "email",
                "active",
                "loyaltyPoint"
        };
    }


}
