package com.kinglogi.dto.filter;

import com.kinglogi.dto.filter.base.BaseFilter;

public class PriceRangeFilter extends BaseFilter {
    @Override
    public String[] getSearchableFields() {
        return new String[] {
            "goArea", "destinationArea"
        };
    }

    @Override
    public String[] getSortableFields() {
        return new String[] {
                "goArea", "destinationArea"
        };
    }
}
