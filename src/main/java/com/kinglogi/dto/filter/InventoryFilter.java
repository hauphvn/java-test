package com.kinglogi.dto.filter;

import com.kinglogi.dto.filter.base.BaseFilter;

public class InventoryFilter extends BaseFilter {
    @Override
    public String[] getSearchableFields() {
        return new String[] {
            "name"
        };
    }

    @Override
    public String[] getSortableFields() {
        return new String[] {
            "name", "numberOfSeat", "numberOfTrip", "price", "createdAt", "expireTime"
        };
    }
}
