package com.kinglogi.dto.filter;

import com.kinglogi.dto.filter.base.BaseFilter;

public class BankAccountFilter extends BaseFilter {

    @Override
    public String[] getSearchableFields() {
        return new String[] {
                "accountName",
                "address",
                "bankName",
                "bankAddress",
                "accountNumber",
                "accountCardNumber",
        };
    }

    @Override
    public String[] getSortableFields() {
        return new String[] {
                "accountName",
                "address",
                "bankName",
                "bankAddress",
                "accountNumber",
                "accountCardNumber"
        };
    }
}