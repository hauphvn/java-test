package com.kinglogi.dto.filter;

import com.kinglogi.constant.BidStatus;
import com.kinglogi.dto.filter.base.BaseFilter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AuctionBidFilter extends BaseFilter {

    private Long userId;

    private Long auctionId;

    private Integer status;

    @Override
    public String[] getSortableFields() {
        return new String[] {};
    }

    @Override
    public String[] getSearchableFields() {
        return new String[0];
    }
}
