package com.kinglogi.dto.filter;

import com.kinglogi.dto.filter.base.BaseFilter;

public class AccountHistoryFilter extends BaseFilter {
    @Override
    public String[] getSearchableFields() {
        return new String[] {
        };
    }

    @Override
    public String[] getSortableFields() {
        return new String[] {
            "updatedBy", "createdAt", "updatedAt", "createdBy"
        };
    }
}
