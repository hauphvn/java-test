package com.kinglogi.dto.loyalty_point_history;

import com.kinglogi.constant.LoyaltyActionType;
import com.kinglogi.entity.Account;

import java.time.LocalDateTime;

public class LoyaltyPointHistoryInputDTO {
    private LoyaltyActionType actionType;
    private Integer amount;

    private Account account;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public LoyaltyActionType getActionType() {
        return actionType;
    }

    public void setActionType(LoyaltyActionType actionType) {
        this.actionType = actionType;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
