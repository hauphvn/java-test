package com.kinglogi.dto.loyalty_point_history;

import com.kinglogi.constant.LoyaltyActionType;
import com.kinglogi.entity.LoyaltyPointHistory;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import java.time.LocalDateTime;

public class LoyaltyPointHistoryOutputDTO {
    private Long id;
    private LoyaltyActionType actionType;
    private Integer amount;
    private Long updatedBy;
    private LocalDateTime updatedAt;
    private Long createdBy;
    private LocalDateTime createdAt;

    public LoyaltyPointHistoryOutputDTO(LoyaltyPointHistory loyaltyPointHistory) {
        this.id = loyaltyPointHistory.getId();
        this.actionType = loyaltyPointHistory.getActionType();
        this.amount = loyaltyPointHistory.getAmount();
        this.updatedBy = loyaltyPointHistory.getUpdatedBy();
        this.updatedAt = loyaltyPointHistory.getUpdatedAt();
        this.createdBy = loyaltyPointHistory.getCreatedBy();
        this.createdAt = loyaltyPointHistory.getCreatedAt();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LoyaltyActionType getActionType() {
        return actionType;
    }

    public void setActionType(LoyaltyActionType actionType) {
        this.actionType = actionType;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
