package com.kinglogi.dto.response;

import lombok.Data;

@Data
public class OrderStopPointResponse {

    private String stopPoint;

    private Long price;

}
