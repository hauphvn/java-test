package com.kinglogi.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PartnerPaymentResponse {

    private String status;

    private String message;

    private String orderCode;

    private Long amount;

    private String orderInfo;

    private String bankCode;

    private String bankTranNo;

    private String cardType;

    private Date payDate;

    private String transactionNo;

}
