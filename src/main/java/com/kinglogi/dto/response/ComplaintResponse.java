package com.kinglogi.dto.response;

import com.kinglogi.constant.ComplaintConstant;
import com.kinglogi.entity.Complaint;
import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.Lob;
import javax.validation.constraints.Email;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class ComplaintResponse {
    private Long id;

    private String title;

    @Lob
    private String description;

    private String email;

    private String phoneNumber;

    private String type;

    private Long plusPoint;

    private ComplaintConstant status;


    private List<FileAttachmentResponse> fileAttachment;

    private Long updatedBy;

    private LocalDateTime updatedAt;

    private Long createdBy;

    private LocalDateTime createdAt;

    private String adminResponse;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getPlusPoint() {
        return plusPoint;
    }

    public void setPlusPoint(Long plusPoint) {
        this.plusPoint = plusPoint;
    }

    public ComplaintConstant getStatus() {
        return status;
    }

    public void setStatus(ComplaintConstant status) {
        this.status = status;
    }

    public List<FileAttachmentResponse> getFileAttachment() {
        return fileAttachment;
    }

    public void setFileAttachment(List<FileAttachmentResponse> fileAttachment) {
        this.fileAttachment = fileAttachment;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ComplaintResponse(Complaint complaint) {
        this.id = complaint.getId();
        this.title = complaint.getTitle();
        this.description = complaint.getDescription();
        this.email = complaint.getEmail();
        this.type = complaint.getType();
        this.phoneNumber = complaint.getPhoneNumber();
        this.plusPoint = complaint.getPlusPoint();
        this.status = complaint.getStatus();
        this.createdAt = complaint.getCreatedAt();
        this.createdBy = complaint.getCreatedBy();
        this.updatedAt = complaint.getUpdatedAt();
        this.updatedBy = complaint.getUpdatedBy();
        this.adminResponse = complaint.getAdminResponse();
    }
}
