package com.kinglogi.dto.response;

import com.kinglogi.entity.FileAttachment;

public class FileAttachmentResponse {
    private Long id;

    private String name;

    private String url;


    private Long complaintId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getComplaintId() {
        return complaintId;
    }

    public void setComplaintId(Long complaintId) {
        this.complaintId = complaintId;
    }

    public FileAttachmentResponse(FileAttachment fileAttachment) {
        this.id = fileAttachment.getId();
        this.name = fileAttachment.getFileName();
        this.complaintId = fileAttachment.getComplaint().getId();
        this.url = "/complaints/" + complaintId + "/files/" + name;
    }
}
