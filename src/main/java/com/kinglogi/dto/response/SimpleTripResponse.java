package com.kinglogi.dto.response;


import lombok.Data;

@Data
public class SimpleTripResponse {

    private Long id;

    private String pickingUpLocation;

    private String droppingOffLocation;

    private Float distanceInKm;

    private Long totalPrice;

    private Integer numberOfSeat;

    private String description;

}
