package com.kinglogi.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PartnerOrderResponse extends OrderWithPaymentResponse {
}
