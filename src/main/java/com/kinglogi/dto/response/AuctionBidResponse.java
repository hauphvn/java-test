package com.kinglogi.dto.response;

import com.kinglogi.constant.AuctionType;
import com.kinglogi.constant.BidStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
public class AuctionBidResponse {

	private Long id;

	private String name;

	private Date startAt;

	private Date endAt;

	private Long totalPrice;

	private String description;

	private Integer status;

	private LocalDateTime updatedAt;

	private Boolean acceptance;

	private LocalDateTime bidAt;

    private Long bidPrice;

	private AuctionType type;

	public AuctionBidResponse(Long id, String name, Date startAt, Date endAt, Long totalPrice, String description,
							  Integer status, LocalDateTime updatedAt, BidStatus bidStatus, LocalDateTime bidAt, Long bidPrice, AuctionType type) {
		this.id = id;
		this.name = name;
		this.startAt = startAt;
		this.endAt = endAt;
		this.totalPrice = totalPrice;
		this.description = description;
		this.status = status;
		this.updatedAt = updatedAt;
		if (bidStatus.equals(BidStatus.SUCCESS)){
			this.acceptance = true;
		}else if (bidStatus.equals(BidStatus.FAIL)){
			this.acceptance = false;
		}
		this.bidAt = bidAt;
        this.bidPrice = bidPrice;
		this.type = type;
	}

	private List<SimpleOrderResponse> orders;
}
