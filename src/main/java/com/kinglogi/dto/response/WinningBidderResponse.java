package com.kinglogi.dto.response;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class WinningBidderResponse {

    private Long id;

    private String username;

    private String fullName;

    private String phoneNumber;

    private Character gender;

    private String email;

    private LocalDateTime bidAt;

    private String avatar;

    private String licensePlates;
}
