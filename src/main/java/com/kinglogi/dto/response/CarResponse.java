package com.kinglogi.dto.response;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class CarResponse {

	private Long id;

	private String driverName;

	private String brand;

	private String driverPhoneNumber;

	private Integer numberOfSeat;

	private String description;

	private LocalDateTime updatedAt;

	private String licensePlates;

}
