package com.kinglogi.dto.response;

import com.kinglogi.dto.response.file_response.FileProviderServiceResponse;
import com.kinglogi.entity.ProviderService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProviderServiceResponse {
    private Long id;

    private String title;

    private String description;


    private List<FileProviderServiceResponse> files;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<FileProviderServiceResponse> getFiles() {
        return files;
    }

    public void setFiles(List<FileProviderServiceResponse> files) {
        this.files = files;
    }


    public ProviderServiceResponse(ProviderService providerService) {
        this.id = providerService.getId();
        this.title = providerService.getTitle();
        this.description = providerService.getDescription();
        this.files = providerService.getFiles() == null ? new ArrayList<>() : providerService.getFiles().stream().map(file -> new FileProviderServiceResponse(file, id)).collect(Collectors.toList());
    }
}
