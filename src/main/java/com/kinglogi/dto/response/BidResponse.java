package com.kinglogi.dto.response;

import com.kinglogi.constant.BidStatus;
import lombok.Data;

@Data
public class BidResponse {

    private Long id;
    private BidStatus status;
}
