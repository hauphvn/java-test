package com.kinglogi.dto.response;

import lombok.Data;

import java.util.Date;

@Data
public class PaymentResponse {

    private String tmnCode;

    private String txnRef;

    private Long amount;

    private String orderInfo;

    private String responseCode;

    private String message;

    private String bankCode;

    private String bankTranNo;

    private String cardType;

    private Date payDate;

    private String transactionNo;

    private String transactionType;

    private String transactionStatus;

    private String secureHash;

    private String secureHashType;

}
