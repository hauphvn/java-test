package com.kinglogi.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.minidev.json.annotate.JsonIgnore;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserCredential {

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Long id;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String fullName;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String username;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String phoneNumber;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Character gender;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<String> roles;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<FeatureResponse> features;


}
