package com.kinglogi.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderWithPaymentResponse {

    private String status;
    private String code;
    private String message;
    private String orderCode;
    private PaymentUrlResponse paymentInfo;
    private Long discount;
}
