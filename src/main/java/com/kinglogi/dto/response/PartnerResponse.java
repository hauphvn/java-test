package com.kinglogi.dto.response;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class PartnerResponse {

    private Long id;

    private String name;

    private String description;

    private Date createdAt;

    private Date lastModifiedAt;

    private AccountResponse accountResponse;

    private List<AppClientResponse> appClientResponses;
}
