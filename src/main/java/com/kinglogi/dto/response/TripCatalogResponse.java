package com.kinglogi.dto.response;


import lombok.Data;

import java.time.LocalDateTime;

@Data
public class TripCatalogResponse {

    private Long id;

    private String pickingUpLocation;

    private String droppingOffLocation;

    private Float distanceInKm;

    private Long pricePerKm;

    private String keyword;

    private String language;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

}
