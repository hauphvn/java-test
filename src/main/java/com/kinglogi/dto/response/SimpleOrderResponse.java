package com.kinglogi.dto.response;

import lombok.Data;

import java.util.Date;

@Data
public class SimpleOrderResponse {

	private Long id;
	private String fullName;
	private String email;
	private String phoneNumber;
	private String orderCode;
	private String pickingUpLocation;
	private String droppingOffLocation;
	private Float distanceInKm;
	private String brand;
	private Integer numberOfSeat;
	private Integer status;
	private String note;
	private Boolean twoWay;
	private Integer numberOfCustomer;
	private Integer numberOfPickupPoint;
	private Date pickingUpDatetime;
}
