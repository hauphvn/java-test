package com.kinglogi.dto.response;

import com.kinglogi.dto.response.file_response.FileCarGroupResponse;
import com.kinglogi.entity.CarGroup;

import java.util.List;

public class CarGroupResponse {
    private Long id;

    private String title;

    private Long price;

    private boolean priceNegotiate;

    private int numberOfSeat;

    private List<FileCarGroupResponse> files;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public boolean isPriceNegotiate() {
        return priceNegotiate;
    }

    public void setPriceNegotiate(boolean priceNegotiate) {
        this.priceNegotiate = priceNegotiate;
    }

    public int getNumberOfSeat() {
        return numberOfSeat;
    }

    public void setNumberOfSeat(int numberOfSeat) {
        this.numberOfSeat = numberOfSeat;
    }

    public List<FileCarGroupResponse> getFiles() {
        return files;
    }

    public void setFiles(List<FileCarGroupResponse> files) {
        this.files = files;
    }

    public CarGroupResponse(CarGroup carGroup) {
        this.id = carGroup.getId();
        this.title = carGroup.getTitle();
        this.price = carGroup.getPrice();
        this.priceNegotiate = carGroup.isPriceNegotiate();
        this.numberOfSeat = carGroup.getNumberOfSeat();
    }
}
