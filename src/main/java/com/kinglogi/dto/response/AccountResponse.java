package com.kinglogi.dto.response;

import com.kinglogi.constant.RankPoint;
import com.kinglogi.dto.collaborator_group.CollaboratorGroupResponse;
import com.kinglogi.dto.commission.CommissionOutputDTO;
import com.kinglogi.entity.Account;
import com.kinglogi.entity.Commission;
import com.kinglogi.entity.Role;
import lombok.Data;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

@Data
public class AccountResponse {

    private Long id;

    private String username;

    private String fullName;

    private String phoneNumber;

    private Character gender;

    private String email;

    private Set<String> roles;

    private Boolean active;

    private Long minBalance;

    private Date createdAt;

    private Date lastModifiedAt;

    private Long balance;

    private Boolean allowNotification;

    private CollaboratorGroupResponse collaboratorGroup;

    private String avatar;

    private CommissionOutputDTO commissionOutputDTO;

    private Integer loyaltyPoint;

    private String address;

    private Date birthday;

    private Instant penaltyTime;

    private RankPoint rankPoint;
}
