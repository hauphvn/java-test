package com.kinglogi.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PaymentUrlResponse {

    private String url;

    private Date createdAt;
}
