package com.kinglogi.dto.response;

import com.kinglogi.dto.KlConfiguration;
import lombok.Data;

import java.util.List;

@Data
public class MasterDataResponse {

    private List<TripCatalogResponse> tripCatalogs;

    private List<TripResponse> trips;

    private List<BankAccountResponse> bankAccounts;

    private KlConfiguration configuration;

}
