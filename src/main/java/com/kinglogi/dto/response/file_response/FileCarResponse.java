package com.kinglogi.dto.response.file_response;

import com.kinglogi.entity.File;
import com.kinglogi.entity.Image;

public class FileCarResponse {
    private Long id;

    private String name;

    private String url;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public FileCarResponse(Image image) {
        this.id = image.getId();
        this.name = image.getDisplayName();
        this.url = "/api/v2/cars/" + id + "/images/"  + name;
    }
}
