package com.kinglogi.dto.response.file_response;

import com.kinglogi.entity.File;

public class FileProviderServiceResponse {
    private Long id;

    private String name;

    private String url;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public FileProviderServiceResponse(File file, Long serviceId) {
        this.id = file.getId();
        this.name = file.getName();
        this.url =  "/api/v2/services/" + serviceId + "/images/"  + name;
    }
}
