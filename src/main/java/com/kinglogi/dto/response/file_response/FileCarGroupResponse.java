package com.kinglogi.dto.response.file_response;

import com.kinglogi.entity.File;

public class FileCarGroupResponse {
    private Long id;

    private String name;

    private String url;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public FileCarGroupResponse(File file, Long carGroupId) {
        this.id = file.getId();
        this.name = file.getName();
        this.url = "/api/v2/car-groups/" + carGroupId + "/images/"  + name;
    }
}
