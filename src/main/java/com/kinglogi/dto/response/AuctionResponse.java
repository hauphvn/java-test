package com.kinglogi.dto.response;

import com.kinglogi.constant.AuctionType;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Data
public class AuctionResponse {

	private Long id;

	private String name;

	private Date startAt;

	private Date endAt;

	private Long totalPrice;

	private String bidUserName;

	private WinningBidderResponse bidder;

	private String description;

	private Integer status;

	private LocalDateTime updatedAt;

	private Long updatedBy;

	private List<Long> orderIds;

    private AuctionType type = AuctionType.BUY;

    private Long bidPrice;

    private Long bidId;
}
