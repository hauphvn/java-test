package com.kinglogi.dto.response;


import lombok.Data;

import java.time.LocalDateTime;

@Data
public class BankAccountResponse {

    private Long id;

    private String accountName;

    private String bankName;

    private String bankAddress;

    private String accountNumber;

    private String accountCardNumber;

    private String language;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

}
