package com.kinglogi.dto.response;


import java.time.LocalDateTime;
import java.util.Date;

import com.kinglogi.dto.price_range.PriceRangeOutputDTO;
import com.kinglogi.entity.Trip;
import lombok.Data;

@Data
public class TripResponse {

    private Long id;

    private String pickingUpLocation;

    private String droppingOffLocation;

    private Date pickingUpDatetime;

    private Float distanceInKm;

    private Long totalPrice;

    private String brand;

    private Integer numberOfSeat;

    private String description;

    private String language;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    private PriceRangeOutputDTO priceRangeOutputDTO;

    public TripResponse() {
    }

    public TripResponse(Trip trip) {
        this.id = trip.getId();
        this.pickingUpLocation = trip.getPickingUpLocation();
        this.droppingOffLocation = trip.getDroppingOffLocation();
        this.pickingUpDatetime = trip.getPickingUpDatetime();
        this.distanceInKm = trip.getDistanceInKm();
        this.totalPrice = trip.getTotalPrice();
        this.brand = trip.getBrand();
        this.numberOfSeat = trip.getNumberOfSeat();
        this.description = trip.getDescription();
        this.language = trip.getLanguage();
        this.createdAt = trip.getCreatedAt();
        this.updatedAt = trip.getUpdatedAt();
    }
}
