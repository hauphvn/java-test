package com.kinglogi.dto.response;

import com.kinglogi.dto.price_range.PriceRangeOutputDTO;
import com.kinglogi.dto.promotion.PromotionResponse;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Data
public class OrderResponse {

	private Long id;
	private Long tripId;
	private String fullName;
	private String email;
	private String phoneNumber;
	private String orderCode;
	private String pickingUpLocation;
	private String droppingOffLocation;
	private Float distanceInKm;
	private Long totalPrice;
	private Long endPrice;
	private String brand;
	private Integer numberOfSeat;
	private Integer status;
	private String note;
	private Boolean twoWay;
	private Integer numberOfCustomer;
	private Integer numberOfPickupPoint;
	private Date pickingUpDatetime;
	private String paymentType;
	private String verifyComment;
	private LocalDateTime createdAt;
	private PromotionResponse promotionResponse;
	private Long discount;
	private List<OrderStopPointResponse> orderStopPoints;
	private int totalTime;
    private Long collaboratorId;
	private PriceRangeOutputDTO priceRange;
}
