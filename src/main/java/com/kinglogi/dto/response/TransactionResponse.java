package com.kinglogi.dto.response;

import lombok.Data;

import java.util.Date;
import java.util.Set;

@Data
public class TransactionResponse {

    private Long id;

    private Long amount;

    private String content;

    private String transactionType;

    private Date transactionAt;

    private Long createdBy;

    private String modifyBy;

    private Date createdAt;

}
