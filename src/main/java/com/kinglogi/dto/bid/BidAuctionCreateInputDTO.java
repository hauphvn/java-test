package com.kinglogi.dto.bid;

import com.kinglogi.constant.AuctionType;

public class BidAuctionCreateInputDTO {
    private Long auctionId;
    private Long bidPrice;
    private AuctionType type = AuctionType.BUY; // default

    public AuctionType getType() {
        return type;
    }

    public void setType(AuctionType type) {
        this.type = type;
    }

    public Long getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(Long auctionId) {
        this.auctionId = auctionId;
    }

    public Long getBidPrice() {
        return bidPrice;
    }

    public void setBidPrice(Long bidPrice) {
        this.bidPrice = bidPrice;
    }
}
