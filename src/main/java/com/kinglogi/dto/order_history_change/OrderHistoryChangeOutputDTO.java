package com.kinglogi.dto.order_history_change;

import com.kinglogi.dto.response.OrderResponse;
import lombok.Data;

import java.time.Instant;
import java.time.LocalDateTime;

@Data
public class OrderHistoryChangeOutputDTO {

    private String oldValue;
    private String newValue;
    private String orderCode;
    private String modifyBy;
    private LocalDateTime createdAt;


}
