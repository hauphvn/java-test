package com.kinglogi.dto.sms;

public class SmsResponse {

    private int code;

    private String message;

    private String transId;

    private String oper;

    private int totalSMS;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public String getOper() {
        return oper;
    }

    public void setOper(String oper) {
        this.oper = oper;
    }

    public int getTotalSMS() {
        return totalSMS;
    }

    public void setTotalSMS(int totalSMS) {
        this.totalSMS = totalSMS;
    }
}
