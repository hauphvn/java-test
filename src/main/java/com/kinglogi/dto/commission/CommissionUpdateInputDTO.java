package com.kinglogi.dto.commission;

import com.kinglogi.constant.CommissionPriceType;

import javax.validation.constraints.NotNull;
import java.util.List;

public class CommissionUpdateInputDTO {

    @NotNull
    private String title;

    @NotNull
    private String description;

    @NotNull
    private CommissionPriceType priceType;

    @NotNull
    private Integer priceValue;

    private List<Long> collaboratorIds;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CommissionPriceType getPriceType() {
        return priceType;
    }

    public void setPriceType(CommissionPriceType priceType) {
        this.priceType = priceType;
    }

    public Integer getPriceValue() {
        return priceValue;
    }

    public void setPriceValue(Integer priceValue) {
        this.priceValue = priceValue;
    }

    public List<Long> getCollaboratorIds() {
        return collaboratorIds;
    }

    public void setCollaboratorIds(List<Long> collaboratorIds) {
        this.collaboratorIds = collaboratorIds;
    }
}
