package com.kinglogi.dto.commission;

import com.kinglogi.constant.CommissionPriceType;
import com.kinglogi.entity.Commission;

public class CommissionOutputDTO {
    private Long id;
    private String title;
    private String description;
    private CommissionPriceType priceType;
    private int priceValue;

    public CommissionOutputDTO(Commission commission) {
        this.id = commission.getId();
        this.title = commission.getTitle();
        this.description = commission.getDescription();
        this.priceType = commission.getPriceType();
        this.priceValue = commission.getPriceValue();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CommissionPriceType getPriceType() {
        return priceType;
    }

    public void setPriceType(CommissionPriceType priceType) {
        this.priceType = priceType;
    }

    public int getPriceValue() {
        return priceValue;
    }

    public void setPriceValue(int priceValue) {
        this.priceValue = priceValue;
    }
}
