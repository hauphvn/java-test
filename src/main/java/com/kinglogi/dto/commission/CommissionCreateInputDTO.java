package com.kinglogi.dto.commission;

import com.kinglogi.constant.CommissionPriceType;

import javax.validation.constraints.NotNull;
import java.util.List;

public class CommissionCreateInputDTO {
    @NotNull
    private String title;

    @NotNull
    private String description;

    @NotNull
    private CommissionPriceType priceType;

    @NotNull
    private int priceValue;

    private List<Long> collaboratorIds;

    public CommissionPriceType getPriceType() {
        return priceType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPriceType(CommissionPriceType priceType) {
        this.priceType = priceType;
    }

    public int getPriceValue() {
        return priceValue;
    }

    public void setPriceValue(int priceValue) {
        this.priceValue = priceValue;
    }

    public List<Long> getCollaboratorIds() {
        return collaboratorIds;
    }

    public void setCollaboratorIds(List<Long> collaboratorIds) {
        this.collaboratorIds = collaboratorIds;
    }
}
