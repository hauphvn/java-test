package com.kinglogi.dto.inventory;

import com.kinglogi.InventoryTargetUser;
import com.kinglogi.constant.InventoryTripType;
import lombok.Data;

import java.time.Instant;
import java.util.List;

@Data
public class InventoryInputDTO {
    private String name;
    private Long tripId;
    private InventoryTripType inventoryTripType;
    private List<InventoryTargetUser> targetUsers;
    private Integer numberOfSeat;
    private Integer numberOfTrip;
    private Long price;
    private Instant expireTime;
    private String pickingUpLocation;
    private String droppingOffLocation;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getTripId() {
        return tripId;
    }

    public void setTripId(Long tripId) {
        this.tripId = tripId;
    }

    public InventoryTripType getInventoryTripType() {
        return inventoryTripType;
    }

    public void setInventoryTripType(InventoryTripType inventoryTripType) {
        this.inventoryTripType = inventoryTripType;
    }



    public Integer getNumberOfSeat() {
        return numberOfSeat;
    }

    public void setNumberOfSeat(Integer numberOfSeat) {
        this.numberOfSeat = numberOfSeat;
    }

    public Integer getNumberOfTrip() {
        return numberOfTrip;
    }

    public void setNumberOfTrip(Integer numberOfTrip) {
        this.numberOfTrip = numberOfTrip;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Instant getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Instant expireTime) {
        this.expireTime = expireTime;
    }


    public List<InventoryTargetUser> getTargetUsers() {
        return targetUsers;
    }

    public void setTargetUsers(List<InventoryTargetUser> targetUsers) {
        this.targetUsers = targetUsers;
    }
}
