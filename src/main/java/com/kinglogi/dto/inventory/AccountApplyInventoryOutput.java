package com.kinglogi.dto.inventory;

import lombok.Data;

import java.time.Instant;
import java.time.LocalDateTime;

@Data
public class AccountApplyInventoryOutput {

    private String username;

    private LocalDateTime createdAt;

}
