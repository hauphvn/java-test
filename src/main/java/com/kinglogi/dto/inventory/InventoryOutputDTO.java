package com.kinglogi.dto.inventory;

import com.kinglogi.InventoryTargetUser;
import com.kinglogi.constant.InventoryTripType;
import com.kinglogi.dto.response.TripResponse;
import com.kinglogi.entity.Inventory;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class InventoryOutputDTO {
    private Long id;
    private String name;

    private TripResponse trip;
    private InventoryTripType inventoryTripType;
    private List<InventoryTargetUser> targetUsers;
    private Integer numberOfSeat;

    private Integer totalOfTrip;
    private Integer numberOfTrip;
    private Long price;
    private Instant expireTime;

    private Long updatedBy;

    private LocalDateTime updatedAt;

    private Long createdBy;

    private LocalDateTime createdAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void setPrice(Long price) {
        this.price = price;
    }

    public InventoryTripType getInventoryTripType() {
        return inventoryTripType;
    }

    public void setInventoryTripType(InventoryTripType inventoryTripType) {
        this.inventoryTripType = inventoryTripType;
    }

    public List<InventoryTargetUser> getTargetUser() {
        return targetUsers;
    }

    public void setTargetUser(List<InventoryTargetUser> targetUser) {
        this.targetUsers = targetUser;
    }

    public Integer getNumberOfSeat() {
        return numberOfSeat;
    }

    public void setNumberOfSeat(Integer numberOfSeat) {
        this.numberOfSeat = numberOfSeat;
    }

    public Integer getNumberOfTrip() {
        return numberOfTrip;
    }

    public void setNumberOfTrip(Integer numberOfTrip) {
        this.numberOfTrip = numberOfTrip;
    }


    public TripResponse getTrip() {
        return trip;
    }

    public void setTrip(TripResponse trip) {
        this.trip = trip;
    }

    public List<InventoryTargetUser> getTargetUsers() {
        return targetUsers;
    }

    public void setTargetUsers(List<InventoryTargetUser> targetUsers) {
        this.targetUsers = targetUsers;
    }

    public Long getPrice() {
        return price;
    }

    public Instant getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Instant expireTime) {
        this.expireTime = expireTime;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public InventoryOutputDTO() {
    }

    public Integer getTotalOfTrip() {
        return totalOfTrip;
    }

    public void setTotalOfTrip(Integer totalOfTrip) {
        this.totalOfTrip = totalOfTrip;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public InventoryOutputDTO(Inventory inventory) {
        this.id = inventory.getId();
        this.name = inventory.getName();
        this.inventoryTripType = inventory.getInventoryTripType();
        this.numberOfSeat = inventory.getNumberOfSeat();
        this.numberOfTrip = inventory.getNumberOfTrip();
        this.totalOfTrip = inventory.getTotalOfTrip();
        this.price = inventory.getPrice();
        this.trip = new TripResponse(inventory.getTrip());
        this.targetUsers = convertTargetUser(new ArrayList<>(Arrays.asList(inventory.getTargetUsers().split(","))));
        this.expireTime = inventory.getExpireTime();
        this.createdAt = inventory.getCreatedAt();
        this.createdBy = inventory.getCreatedBy();
        this.updatedAt = inventory.getUpdatedAt();
        this.updatedBy = inventory.getUpdatedBy();
  }

    public List<InventoryTargetUser> convertTargetUser(List<String> users) {
        return users.stream().map(InventoryTargetUser::valueOf).collect(Collectors.toList());
  }
}
