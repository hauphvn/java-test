package com.kinglogi.dto.log;

import com.kinglogi.entity.Log;
import lombok.Data;

import java.time.Instant;
import java.util.Map;

@Data
public class LogOutput {

    private Long id;

    private String eventName;

    private Map<String, Object> data;

    private Instant eventDate;

    public LogOutput(Log log) {
        this.id = log.getId();
        this.eventName = log.getEventName();
        this.data = log.getData();
        this.eventDate = log.getEventDate();
    }
}
