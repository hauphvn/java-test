package com.kinglogi.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AuctionMail {

    private String name;

    private Long totalPrice;

    private String startAt;

    private String bidUserName;

}
