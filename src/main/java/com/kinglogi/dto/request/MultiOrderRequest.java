package com.kinglogi.dto.request;

import lombok.Data;

import java.util.List;

@Data
public class MultiOrderRequest {

    private List<Long> ids;
}
