package com.kinglogi.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class PaymentRequest {

    @NotBlank
    private String content;

    @NotNull
    @Min(1)
    private Long amount;

    @JsonIgnore
    private String txnRef;

    private String returnUrl;
}
