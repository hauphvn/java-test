package com.kinglogi.dto.request;

import com.kinglogi.utils.BeanUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Data
public class FcmSubscriptionRequest extends FcmPushNotificationRequest {

    private String topicName;

    public void withParentBuilder(FcmPushNotificationRequest.FcmPushNotificationRequestBuilder parentBuilder) {
        BeanUtil.mergeProperties(parentBuilder.build(), this);
    }
}