package com.kinglogi.dto.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class CarRequest {

	@NotBlank
	private String driverName;

	@NotBlank
	private String brand;

	@NotBlank
	private String driverPhoneNumber;

	@NotNull
	private Integer numberOfSeat;

	private String description;

	@NotNull
	private Long accountId;

	private String licensePlates;

}
