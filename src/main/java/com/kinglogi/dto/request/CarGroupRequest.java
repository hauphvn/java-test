package com.kinglogi.dto.request;

import javax.persistence.Column;

public class CarGroupRequest {

    private String title;

    private Long price;

    private boolean priceNegotiate;

    private int numberOfSeat;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public boolean isPriceNegotiate() {
        return priceNegotiate;
    }

    public void setPriceNegotiate(boolean priceNegotiate) {
        this.priceNegotiate = priceNegotiate;
    }

    public int getNumberOfSeat() {
        return numberOfSeat;
    }

    public void setNumberOfSeat(int numberOfSeat) {
        this.numberOfSeat = numberOfSeat;
    }
}
