package com.kinglogi.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kinglogi.constant.Role;
import com.kinglogi.service.AccountService;
import com.kinglogi.validator.Gender;
import com.kinglogi.validator.Unique;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringExclude;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Data
public class RegisterRequest {

    @NotBlank
    @Size(min = 1, max = 128)
    private String fullName;

    @NotBlank
    @Size(min = 1, max = 20)
    private String phoneNumber;


    @NotBlank
    @Email
    @Size(min = 4, max = 256)
    private String username;

    @ToStringExclude
    @NotBlank
    @Size(min = 4, max = 128)
    private String password;

    @Gender
    private Character gender;

    private String language;

    @JsonIgnore
    private Boolean active;

    private List<String> roles;

    private String address;

    private Date birthday;

}