package com.kinglogi.dto.request;


import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

@Data
public class TripRequest {

    private Long id;

    @NotBlank
    private String pickingUpLocation;

    private String droppingOffLocation;

    @NotNull
    private Date pickingUpDatetime;

    @NotNull
    @Range(min = 0, max = 9999)
    private Float distanceInKm;

    @NotNull
    @Range(min = 0, max = 999999999, message = "Price must between 0 and 999999999")
    private Long totalPrice;

    private String brand;

    @NotNull
    @Range(min = 0, max = 45, message = "Price must between 0 and 45")
    private Integer numberOfSeat;

    private String description;

    private String keyword;

    @Pattern(regexp = "^(en)|(vi)$", message = "Only en or vi are accepted.")
    private String language;

    private Long priceRangeId;

    private MetaData goMetaData;

}
