package com.kinglogi.dto.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class OrderStopPointRequest {

    @NotNull
    @NotBlank
    private String stopPoint;

    @NotNull
    @NotBlank
    private Long price;

}
