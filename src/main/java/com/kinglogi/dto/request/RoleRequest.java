package com.kinglogi.dto.request;

import lombok.Data;

@Data
public class RoleRequest {

	private Long id;
	private String name;
	private String code;


}
