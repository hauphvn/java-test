package com.kinglogi.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ConfirmOrderMailDto {

    private String fullName;

    private String email;

    private String phoneNumber;

    private String orderCode;

    private String pickingUpLocation;

    private String droppingOffLocation;

    private Long totalPrice;

    private Long endPrice;

    private String brand;

    private Integer numberOfSeat;

    private String note;

    private String pickingUpDatetime;

    private String status;

    private String verifyComment;
}
