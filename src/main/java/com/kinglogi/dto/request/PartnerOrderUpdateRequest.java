package com.kinglogi.dto.request;

import com.kinglogi.constant.OrderStatus;
import com.kinglogi.constant.PaymentType;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class PartnerOrderUpdateRequest {

    @NotNull
    private Long appClientId;

    @NotBlank
    @Length(max = 128)
    private String appClientSecret;

    private String orderCode;

    private OrderStatus status;

    private Long price;

    private PaymentType paymentType;

}
