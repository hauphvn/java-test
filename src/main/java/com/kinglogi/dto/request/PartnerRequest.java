package com.kinglogi.dto.request;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
public class PartnerRequest {

	@NotNull
	@Length(max = 128)
	private String name;

	@Length(max = 512)
	private String description;

	@Valid
	@NotNull
	private AccountRequest accountRequest;

}
