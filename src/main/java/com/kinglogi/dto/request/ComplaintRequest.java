package com.kinglogi.dto.request;

import com.kinglogi.constant.ComplaintConstant;
import lombok.Data;

import javax.validation.constraints.Email;

@Data
public class ComplaintRequest {
    private String title;

    private String description;

    private String email;

    private String phoneNumber;

    private String type;

    private Long plusPoint;

    private ComplaintConstant status;

    private String message;
}
