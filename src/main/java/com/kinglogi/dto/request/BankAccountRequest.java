package com.kinglogi.dto.request;


import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
public class BankAccountRequest {

    @NotBlank
    private String accountName;

    @NotBlank
    private String bankName;

    @NotBlank
    private String bankAddress;

    @NotBlank
    private String accountNumber;

    @NotBlank
    private String accountCardNumber;

    @Pattern(regexp = "^(en)|(vi)$", message = "Only en or vi are accepted.")
    private String language;

}
