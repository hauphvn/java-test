package com.kinglogi.dto.request;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class PartnerFixedTripOrderRequest extends PartnerOrderRequest {

	@NotNull
	private Long tripId;

	@NotNull
	private Long totalPrice;

	@URL
	@Length(max = 512)
	private String paymentCallbackUrl;
}
