package com.kinglogi.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderMailDto {

    private String orderCode;

    private String pickingUpLocation;

    private String droppingOffLocation;

    private String pickingUpDatetime;
}
