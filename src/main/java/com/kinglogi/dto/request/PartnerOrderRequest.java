package com.kinglogi.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kinglogi.constant.LocaleConstant;
import com.kinglogi.constant.PaymentType;
import com.kinglogi.entity.Inventory;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class PartnerOrderRequest {

	@NotBlank
	@Length(max = 128)
	private String fullName;

	@NotBlank
	@Length(max = 20)
	private String phoneNumber;

	@Email
	@Length(max = 128)
	private String email;

	@NotBlank
	@Length(max = 256)
	private String pickingUpLocation;

	@NotBlank
	@Length(max = 256)
	private String droppingOffLocation;

	@NotNull
	private Date pickingUpDatetime;

	@Range(min = 0, max = 45)
	private Integer numberOfSeat;

	private Integer numberOfCustomer = 1;

	private Integer numberOfPickupPoint = 1;

	@Length(max = 2048)
	private String note;

	@JsonIgnore
	private String language = LocaleConstant.VIETNAMESE;

	private Long inventoryId;

	private boolean sendEmail = true;

	private PaymentType paymentType;
}
