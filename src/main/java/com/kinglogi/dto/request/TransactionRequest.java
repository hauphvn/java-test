package com.kinglogi.dto.request;

import com.kinglogi.constant.TransactionReason;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class TransactionRequest {

    @NotNull
    private Long targetId;

    @NotNull
    private Integer transactionReason = TransactionReason.PAY_IN.getType();

    private String transactionType;

    @NotNull
    private Long amount;

    @NotBlank
    private String content;

    @NotNull
    private Date transactionAt;

}
