package com.kinglogi.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kinglogi.service.AccountService;
import com.kinglogi.validator.UniqueEntity;
import com.kinglogi.validator.UniqueIdentifier;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(
        value = {"password"},
        allowSetters = true
)
public class AccountRequest implements UniqueIdentifier<Long> {

    private Long id;

    @NotBlank
    @Email
    private String username;

    private String password;

    @NotBlank
    private String fullName;

    private String phoneNumber;

    private Character gender;

    @Email
    @JsonIgnore
    private String email;

    private List<String> roles;

    @NotNull
    private Boolean active;

    private Boolean allowNotification;

    private String pushToken;

    private Long minBalance;

    private Integer loyaltyPointActionType; // 0 for plus, 1 for minus

    private Integer loyaltyPointActionAmount;

    private String address;

    private Date birthday;
}
