package com.kinglogi.dto.request;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.kinglogi.constant.AuctionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AuctionRequest {

	private Long id;

	@NotBlank
	private String name;

	private Date startAt;

	@NotNull
	private Date endAt;

	private Long totalPrice;

	private String description;

	private Integer status;

	private List<Long> orderIds;

    private AuctionType type = AuctionType.BUY;
}
