package com.kinglogi.dto.request;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class PartnerFlexibleTripOrderRequestFromIframe extends PartnerOrderRequest {

	@NotNull
	private Long appClientId;

	@NotBlank
	@Length(max = 128)
	private String appClientSecret;
}
