package com.kinglogi.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceMail {
    private String orderCode;

    private String phoneNumber;

    private String email;

    private String fullName;

    private String address;
}
