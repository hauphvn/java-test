package com.kinglogi.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderRequest {

	private Long id;

	@JsonIgnore
	private Long accountId;

	private Long tripId;

	@JsonIgnore
	private Long transactionId;

	@NotBlank
	@Length(max = 128)
	private String fullName;

	@Email
	private String email;

	@NotBlank
	@Length(max = 20)
	private String phoneNumber;

	@JsonIgnore
	private String orderCode;

	@NotBlank
	@Length(max = 256)
	private String pickingUpLocation;

	@NotBlank
	@Length(max = 256)
	private String droppingOffLocation;

	private Float distanceInKm;

	private Long totalPrice;

	private String brand;

	private Integer numberOfSeat;

	@Length(max = 2048)
	private String note;

	private Boolean twoWay;

	private Integer numberOfCustomer = 1;

	private Integer numberOfPickupPoint = 1;

	@NotNull
	private Date pickingUpDatetime;

	private String paymentType;

	private Integer status;

	@Length(max = 2048)
	private String verifyComment;

	@Length(max = 3)
	private String language;

	@JsonIgnore
	@URL
	private String paymentCallbackUrl;

	private String promotionCode;

	@JsonIgnore
	private Long userPromotionId;

	@JsonIgnore
	private Long endPrice;

	private List<OrderStopPointRequest> orderStopPoints;

	private int totalTime;

	private  Long inventoryId;

	private Long priceRangeId;

	private boolean sendMail = true;
}
