package com.kinglogi.dto.request;


import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.*;

@Data
public class TripCatalogRequest {

    @NotBlank
    private String pickingUpLocation;

    @NotBlank
    private String droppingOffLocation;

    @NotNull
    @Range(min = 0, max = 9999)
    private Float distanceInKm;

    @NotNull
    @Range(min = 0, max = 999999999, message = "Price must between 0 and 999999999")
    private Long pricePerKm;

    private String keyword;

    @Pattern(regexp = "^(en)|(vi)$", message = "Only en or vi are accepted.")
    private String language;

}
