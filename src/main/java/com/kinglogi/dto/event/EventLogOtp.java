package com.kinglogi.dto.event;

import com.kinglogi.entity.AccountOtp;
import org.springframework.context.ApplicationEvent;

import java.util.Map;

public class EventLogOtp extends ApplicationEvent {

    private String event;

    private Map<String, Object> data;

    public EventLogOtp(Object source, String event, Map<String, Object> data) {
        super(source);
        this.event = event;
        this.data = data;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }
}
