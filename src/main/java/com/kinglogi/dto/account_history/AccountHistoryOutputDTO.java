package com.kinglogi.dto.account_history;

import com.kinglogi.dto.response.AccountResponse;
import com.kinglogi.entity.Account;
import com.kinglogi.entity.AccountHistory;
import com.kinglogi.service.utils.Utils;
import com.mysql.cj.util.Util;
import lombok.Data;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Data
public class AccountHistoryOutputDTO {
    private Long id;
    private String oldValue;
    private String newValue;

    private String updatedBy;

    private Instant updatedAt;

    private AccountResponse createdBy;

    private Instant createdAt;

    public AccountHistoryOutputDTO(AccountHistory accountHistory) {
        this.id = accountHistory.getId();
        this.oldValue = accountHistory.getOldValue();
        this.newValue = accountHistory.getNewValue();
        this.updatedAt = accountHistory.getUpdatedAt().toInstant(ZoneOffset.UTC);
        this.createdAt = accountHistory.getCreatedAt().toInstant(ZoneOffset.UTC);;
    }

}
