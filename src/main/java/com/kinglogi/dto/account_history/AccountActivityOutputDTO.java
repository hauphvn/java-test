package com.kinglogi.dto.account_history;

import com.kinglogi.entity.Account;
import lombok.Data;

import java.time.Instant;

@Data
public class AccountActivityOutputDTO {

    private String userName;

    private Instant lastLogin;

    private Long accountId;

    public AccountActivityOutputDTO(Account account) {
        this.userName = account.getUsername();
        this.lastLogin = account.getLastLogin();
        this.accountId = account.getId();
    }
}
