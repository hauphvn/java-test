package com.kinglogi.dto.promotion;

import com.kinglogi.constant.UserSelectOption;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;

@Data
public class PromotionRequest {

    @NotNull
    @NotBlank
    private String name;

    @NotNull
    @NotBlank
    private String code;

    @NotNull
    private String description;

    @NotNull
    private Instant expireTime;

    private long numberOfUsage;

    private long price;

    private List<Long> listUserIds;

    private UserSelectOption userSelectOption;

}
