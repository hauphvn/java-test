package com.kinglogi.dto.promotion;

import com.kinglogi.constant.UserSelectOption;
import com.kinglogi.dto.user_promotion.UserPromotionResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PromotionResponse {

    private Long id;

    private String name;

    private String code;

    private String description;

    private Instant expireTime;

    private long numberOfUsage;

    private long price;

    private List<Long> listUserIds;

    private List<UserPromotionResponse> userPromotionResponses;

    private UserSelectOption userSelectOption;

}
