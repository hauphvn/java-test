package com.kinglogi.dto.partner_link;

import com.kinglogi.constant.PartnerLinkPosition;
import com.kinglogi.entity.PartnerLink;
import org.apache.commons.io.FilenameUtils;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import java.time.LocalDateTime;

public class PartnerLinkOutputDTO {
    private Long id;
    private String name;

    private PartnerLinkPosition position;
    private String url;
    private String logoUrl;
    private String adImageUrl;

    private Long updatedBy;

    private LocalDateTime updatedAt;

    private Long createdBy;

    private LocalDateTime createdAt;

    public PartnerLinkOutputDTO(PartnerLink partnerLink) {
        this.id = partnerLink.getId();
        this.name = partnerLink.getName();
        this.url = partnerLink.getUrl();
        this.position = partnerLink.getPosition();
        this.logoUrl = partnerLink.getLogoUrl() != null ? "/api/v2/partner-links/" + id + "/images/" + FilenameUtils.getName(partnerLink.getLogoUrl()) : null;
        this.adImageUrl = partnerLink.getAdImageUrl() != null ? "/api/v2/partner-links/" + id + "/images/" + FilenameUtils.getName(partnerLink.getAdImageUrl()) : null;
        this.createdAt = partnerLink.getCreatedAt();
        this.createdBy = partnerLink.getCreatedBy();
        this.updatedAt = partnerLink.getUpdatedAt();
        this.updatedBy = partnerLink.getUpdatedBy();
    }


    public PartnerLinkPosition getPosition() {
        return position;
    }

    public void setPosition(PartnerLinkPosition position) {
        this.position = position;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getAdImageUrl() {
        return adImageUrl;
    }

    public void setAdImageUrl(String adImageUrl) {
        this.adImageUrl = adImageUrl;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
