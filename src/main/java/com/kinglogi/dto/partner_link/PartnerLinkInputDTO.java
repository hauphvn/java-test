package com.kinglogi.dto.partner_link;

import com.kinglogi.constant.PartnerLinkPosition;

import javax.validation.constraints.NotNull;

public class PartnerLinkInputDTO {
    @NotNull
    private String name;

    @NotNull
    private String url;

    @NotNull
    private PartnerLinkPosition position;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public PartnerLinkPosition getPosition() {
        return position;
    }

    public void setPosition(PartnerLinkPosition position) {
        this.position = position;
    }
}
