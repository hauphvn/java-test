package com.kinglogi.dto.user_promotion;

import lombok.Data;

@Data
public class UserPromotionResponse {

    private Long id;

    private String fullName;

    private String username;

    private String phoneNumber;

    private long usageRemain;

}
