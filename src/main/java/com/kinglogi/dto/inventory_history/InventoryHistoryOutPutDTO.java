package com.kinglogi.dto.inventory_history;
import com.google.gson.Gson;
import com.kinglogi.dto.inventory.InventoryOutputDTO;
import com.kinglogi.dto.response.AccountResponse;
import com.kinglogi.entity.InventoryHistory;

import java.time.LocalDateTime;

public class InventoryHistoryOutPutDTO {
    private Long id;

    private InventoryOutputDTO oldValue;

    private InventoryOutputDTO newValue;

    private Long updatedBy;

    private LocalDateTime updatedAt;

    private AccountResponse createdBy;

    private LocalDateTime createdAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public InventoryOutputDTO getOldValue() {
        return oldValue;
    }

    public void setOldValue(InventoryOutputDTO oldValue) {
        this.oldValue = oldValue;
    }

    public InventoryOutputDTO getNewValue() {
        return newValue;
    }

    public void setNewValue(InventoryOutputDTO newValue) {
        this.newValue = newValue;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public AccountResponse getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(AccountResponse createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public InventoryHistoryOutPutDTO(InventoryHistory inventoryHistory) {
        this.id = inventoryHistory.getId();
        this.oldValue = convertStringToObject(inventoryHistory.getOldValue());
        this.newValue = convertStringToObject(inventoryHistory.getNewValue());
        this.updatedBy = inventoryHistory.getUpdatedBy();
        this.updatedAt = inventoryHistory.getUpdatedAt();
        this.createdAt = inventoryHistory.getCreatedAt();
    }

    public InventoryOutputDTO convertStringToObject(String value) {
        Gson g = new Gson();
        return g.fromJson(value, InventoryOutputDTO.class);
    }
}
