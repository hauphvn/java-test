package com.kinglogi.dto;

import lombok.Data;

import java.util.List;

@Data
public class KlConfiguration {

    private Contact contact;

    private AppStoreLink appStoreLink;

    private CompanyInformation companyInformation;

    private List<SocialNetwork> socialNetworks;
}
