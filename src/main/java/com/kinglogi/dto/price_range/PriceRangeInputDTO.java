package com.kinglogi.dto.price_range;

import lombok.Data;

import java.util.List;

@Data
public class PriceRangeInputDTO {
    private Long id;
    private String goArea;
    private String goAreaMetadata;
    private String destinationArea;
    private String destinationAreaMetadata;
    private Long price;
}
