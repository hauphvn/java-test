package com.kinglogi.dto.price_range;

import lombok.Data;

@Data
public class AreaMetadata {

    private String province;

    private String district;

    private String commune;

}
