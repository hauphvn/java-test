package com.kinglogi.dto.price_range;

import lombok.Data;

import java.util.List;

@Data
public class PriceRangeOutputDTO {
    private Long id;
    private String goArea;
    private String goAreaMetadata;
    private String destinationArea;
    private String destinationAreaMetadata;
    private Long price;
//    private List<DTO> districtDTO; //TODO: Need to change this to DTO object of Administrative units table
}
