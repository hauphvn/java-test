package com.kinglogi.dto.collaborator_group;

import lombok.Data;

@Data
public class CollaboratorGroupResponse {
    private Long id;
    private String name;
}
