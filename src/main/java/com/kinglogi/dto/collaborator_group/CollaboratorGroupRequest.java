package com.kinglogi.dto.collaborator_group;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class CollaboratorGroupRequest {
    @NotNull
    private String name;

    private List<Long> listUserId;
}
