package com.kinglogi.event;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class SaveTripListener implements ApplicationListener<SaveTripEvent> {

    @Override
    public void onApplicationEvent(SaveTripEvent saveTripEvent) {
        System.out.println("Received spring custom event - " + saveTripEvent.getSource().toString());
    }
}
