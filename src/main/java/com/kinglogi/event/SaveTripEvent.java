package com.kinglogi.event;

import org.springframework.context.ApplicationEvent;

public class SaveTripEvent extends ApplicationEvent {

    private String message;

    public SaveTripEvent(Object source, String message) {
        super(source);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
