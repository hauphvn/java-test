package com.kinglogi.validator;

public interface UniqueIdentifier<ID> {

    ID getId();
}
