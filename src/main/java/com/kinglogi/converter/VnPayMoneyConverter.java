package com.kinglogi.converter;

public class VnPayMoneyConverter {

    private static final int VN_PAY_DIFF = 100;

    public long toVnPay(Long amount) {
        if (amount == null) {
            return 0L;
        }
        return amount * VN_PAY_DIFF;
    }

    public long fromVnPay(Long amount) {
        if (amount == null) {
            return 0L;
        }
        return amount / VN_PAY_DIFF;
    }
}
