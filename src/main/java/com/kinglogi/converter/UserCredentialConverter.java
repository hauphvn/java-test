package com.kinglogi.converter;

import com.kinglogi.dto.AuthUserDetails;
import com.kinglogi.dto.response.UserCredential;
import com.kinglogi.utils.BeanUtil;

public class UserCredentialConverter {

    public UserCredential convert(AuthUserDetails userDetail) {
        UserCredential user = BeanUtil.copyProperties(userDetail, UserCredential.class);
        user.setRoles(new RoleConverter().convert(userDetail.getRoles()));
//        user.setFeatures(userDetail.getFeatures());
        return user;
    }
}
