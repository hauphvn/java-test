package com.kinglogi.utils;

import java.util.regex.Pattern;

public class EmailUtils {
    public static boolean isValidEmail(String email) {
        Pattern pattern = Pattern.compile("^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+\\.)+([a-zA-Z0-9]{2,4})+$", Pattern.CASE_INSENSITIVE);
        return pattern.matcher(email).matches();
    }
}
