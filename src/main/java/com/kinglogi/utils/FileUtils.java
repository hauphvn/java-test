package com.kinglogi.utils;

import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileOutputStream;
import java.io.IOException;

public class FileUtils {
    public static java.io.File convertMultiPartToFile(MultipartFile file, String filePath) throws IOException {
        String fileNameWithoutExtension = FilenameUtils.removeExtension(file.getOriginalFilename());
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());
        java.io.File convFile = new java.io.File(filePath, fileNameWithoutExtension + "." + extension);
        int num = 0;
        while (convFile.exists()) {
            convFile = new java.io.File(filePath, fileNameWithoutExtension + "(" + (++num) + ")." + extension);
        }
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write( file.getBytes() );
        fos.close();
        return convFile;
    }
}
