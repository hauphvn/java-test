package com.kinglogi.provider;


import com.kinglogi.dto.response.UserCredential;
import com.kinglogi.entity.Account;
import com.kinglogi.repository.AccountRepository;
import com.kinglogi.service.utils.Utils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AuthenticatedProvider {

    private final AccountRepository accountRepository;

    public AuthenticatedProvider(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Optional<Long> getUserId() {
        return getUser().map(UserCredential::getId);
    }

    public Optional<UserCredential> getUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || !authentication.isAuthenticated() || !(authentication.getPrincipal() instanceof UserCredential)) {
            return Optional.empty();
        }
        Optional<UserCredential> userCredential =  Optional.ofNullable( (UserCredential) authentication.getPrincipal() );
        convertAccountToUserCredential(userCredential.get());
        return userCredential;
    }

    private void convertAccountToUserCredential(UserCredential userCredential) {
        if (userCredential == null) {
            return;
        }
        Account account = Utils.requireExists(accountRepository.findById(userCredential.getId()), "error.notFound");
        userCredential.setUsername(account.getUsername());
        userCredential.setFullName(account.getFullName());
    }
}
