package com.kinglogi.entity;

import com.kinglogi.entity.base.AuditEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "promotion")
public class Promotion extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private String name;

    private String code;

    @Lob
    private String description;

    @Column(name = "expire_time")
    private Instant expireTime;

    @Column(name = "number_of_usage")
    private long numberOfUsage;

    private long price;

    private String status;

    @OneToMany(mappedBy = "promotion", fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE})
    private List<UserPromotion> userPromotions;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "deleted_at", length = 19)
    private Date deletedAt;

}
