package com.kinglogi.entity;

import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;

import java.time.Instant;
import java.util.Map;

import static javax.persistence.GenerationType.IDENTITY;

@TypeDef(
        name = "json",
        typeClass = JsonStringType.class
)
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tbl_log")
public class Log {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Lob
    @Column(name = "event_name")
    private String eventName;

    @org.hibernate.annotations.Type(type = "json")
    private Map<String, Object> data;

    @Column(name = "event_date")
    private Instant eventDate;

}
