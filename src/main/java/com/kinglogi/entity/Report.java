package com.kinglogi.entity;

import com.kinglogi.constant.ReportActivityType;
import com.kinglogi.entity.base.AuditEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

import java.time.Instant;

import static javax.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "report")
public class Report extends AuditEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    private Instant date;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id")
    private Account account;

    private String code;

    @Enumerated(EnumType.STRING)
    private ReportActivityType activity;

    @Lob
    @Column(name = "pick_up")
    private String pickUp;

    @Lob
    @Column(name = "drop_off")
    private String dropOff;

    private Long price;

    @Lob
    private String car;

    private String currency;

    @Column(name = "total_charged")
    private Long totalCharged;

    @Column(name = "klg_revenue")
    private Long klgRevenue;

    @Column(name = "driver_revenue")
    private Long driverRevenue;

    @Column(name = "customer")
    private String customer;

    @Column(name = "customer_phone")
    private String customerPhone;

    @Column(name = "driver_phone")
    private String driverPhone;

    @Column(name = "loyalty_number")
    private Long loyaltyNumber;

}
