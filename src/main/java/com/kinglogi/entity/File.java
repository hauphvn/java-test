package com.kinglogi.entity;

import com.kinglogi.entity.base.AuditEntity;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "file")
public class File extends AuditEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String url;

    @Column(name = "mime_type")
    private String mimeType;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "car_group_file", uniqueConstraints = @UniqueConstraint(columnNames = "car_group_id"), joinColumns = {
            @JoinColumn(name = "file_id", nullable = false, updatable = false) }, inverseJoinColumns = {
            @JoinColumn(name = "car_group_id", unique = true, nullable = false, updatable = false) })
    private Set<CarGroup> carGroups;


    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "service_file", uniqueConstraints = @UniqueConstraint(columnNames = "service_id"), joinColumns = {
            @JoinColumn(name = "file_id", nullable = false, updatable = false) }, inverseJoinColumns = {
            @JoinColumn(name = "service_id", unique = true, nullable = false, updatable = false) })
    private Set<ProviderService> services;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public Set<CarGroup> getCarGroups() {
        return carGroups;
    }

    public void setCarGroups(Set<CarGroup> carGroups) {
        this.carGroups = carGroups;
    }

    public Set<ProviderService> getServices() {
        return services;
    }

    public void setServices(Set<ProviderService> services) {
        this.services = services;
    }
}
