package com.kinglogi.entity;

import com.kinglogi.entity.base.AuditEntity;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "car_group")
public class CarGroup extends AuditEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    private Long price;

    @Column(name = "price_negotiate")
    private boolean priceNegotiate;

    @Column(name = "number_of_seat")
    private int numberOfSeat;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "car_group_file", uniqueConstraints = @UniqueConstraint(columnNames = "car_group_id"), joinColumns = {
            @JoinColumn(name = "car_group_id", nullable = false, updatable = false) }, inverseJoinColumns = {
            @JoinColumn(name = "file_id", nullable = false, updatable = false) })
    private List<File> files;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public boolean isPriceNegotiate() {
        return priceNegotiate;
    }

    public void setPriceNegotiate(boolean priceNegotiate) {
        this.priceNegotiate = priceNegotiate;
    }

    public int getNumberOfSeat() {
        return numberOfSeat;
    }

    public void setNumberOfSeat(int numberOfSeat) {
        this.numberOfSeat = numberOfSeat;
    }

    public List<File> getFiles() {
        return files;
    }

    public void setFiles(List<File> files) {
        this.files = files;
    }
}
