package com.kinglogi.entity;

import com.kinglogi.constant.ComplaintConstant;
import com.kinglogi.entity.base.AuditEntity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "complaint")
public class Complaint extends AuditEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    @Lob
    private String description;

    private String email;

    @Enumerated(EnumType.STRING)
    private ComplaintConstant status;

    private String type;

    @Column(name = "plus_point")
    private Long plusPoint;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Lob
    @Column(name = "admin_response")
    private String adminResponse;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Account user;

    @OneToMany(mappedBy = "complaint", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<FileAttachment> fileAttachments;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ComplaintConstant getStatus() {
        return status;
    }

    public void setStatus(ComplaintConstant status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getPlusPoint() {
        return plusPoint;
    }

    public void setPlusPoint(Long plusPoint) {
        this.plusPoint = plusPoint;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAdminResponse() {
        return adminResponse;
    }

    public void setAdminResponse(String adminResponse) {
        this.adminResponse = adminResponse;
    }

    public Account getUser() {
        return user;
    }

    public void setUser(Account user) {
        this.user = user;
    }

    public Set<FileAttachment> getFileAttachments() {
        return fileAttachments;
    }

    public void setFileAttachments(Set<FileAttachment> fileAttachments) {
        this.fileAttachments = fileAttachments;
    }
}
