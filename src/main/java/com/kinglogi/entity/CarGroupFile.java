package com.kinglogi.entity;

import com.kinglogi.entity.base.AuditEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "car_group_file")
@Getter
@Setter
public class CarGroupFile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "car_group_id")
    private CarGroup carGroup;

    @ManyToOne
    @JoinColumn(name = "file_id")
    private File file;
}
