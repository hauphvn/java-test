package com.kinglogi.entity;

import com.kinglogi.constant.InventoryStatus;
import com.kinglogi.constant.InventoryTripType;
import com.kinglogi.entity.base.AuditEntity;

import javax.persistence.*;
import java.time.Instant;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "inventory")
public class Inventory extends AuditEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private String name;

    @ManyToOne
    @JoinColumn(name = "trip_id")
    private Trip trip;

    @Column(name = "total_of_trip")
    private int totalOfTrip;

    @Column(name = "number_of_trip")
    private int numberOfTrip;

    private Long price;

    @Column(name = "inventory_trip_type")
    private InventoryTripType inventoryTripType;


    @Column(name = "target_users")
    private String targetUsers;

    @Column(name = "number_of_seat")
    private Integer numberOfSeat;

    @Column(name = "expire_time")
    private Instant expireTime;

    private InventoryStatus status;

    @OneToMany(mappedBy = "inventory", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<Order> orders;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "deleted_at", length = 19)
    private Date deletedAt;

    @OneToMany(mappedBy = "inventory", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<AccountInventoryOrder> accountInventoryOrders;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public int getNumberOfTrip() {
        return numberOfTrip;
    }

    public void setNumberOfTrip(int numberOfTrip) {
        this.numberOfTrip = numberOfTrip;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getTargetUsers() {
        return targetUsers;
    }

    public void setTargetUsers(String targetUsers) {
        this.targetUsers = targetUsers;
    }

    public Integer getNumberOfSeat() {
        return numberOfSeat;
    }

    public void setNumberOfSeat(Integer numberOfSeat) {
        this.numberOfSeat = numberOfSeat;
    }

    public Instant getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Instant expireTime) {
        this.expireTime = expireTime;
    }

    public InventoryTripType getInventoryTripType() {
        return inventoryTripType;
    }

    public void setInventoryTripType(InventoryTripType inventoryTripType) {
        this.inventoryTripType = inventoryTripType;
    }

    public InventoryStatus getStatus() {
        return status;
    }

    public void setStatus(InventoryStatus status) {
        this.status = status;
    }

    public int getTotalOfTrip() {
        return totalOfTrip;
    }

    public void setTotalOfTrip(int totalOfTrip) {
        this.totalOfTrip = totalOfTrip;
    }

    public Set<Order> getOrders() {
        return orders;
    }

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Set<AccountInventoryOrder> getAccountInventoryOrders() {
        return accountInventoryOrders;
    }

    public void setAccountInventoryOrders(Set<AccountInventoryOrder> accountInventoryOrders) {
        this.accountInventoryOrders = accountInventoryOrders;
    }
}
