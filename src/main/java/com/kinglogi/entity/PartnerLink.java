package com.kinglogi.entity;

import com.kinglogi.constant.PartnerLinkPosition;
import com.kinglogi.entity.base.AuditEntity;

import javax.persistence.*;

@Entity
@Table(name = "partner-link")
public class PartnerLink extends AuditEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Lob
    private String logoUrl;

    private String name;

    @Column(name = "position")
    private PartnerLinkPosition position;

    @Lob
    private String url;

    @Lob
    @Column(name = "ad_image_url")
    private String adImageUrl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PartnerLinkPosition getPosition() {
        return position;
    }

    public void setPosition(PartnerLinkPosition position) {
        this.position = position;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAdImageUrl() {
        return adImageUrl;
    }

    public void setAdImageUrl(String adImageUrl) {
        this.adImageUrl = adImageUrl;
    }
}
