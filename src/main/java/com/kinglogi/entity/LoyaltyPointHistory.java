package com.kinglogi.entity;

import com.kinglogi.constant.LoyaltyActionType;
import com.kinglogi.entity.base.AuditEntity;

import javax.persistence.*;

@Entity
@Table(name = "loyalty_point_history")
public class LoyaltyPointHistory extends AuditEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "action_type")
    private LoyaltyActionType actionType;

    private Integer amount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id")
    private Account account;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LoyaltyActionType getActionType() {
        return actionType;
    }

    public void setActionType(LoyaltyActionType actionType) {
        this.actionType = actionType;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
