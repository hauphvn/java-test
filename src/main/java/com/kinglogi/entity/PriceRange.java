package com.kinglogi.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kinglogi.entity.base.AuditEntity;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "price_range")
public class PriceRange extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Lob
    @Column(name = "go_area")
    private String goArea;

    @Lob
    @Column(name = "go_area_metadata")
    private String goAreaMetadata;

    @Lob
    @Column(name = "destination_area")
    private String destinationArea;

    @Lob
    @Column(name = "destination_area_metadata")
    private String destinationAreaMetadata;

    private Long price;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "deleted_at", length = 19)
    private Date deletedAt;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "priceRange")
    private Set<Trip> trips;

    @OneToMany(mappedBy = "priceRange", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<Order> orders;

}
