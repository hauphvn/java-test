package com.kinglogi.entity;

import com.kinglogi.constant.CommissionPriceType;
import com.kinglogi.entity.base.AuditEntity;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;
@Entity
@Table(name = "commission")
public class Commission extends AuditEntity {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    private String title;

    @Lob
    private String description;

    @Column(name = "price_type")
    private CommissionPriceType priceType;

    @Column(name = "price_value")
    private int priceValue;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CommissionPriceType getPriceType() {
        return priceType;
    }

    public void setPriceType(CommissionPriceType priceType) {
        this.priceType = priceType;
    }

    public int getPriceValue() {
        return priceValue;
    }

    public void setPriceValue(int priceValue) {
        this.priceValue = priceValue;
    }
}
