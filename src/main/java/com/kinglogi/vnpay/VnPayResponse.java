package com.kinglogi.vnpay;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.kinglogi.vnpay.provider.VnPayProvider;
import lombok.Data;

import java.util.Date;

@Data
@JsonPropertyOrder(alphabetic = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VnPayResponse {

    /**
     * Mã website của merchant trên hệ thống của VNPAY
     */
    @JsonProperty("vnp_TmnCode")
    private String tmnCode;

    /**
     * Mã tham chiếu của giao dịch tại hệ thống của merchant.
     * Mã này do merchant gửi sang khi yêu cầu thanh toán.
     * VNPAY gửi lại để merchant cập nhật
     */
    @JsonProperty("vnp_TxnRef")
    private String txnRef;

    /**
     * Số tiền thanh toán. Số tiền không mang các ký tự phân tách thập phân, phần nghìn, ký tự tiền tệ.
     * Để gửi số tiền thanh toán là 10,000 VND (mười nghìn VNĐ) thì merchant cần nhân thêm 100 lần (khử phần thập phân),
     * sau đó gửi sang VNPAY là: 1000000
     */
    @JsonProperty("vnp_Amount")
    private Long amount;

    /**
     * Thông tin mô tả nội dung thanh toán (Tiếng Việt, không dấu)
     */
    @JsonProperty("vnp_OrderInfo")
    private String orderInfo;

    /**
     * Mã phản hồi kết quả thanh toán.
     * Quy định mã trả lời 00 ứng với kết quả Thành công cho tất cả các API.
     *
     * Đây là kết quả phản hồi của hệ thống. Kết quả của giao dịch (thành công/ không thành công) xem thêm tại: vnp_TransactionStatus
     */
    @JsonProperty("vnp_ResponseCode")
    private String responseCode;

    /**
     * Mô tả thông tin tương ứng với vnp_ResponseCode
     */
    @JsonProperty("vnp_Message")
    private String message;

    /**
     * Mã Ngân hàng phát hành thẻ ( Ngân hàng thanh toán)
     */
    @JsonProperty("vnp_BankCode")
    private String bankCode;

    /**
     * Mã giao dịch tại Ngân hàng
     */
    @JsonProperty("vnp_BankTranNo")
    private String bankTranNo;

    @JsonProperty("vnp_CardType")
    private String cardType;

    /**
     * Thời gian khách hàng thanh toán, ghi nhận tại VNPAY tính theo GMT+7. Định dạng: yyyyMMddHHmmss
     */
    @JsonProperty("vnp_PayDate")
    @JsonFormat(pattern = VnPayProvider.DATE_FORMAT_PATTERN)
    private Date payDate;

    /**
     * Mã giao dịch ghi nhận tại hệ thống VNPAY
     */
    @JsonProperty("vnp_TransactionNo")
    private String transactionNo;

    /**
     * Loại giao dịch tại hệ thống VNPAY:
     * 01: Giao dịch thanh toán
     * 02: Giao dịch hoàn trả toàn phần
     * 03: Giao dịch hoàn trả một phần
     */
    @JsonProperty("vnp_TransactionType")
    private String transactionType;

    /**
     * Tình trạng của giao dịch tại Cổng thanh toán VNPAY.
     */
    @JsonProperty("vnp_TransactionStatus")
    private String transactionStatus;

    /**
     * Mã kiểm tra (checksum) để đảm bảo dữ liệu của giao dịch không bị thay đổi trong quá trình chuyển từ VNPAY về merchant.
     * Việc tạo ra mã này phụ thuộc vào cấu hình của merchant và phiên bản api sử dụng.
     * Phiên bản hiện tại hỗ trợ SHA256 và MD5. Quy tắc và Phương thức sử dụng giống với bước merchant gửi sang VNPAY
     */
    @JsonProperty("vnp_SecureHash")
    private String secureHash;

    /**
     * Type of hash used: MD5 or SHA256
     */
    @JsonProperty("vnp_SecureHashType")
    private String secureHashType;

}
