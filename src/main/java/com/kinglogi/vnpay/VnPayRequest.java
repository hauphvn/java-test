package com.kinglogi.vnpay;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonPropertyOrder(alphabetic = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VnPayRequest {

    /**
     * Phiên bản api mà merchant kết nối. Phiên bản hiện tại là 2.0.0
     */
    @NotBlank
    @JsonProperty("vnp_Version")
    private String version;

    /**
     * Mã API sử dụng, mã cho giao dịch thanh toán là pay
     */
    @NotBlank
    @JsonProperty("vnp_Command")
    private String command;

    /**
     * Mã website của merchant trên hệ thống của VNPAY
     */
    @NotBlank
    @JsonProperty("vnp_TmnCode")
    private String tmnCode;

    /**
     * Mã Ngân hàng thanh toán
     */
    @JsonProperty("vnp_BankCode")
    private String bankCode;

    /**
     * Ngôn ngữ khách hàng đang sử dụng. Hiện tại hỗ trợ Tiếng Việt (vn), Tiếng Anh (en)
     */
    @NotBlank
    @JsonProperty("vnp_Locale")
    private String locale;

    /**
     * Đơn vị tiền tệ sử dụng thanh toán. Hiện tại chỉ hỗ trợ VND
     */
    @NotBlank
    @JsonProperty("vnp_CurrCode")
    private String currCode;

    /**
     * Mã tham chiếu của giao dịch tại hệ thống của merchant.
     * Mã này là duy nhất đùng để phân biệt các đơn hàng gửi sang VNPAY.
     * Không được trùng lặp trong ngày
     */
    @NotBlank
    @Size(min = 1, max = 100)
    @JsonProperty("vnp_TxnRef")
    private String txnRef;

    /**
     * Thông tin mô tả nội dung thanh toán (Tiếng Việt, không dấu)
     */
    @NotBlank
    @JsonProperty("vnp_OrderInfo")
    private String orderInfo;

    /**
     * Mã danh mục hàng hóa. Mỗi hàng hóa sẽ thuộc một nhóm danh mục do VNPAY quy định.
     * VNPAY sẽ cung cấp bảng danh mục này tại trang dành cho merchant của VNPAY
     */
    @NotBlank
    @JsonProperty("vnp_OrderType")
    private String orderType;

    /**
     * Số tiền thanh toán. Số tiền không mang các ký tự phân tách thập phân, phần nghìn, ký tự tiền tệ.
     * Để gửi số tiền thanh toán là 10,000 VND (mười nghìn VNĐ) thì merchant cần nhân thêm 100 lần (khử phần thập phân),
     * sau đó gửi sang VNPAY là: 1000000
     */
    @NotBlank
    @JsonProperty("vnp_Amount")
    private Long amount;

    /**
     * Địa chỉ trả về khi khách hàng thực hiện thanh toán xong
     */
    @NotBlank
    @JsonProperty("vnp_ReturnUrl")
    private String returnUrl;

    /**
     * Địa chỉ IP của khách hàng thực hiện giao dịch.
     */
    @NotBlank
    @JsonProperty("vnp_IpAddr")
    private String ipAddr;

    /**
     * Thời gian ghi nhận giao dịch tại website của merchant GMT+7, định dạng: yyyyMMddHHmmss
     */
    @NotBlank
    @JsonProperty("vnp_CreateDate")
    private String createDate;

    /**
     * Thời gian hết hạn thanh toán GMT+7, định dạng: yyyyMMddHHmmss
     */
    @NotBlank
    @JsonProperty("vnp_ExpireDate")
    private String expireDate;

    /**
     * Số điện thoại của khách hàng
     */
    @JsonProperty("vnp_Bill_Mobile")
    private String billMobile;

    /**
     * Địa chỉ email của khách hàng
     */
    @JsonProperty("vnp_Bill_Email")
    private String billEmail;

    /**
     * Họ của khách hàng (trong tên đầy đủ gồm có Họ + Đệm + Tên)
     */
    @JsonProperty("vnp_Bill_FirstName")
    private String billFirstName;

    /**
     * Đệm và Tên của khách hàng
     */
    @JsonProperty("vnp_Bill_LastName")
    private String billLastName;

    /**
     * Địa chỉ của khách hàng
     */
    @JsonProperty("vnp_Bill_Address")
    private String billAddress;

    /**
     * Tỉnh/Thành phố của khách hàng
     */
    @JsonProperty("vnp_Bill_City")
    private String billCity;

    /**
     * Mã Quốc gia 02 ký tự của khách hàng, theo bảng mã:
     * https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2
     */
    @JsonProperty("vnp_Bill_Country")
    private String billCountry;

    /**
     * Mã Bang, áp dụng cho các quốc gia như: Mỹ, Canada,...
     * Mã bang tham khảo tại link:
     * Mỹ (US): https://pe.usps.com/text/pub28/28apb.htm Canada(CA): https://pe.usps.com/text/pub28/28apa_005.htm
     */
    @JsonProperty("vnp_Bill_State")
    private String billState;

    /**
     * Số điện thoại của cá nhân/tổ chức in trên hóa đơn điện tử
     */
    @JsonProperty("vnp_Inv_Phone")
    private String invPhone;

    /**
     * Địa chỉ email nhận Hóa đơn điện tử
     */
    @JsonProperty("vnp_Inv_Email")
    private String invEmail;

    /**
     * Họ tên của khách hàng in trên Hóa đơn điện tử
     */
    @JsonProperty("vnp_Inv_Customer")
    private String invCustomer;

    /**
     * Địa chỉ ghi trên hóa đơn điện tử
     */
    @JsonProperty("vnp_Inv_Address")
    private String invAddress;

    /**
     * Tên Công ty/Tổ chức in trên hóa đơn điện tử
     */
    @JsonProperty("vnp_Inv_Company")
    private String invCompany;

    /**
     * Mã số thuế của Công ty/Tổ chức
     */
    @JsonProperty("vnp_Inv_Taxcode")
    private String invTaxcode;

    /**
     * Loại hóa đơn điện tử:
     * - I: Cá nhân
     * - O: Tổ chức
     */
    @JsonProperty("vnp_Inv_Type")
    private String invType;

    /**
     * Mã kiểm tra (checksum) để đảm bảo dữ liệu của giao dịch không bị thay đổi trong quá trình chuyển từ merchant sang VNPAY.
     * Việc tạo ra mã này phụ thuộc vào cấu hình của merchant và phiên bản api sử dụng. Phiên bản hiện tại hỗ trợ SHA256 và MD5
     */
    @NotBlank
    @JsonProperty("vnp_SecureHash")
    private String secureHash;

    /**
     * Type of hash used: MD5 or SHA256
     */
    @JsonProperty("vnp_SecureHashType")
    private String secureHashType;
}
