package com.kinglogi.vnpay;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "vnp")
@Data
public class VnPaySetting {

    private String tmnCode;
    private String hashSecret;
    private String url;
    private String apiUrl;
    private String version;
    private String name;
    private String returnUrl;
}
