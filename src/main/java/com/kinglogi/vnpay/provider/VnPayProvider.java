package com.kinglogi.vnpay.provider;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kinglogi.exception.KlgApplicationException;
import com.kinglogi.vnpay.VnPayRequest;
import com.kinglogi.vnpay.VnPaySetting;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.Map;

@Component
@Slf4j
public class VnPayProvider {

    public static final String DATE_FORMAT_PATTERN = "yyyyMMddHHmmss";
    private static final String[] IGNORED_HASH = { "vnp_SecureHashType", "vnp_SecureHash" };
    private final VnPaySetting vnPaySetting;

    @AllArgsConstructor
    enum SUPPORTED_HASH {
        MD5("MD5"), SHA_256("SHA-256");

        @Getter
        private String value;
    }

    @Autowired
    public VnPayProvider(VnPaySetting vnPaySetting) {
        this.vnPaySetting = vnPaySetting;
    }

    public String md5(String message) {
        String digest = "";
        try {
            MessageDigest md = MessageDigest.getInstance(SUPPORTED_HASH.MD5.getValue());
            byte[] hash = md.digest(message.getBytes("UTF-8"));
            // converting byte array to Hexadecimal String
            StringBuilder sb = new StringBuilder(2 * hash.length);
            for (byte b : hash) {
                sb.append(String.format("%02x", b & 0xff));
            }
            digest = sb.toString();
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException ex) {
            log.error("Can not md5 message [" + message + "]", ex);
        }
        return digest;
    }

    public String sha256(String message) {
        String digest = "";
        try {
            MessageDigest md = MessageDigest.getInstance(SUPPORTED_HASH.SHA_256.getValue());
            byte[] hash = md.digest(message.getBytes("UTF-8"));

            // converting byte array to Hexadecimal String
            StringBuilder sb = new StringBuilder(2 * hash.length);
            for (byte b : hash) {
                sb.append(String.format("%02x", b & 0xff));
            }
            digest = sb.toString();
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException ex) {
            log.error("Can not sha256 message [" + message + "]", ex);
        }
        return digest;
    }

    public String buildRequestUrl(VnPayRequest request) {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = mapper.convertValue(request, new TypeReference<LinkedHashMap<String, Object>>() { });
        return buildRequestUrl(map);
    }

    public String buildRequestUrl( Map<String, Object> map) {
        try {
            String secureHash = hashAllFields(map);
            String queryUrl = buildQueryString(map);
            log.error("queryUrl " + queryUrl);
            queryUrl += "&vnp_SecureHashType=SHA256&vnp_SecureHash=" + secureHash;
            return vnPaySetting.getUrl() + "?" + queryUrl;
        } catch (UnsupportedEncodingException e) {
            throw new KlgApplicationException("Can not encode data ", e);
        }
    }

    public String hashAllFields(Map<String, Object> fields) {
        StringBuilder sb = new StringBuilder();
        boolean isFirst = true;
        for (Map.Entry<String, Object> entry : fields.entrySet()) {
            String fieldValue = entry.getValue() == null ? "" :  entry.getValue().toString();
            if (fieldValue.length() == 0 || StringUtils.containsAny(entry.getKey(), IGNORED_HASH)) {
                continue;
            }
            if (!isFirst) {
                sb.append("&");
            }
            sb.append(entry.getKey());
            sb.append("=");
            sb.append(fieldValue);
            isFirst = false;
        }
        log.error("build " + sb.toString());
        return sha256(vnPaySetting.getHashSecret() + sb.toString());
    }

    public String buildQueryString(Map<String, Object> fields) throws UnsupportedEncodingException {
        StringBuilder sb = new StringBuilder();
        boolean isFirst = true;
        for (Map.Entry<String, Object> entry : fields.entrySet()) {
            String fieldValue = entry.getValue() == null ? "" :  entry.getValue().toString();
            if (fieldValue.length() == 0) {
                continue;
            }
            if (!isFirst) {
                sb.append("&");
            }
            sb.append(URLEncoder.encode(entry.getKey(), StandardCharsets.UTF_8.toString()));
            sb.append("=");
            sb.append(URLEncoder.encode(fieldValue, StandardCharsets.UTF_8.toString()));
            isFirst = false;
        }

        return sb.toString();
    }

    public String currentDate() {
        return Instant.now().atZone(ZoneId.of("Asia/Ho_Chi_Minh")).format(DateTimeFormatter.ofPattern(DATE_FORMAT_PATTERN));
    }
}
