package com.kinglogi.vnpay.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum VnPayLocale {

    VIETNAM("vn"), ENGLISH("en");

    private String value;
}
