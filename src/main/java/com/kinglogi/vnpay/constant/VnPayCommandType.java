package com.kinglogi.vnpay.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum  VnPayCommandType {

    /**
     * Yêu cầu thanh toán
     */
    PAYMENT("pay"),

    /**
     * Yêu cầu truy vấn giao dịch
     */
    QUERY_TRANSACTION("querydr"),

    /**
     * Yêu cầu hoàn trả giao dịch
     */
    REFUND("refund"),
    ;

    private String value;
}
