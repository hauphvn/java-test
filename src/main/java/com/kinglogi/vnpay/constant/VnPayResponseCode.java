package com.kinglogi.vnpay.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum VnPayResponseCode {

    SUCCESS("00", "Giao dịch thành công"),

    // Khởi tạo GD (vnp_Command=pay)

    ERROR_01("01", "Giao dịch đã tồn tại."),
    ERROR_02("02", "Merchant không hợp lệ (kiểm tra lại vnp_TmnCode)."),
    ERROR_03("03", "Dữ liệu gửi sang không đúng định dạng."),
    ERROR_04("04", "Khởi tạo GD không thành công do Website đang bị tạm khóa."),
    ERROR_08("08", "Giao dịch không thành công do: Hệ thống Ngân hàng đang bảo trì. " +
            "Xin quý khách tạm thời không thực hiện giao dịch bằng thẻ/tài khoản của Ngân hàng này."),

    // Cập nhật kết quả giao dịch qua IPN URL

    ERROR_05("05", "Giao dịch không thành công do: Quý khách nhập sai mật khẩu thanh toán quá số lần quy định. " +
            "Xin quý khách vui lòng thực hiện lại giao dịch."),
    ERROR_06("06", "Giao dịch không thành công do Quý khách nhập sai mật khẩu xác thực giao dịch (OTP). " +
            "Xin quý khách vui lòng thực hiện lại giao dịch."),
    ERROR_07("07", "Trừ tiền thành công. Giao dịch bị nghi ngờ (liên quan tới lừa đảo, giao dịch bất thường). " +
            "Đối với giao dịch này cần merchant xác nhận thông qua merchant admin: Từ chối/Đồng ý giao dịch."),
    ERROR_12("12", "Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng bị khóa."),
    ERROR_09("09", "Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng chưa đăng ký dịch vụ Internet Banking tại ngân hàng."),
    ERROR_10("10", "Giao dịch không thành công do: Khách hàng xác thực thông tin thẻ/tài khoản không đúng quá 3 lần."),
    ERROR_11("11", "Giao dịch không thành công do: Đã hết hạn chờ thanh toán. Xin quý khách vui lòng thực hiện lại giao dịch."),
    ERROR_24("24", "Giao dịch không thành công do: Khách hàng hủy giao dịch."),
    ERROR_51("51", "Giao dịch không thành công do: Tài khoản của quý khách không đủ số dư để thực hiện giao dịch."),
    ERROR_65("65", "Giao dịch không thành công do: Tài khoản của Quý khách đã vượt quá hạn mức giao dịch trong ngày."),
    ERROR_75("75", "Ngân hàng thanh toán đang bảo trì."),
    ERROR_99("99", "Các lỗi khác (lỗi còn lại, không có trong danh sách mã lỗi đã liệt kê)."),

    // Tra cứu giao dịch (vnp_Command=refund) / (vnp_Command=querydr)

    ERROR_97("97", "Sai chữ ký (checksum không khớp)"),
    ERROR_91("91", "Không tìm thấy giao dịch yêu cầu hoàn trả"),
    ERROR_93("93", "Số tiền hoàn trả không hợp lệ. Số tiền hoàn trả phải nhỏ hơn hoặc bằng số tiền thanh toán."),
    ERROR_94("94", "Giao dịch đã được gửi yêu cầu hoàn tiền trước đó. Yêu cầu này VNPAY đang xử lý."),
    ERROR_95("95", "Giao dịch này không thành công bên VNPAY. VNPAY từ chối xử lý yêu cầu."),
    ;

    private String code;

    private String message;
}
