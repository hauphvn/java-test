package com.kinglogi.vnpay.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum VnPayTransactionStatus {

    STATUS_00("00", "Giao dịch thành công."),
    STATUS_01("01", "Giao dịch chưa hoàn tất."),
    STATUS_02("02", "Giao dịch bị lỗi."),
    STATUS_04("04", "Giao dịch đảo (Khách hàng đã bị trừ tiền tại Ngân hàng nhưng GD chưa thành công ở VNPAY)."),
    STATUS_05("05", "VNPAY đang xử lý giao dịch này (GD hoàn tiền)."),
    STATUS_06("06", "VNPAY đã gửi yêu cầu hoàn tiền sang Ngân hàng (GD hoàn tiền)."),
    STATUS_07("07", "Giao dịch bị nghi ngờ gian lận."),
    STATUS_09("09", "GD Hoàn trả bị từ chối."),
    STATUS_10("10", "Đã giao hàng."),
    STATUS_20("20", "Giao dịch đã được thanh quyết toán cho merchant."),
    ;

    private String code;

    private String message;
}
