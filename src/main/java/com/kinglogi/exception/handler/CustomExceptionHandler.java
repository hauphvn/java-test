package com.kinglogi.exception.handler;

import com.kinglogi.dto.response.ApiErrorResponse;
import com.kinglogi.exception.KlgApplicationException;
import com.kinglogi.exception.KlgResourceNotFoundException;
import com.kinglogi.exception.TransactionException;
import com.kinglogi.exception.ValidatingException;
import com.kinglogi.service.error.AccessForbiddenException;
import com.kinglogi.service.error.AuthenticationException;
import com.kinglogi.service.error.BadRequestException;
import com.kinglogi.service.error.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
@Slf4j
public class CustomExceptionHandler {

    // Resource Not Found Exception
    @ExceptionHandler({ KlgResourceNotFoundException.class, ValidatingException.class})
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public ResponseEntity<ApiErrorResponse> handleResourceNotFoundException(KlgResourceNotFoundException ex, HttpServletRequest request) {
        log.error("KlgResourceNotFoundException: ", ex);
        //
        ApiErrorResponse response = new ApiErrorResponse();
        response.setCode(HttpStatus.BAD_REQUEST.value());
        response.setMessage(ex.getMessage());
        response.setTrace(buildTrace(ex));
        response.setPath(request.getRequestURL().toString());

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    // Application exception
    @ExceptionHandler(KlgApplicationException.class)
    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<ApiErrorResponse> handleBadCredentialsException(KlgApplicationException ex, HttpServletRequest request) {
        log.error("KlgApplicationException: ", ex);
        //
        ApiErrorResponse response = new ApiErrorResponse();
        response.setCode(ex.getErrorStatus().value());
        response.setMessage(ex.getMessage());
        response.setTrace(buildTrace(ex));
        response.setPath(request.getRequestURL().toString());

        return new ResponseEntity<>(response, ex.getErrorStatus());
    }

    // Transaction exception
    @ExceptionHandler(TransactionException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public ResponseEntity<ApiErrorResponse> handleTransactionException(KlgApplicationException ex, HttpServletRequest request) {
        log.error("TransactionException: ", ex);
        //
        ApiErrorResponse response = new ApiErrorResponse();
        response.setCode(HttpStatus.BAD_REQUEST.value());
        response.setMessage(ex.getMessage());
        response.setTrace(buildTrace(ex));
        response.setPath(request.getRequestURL().toString());

        return new ResponseEntity<>(response, ex.getErrorStatus());
    }

    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public ResponseEntity<ApiErrorResponse> handleTransactionException(BadRequestException ex, HttpServletRequest request) {
        log.error("BadRequestException: ", ex);
        //
        ApiErrorResponse response = new ApiErrorResponse();
        response.setCode(HttpStatus.BAD_REQUEST.value());
        response.setMessage(ex.getMessage());
        response.setTrace(buildTrace(ex));
        response.setPath(request.getRequestURL().toString());

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    public ResponseEntity<ApiErrorResponse> handleTransactionException(ResourceNotFoundException ex, HttpServletRequest request) {
        log.error("ResourceNotFoundException: ", ex);
        //
        ApiErrorResponse response = new ApiErrorResponse();
        response.setCode(HttpStatus.NOT_FOUND.value());
        response.setMessage(ex.getMessage());
        response.setTrace(buildTrace(ex));
        response.setPath(request.getRequestURL().toString());

        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(AccessForbiddenException.class)
    @ResponseStatus(code = HttpStatus.FORBIDDEN)
    public ResponseEntity<ApiErrorResponse> handleTransactionException(AccessForbiddenException ex, HttpServletRequest request) {
        log.error("AccessForbiddenException: ", ex);
        //
        ApiErrorResponse response = new ApiErrorResponse();
        response.setCode(HttpStatus.FORBIDDEN.value());
        response.setMessage(ex.getMessage());
        response.setTrace(buildTrace(ex));
        response.setPath(request.getRequestURL().toString());

        return new ResponseEntity<>(response, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(AuthenticationException.class)
    @ResponseStatus(code = HttpStatus.UNAUTHORIZED)
    public ResponseEntity<ApiErrorResponse> handleTransactionException(AuthenticationException ex, HttpServletRequest request) {
        log.error("UNAUTHORIZATION: ", ex);
        //
        ApiErrorResponse response = new ApiErrorResponse();
        response.setCode(HttpStatus.UNAUTHORIZED.value());
        response.setMessage(ex.getMessage());
        response.setTrace(buildTrace(ex));
        response.setPath(request.getRequestURL().toString());

        return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
    }

    private String buildTrace(Throwable e) {
        if (e == null) {
            return "";
        }

        final int max = 20;
        int currentCause = 0;
        StringBuilder sb = new StringBuilder();
        sb.append(e.getMessage());

        sb.append("\nCause: \n");
        while (e.getCause() != null && currentCause < max) {
            sb.append("\n ").append(e.getCause());
            e = e.getCause();
            currentCause++;
        }

        return sb.toString();
    }
}
