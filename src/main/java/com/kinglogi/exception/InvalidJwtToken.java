package com.kinglogi.exception;

public class InvalidJwtToken extends KlgAuthenticationException {

    public InvalidJwtToken(String errorMessage) {
        super(errorMessage);
    }

    public InvalidJwtToken(String errorMessage, Throwable cause) {
        super(errorMessage, cause);
    }
}
