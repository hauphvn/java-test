package com.kinglogi.exception;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

@Getter
@ToString
@EqualsAndHashCode(callSuper = false)
public class KlgAuthenticationException extends RuntimeException {

    private final String errorMessage;

    private final HttpStatus errorStatus;

    public KlgAuthenticationException(String errorMessage) {
        super(errorMessage);
        this.errorMessage = errorMessage;
        this.errorStatus = HttpStatus.UNAUTHORIZED;
    }

    public KlgAuthenticationException(String errorMessage, Throwable cause) {
        super(errorMessage, cause);
        this.errorMessage = errorMessage;
        this.errorStatus = HttpStatus.UNAUTHORIZED;
    }
}