package com.kinglogi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class KlgResourceNotFoundException extends RuntimeException {

    public KlgResourceNotFoundException(String message) {
        super(message);
    }

    public KlgResourceNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
