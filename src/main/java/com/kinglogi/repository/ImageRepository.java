package com.kinglogi.repository;
// Generated May 31, 2020, 11:28:53 PM by Hibernate Tools 5.2.12.Final

import com.kinglogi.entity.Image;

import com.kinglogi.repository.base.BaseRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Repository for domain model class Image.
 * @see Image;
 * @author GMT
 */
@Repository
public interface ImageRepository extends JpaRepository<Image, Long> {
    @Query("select distinct i from Image i join i.cars c join c.images image where c.id = ?1 order by  i.createdAt desc")
    Page<Image> findAllByCarId(Long id, Pageable pageable);

    @Query("select i from Image i join i.cars c join c.images image where c.id = ?1 and i.displayName = ?2")
    Optional<Image> findByCarIdAndName(Long id, String fileName);
}
