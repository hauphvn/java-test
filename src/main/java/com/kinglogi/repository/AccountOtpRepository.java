package com.kinglogi.repository;

import com.kinglogi.entity.Account;
import com.kinglogi.entity.AccountOtp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.Optional;

@Repository
public interface AccountOtpRepository extends JpaRepository<AccountOtp, Long> {

    Optional<AccountOtp> findByPhoneNumberAndExpiredAtAfter(String number, Instant instant);

    @Query(value = "select o from AccountOtp o where o.otp = ?1 and o.phoneNumber = ?2 and o.expiredAt >= ?3")
    Optional<AccountOtp> findByOtpAndPhoneNumber(String otp, String phoneNumber, Instant expireTime);

    @Modifying
    void deleteAllByPhoneNumber(String phoneNumber);

    Optional<AccountOtp> findByOtpAndPhoneNumber(String otp, String phoneNumber);

}
