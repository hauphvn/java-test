package com.kinglogi.repository;

import com.kinglogi.entity.Account;
import com.kinglogi.entity.Promotion;
import com.kinglogi.entity.UserPromotion;
import com.kinglogi.repository.base.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserPromotionRepository extends BaseRepository<UserPromotion, Long> {

    Optional<UserPromotion> findByPromotionAndAccount(Promotion promotion, Account account);

    List<UserPromotion> findAllByPromotionAndAccountNotIn(Promotion promotion, List<Account> accounts);

    List<UserPromotion> findAllByPromotion(Promotion promotion);
}
