package com.kinglogi.repository;
// Generated May 31, 2020, 11:28:53 PM by Hibernate Tools 5.2.12.Final

import com.kinglogi.constant.BidStatus;
import com.kinglogi.entity.Account;
import com.kinglogi.entity.Auction;
import com.kinglogi.entity.Bid;

import com.kinglogi.repository.base.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Repository for domain model class Bid.
 * @see Bid;
 * @author GMT
 */
@Repository
public interface BidRepository extends BaseRepository<Bid, Long> {
    Optional<Bid> findByAccountAndAuctionId(Account account, Long auctionId);

    List<Bid> findAllByAccountIdNotInAndStatusAndAuctionId(List<Long> winnningBidderId, BidStatus bidStatus, Long auctionId);

    List<Bid> findTopByAuctionIdAndStatusOrderByBidPriceAscCreatedAtAsc(Long auctionId, BidStatus status);

    List<Bid> findAllByAuctionId(Long auctionId);

    List<Bid> findAllByAccountIdAndStatus(Long accountId, BidStatus bidStatus);

    List<Bid> findAllByAuctionAndAccount(Auction auction, Account account);
}
