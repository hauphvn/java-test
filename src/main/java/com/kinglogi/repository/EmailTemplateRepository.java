package com.kinglogi.repository;
// Generated May 31, 2020, 11:28:53 PM by Hibernate Tools 5.2.12.Final

import com.kinglogi.entity.EmailTemplate;
import com.kinglogi.repository.base.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Repository for domain model class EmailTemplate.
 * @see EmailTemplate;
 * @author GMT
 */
@Repository
public interface EmailTemplateRepository extends BaseRepository<EmailTemplate, Long> {

    Optional<EmailTemplate> findByTypeAndLanguage(String type, String language);
}
