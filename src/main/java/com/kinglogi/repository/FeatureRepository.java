package com.kinglogi.repository;
// Generated May 31, 2020, 11:28:53 PM by Hibernate Tools 5.2.12.Final

import com.kinglogi.entity.Feature;
import com.kinglogi.repository.base.BaseRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for domain model class Feature.
 * @see Feature;
 * @author GMT
 */
@Repository
public interface FeatureRepository extends BaseRepository<Feature, Long> {

}
