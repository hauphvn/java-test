package com.kinglogi.repository;
// Generated May 31, 2020, 11:28:53 PM by Hibernate Tools 5.2.12.Final

import com.kinglogi.constant.AuctionType;
import com.kinglogi.constant.BidStatus;
import com.kinglogi.dto.response.AuctionBidResponse;
import com.kinglogi.entity.Auction;
import com.kinglogi.repository.base.BaseRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.Date;
import java.util.List;

/**
 * Repository for domain model class Auction.
 * @see Auction;
 * @author GMT
 */
@Repository
public interface AuctionRepository extends BaseRepository<Auction, Long> {

    @Query("SELECT new com.kinglogi.dto.response.AuctionBidResponse(" +
            "   a.id, a.name, a.startAt, a.endAt, a.totalPrice, a.description, a.status, a.updatedAt, " +
            "   b.status, b.createdAt, b.bidPrice, a.type) " +
            "FROM Auction a INNER JOIN Bid b ON a.id = b.auction.id " +
            "WHERE (:userId is null OR b.account.id = :userId) " +
            "   AND (:auctionId is null OR a.id = :auctionId)" +
            "   AND b.status = :status " +
            "ORDER BY a.updatedAt DESC")
    List<AuctionBidResponse> getAuctionBids(
            @Param("userId") Long userId,
            @Param("auctionId") Long auctionId,
            @Param("status") BidStatus status,
            Pageable pageable
            );

    @Query("SELECT new com.kinglogi.dto.response.AuctionBidResponse(" +
            "   a.id, a.name, a.startAt, a.endAt, a.totalPrice, a.description, a.status, a.updatedAt, " +
            "   b.status, b.createdAt, b.bidPrice, a.type) " +
            "FROM Auction a INNER JOIN Bid b ON a.id = b.auction.id " +
            "WHERE (:userId is null OR b.account.id = :userId) " +
            "   AND (:auctionId is null OR a.id = :auctionId)" +
            "   AND b.status <> com.kinglogi.constant.BidStatus.PENDING " +
            "ORDER BY a.updatedAt DESC")
    List<AuctionBidResponse> getAllAuctionBids(
            @Param("userId") Long userId,
            @Param("auctionId") Long auctionId,
            Pageable pageable
    );

    List<Auction> findAllByEndAtBeforeAndStatusIsAndType(Date now, Integer status, AuctionType type);
}
