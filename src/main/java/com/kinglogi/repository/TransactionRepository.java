package com.kinglogi.repository;
// Generated May 31, 2020, 11:28:53 PM by Hibernate Tools 5.2.12.Final

import com.kinglogi.entity.Transaction;
import com.kinglogi.repository.base.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for domain model class Transaction.
 * @see Transaction;
 * @author GMT
 */
@Repository
public interface TransactionRepository extends BaseRepository<Transaction, Long> {

    List<Transaction> findByAccountIdOrderByTransactionAtDesc(Long accountId);

    @Query("select e from Transaction e LEFT JOIN e.orders o where o.id = :orderId")
    List<Transaction> findByOrderIdOrderByTransactionAtDesc(@Param("orderId") Long orderId);
}
