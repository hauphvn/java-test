package com.kinglogi.repository;

import com.kinglogi.entity.FileAttachment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FileAttachmentRepository extends JpaRepository<FileAttachment, Long> {
    List<FileAttachment> findAllByComplaintId(Long complaintId);

    Optional<FileAttachment> findByComplaintIdAndFileName(Long id, String fileName);

    List<FileAttachment> findAllByComplaintIdOrderByCreatedAtDesc(Long complaintId);
}
