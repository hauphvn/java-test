package com.kinglogi.repository;

import com.kinglogi.entity.Promotion;
import com.kinglogi.repository.base.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface PromotionRepository extends BaseRepository<Promotion, Long> {

    @Query(value = "select p from Promotion p where p.id = ?1 and p.deletedAt is null ")
    Optional<Promotion> findByIdAndDeletedAtIsNull(Long id);

    Optional<Promotion> findByIdAndDeletedAtIsNullAndStatus(Long id, String status);

    Optional<Promotion> findByCodeAndDeletedAtIsNullAndStatus(String code, String status);

    @Query(value = "select p from Promotion p where p.status = ?1 and p.expireTime <= ?2 and p.deletedAt is null ")
    List<Promotion> findAllByStatusAndExpireTimeBefore(String status, Instant expireTime);

    Optional<Promotion> findByCodeAndDeletedAtIsNull(String code);

}
