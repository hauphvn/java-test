package com.kinglogi.repository;

import com.kinglogi.entity.CarGroup;
import com.kinglogi.entity.File;
import com.kinglogi.repository.base.BaseRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CarGroupRepository extends BaseRepository<CarGroup, Long> {
    Optional<CarGroup> findByNumberOfSeat(int numberOfSeat);

    @Query("select c from CarGroup c where c.numberOfSeat = ?1 and upper(c.title) like upper(concat('%', ?2, '%')) ")
    Page<CarGroup> findAllByFilter(Pageable pageable, int numberOfSeat, String keyword);

}
