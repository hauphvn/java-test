package com.kinglogi.repository;

import com.kinglogi.entity.CollaboratorGroup;
import com.kinglogi.repository.base.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CollaboratorGroupRepository extends BaseRepository<CollaboratorGroup, Long> {
    @Query("select count(p) = 1 from CollaboratorGroup p where p.name = ?1")
    boolean findExistByName(String name);
}
