package com.kinglogi.repository;

import com.kinglogi.constant.TypeSms;
import com.kinglogi.entity.SmsTemplate;
import com.kinglogi.repository.base.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SmsTemplateRepository extends BaseRepository<SmsTemplate, Long> {

    SmsTemplate findByType(TypeSms typeSms);

}
