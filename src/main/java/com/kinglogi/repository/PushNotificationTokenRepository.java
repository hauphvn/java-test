package com.kinglogi.repository;

// Generated May 31, 2020, 11:28:53 PM by Hibernate Tools 5.2.12.Final

import com.kinglogi.dto.UserToken;
import com.kinglogi.entity.Account;
import com.kinglogi.entity.PushNotificationToken;
import com.kinglogi.repository.base.BaseRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Repository for domain model class Role.
 * @see PushNotificationToken;
 * @author GMT
 */
@Repository
public interface PushNotificationTokenRepository extends BaseRepository<PushNotificationToken, Long> {

    Optional<PushNotificationToken> findByToken(String token);

    List<PushNotificationToken> getAllByAccount(Account account);

    Optional<PushNotificationToken> findByDeviceIdAndAppNameAndAccount(String deviceId, String appName, Account account);

    @Query("SELECT new com.kinglogi.dto.UserToken(t.account.id, t.token) " +
            "FROM PushNotificationToken t INNER JOIN t.account a ON a.id = t.account.id " +
            "WHERE t.account.id = :accountId AND (a.allowNotification is null OR a.allowNotification = true)")
    List<UserToken> getTokensByAccountId(@Param("accountId") Long accountId);

    @Query("SELECT new com.kinglogi.dto.UserToken(t.account.id, t.token) " +
            "FROM PushNotificationToken t INNER JOIN t.account a ON a.id = t.account.id INNER JOIN a.roles r " +
            "WHERE r.code IN (:roleCodes) AND (a.allowNotification is null OR a.allowNotification = true)")
    List<UserToken> getTokensByRoleCodes(@Param("roleCodes") List<String> roles);

    @Query("select new com.kinglogi.dto.UserToken(t.account.id, t.token) " +
            "from PushNotificationToken t INNER JOIN t.account a ON a.id = t.account.id " +
            "where a.id in (:accountIds) AND (a.allowNotification is null OR a.allowNotification = true)")
    List<UserToken> getTokensByAccountIds(@Param("accountIds") List<Long> accountIds);

    @Query("select p from PushNotificationToken p INNER JOIN p.account a ON a.id = p.account.id" +
            " where a.id = ?1 AND (a.allowNotification is null OR a.allowNotification = true) AND p.deviceId = ?2 AND p.appName = ?3")
    List<PushNotificationToken> getTokenByAccountId(Long accountId, String deviceId, String appName);

    @Modifying
    @Query("delete from PushNotificationToken where account = ?1")
    void deleteByAccountId(Account account);
}
