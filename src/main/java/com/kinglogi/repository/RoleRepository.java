package com.kinglogi.repository;
// Generated May 31, 2020, 11:28:53 PM by Hibernate Tools 5.2.12.Final

import org.springframework.stereotype.Repository;

import com.kinglogi.entity.Role;
import com.kinglogi.repository.base.BaseRepository;

import java.util.List;

/**
 * Repository for domain model class Role.
 * @see Role;
 * @author GMT
 */
@Repository
public interface RoleRepository extends BaseRepository<Role, Long> {
    List<Role> findAllByIdIn(List<Long> roleId);

}
