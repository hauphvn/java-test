package com.kinglogi.repository;

import com.kinglogi.entity.LoyaltyPointHistory;
import com.kinglogi.repository.base.BaseRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface LoyaltyPointHistoryRepository extends BaseRepository<LoyaltyPointHistory, Long> {
    Page<LoyaltyPointHistory> findAllByAccountId(Pageable pageable, Long accountId);
}
