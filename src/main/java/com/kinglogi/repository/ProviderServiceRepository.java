package com.kinglogi.repository;

import com.kinglogi.entity.ProviderService;
import com.kinglogi.repository.base.BaseRepository;
import com.kinglogi.service.PromotionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProviderServiceRepository extends BaseRepository<ProviderService, Long> {
}
