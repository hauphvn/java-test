package com.kinglogi.repository;
// Generated May 31, 2020, 11:28:53 PM by Hibernate Tools 5.2.12.Final

import com.kinglogi.constant.OrderStatus;
import com.kinglogi.entity.Auction;
import com.kinglogi.entity.Order;
import com.kinglogi.repository.base.BaseRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Repository for domain model class Order.
 * @see Order;
 * @author GMT
 */
@Repository
public interface OrderRepository extends BaseRepository<Order, Long> {

    Optional<Order> getOrderByOrderCode(String orderCode);

    List<Order> getAllByAuctions(Auction auction);

    @Query("SELECT o FROM Order o WHERE o.id IN (:ids)")
    List<Order> getByIds(@Param("ids") List<Long> ids);

    Set<Order> findAllByIdIn(List<Long> ids);

    @Query("SELECT o FROM Order o WHERE o.id IN (:ids) and o.status = 20 and o.auctions.size = 0")
    Set<Order> getPaidOrderByIds(@Param("ids") List<Long> ids);

    @Query("select a from Auction a join a.orders o where o.id = ?1")
    Set<Auction> getAuctionByOrderId(Long orderId);

    @Query("select o.account.agentId from Order o join o.account where o.collaboratorId = ?1 ")
    Optional<Long> getAgentId(Long orderId);

    @Query("select o from Order o where o.id = ?1 and o.deletedAt is null")
    Optional<Order> findByIdAndDeletedAtIsNull(Long id);

    @Query("select o from Order o where o.status in ?1 and o.pickingUpDatetime < ?2 and o.deletedAt is null")
    List<Order> findAllByStatusInAndPickingUpDatetimeAndDeletedAtNull(List<Integer> statuses, Date now);

    @Query("select o from Order o where o.account.id = ?1 and o.deletedAt is null")
    Page<Order> findAllByAccountId(Long id, Pageable pageable);

    @Query("select o from Order o where (o.account.id = ?1 or o.createdBy = ?1) and o.deletedAt is null order by o.createdAt desc")
    List<Order> findAllByPartner(Long accountId);
}
