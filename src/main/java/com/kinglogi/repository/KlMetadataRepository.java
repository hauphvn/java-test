package com.kinglogi.repository;
// Generated May 31, 2020, 11:28:53 PM by Hibernate Tools 5.2.12.Final

import com.kinglogi.entity.KlMetadata;
import com.kinglogi.entity.Role;
import com.kinglogi.repository.base.BaseRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for domain model class KlMetadata.
 * @see KlMetadata;
 * @author GMT
 */
@Repository
public interface KlMetadataRepository extends BaseRepository<KlMetadata, Long> {

}
