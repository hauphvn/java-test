package com.kinglogi.repository;

import com.kinglogi.entity.OrderStopPoint;
import com.kinglogi.repository.base.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderStopPointRepository extends BaseRepository<OrderStopPoint, Long> {
}
