package com.kinglogi.repository.impl;

import com.kinglogi.constant.PromotionStatus;
import com.kinglogi.dto.filter.PromotionFillter;
import com.kinglogi.entity.Account;
import com.kinglogi.entity.Promotion;
import com.kinglogi.query.HibernateQueryResult;
import com.kinglogi.repository.base.impl.BaseRepositoryImpl;
import com.kinglogi.repository.custom.CustomPromotionRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Repository
public class PromotionRepositoryImpl extends BaseRepositoryImpl<Promotion, Long> implements CustomPromotionRepository {

    public PromotionRepositoryImpl(EntityManager em) {
        super(Promotion.class, em);
    }

    @Override
    public Page<Promotion> findAllByUser(Account account, PromotionFillter promotionFillter, Pageable pageable) {
        StringBuilder queryBuilder = new StringBuilder("Select promotion" +
                " from " + Promotion.class.getName() + " promotion" +
                " join promotion.userPromotions as userPromotion " +
                " join userPromotion.account as account " +
                " where account.id = :accountId " +
                " and userPromotion.usageRemain != 0 " +
                " and promotion.deletedAt is null " +
                " and promotion.status = :status" +
                " and ( promotion.name like :keyword" +
                " or promotion.code like :keyword" +
                " or promotion.description like :keyword)"  );
        Map<String, Object> parameterMap = new HashMap<>();
        parameterMap.put("accountId", account.getId());
        parameterMap.put("status", PromotionStatus.ACTIVE.getStatus());
        parameterMap.put("keyword", "%" + promotionFillter.getKeyword() + "%");
        HibernateQueryResult queryResult = new HibernateQueryResult(getEntityManager(), queryBuilder, pageable);
        return queryResult.getResultList(parameterMap, Promotion.class);
    }
}
