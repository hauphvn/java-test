package com.kinglogi.repository.impl;

import com.kinglogi.entity.Auction;
import com.kinglogi.repository.AuctionBidRepository;
import com.kinglogi.repository.base.impl.BaseRepositoryImpl;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
public class AuctionBidRepositoryImpl extends BaseRepositoryImpl<Auction, Long> implements AuctionBidRepository {

    public AuctionBidRepositoryImpl(EntityManager em) {
        super(Auction.class, em);
    }
}
