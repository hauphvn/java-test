package com.kinglogi.repository;

import com.kinglogi.entity.Complaint;
import com.kinglogi.repository.base.BaseRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ComplaintRepository extends BaseRepository<Complaint, Long> {
    @Query("select c from Complaint c where c.user.id = ?1 and (upper(c.title) like upper(concat('%', ?2, '%')) " +
            "or upper(c.description) like upper(concat('%', ?2, '%')))")
    Page<Complaint> findAllByUserId(Pageable pageable, Long userId, String keyword);

    @Query("select c from Complaint c where c.user.id = ?1 and (upper(c.title) like upper(concat('%', ?2, '%')) " +
            "or upper(c.description) like upper(concat('%', ?2, '%'))) " +
            "and  c.type = ?3")
    Page<Complaint> findAllByFilter(Pageable pageable, Long userId, String keyword, String type);
}
