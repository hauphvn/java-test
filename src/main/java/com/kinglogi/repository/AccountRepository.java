package com.kinglogi.repository;
// Generated May 31, 2020, 11:28:53 PM by Hibernate Tools 5.2.12.Final

import com.kinglogi.constant.Role;
import com.kinglogi.dto.filter.AccountFilter;
import com.kinglogi.entity.Account;
import com.kinglogi.repository.base.BaseRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Repository for domain model class Account.
 * @see Account;
 * @author GMT
 */
public interface AccountRepository extends BaseRepository<Account, Long> {

    @Query( "select o from Account o where o.id in :ids" )
    List<Account> findAllByIdIn(List<Long> ids);

    @Query( "select o from Account o where o.collaboratorGroup.id = :id" )
    List<Account> findAllByCollaboratorGroupId(Long id);

    @Query( "select o from Account o where o.collaboratorGroup.id = :id and o.email like upper(concat('%', :keyword , '%')) and o.deletedAt is null" )
    Page<Account> findAllByCollaboratorGroupId(Long id, Pageable pageable, String keyword);

    @Query( "select o from Account o join o.roles r where o.deletedAt is null and o.collaboratorGroup is null and r.id = 4 and ( o.username like upper(concat('%', :keyword , '%')) or o.fullName like upper(concat('%', :keyword , '%')) or o.phoneNumber like upper(concat('%', :keyword , '%')))" )
    Page<Account> findAllByCollaboratorGroupNull(Pageable pageable, String keyword);

    @Query("select o from Account o JOIN o.roles r where o.deletedAt is null and o.commission is null and r.id = 4 and ( o.username like upper(concat('%', :keyword , '%')) or o.fullName like upper(concat('%', :keyword , '%')) or o.phoneNumber like upper(concat('%', :keyword , '%')))")
    Page<Account> findAllAccountCommissionNull(Pageable pageable, String keyword);

    @Query("select o from Account o where o.deletedAt is null and o.commission.id =:commissionId and o.email like upper(concat('%', :keyword , '%'))")
    Page<Account> findAllAccountInCommission(Pageable pageable, Long commissionId, String keyword);

    Optional<Account> findByUsernameOrPhoneNumber(String username);

    @Query( "select a.id from Account a join a.roles r where a.agentId = ?1 and r.id = 8")
    Set<Long> getSubAgentAdmin(Long agentId);

    @Query( "select o from Account o where o.commission.id = ?1")
    List<Account> findAllByIdInCommission(Long commissionId);

    @Query("select a from Account a where a.id = ?1 and a.active = true and a.deletedAt is null")
    Optional<Account> findAllByIdAndActiveIsTrueAndDeletedAtNull(Long id);

    @Query("select a from Account a where a.username like upper(concat('%', :username , '%')) and a.deletedAt is null and a.active = true")
    Page<Account> findByUsernameContaining(String username, Pageable pageable);

    @Query("select a from Account a join a.roles r where r.code = ?1")
    List<Account> findByUserRole(String role);

    @Query("select a.id from Account a where a.agentId in ?1")
    List<Long> findAllByAgentIds(List<Long> agentId);

    @Query("select a from Account a where a.active = true and a.deletedAt is null")
    List<Account> findAllByActiveIsTrueAndDeletedAtIsNull();

    @Query("select a from Account a where a.phoneNumber = ?1 and a.deletedAt is null")
    Optional<Account> findAccountByPhoneNumber(String phoneNumber);

    Optional<Account> findAccountByEmail(String email);

    @Query("select a.id from Account a left join a.roles r where a.agentId = ?1 and r.code = ?2")
    List<Long> findAllSubAgentAdmin(Long agentId, String role);
}
