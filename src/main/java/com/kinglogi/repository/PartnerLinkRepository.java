package com.kinglogi.repository;

import com.kinglogi.constant.PartnerLinkPosition;
import com.kinglogi.entity.Commission;
import com.kinglogi.entity.PartnerLink;
import com.kinglogi.repository.base.BaseRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PartnerLinkRepository extends BaseRepository<PartnerLink, Long> {
    Optional<PartnerLink> findByPosition(PartnerLinkPosition position);

    @Query("select p from PartnerLink p where p.position = :left or p.position = :right")
    List<PartnerLink> findAllByPositionIn(@Param("left") PartnerLinkPosition left, @Param("right") PartnerLinkPosition right);
}
