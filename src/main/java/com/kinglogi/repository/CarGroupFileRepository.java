package com.kinglogi.repository;

import com.kinglogi.entity.CarGroupFile;
import com.kinglogi.entity.File;
import com.kinglogi.repository.base.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarGroupFileRepository extends BaseRepository<CarGroupFile, Long> {
    @Query("select cf.file from CarGroupFile cf where cf.carGroup.id = ?1 order by cf.id desc")
    List<File> findAllByCarGroupId(Long carGroupId);
}
