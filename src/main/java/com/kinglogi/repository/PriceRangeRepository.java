package com.kinglogi.repository;

import com.kinglogi.entity.PriceRange;
import com.kinglogi.repository.base.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PriceRangeRepository extends BaseRepository<PriceRange, Long> {

    // Tìm kiếm vùng giá còn hoạt động
    @Query("select p from PriceRange p where p.deletedAt is null and p.goAreaMetadata like upper(concat('%', :cityGo, '%')) and p.goAreaMetadata like upper(concat('%', :districtGo, '%')) and p.destinationAreaMetadata like upper(concat('%', :cityDestination, '%')) and p.destinationAreaMetadata like upper(concat('%', :districtDestination, '%'))")
    List<PriceRange> findAllByMatchingDestinationArea(String cityGo, String districtGo, String cityDestination, String districtDestination);

    Optional<PriceRange> findByIdAndDeletedAtIsNull(Long id);
}
