package com.kinglogi.repository;

import com.kinglogi.entity.AccountInventoryOrder;
import com.kinglogi.repository.base.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountInventoryOrderRepository extends BaseRepository<AccountInventoryOrder, Long> {
}
