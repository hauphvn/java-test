package com.kinglogi.repository;

import com.kinglogi.entity.Notification;
import com.kinglogi.entity.OrderHistory;
import com.kinglogi.repository.base.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderHistoryChangeRepository extends BaseRepository<OrderHistory, Long> {
}
