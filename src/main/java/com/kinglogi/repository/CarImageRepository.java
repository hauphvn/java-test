package com.kinglogi.repository;

import com.kinglogi.entity.CarImage;
import com.kinglogi.repository.base.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarImageRepository extends BaseRepository<CarImage, Long> {
}
