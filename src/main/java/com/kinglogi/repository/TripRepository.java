package com.kinglogi.repository;
// Generated May 31, 2020, 11:28:53 PM by Hibernate Tools 5.2.12.Final

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.kinglogi.entity.Trip;
import com.kinglogi.repository.base.BaseRepository;

import java.util.List;
import java.util.Optional;

/**
 * Repository for domain model class Trip.
 * @see Trip;
 * @author GMT
 */
@Repository
public interface TripRepository extends BaseRepository<Trip, Long> {

	 @Query("SELECT e "
			 + " FROM #{#entityName} e "
			 + " JOIN e.trip t "
			 + " WHERE e.deletedAt is null "
			 + " AND t.id = ?1 ")
	 Trip getTripByParentId(Long parentTripId);

	 List<Trip> findAllByPickingUpLocationAndDroppingOffLocation(String from, String to);

	 @Query("select distinct t.pickingUpLocation from Trip t where t.pickingUpLocation like concat('%', ?1, '%') and t.language = ?2 and t.deletedAt is null")
	 Page<String> getPickingUpLocations(String getPickingUpLocations, String language, Pageable pageable);

	@Query("select t from Trip t where t.pickingUpLocation = ?1 and t.language = ?2 and t.droppingOffLocation like concat('%', ?3, '%') and t.deletedAt is null")
	Page<Trip> getDroppingOffLocations(String getPickingUpLocations, String language, String droppingOffLocation, Pageable pageable);

	@Query("select t from Trip t where t.pickingUpLocation = ?1 and t.language = ?2 and t.droppingOffLocation like concat('%', ?3, '%') and t.numberOfSeat = ?4 and t.deletedAt is null")
	Page<Trip> getDroppingOffLocations(String getPickingUpLocations, String language, String droppingOffLocation, Integer numberOfSeat, Pageable pageable);

}
