package com.kinglogi.repository;

import com.kinglogi.entity.InventoryHistory;
import com.kinglogi.repository.base.BaseRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface InventoryHistoryRepository extends BaseRepository<InventoryHistory, Long> {
    Page<InventoryHistory> findAllByInventoryId(Long id, Pageable pageable);
}
