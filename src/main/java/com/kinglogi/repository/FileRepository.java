package com.kinglogi.repository;

import com.kinglogi.entity.File;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FileRepository extends JpaRepository<File, Long> {

    @Query("select f from File f left join f.carGroups cg where cg.id = ?1 and f.name = ?2")
    Optional<File> findByNameAndGroupId(Long id, String name);

    @Query("select f from File f left join f.services s where s.id = ?1 and f.name = ?2")
    Optional<File> findByNameAndServiceId(Long id, String name);
}
