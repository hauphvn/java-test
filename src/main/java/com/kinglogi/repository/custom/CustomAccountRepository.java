package com.kinglogi.repository.custom;

import com.kinglogi.dto.filter.AccountFilter;
import com.kinglogi.entity.Account;
import com.kinglogi.repository.AccountRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Optional;

@NoRepositoryBean
public interface CustomAccountRepository extends AccountRepository {

    Optional<Account> findByActivatedUsername(String username);

    Optional<Account> findActivatedUserByEmail(String email);

    Optional<Account> findByUsername(String username);

    Optional<Account> findByPhoneNumber(String phoneNumber);

    Optional<Account> findByPushToken(String pushToken);

    Page<Account> getAllAccounts(AccountFilter baseFilter, Pageable pageable);

    Page<Account> getAllAccountsByAgent(AccountFilter baseFilter, Pageable pageable, Long agentId);

    Page<Account> getAllAccountsInActive(AccountFilter accountFilter, Pageable pageable);
}
