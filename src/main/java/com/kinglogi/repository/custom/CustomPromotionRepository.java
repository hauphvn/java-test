package com.kinglogi.repository.custom;

import com.kinglogi.dto.filter.PromotionFillter;
import com.kinglogi.entity.Account;
import com.kinglogi.entity.Promotion;
import com.kinglogi.repository.PromotionRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
@Qualifier("customPromotionRepository")
public interface CustomPromotionRepository{

    Page<Promotion> findAllByUser(Account account, PromotionFillter promotionFillter, Pageable pageable);

}
