package com.kinglogi.repository.custom;

import com.kinglogi.entity.Role;
import com.kinglogi.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@Qualifier("customRoleRepository")
public interface CustomRoleRepository extends RoleRepository {

    Optional<Role> findByCode(String code);
}
