package com.kinglogi.repository.custom;

import com.kinglogi.entity.KlMetadata;
import com.kinglogi.repository.KlMetadataRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@Qualifier("customKlMetadataRepository")
public interface CustomKlMetadataRepository extends KlMetadataRepository {

    Optional<KlMetadata> findByCode(String code);
}
