package com.kinglogi.repository;

import com.kinglogi.entity.Account;
import com.kinglogi.entity.AccountHistory;
import com.kinglogi.repository.base.BaseRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountHistoryRepository extends BaseRepository<AccountHistory, Long> {

    Page<AccountHistory> findAllByAccount(Account account, Pageable pageable);

}
