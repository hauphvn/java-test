package com.kinglogi.repository;

import com.kinglogi.entity.VnPayTransaction;
import com.kinglogi.repository.base.BaseRepository;
import org.springframework.stereotype.Repository;
// Generated May 31, 2020, 11:28:53 PM by Hibernate Tools 5.2.12.Final

/**
 * Repository for domain model class VnPayTransaction.
 * @see VnPayTransaction ;
 * @author GMT
 */
@Repository
public interface VnPayTransactionRepository extends BaseRepository<VnPayTransaction, Long> {

}
