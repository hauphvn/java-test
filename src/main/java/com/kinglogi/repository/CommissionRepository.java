package com.kinglogi.repository;

import com.kinglogi.entity.Commission;
import com.kinglogi.repository.base.BaseRepository;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Repository;

@Repository
public interface CommissionRepository extends BaseRepository<Commission, Long> {
}
