package com.kinglogi.repository;

import com.kinglogi.constant.InventoryStatus;
import com.kinglogi.entity.Inventory;
import com.kinglogi.entity.Trip;
import com.kinglogi.repository.base.BaseRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Repository
public interface InventoryRepository extends BaseRepository<Inventory, Long> {
    @Query("select i from Inventory i where i.deletedAt is null and i.numberOfSeat = ?1 and i.trip.pickingUpLocation = ?2 and i.trip.droppingOffLocation = ?3 and i.expireTime > ?4 and  i.numberOfTrip > 0")
    Page<Inventory> findAllByNumberOfSeatAndTripId(int numberOfSeat, String pickingUpLocation, String droppingOffLocation, Instant now, Pageable pageable);

    @Modifying
    @Query("update Inventory i set i.status = ?1 where i.expireTime < ?2 and i.status = ?3")
    void updateAllByExpireTimeBeforeAndStatus(InventoryStatus inventoryStatus, Instant now, InventoryStatus status);

    Inventory findTopByTrip(Trip trip);

}
