package com.kinglogi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.controller.base.CrudController;
import com.kinglogi.dto.filter.AccountFilter;
import com.kinglogi.dto.request.AccountRequest;
import com.kinglogi.dto.response.AccountResponse;
import com.kinglogi.service.AdminService;

@RestController
@RequestMapping(ApiVersion.API_V1 + "/admin-accounts")
public class AdminController extends CrudController<AccountRequest, AccountResponse, Long, AccountFilter> {

    @Autowired
    public AdminController(AdminService service) {
        super(service);
    }
}
