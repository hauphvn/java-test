package com.kinglogi.controller;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.controller.base.CrudController;
import com.kinglogi.dto.filter.PartnerFilter;
import com.kinglogi.dto.request.PartnerRequest;
import com.kinglogi.dto.response.PartnerResponse;
import com.kinglogi.service.PartnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ApiVersion.API_V1 + "/partners")
public class PartnerController extends CrudController<PartnerRequest, PartnerResponse, Long, PartnerFilter> {

    @Autowired
    public PartnerController(PartnerService service) {
        super(service);
    }
}
