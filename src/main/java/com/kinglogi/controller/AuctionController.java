package com.kinglogi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.controller.base.CrudController;
import com.kinglogi.dto.filter.AuctionFilter;
import com.kinglogi.dto.request.AuctionRequest;
import com.kinglogi.dto.response.AuctionResponse;
import com.kinglogi.service.AuctionService;

@RestController
@RequestMapping(ApiVersion.API_V1 + "/auctions")
public class AuctionController extends CrudController<AuctionRequest, AuctionResponse, Long, AuctionFilter> {

    @Autowired
    public AuctionController(AuctionService service) {
        super(service);
    }
}
