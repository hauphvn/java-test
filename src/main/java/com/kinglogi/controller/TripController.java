package com.kinglogi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.controller.base.CrudController;
import com.kinglogi.dto.filter.TripFilter;
import com.kinglogi.dto.request.TripRequest;
import com.kinglogi.dto.response.TripResponse;
import com.kinglogi.service.TripService;

@RestController
@RequestMapping(ApiVersion.API_V1 + "/trips")
public class TripController extends CrudController<TripRequest, TripResponse, Long, TripFilter> {

	@Autowired
	private TripService tripService;
	
    @Autowired
    public TripController(TripService service) {
        super(service);
    }
    
    @GetMapping("/{id}/trip")
    public TripResponse getByParentTrip(@PathVariable Long id) {
        return tripService.getTripByParentTripId(id);
    }
}
