package com.kinglogi.controller;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.controller.base.CrudController;
import com.kinglogi.dto.filter.CarFilter;
import com.kinglogi.dto.request.CarRequest;
import com.kinglogi.dto.response.CarResponse;
import com.kinglogi.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ApiVersion.API_V1 + "/cars")
public class CarController extends CrudController<CarRequest, CarResponse, Long, CarFilter> {

    @Autowired
    public CarController(CarService service) {
        super(service);
    }
}
