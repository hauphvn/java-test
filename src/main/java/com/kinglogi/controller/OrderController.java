package com.kinglogi.controller;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.controller.base.CrudController;
import com.kinglogi.dto.filter.OrderFilter;
import com.kinglogi.dto.request.MultiOrderRequest;
import com.kinglogi.dto.request.OrderRequest;
import com.kinglogi.dto.response.OrderResponse;
import com.kinglogi.dto.response.WinningBidderResponse;
import com.kinglogi.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(ApiVersion.API_V1 + "/orders")
public class OrderController extends CrudController<OrderRequest, OrderResponse, Long, OrderFilter> {

    @Autowired
    public OrderController(OrderService service) {
        super(service);
    }

    @GetMapping("/{id}/winning-bidder")
    public WinningBidderResponse getWinningBidder(@PathVariable Long id) {
        return ((OrderService) getCrudService()).getWinningBidder(id);
    }

    @PostMapping("/multi-orders")
    public List<OrderResponse> getOrdersByIds(@Valid @RequestBody MultiOrderRequest request) {
        return ((OrderService) getCrudService()).getByIds(request.getIds());
    }
}
