package com.kinglogi.controller;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.controller.base.CrudController;
import com.kinglogi.dto.filter.AccountFilter;
import com.kinglogi.dto.request.AccountRequest;
import com.kinglogi.dto.response.AccountResponse;
import com.kinglogi.service.CollaboratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ApiVersion.API_V1 + "/collaborator-accounts")
public class CollaboratorController extends CrudController<AccountRequest, AccountResponse, Long, AccountFilter> {

    @Autowired
    public CollaboratorController(CollaboratorService service) {
        super(service);
    }
}
