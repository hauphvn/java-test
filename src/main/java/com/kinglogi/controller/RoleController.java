package com.kinglogi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.controller.base.MasterDataController;
import com.kinglogi.dto.response.RoleResponse;
import com.kinglogi.service.RoleService;

@RestController
@RequestMapping(ApiVersion.API_V1 + "/roles")
public class RoleController extends MasterDataController<RoleResponse> {

    @Autowired
    public RoleController(RoleService service) {
        super(service);
    }
}
