package com.kinglogi.controller;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.filter.AuctionBidFilter;
import com.kinglogi.dto.response.AuctionBidResponse;
import com.kinglogi.service.AuctionBidService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(ApiVersion.API_V1 + "/auction-bids")
public class AuctionBidController {

    private final AuctionBidService auctionBidService;

    @Autowired
    public AuctionBidController(AuctionBidService auctionBidService) {
        this.auctionBidService = auctionBidService;
    }

    @GetMapping("histories")
    public List<AuctionBidResponse> getAuctionBidsByLoggedInUser(AuctionBidFilter filter) {
        return auctionBidService.getAuctionBidsByLoggedInUser(filter);
    }

    @GetMapping("/{auctionId}")
    public AuctionBidResponse getDetail(@PathVariable Long auctionId) {
        return auctionBidService.getDetail(auctionId);
    }
}
