package com.kinglogi.controller;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.bid.BidAuctionCreateInputDTO;
import com.kinglogi.dto.response.BidResponse;
import com.kinglogi.service.BidService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(ApiVersion.API_V1 + "/bids")
public class BidController {

    private final BidService bidService;

    @Autowired
    public BidController(BidService bidService) {
        this.bidService = bidService;
    }

    // FIXME: need to delete this
    @Deprecated
    @PostMapping("/{auctionId}")
    @ResponseStatus(code = HttpStatus.CREATED)
    public BidResponse createOrder(@PathVariable Long auctionId) {
        BidAuctionCreateInputDTO bidAuctionCreateInputDTO = new BidAuctionCreateInputDTO();
        bidAuctionCreateInputDTO.setAuctionId(auctionId);
        return bidService.bid(bidAuctionCreateInputDTO);
    }
}
