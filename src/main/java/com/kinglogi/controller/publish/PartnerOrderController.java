package com.kinglogi.controller.publish;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.request.PartnerFixedTripOrderRequest;
import com.kinglogi.dto.request.PartnerFlexibleTripOrderRequest;
import com.kinglogi.dto.request.PartnerFlexibleTripOrderRequestFromIframe;
import com.kinglogi.dto.response.PartnerOrderResponse;
import com.kinglogi.dto.response.PartnerPaymentResponse;
import com.kinglogi.service.PartnerOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping(ApiVersion.API_V1 + "/partner-orders")
public class PartnerOrderController {

    private final PartnerOrderService service;

    @Autowired
    public PartnerOrderController(PartnerOrderService service) {
        this.service = service;
    }

    @PostMapping("fixed-route")
    @ResponseStatus(code = HttpStatus.CREATED)
    public PartnerOrderResponse createFixedTripOrderFromApi(@Valid @RequestBody PartnerFixedTripOrderRequest request) {
        return service.createOrder(request);
    }

    @PostMapping("flexible-route")
    @ResponseStatus(code = HttpStatus.CREATED)
    public PartnerOrderResponse createFlexibleTripOrderFromApi(@Valid @RequestBody PartnerFlexibleTripOrderRequest request) {
        return service.createOrder(request);
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public PartnerOrderResponse createFlexibleTripOrderFromIframe(@Valid @RequestBody PartnerFlexibleTripOrderRequestFromIframe request) {
        return service.createOrder(request);
    }

    @PutMapping
    public PartnerPaymentResponse verifyPayment(@RequestParam Map<String, Object> allParams) {
        return service.verifyPayment(allParams);
    }
}
