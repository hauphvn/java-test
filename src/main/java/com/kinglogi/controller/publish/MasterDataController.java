package com.kinglogi.controller.publish;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kinglogi.constant.ApiParamConstant;
import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.AppStoreLink;
import com.kinglogi.dto.CompanyInformation;
import com.kinglogi.dto.Contact;
import com.kinglogi.dto.KlConfiguration;
import com.kinglogi.dto.SocialNetwork;
import com.kinglogi.dto.filter.MasterDataFilter;
import com.kinglogi.dto.filter.TripFilter;
import com.kinglogi.dto.response.MasterDataResponse;
import com.kinglogi.service.BankAccountService;
import com.kinglogi.service.KlMetadataService;
import com.kinglogi.service.TripCatalogService;
import com.kinglogi.service.TripService;
import com.kinglogi.utils.BeanUtil;


@RestController
@RequestMapping(ApiVersion.API_V1 + "/masters")
public class MasterDataController {

    private final KlMetadataService metadataService;
    private final BankAccountService bankAccountService;
    private final TripCatalogService tripCatalogService;
    private final TripService tripService;

    @Autowired
    public MasterDataController(
            KlMetadataService metadataService, BankAccountService bankAccountService,
            TripCatalogService tripCatalogService, TripService tripService) {
        this.metadataService = metadataService;
        this.bankAccountService = bankAccountService;
        this.tripCatalogService = tripCatalogService;
        this.tripService = tripService;
    }

    @GetMapping
    public MasterDataResponse getAllMasterData(MasterDataFilter filter) {
    	// Set unpage to get all
    	filter.setLimit(ApiParamConstant.UNPAGED_LIMIT_VALUE);
//        this.createSampleConfiguration(); // TODO remove
        MasterDataResponse response = new MasterDataResponse();
        response.setConfiguration(metadataService.getConfiguration());
        response.setBankAccounts(bankAccountService.findAll());
        response.setTripCatalogs(tripCatalogService.findAll());
        response.setTrips(tripService.findAll(BeanUtil.copyProperties(filter, TripFilter.class)).getContent());
        return response;
    }

    // TODO remove
    private void createSampleConfiguration() {
        KlConfiguration configuration = new KlConfiguration();
        CompanyInformation companyInformation = new CompanyInformation();
        companyInformation.setAddress("Số 12, Ngõ 983, Hồng Hà, Chương Dương, Hoàn Kiếm, Hà Nội");
        companyInformation.setName("Công Ty Cổ Phần KING LOGI");
        companyInformation.setBusinessCode("123456789");
        companyInformation.setLegalRepresentative("Vương Toàn Thiện");
        companyInformation.setPosition("Giám Đốc");
        configuration.setCompanyInformation(companyInformation);

        AppStoreLink appStoreLink = new AppStoreLink();
        appStoreLink.setAppleStore("https://apps.apple.com/vn/app/queue-master/id1493558608");
        appStoreLink.setGooglePlayStore("https://play.google.com/store/apps/details?id=sg.queuemaster");
        configuration.setAppStoreLink(appStoreLink);

        Contact contact = new Contact();
        contact.setAddress("13 floor, Hanoi Towers, 49 Hai Ba Trung, Hanoi, Vietnam");
        contact.setEmail("admin@kinglogi.com");
        contact.setPhoneNumber1("96392 1979");
        contact.setPhoneNumber2("+84 24 32663288");
        contact.setGoogleMapLink("https://goo.gl/maps/uDoVi3C86wLKFYkTA");
        configuration.setContact(contact);

        SocialNetwork socialNetwork = new SocialNetwork();
        socialNetwork.setName("Twitter");
        socialNetwork.setLink("https://twitter.com/");
        socialNetwork.setIcon("fa fa-twitter");

        SocialNetwork socialNetwork2 = new SocialNetwork();
        socialNetwork2.setName("Facebook");
        socialNetwork2.setLink("https://facebook.com/");
        socialNetwork2.setIcon("fa fa-facebook");


        SocialNetwork socialNetwork3 = new SocialNetwork();
        socialNetwork3.setName("Youtube");
        socialNetwork3.setLink("https://www.youtube.com/");
        socialNetwork3.setIcon("fa fa-youtube");

        configuration.setSocialNetworks(new ArrayList<>());
        configuration.getSocialNetworks().add(socialNetwork2);
        configuration.getSocialNetworks().add(socialNetwork);
        configuration.getSocialNetworks().add(socialNetwork3);

        metadataService.updateConfiguration(configuration);
    }
}
