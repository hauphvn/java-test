package com.kinglogi.controller.publish;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.request.*;
import com.kinglogi.dto.response.JwtAuthenticationToken;
import com.kinglogi.service.AuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping(value = ApiVersion.API_V1 + "/auth")
public class AuthController {

    private final AuthService authService;

    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("/sign-in")
    @ResponseStatus(code = HttpStatus.OK)
    public JwtAuthenticationToken login(@Valid @RequestBody LoginRequest request) {
        return authService.login(request);
    }

    // TODO Will be removed. It's just for apple reviewing
    @PostMapping("/partners/sign-up")
    @ResponseStatus(code = HttpStatus.OK)
    public String partnerRegister(@Valid @RequestBody RegisterRequest request) {
        request.setActive(false);
        log.info("Partner registered with data={}", request);
        authService.registerByPartner(request);
        return "Đã đăng ký thành công. Vui lòng chờ kết quả phê duyệt từ KING Logi để được sử dụng.";
    }

    @PostMapping("/refresh")
    @ResponseStatus(code = HttpStatus.OK)
    public JwtAuthenticationToken refreshToken(@Valid @RequestBody RefreshTokenRequest request) {
        return authService.refreshToken(request);
    }

    @PostMapping("/forgot-password")
    @ResponseStatus(code = HttpStatus.OK)
    public Boolean forgotPassword(@Valid @RequestBody ForgotPasswordRequest request) {
        return authService.forgotPassword(request);
    }

    @PutMapping("/change-password")
    @ResponseStatus(code = HttpStatus.OK)
    public JwtAuthenticationToken changePassword(@Valid @RequestBody ChangePasswordRequest request) {
        return authService.changePassword(request);
    }
}
