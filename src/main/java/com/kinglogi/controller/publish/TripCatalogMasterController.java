package com.kinglogi.controller.publish;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.controller.base.MasterDataController;
import com.kinglogi.dto.response.TripCatalogResponse;
import com.kinglogi.service.TripCatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ApiVersion.API_V1 + "/publish/trip-catalogs")
public class TripCatalogMasterController extends MasterDataController<TripCatalogResponse> {

    @Autowired
    public TripCatalogMasterController(TripCatalogService service) {
        super(service);
    }
}
