package com.kinglogi.controller.publish;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.request.PaymentRequest;
import com.kinglogi.dto.response.PaymentResponse;
import com.kinglogi.dto.response.PaymentUrlResponse;
import com.kinglogi.service.VnPayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

/**
 * @deprecated Should use VisitorOrderController instead
 */
@Deprecated
@RestController
@RequestMapping(ApiVersion.API_V1 + "/payments")
@Slf4j
public class VnPayController {

    private final VnPayService service;

    @Autowired
    public VnPayController(VnPayService service) {
        this.service = service;
    }

    @GetMapping
    public PaymentResponse verifyPayment(@RequestParam Map<String, Object> allParams) {
        return service.verifyPayment(allParams);
    }

    @PostMapping
    public PaymentUrlResponse generateVnPayRequestUrl(@Valid @RequestBody PaymentRequest paymentRequest) {
        return service.generateVnPayRequestUrl(paymentRequest);
    }
}
