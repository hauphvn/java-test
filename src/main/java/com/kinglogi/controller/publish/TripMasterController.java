package com.kinglogi.controller.publish;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.filter.TripFilter;
import com.kinglogi.dto.response.SimpleTripResponse;
import com.kinglogi.service.TripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ApiVersion.API_V1 + "/publish/trips")
public class TripMasterController {

    private final TripService service;

    @Autowired
    public TripMasterController(TripService service) {
        this.service = service;
    }

    @GetMapping
    public Page<SimpleTripResponse> getAll(TripFilter baseFilter) {
        return service.findAllSimpleTrip(baseFilter);
    }
}
