package com.kinglogi.controller.publish;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.request.OrderRequest;
import com.kinglogi.dto.response.PaymentResponse;
import com.kinglogi.dto.response.OrderWithPaymentResponse;
import com.kinglogi.service.VisitorOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping(ApiVersion.API_V1 + "/visitor-orders")
public class VisitorOrderController {

    private final VisitorOrderService service;

    @Autowired
    public VisitorOrderController(VisitorOrderService service) {
        this.service = service;
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public OrderWithPaymentResponse createOrder(@Valid @RequestBody OrderRequest request) {
        return service.createOrder(request);
    }

    @PutMapping
    public PaymentResponse verifyPayment(@RequestParam Map<String, Object> allParams) {
        return service.verifyPayment(allParams);
    }
}
