package com.kinglogi.controller;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.KlConfiguration;
import com.kinglogi.service.KlMetadataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(ApiVersion.API_V1 + "/configurations")
public class ConfigurationController {

    private final KlMetadataService metadataService;

    @Autowired
    public ConfigurationController(KlMetadataService metadataService) {
        this.metadataService = metadataService;
    }

    @GetMapping
    public KlConfiguration getConfiguration() {
        return metadataService.getConfiguration();
    }

    @PutMapping
    public boolean getConfiguration(@RequestBody @Valid KlConfiguration configuration) {
        return metadataService.updateConfiguration(configuration);
    }
}
