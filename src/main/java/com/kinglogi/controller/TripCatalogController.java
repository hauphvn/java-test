package com.kinglogi.controller;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.controller.base.CrudController;
import com.kinglogi.dto.filter.TripCatalogFilter;
import com.kinglogi.dto.request.TripCatalogRequest;
import com.kinglogi.dto.response.TripCatalogResponse;
import com.kinglogi.service.TripCatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ApiVersion.API_V1 + "/trip-catalogs")
public class TripCatalogController extends CrudController<TripCatalogRequest, TripCatalogResponse, Long, TripCatalogFilter> {

    @Autowired
    public TripCatalogController(TripCatalogService service) {
        super(service);
    }
}
