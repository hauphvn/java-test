package com.kinglogi.controller;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.request.PaymentRequest;
import com.kinglogi.dto.response.PaymentUrlResponse;
import com.kinglogi.service.PayInService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping(ApiVersion.API_V1 + "/pay-in")
public class PayInController {

    private final PayInService service;

    @Autowired
    public PayInController(PayInService service) {
        this.service = service;
    }

    @GetMapping
    public Boolean verifyPayIn(@RequestParam Map<String, Object> allParams) {
        return service.verifyPayIn(allParams);
    }

    @PostMapping
    public PaymentUrlResponse buildPayInRequest(@Valid @RequestBody PaymentRequest paymentRequest) {
        return service.buildPayInRequest(paymentRequest);
    }
}
