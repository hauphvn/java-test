package com.kinglogi.controller;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.filter.OrderFilter;
import com.kinglogi.dto.request.UserProfileRequest;
import com.kinglogi.dto.response.AccountResponse;
import com.kinglogi.dto.response.OrderResponse;
import com.kinglogi.dto.response.TransactionResponse;
import com.kinglogi.service.UserProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(ApiVersion.API_V1 + "/profile")
public class UserProfileController {

    private final UserProfileService userProfileService;

    @Autowired
    public UserProfileController(UserProfileService userProfileService) {
        this.userProfileService = userProfileService;
    }

    @GetMapping
    public AccountResponse getProfile() {
        return userProfileService.getProfile();
    }

    @PutMapping
    public AccountResponse updateProfile(@Valid @RequestBody UserProfileRequest request) {
        return userProfileService.updateProfile(request);
    }

    @GetMapping("/transactions")
    public List<TransactionResponse> getTransactionsByAccountId() {
        return userProfileService.getTransactionsByAccountId();
    }

    @GetMapping("/orders")
    public Page<OrderResponse> getOrdersByAccountId(OrderFilter orderFilter) {
        return userProfileService.getOrdersByAccountId(orderFilter);
    }
}
