package com.kinglogi.controller;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.controller.base.CrudController;
import com.kinglogi.dto.filter.BankAccountFilter;
import com.kinglogi.dto.request.BankAccountRequest;
import com.kinglogi.dto.response.BankAccountResponse;
import com.kinglogi.service.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ApiVersion.API_V1 + "/bank-accounts")
public class BankAccountController extends CrudController<BankAccountRequest, BankAccountResponse, Long, BankAccountFilter> {

    @Autowired
    public BankAccountController(BankAccountService service) {
        super(service);
    }
}
