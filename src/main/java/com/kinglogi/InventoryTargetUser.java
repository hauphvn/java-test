package com.kinglogi;

public enum InventoryTargetUser {
    MEMBER, COLLABORATOR, PARTNER, COLLABORATOR_GROUP
}
