package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.filter.LoyaltyPointHistoryFilter;
import com.kinglogi.dto.loyalty_point_history.LoyaltyPointHistoryOutputDTO;
import com.kinglogi.service.LoyaltyPointHistoryService;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/loyalty-point-histories")
public class LoyaltyPointHistoryControllerV2 {
    private final LoyaltyPointHistoryService loyaltyPointHistoryService;

    public LoyaltyPointHistoryControllerV2(LoyaltyPointHistoryService loyaltyPointHistoryService) {
        this.loyaltyPointHistoryService = loyaltyPointHistoryService;
    }

    @GetMapping("")
    public Page<LoyaltyPointHistoryOutputDTO> findAll(LoyaltyPointHistoryFilter filter) {
        return this.loyaltyPointHistoryService.findAll(filter);
    }
}
