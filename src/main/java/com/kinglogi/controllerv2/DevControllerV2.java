package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.log.LogOutput;
import com.kinglogi.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Profile("log")
@RestController
@RequestMapping(ApiVersion.API_V2 + "/devs")
public class DevControllerV2 {

    @Autowired
    LogService logService;

    @GetMapping("/logs")
    public Page<LogOutput> getLogs(Pageable pageable){
        return logService.getLogs(pageable);
    }

}
