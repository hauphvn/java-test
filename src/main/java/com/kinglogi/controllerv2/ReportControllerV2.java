package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.report.ReportOutputDTO;
import com.kinglogi.service.ReportService;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nullable;
import java.io.IOException;
import java.time.Instant;
import java.util.List;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/report")
public class ReportControllerV2 {

    private final ReportService reportService;

    public ReportControllerV2(ReportService reportService) {
        this.reportService = reportService;
    }

    @GetMapping("/export")
    public ResponseEntity<Resource> exportReport() throws IOException {
        InputStreamResource inputStreamResource = reportService.exportReport();
        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=Report.xlsx");
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_OCTET_STREAM).headers(header).body(inputStreamResource);
    }

    @GetMapping
    public Page<ReportOutputDTO> getReports(@RequestParam @Nullable Instant date, Pageable pageable){
        return reportService.getReports(date, pageable);
    }

}
