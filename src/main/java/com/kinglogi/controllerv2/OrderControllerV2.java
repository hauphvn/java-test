package com.kinglogi.controllerv2;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.kinglogi.constant.ApiVersion;
import com.kinglogi.controllerv2.base.CrudController;
import com.kinglogi.dto.filter.OrderFilter;
import com.kinglogi.dto.filter.base.BaseFilter;
import com.kinglogi.dto.request.MultiOrderRequest;
import com.kinglogi.dto.request.OrderRequest;
import com.kinglogi.dto.response.OrderResponse;
import com.kinglogi.dto.response.WinningBidderResponse;
import com.kinglogi.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/orders")
public class OrderControllerV2 extends CrudController<OrderRequest, OrderResponse, Long, OrderFilter> {

    @Autowired
    public OrderControllerV2(OrderService service) {
        super(service);
    }

    @GetMapping("/{id}/winning-bidder")
    public WinningBidderResponse getWinningBidder(@PathVariable Long id) {
        return ((OrderService) getCrudService()).getWinningBidder(id);
    }

    @PostMapping("/multi-orders")
    public List<OrderResponse> getOrdersByIds(@Valid @RequestBody MultiOrderRequest request) {
        return ((OrderService) getCrudService()).getByIds(request.getIds());
    }

    @PostMapping("/{id}/send-invoice")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void triggerInvoiceMail(@PathVariable Long id) {
        ((OrderService) getCrudService()).sendEmailInvoice(id);
    }

    @GetMapping("/collaborators")
    public Page<OrderResponse> getOrdersBelongToCollaborator(OrderFilter baseFilter, @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam(required = false) Date day) {
        return ((OrderService) getCrudService()).getOrdersBelongToCollaborator(baseFilter, day);
    }

    @GetMapping("/collaborators/daysHaveOrder")
    public List<Date> getDayHaveOrdersInAMonthBelongToCollaborator(@RequestParam int year, @RequestParam int month) {
        return ((OrderService) getCrudService()).getOrdersInMonthOfCollaborator(month, year);
    }

    @PutMapping("/{id}/start")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void startOrder(@PathVariable Long id) {
        ((OrderService) getCrudService()).startOrderFromCollaborator(id);
    }

    @PutMapping("/{id}/finish")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void finishOrder(@PathVariable Long id) {
        ((OrderService) getCrudService()).finishOrderFromCollaborator(id);
    }

    @PutMapping("/{id}/cancel")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void cancelOrder(@PathVariable Long id) {
        ((OrderService) getCrudService()).cancelOrderFromCollaborator(id);
    }
}
