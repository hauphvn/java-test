package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.controllerv2.base.CrudController;
import com.kinglogi.dto.filter.ComplaintFilter;
import com.kinglogi.dto.filter.base.BaseFilter;
import com.kinglogi.dto.request.ComplaintRequest;
import com.kinglogi.dto.response.ComplaintResponse;
import com.kinglogi.service.ComplaintService;
import liquibase.pro.packaged.L;
import org.apache.commons.io.FilenameUtils;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.mail.internet.ContentType;
import javax.servlet.http.HttpServletRequest;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Files;
import java.util.List;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/complaints")
public class ComplaintControllerV2 {

    private final ComplaintService complaintService;

    public ComplaintControllerV2(ComplaintService complaintService) {
        this.complaintService = complaintService;
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public ComplaintResponse save(@RequestBody ComplaintRequest complaintRequest) {
        return complaintService.save(complaintRequest);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ComplaintResponse update(@PathVariable Long id, @RequestBody ComplaintRequest complaintRequest) {
        return complaintService.update(id, complaintRequest);
    }

    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    public Page<ComplaintResponse>  getAll(ComplaintFilter complaintFilter) {
        return complaintService.findAll(complaintFilter);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        complaintService.deleteById(id);
    }

    @GetMapping("/user")
    @ResponseStatus(code = HttpStatus.OK)
    public Page<ComplaintResponse> getByUserId(@RequestParam(required = false) String keyword, ComplaintFilter baseFilter) {
        return  complaintService.findByUserId(keyword, baseFilter);
    }

    @PostMapping("{id}/files")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void uploadFiles(@PathVariable Long id, @RequestParam(name = "files") List<MultipartFile> multipartFiles) {
        complaintService.uploadFiles(id, multipartFiles);
    }

    @GetMapping("{id}/files/{fileName}")
    public ResponseEntity<InputStreamResource> viewFile(@PathVariable Long id, @PathVariable String fileName) throws IOException {
        FileInputStream inputStreamResource = complaintService.viewFile(id, fileName);
        String mimeType = URLConnection.guessContentTypeFromName(fileName);
        MediaType mediaType;
        if (mimeType != null) {
            mediaType = MediaType.parseMediaType(mimeType);
            return ResponseEntity.ok()
                    .contentType(mediaType)
                    .body(new InputStreamResource(inputStreamResource));
        } else {
            mediaType = MediaType.APPLICATION_OCTET_STREAM;
            return ResponseEntity.ok()
                    .contentType(mediaType)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName)
                    .body(new InputStreamResource(inputStreamResource));
        }
    }

    @PutMapping("/{id}/status")
    public ComplaintResponse changeStatus(@PathVariable Long id, @RequestBody ComplaintRequest complaintRequest) {
        return complaintService.changeStatus(id, complaintRequest);
    }

    @PutMapping("{id}/responses")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void response(@PathVariable Long id, @RequestBody ComplaintRequest complaintRequest) {
        complaintService.response(id, complaintRequest);
    }


    @GetMapping("/{id}")
    public ComplaintResponse getById(@PathVariable Long id, HttpServletRequest request) {
        return complaintService.findById(id);
    }

    @DeleteMapping("/{id}/files/{fileName}")
    public void deleteImage(@PathVariable Long id, @PathVariable String fileName) {
        complaintService.deleteImage(id, fileName);
    }
}
