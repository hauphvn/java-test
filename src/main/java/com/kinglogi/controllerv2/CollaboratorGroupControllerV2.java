package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.controllerv2.base.CrudController;
import com.kinglogi.dto.collaborator_group.CollaboratorGroupRequest;
import com.kinglogi.dto.collaborator_group.CollaboratorGroupResponse;
import com.kinglogi.dto.filter.AccountFilter;
import com.kinglogi.dto.filter.CollaboratorGroupFilter;
import com.kinglogi.dto.filter.base.BaseFilter;
import com.kinglogi.dto.response.AccountResponse;
import com.kinglogi.service.CollaboratorGroupService;
import com.kinglogi.service.impl.CollaboratorGroupServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/collaborator-groups")
public class CollaboratorGroupControllerV2 extends CrudController<CollaboratorGroupRequest, CollaboratorGroupResponse, Long, CollaboratorGroupFilter> {
    private final CollaboratorGroupServiceImpl collaboratorGroupService;

    @Autowired
    public CollaboratorGroupControllerV2(CollaboratorGroupService service, CollaboratorGroupServiceImpl collaboratorGroupService) {
        super(service);
        this.collaboratorGroupService = collaboratorGroupService;
    }

    @GetMapping("/{id}/accounts")
    public Page<AccountResponse> getAccountInGroup(@PathVariable Long id, CollaboratorGroupFilter baseFilter) {
        return this.collaboratorGroupService.getCollaboratorInGroup(id, baseFilter);
    }
    @GetMapping("/accounts/notInGroup")
    public Page<AccountResponse> getAccountNotInGroup(AccountFilter baseFilter) {
        return this.collaboratorGroupService.getCollaboratorNotInGroup(baseFilter);
    }

    @PutMapping("/{collaboratorGroupId}/accounts/{accountId}/remove")
    public void removeAccountFromGroup(@PathVariable Long accountId, @PathVariable Long collaboratorGroupId) {
        this.collaboratorGroupService.removeCollaboratorFromGroup(accountId, collaboratorGroupId);
    }
}
