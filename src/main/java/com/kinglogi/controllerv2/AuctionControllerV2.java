package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.controllerv2.base.CrudController;
import com.kinglogi.dto.filter.AuctionFilter;
import com.kinglogi.dto.request.AuctionRequest;
import com.kinglogi.dto.response.AuctionResponse;
import com.kinglogi.service.AuctionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/auctions")
public class AuctionControllerV2 extends CrudController<AuctionRequest, AuctionResponse, Long, AuctionFilter> {

    @Autowired
    public AuctionControllerV2(AuctionService service) {
        super(service);
    }

    @PutMapping("/cancel/{id}")
    public void cancel(@PathVariable Long id) {
        ((AuctionService) getCrudService()).cancelAuctionByAdmin(id);
    }
}
