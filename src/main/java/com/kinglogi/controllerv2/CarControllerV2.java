package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.controllerv2.base.CrudController;
import com.kinglogi.dto.filter.CarFilter;
import com.kinglogi.dto.request.CarRequest;
import com.kinglogi.dto.response.CarResponse;
import com.kinglogi.dto.response.file_response.FileCarResponse;
import com.kinglogi.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/cars")
public class CarControllerV2 extends CrudController<CarRequest, CarResponse, Long, CarFilter> {

    private final CarService carService;

    @Autowired
    public CarControllerV2(CarService service, CarService carService) {
        super(service);
        this.carService = carService;
    }

    @PostMapping("/{id}/images")
    public void uploadImage(@PathVariable Long id, @RequestParam(name = "files")List<MultipartFile> files) {
        carService.uploadImages(id, files);
    }

    @GetMapping("/{id}/images")
    public List<FileCarResponse> uploadImage(@PathVariable Long id) {
        return carService.getImage(id);
    }


    @GetMapping(value = "{imageId}/images/{fileName}")
    public ResponseEntity<InputStreamResource>image(@PathVariable Long imageId, @PathVariable String fileName) throws IOException {
        InputStreamResource inputStreamResources = carService.viewImage(imageId, fileName);
        return ResponseEntity.ok()
                .contentType(MediaType.IMAGE_JPEG)
                .body(inputStreamResources);
    }




}
