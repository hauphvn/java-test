package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.controllerv2.base.CrudController;
import com.kinglogi.dto.filter.BankAccountFilter;
import com.kinglogi.dto.request.BankAccountRequest;
import com.kinglogi.dto.response.BankAccountResponse;
import com.kinglogi.service.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/bank-accounts")
public class BankAccountControllerV2 extends CrudController<BankAccountRequest, BankAccountResponse, Long, BankAccountFilter> {

    @Autowired
    public BankAccountControllerV2(BankAccountService service) {
        super(service);
    }
}
