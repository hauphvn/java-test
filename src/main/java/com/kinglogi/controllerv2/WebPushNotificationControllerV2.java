package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.request.FcmPushNotificationRequest;
import com.kinglogi.dto.request.FcmSubscriptionRequest;
import com.kinglogi.service.FcmPushNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/web-notifications")
public class WebPushNotificationControllerV2 {

    private final FcmPushNotificationService notificationService;

    @Autowired
    public WebPushNotificationControllerV2(FcmPushNotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @PostMapping("/token")
    public Boolean sendPnsToDevice(@Valid @RequestBody FcmPushNotificationRequest notificationRequestDto) {
        return notificationService.sendPnsToDevice(notificationRequestDto);
    }

    @PostMapping("/subscribe")
    public void subscribeToTopic(@Valid @RequestBody FcmSubscriptionRequest subscriptionRequestDto) {
        notificationService.subscribeToTopic(subscriptionRequestDto);
    }

    @PostMapping("/unsubscribe")
    public void unsubscribeFromTopic(FcmSubscriptionRequest subscriptionRequestDto) {
        notificationService.unsubscribeFromTopic(subscriptionRequestDto);
    }
    @PostMapping("/topic")
    public Boolean sendPnsToTopic(@Valid @RequestBody FcmSubscriptionRequest notificationRequestDto) {
        return notificationService.sendPnsToTopic(notificationRequestDto);
    }
}
