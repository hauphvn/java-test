package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.controllerv2.base.CrudController;
import com.kinglogi.dto.filter.AccountFilter;
import com.kinglogi.dto.request.AccountRequest;
import com.kinglogi.dto.request.TransactionRequest;
import com.kinglogi.dto.response.AccountResponse;
import com.kinglogi.dto.response.TransactionResponse;
import com.kinglogi.service.MemberService;
import com.kinglogi.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/member-accounts")
public class MemberControllerV2 extends CrudController<AccountRequest, AccountResponse, Long, AccountFilter> {
    private final TransactionService transactionService;

    @Autowired
    public MemberControllerV2(MemberService service, TransactionService transactionService) {
        super(service);
        this.transactionService = transactionService;
    }

    @GetMapping("/{id}/transactions")
    public List<TransactionResponse> getTransactionsByAccountId(@PathVariable Long id) {
        return transactionService.findByAccountId(id);
    }

    @PostMapping("/{id}/transactions")
    @ResponseStatus(code = HttpStatus.CREATED)
    public TransactionResponse addTransaction(@PathVariable Long id, @Valid @RequestBody TransactionRequest request) {
        request.setTargetId(id);
        return transactionService.save(request);
    }
}
