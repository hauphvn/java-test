package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.request.TransactionRequest;
import com.kinglogi.dto.response.TransactionResponse;
import com.kinglogi.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/transactions")
public class TransactionControllerV2 {

    private final TransactionService service;

    @Autowired
    public TransactionControllerV2(TransactionService service) {
        this.service = service;
    }

    @GetMapping
    public List<TransactionResponse> getTransactions(@RequestParam("id") Long targetId, @RequestParam("type") Integer transactionType) {
        return service.getTransactions(targetId, transactionType);
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public TransactionResponse addTransaction(@Valid @RequestBody TransactionRequest request) {
        return service.save(request);
    }

    @GetMapping("/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public TransactionResponse getDetail(@PathVariable Long id) {
        return service.getDetail(id);
    }
}
