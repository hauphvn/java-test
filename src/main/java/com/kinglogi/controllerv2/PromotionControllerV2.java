package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.controllerv2.base.CrudController;
import com.kinglogi.dto.filter.AccountFilter;
import com.kinglogi.dto.filter.PromotionFillter;
import com.kinglogi.dto.promotion.PromotionRequest;
import com.kinglogi.dto.promotion.PromotionResponse;
import com.kinglogi.dto.response.AccountResponse;
import com.kinglogi.service.PromotionService;
import com.kinglogi.service.impl.PromotionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/promotions")
public class PromotionControllerV2 extends CrudController<PromotionRequest, PromotionResponse, Long, PromotionFillter> {

    private final PromotionServiceImpl promotionService;

    @Autowired
    public PromotionControllerV2(PromotionService service, PromotionServiceImpl promotionService) {
        super(service);
        this.promotionService = promotionService;
    }

    @GetMapping("/user")
    @ResponseStatus(code = HttpStatus.OK)
    public Page<PromotionResponse> getPromotionsOfUser(PromotionFillter promotionFillter){
        return promotionService.getPromotionOfUser(promotionFillter);
    }

    @PutMapping("/{id}/users/{userId}/reactive")
    @ResponseStatus(code = HttpStatus.OK)
    public PromotionResponse reactivePromotion(@PathVariable long id, @PathVariable long userId){
        return promotionService.reactivePromotion(id, userId);
    }

    @DeleteMapping("/{id}/users/{userId}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Boolean deleteUserPromotion(@PathVariable long id, @PathVariable long userId){
        return promotionService.deleteUserPromotion(id, userId);
    }

    @GetMapping("/code")
    @ResponseStatus(code = HttpStatus.OK)
    public PromotionResponse getPromotionByCode(@RequestParam("code") String code){
        return promotionService.findByCode(code);
    }

    @GetMapping("/accounts")
    @ResponseStatus(code = HttpStatus.OK)
    public Page<AccountResponse> getAccounts(AccountFilter accountFilter){
        return promotionService.getAccounts(accountFilter);
    }
}
