package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.request.ExpoPushNotificationRequest;
import com.kinglogi.dto.request.SubscribePushNotificationRequest;
import com.kinglogi.service.PushNotificationService;
import com.kinglogi.service.PushNotificationTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/push-notifications")
public class PushNotificationControllerV2 {

    private final PushNotificationService service;
    private final PushNotificationTokenService tokenService;

    @Autowired
    public PushNotificationControllerV2(
            @Qualifier("expoPushNotificationService") PushNotificationService<ExpoPushNotificationRequest> service,
            PushNotificationTokenService tokenService) {
        this.service = service;
        this.tokenService = tokenService;
    }

    @PostMapping("/subscribe")
    @ResponseStatus(code = HttpStatus.CREATED)
    public Boolean send(@Valid @RequestBody SubscribePushNotificationRequest request) {
        tokenService.subscribe(request);
        return true;
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public Boolean send(@Valid @RequestBody ExpoPushNotificationRequest request) {
        service.sendPushNotificationAsync(request);
        return true;
    }
}
