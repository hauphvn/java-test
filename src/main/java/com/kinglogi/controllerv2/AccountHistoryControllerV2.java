package com.kinglogi.controllerv2;


import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.account_history.AccountActivityOutputDTO;
import com.kinglogi.dto.account_history.AccountHistoryOutputDTO;
import com.kinglogi.dto.filter.AccountHistoryFilter;
import com.kinglogi.dto.filter.AccountLoginFilter;
import com.kinglogi.dto.response.AccountResponse;
import com.kinglogi.service.AccountHistoryService;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/account-histories")
public class AccountHistoryControllerV2 {
    public AccountHistoryControllerV2(AccountHistoryService accountHistoryService) {
        this.accountHistoryService = accountHistoryService;
    }

    private final AccountHistoryService accountHistoryService;

    @GetMapping("/{accountId}")
    public Page<AccountHistoryOutputDTO> findAllAccountHistoryOfAccount(AccountHistoryFilter filter, @PathVariable Long accountId) {
        return this.accountHistoryService.findAllAccountHistoryOfAccount(filter, accountId);
    }

    @GetMapping("")
    public Page<AccountActivityOutputDTO> findAllActivityOfAccounts(AccountLoginFilter filter){
        return this.accountHistoryService.findAllActivityOfAccounts(filter);
    }

    @GetMapping("/excel")
    public InputStreamResource exportAccountHistory(@RequestParam Long accountId) {
        return this.accountHistoryService.exportAccountHistory(accountId);
    }
}
