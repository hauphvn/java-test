package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.controllerv2.base.CrudController;
import com.kinglogi.dto.filter.TripCatalogFilter;
import com.kinglogi.dto.request.TripCatalogRequest;
import com.kinglogi.dto.response.TripCatalogResponse;
import com.kinglogi.service.TripCatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/trip-catalogs")
public class TripCatalogControllerV2 extends CrudController<TripCatalogRequest, TripCatalogResponse, Long, TripCatalogFilter> {

    @Autowired
    public TripCatalogControllerV2(TripCatalogService service) {
        super(service);
    }
}
