package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.controllerv2.base.CrudController;
import com.kinglogi.dto.filter.TripFilter;
import com.kinglogi.dto.request.TripRequest;
import com.kinglogi.dto.response.TripResponse;
import com.kinglogi.entity.Trip;
import com.kinglogi.service.TripService;
import org.hibernate.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/trips")
public class TripControllerV2 extends CrudController<TripRequest, TripResponse, Long, TripFilter> {

	@Autowired
	private TripService tripService;
	
    @Autowired
    public TripControllerV2(TripService service) {
        super(service);
    }
    
    @GetMapping("/{id}/trip")
    public TripResponse getByParentTrip(@PathVariable Long id) {
        return tripService.getTripByParentTripId(id);
    }

    @GetMapping("/picking-up-locations")
    public Page<String> getPickingUpLocations(@RequestParam String pickingUpLocation, String language, Pageable pageable){
        return tripService.getPickingUpLocations(pickingUpLocation, language, pageable);
    }

    @GetMapping("/dropping-off-locations")
    public Page<TripResponse> getDroppingOffLocations(@RequestParam String pickingUpLocation, String droppingOffLocation, String language, Integer numberOfSeat, Pageable pageable){
        return tripService.getDroppingOffLocations(pickingUpLocation, language, droppingOffLocation, numberOfSeat, pageable);
    }

    @PostMapping("/import")
    public void importTrip(@RequestParam("excel") MultipartFile file) throws IOException, ParseException {
        tripService.importTrip(file);
    }
}
