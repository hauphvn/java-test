package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.filter.OrderHistoryChangeFilter;
import com.kinglogi.dto.order_history_change.OrderHistoryChangeOutputDTO;
import com.kinglogi.service.OrderHistoryChangeService;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/order-histories-change")
public class OrderHistoryChangeControllerV2 {
    private final OrderHistoryChangeService orderHistoryChangeService;

    public OrderHistoryChangeControllerV2(OrderHistoryChangeService orderHistoryChangeService) {
        this.orderHistoryChangeService = orderHistoryChangeService;
    }

    @GetMapping("")
    public Page<OrderHistoryChangeOutputDTO> findAllOrderHistoryChange(OrderHistoryChangeFilter filter) {
        return this.orderHistoryChangeService.findAllOrderHistoryChange(filter);
    }
}
