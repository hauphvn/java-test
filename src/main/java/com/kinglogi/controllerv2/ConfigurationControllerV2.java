package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.KlConfiguration;
import com.kinglogi.service.KlMetadataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/configurations")
public class ConfigurationControllerV2 {

    private final KlMetadataService metadataService;

    @Autowired
    public ConfigurationControllerV2(KlMetadataService metadataService) {
        this.metadataService = metadataService;
    }

    @GetMapping
    public KlConfiguration getConfiguration() {
        return metadataService.getConfiguration();
    }

    @PutMapping
    public boolean getConfiguration(@RequestBody @Valid KlConfiguration configuration) {
        return metadataService.updateConfiguration(configuration);
    }
}
