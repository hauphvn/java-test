package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.controllerv2.base.CrudController;
import com.kinglogi.dto.filter.AccountFilter;
import com.kinglogi.dto.filter.base.BaseFilter;
import com.kinglogi.dto.request.AccountRequest;
import com.kinglogi.dto.response.AccountResponse;
import com.kinglogi.service.AdminService;
import com.kinglogi.service.AgentAdminService;
import com.kinglogi.service.CollaboratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/agent-admins/collaborators-accounts")
public class AgentAdminControllerV2 extends CrudController<AccountRequest, AccountResponse, Long, AccountFilter> {
    @Autowired
    public AgentAdminControllerV2(AgentAdminService service) {
        super(service);
    }
}
