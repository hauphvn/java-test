package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.controllerv2.base.CrudController;
import com.kinglogi.dto.filter.PartnerFilter;
import com.kinglogi.dto.request.PartnerRequest;
import com.kinglogi.dto.response.PartnerResponse;
import com.kinglogi.service.PartnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/partners")
public class PartnerControllerV2 extends CrudController<PartnerRequest, PartnerResponse, Long, PartnerFilter> {

    @Autowired
    public PartnerControllerV2(PartnerService service) {
        super(service);
    }
}
