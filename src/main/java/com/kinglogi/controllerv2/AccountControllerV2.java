package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.controllerv2.base.CrudController;
import com.kinglogi.dto.filter.AccountFilter;
import com.kinglogi.dto.request.AccountRequest;
import com.kinglogi.dto.response.AccountResponse;
import com.kinglogi.service.AccountService;
import com.kinglogi.service.impl.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/accounts")
public class AccountControllerV2 extends CrudController<AccountRequest, AccountResponse, Long, AccountFilter> {

    private final UserDetailsServiceImpl userDetailsService;

    @Autowired
    public AccountControllerV2(@Qualifier("accountService") AccountService service, UserDetailsServiceImpl userDetailsService) {
        super(service);
        this.userDetailsService = userDetailsService;
    }

    @GetMapping("/user-details/{username}")
    @ResponseStatus(code = HttpStatus.OK)
    public UserDetails getUserDetails(@PathVariable String username) {
        return userDetailsService.loadUserByUsername(username);
    }
}
