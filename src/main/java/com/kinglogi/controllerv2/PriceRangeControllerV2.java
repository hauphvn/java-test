package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.filter.PriceRangeFilter;
import com.kinglogi.dto.price_range.PriceRangeInputDTO;
import com.kinglogi.dto.price_range.PriceRangeOutputDTO;
import com.kinglogi.service.PriceRangeService;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/price-ranges")
public class PriceRangeControllerV2 {

    public PriceRangeControllerV2(PriceRangeService priceRangeService) {
        this.priceRangeService = priceRangeService;
    }

    private final PriceRangeService priceRangeService;

    @PostMapping
    public PriceRangeOutputDTO create(@RequestBody PriceRangeInputDTO input) {
        return priceRangeService.create(input);
    }

    @PutMapping("/{priceRangeId}")
    public PriceRangeOutputDTO update(@PathVariable Long priceRangeId,@RequestBody PriceRangeInputDTO input) {
        return priceRangeService.update(priceRangeId, input);
    }

    @DeleteMapping("/{priceRangeId}")
    public void delete(@PathVariable Long priceRangeId) {
        priceRangeService.delete(priceRangeId);
    }

    @GetMapping
    public Page<PriceRangeOutputDTO> findAll(PriceRangeFilter priceRangeFilter) {
        return priceRangeService.findAll(priceRangeFilter);
    }

    @GetMapping("/{priceRangeId}")
    public PriceRangeOutputDTO findOne(@PathVariable Long priceRangeId) {
        return priceRangeService.findOne(priceRangeId);
    }

    @GetMapping("/check")
    public PriceRangeOutputDTO findByDroppingOffLocation(@RequestParam String goAreaMetadata, String destinationAreaMetadata) {
        return priceRangeService.findByDroppingOffLocation(goAreaMetadata, destinationAreaMetadata);
    }

}
