package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.filter.PartnerLinkFilter;
import com.kinglogi.dto.partner_link.PartnerLinkInputDTO;
import com.kinglogi.dto.partner_link.PartnerLinkOutputDTO;
import com.kinglogi.service.PartnerLinkService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URLConnection;
import java.util.List;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/partner-links")
public class PartnerLinkControllerV2 {
    private final PartnerLinkService partnerLinkService;

    public PartnerLinkControllerV2(PartnerLinkService partnerLinkService) {
        this.partnerLinkService = partnerLinkService;
    }

    @PostMapping("")
    @ResponseStatus(code = HttpStatus.CREATED)
    public PartnerLinkOutputDTO save(@RequestBody PartnerLinkInputDTO input) {
        return this.partnerLinkService.save(input);
    }

    @PutMapping("/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public PartnerLinkOutputDTO update(@RequestBody PartnerLinkInputDTO input, @PathVariable Long id) {
        return this.partnerLinkService.update(input, id);
    }

    @PutMapping("/{id}/logos")
    @ResponseStatus(code = HttpStatus.OK)
    public void updateLogo(@RequestParam MultipartFile logo, @PathVariable Long id) throws IOException {
        this.partnerLinkService.updateLogo(logo, id);
    }

    @PutMapping("/{id}/adImages")
    @ResponseStatus(code = HttpStatus.OK)
    public void updateAdImage(@RequestParam MultipartFile adImage, @PathVariable Long id) throws IOException {
        this.partnerLinkService.updateAdImage(adImage, id);
    }

    @GetMapping("")
    @ResponseStatus(code = HttpStatus.OK)
    public Page<PartnerLinkOutputDTO> findAll(PartnerLinkFilter filter) {
        return this.partnerLinkService.findAll(filter);
    }

    @GetMapping("/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public PartnerLinkOutputDTO findOne(@PathVariable Long id) {
        return this.partnerLinkService.findOne(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        this.partnerLinkService.delete(id);
    }


    @GetMapping("/{id}/images/{fileName}")
    public ResponseEntity<InputStreamResource> viewImage(@PathVariable Long id, @PathVariable String fileName) throws FileNotFoundException {
        InputStreamResource inputStreamResource = partnerLinkService.viewImage(id, fileName);
        String mimeType = URLConnection.guessContentTypeFromName(fileName);
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(mimeType))
                .body(inputStreamResource);
    }

    @GetMapping("/ads")
    public List<PartnerLinkOutputDTO> ads() {
        return this.partnerLinkService.ads();
    }

}
