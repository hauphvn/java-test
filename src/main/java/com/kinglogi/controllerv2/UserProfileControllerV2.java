package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.filter.OrderFilter;
import com.kinglogi.dto.request.UserProfileRequest;
import com.kinglogi.dto.response.AccountResponse;
import com.kinglogi.dto.response.OrderResponse;
import com.kinglogi.dto.response.TransactionResponse;
import com.kinglogi.service.UserProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;
import java.time.Instant;
import java.util.List;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/profile")
public class UserProfileControllerV2 {

    private final UserProfileService userProfileService;

    @Autowired
    public UserProfileControllerV2(UserProfileService userProfileService) {
        this.userProfileService = userProfileService;
    }

    @GetMapping
    public AccountResponse getProfile() {
        return userProfileService.getProfile();
    }

    @PutMapping("")
    public AccountResponse updateProfile(@Valid @RequestBody UserProfileRequest request) {
        return userProfileService.updateProfile(request);
    }

    @GetMapping("/transactions")
    public List<TransactionResponse> getTransactionsByAccountId() {
        return userProfileService.getTransactionsByAccountId();
    }

    @GetMapping("/orders")
    public Page<OrderResponse> getOrdersByAccountId(OrderFilter orderFilter) {
        return userProfileService.getOrdersByAccountId(orderFilter);
    }

    @GetMapping("order/export")
    public ResponseEntity<Resource> exportOrders() throws IOException {
        String path = userProfileService.exportOrder();
        Resource resource = new FileSystemResource(path);
        System.out.println(resource.contentLength());
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"orders.xlsx\"")
                .contentLength(resource.contentLength())
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(resource);
    }

    @PostMapping("/avatars")
    public void uploadImages(@RequestParam(name = "file")MultipartFile file) throws IOException {
        userProfileService.upLoadAvatar(file);
    }

    @GetMapping(value = "/avatars/{id}")
    public ResponseEntity<InputStreamResource> viewImage(@PathVariable Long id) {
        InputStreamResource inputStreamResource = userProfileService.viewImage(id);
        return ResponseEntity.ok()
                .contentType(MediaType.IMAGE_JPEG)
                .body(inputStreamResource);
    }

    @GetMapping(value = "/avatars")
    public ResponseEntity<InputStreamResource> viewImage() {
        InputStreamResource inputStreamResource = userProfileService.viewImage();
        return ResponseEntity.ok()
                .contentType(MediaType.IMAGE_JPEG)
                .body(inputStreamResource);
    }

    @PutMapping("/phone-number")
    public AccountResponse updatePhoneNumber(@RequestBody UserProfileRequest request, String otp) {
        return userProfileService.updatePhoneNumber(request, otp);
    }

    @PostMapping("/phone-number/sms")
    public Instant sendOtpForUpdatePhoneNumber(@RequestBody UserProfileRequest request) throws IOException {
        return userProfileService.sendOtpForUpdatePhoneNumber(request);
    }

    @DeleteMapping("")
    public void deleteAccount(){
        userProfileService.deleteAccount();
    }

}
