package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.bid.BidAuctionCreateInputDTO;
import com.kinglogi.dto.bid.BidAuctionUpdateInputDTO;
import com.kinglogi.dto.response.BidResponse;
import com.kinglogi.service.BidService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/bids")
public class BidControllerV2 {

    private final BidService bidService;

    @Autowired
    public BidControllerV2(BidService bidService) {
        this.bidService = bidService;
    }

    @PostMapping("")
    public BidResponse bidAuction(@RequestBody BidAuctionCreateInputDTO input) {
        return bidService.bid(input);
    }

    @DeleteMapping("/{id}")
    public void cancelBidOfAnOrder(@PathVariable Long id) {
        bidService.cancelBidReserve(id);
    }
}
