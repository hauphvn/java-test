package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.controllerv2.base.CrudController;
import com.kinglogi.dto.filter.CarGroupFilter;
import com.kinglogi.dto.request.CarGroupRequest;
import com.kinglogi.dto.response.CarGroupResponse;
import com.kinglogi.service.CarGroupService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.FileNotFoundException;
import java.net.URLConnection;
import java.util.List;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/car-groups")
public class CarGroupControllerV2 {

    private final CarGroupService carGroupService;

    @Autowired
    public CarGroupControllerV2(CarGroupService carGroupService1) {
        this.carGroupService = carGroupService1;
    }

    @PostMapping("")
    @ResponseStatus(code = HttpStatus.CREATED)
    public CarGroupResponse save(@RequestBody CarGroupRequest carGroupRequest) {
        return carGroupService.save(carGroupRequest);
    }

    @PutMapping("/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public CarGroupResponse update(@PathVariable Long id , @RequestBody CarGroupRequest carGroupRequest) {
        return carGroupService.update(id, carGroupRequest);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        carGroupService.deleteById(id);
    }

    @GetMapping("")
    @ResponseStatus(code = HttpStatus.OK)
    public Page<CarGroupResponse> getAll(CarGroupFilter carGroupFilter) {
        return carGroupService.findAll(carGroupFilter);
    }

    @GetMapping("/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public CarGroupResponse getDetail(@PathVariable Long id) {
        return carGroupService.findById(id);
    }

    @PostMapping("{id}/images")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void uploadFile(@PathVariable Long id, @RequestParam(name = "files") List<MultipartFile> multipartFiles) {
        carGroupService.uploadImage(id, multipartFiles);
    }

    @GetMapping("/{id}/images/{fileName}")
    public ResponseEntity<InputStreamResource> viewImage(@PathVariable Long id, @PathVariable String fileName) throws FileNotFoundException {
        InputStreamResource inputStreamResource = carGroupService.viewImage(id, fileName);
        String mimeType = URLConnection.guessContentTypeFromName(fileName);
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(mimeType))
                .body(inputStreamResource);
    }

    @DeleteMapping("/{id}/images/{fileName}")
    public void deleteImage(@PathVariable Long id, @PathVariable String fileName) {
        carGroupService.deleteImage(id, fileName);
    }
}
