package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.commission.CommissionCreateInputDTO;
import com.kinglogi.dto.commission.CommissionOutputDTO;
import com.kinglogi.dto.commission.CommissionUpdateInputDTO;
import com.kinglogi.dto.filter.CommissionFilter;
import com.kinglogi.dto.response.AccountResponse;
import com.kinglogi.service.CommissionService;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/commissions")
public class CommissionControllerV2 {
    private final CommissionService commissionService;

    public CommissionControllerV2(CommissionService commissionService) {
        this.commissionService = commissionService;
    }

    @PostMapping("")
    public CommissionOutputDTO save(@RequestBody CommissionCreateInputDTO createInput) {
        return this.commissionService.save(createInput);
    }

    @PutMapping("/{id}")
    public CommissionOutputDTO update(@PathVariable Long id, @RequestBody CommissionUpdateInputDTO updateInput) {
        return this.commissionService.update(id, updateInput);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        this.commissionService.delete(id);
    }

    @PutMapping("/{id}/accounts/{accountId}/remove")
    public void removeCollaboratorFromCommission(@PathVariable Long id, @PathVariable Long accountId) {
        this.commissionService.removeCollaboratorFromCommission(accountId, id);
    }

    @GetMapping("/accounts/notInCommission")
    @ResponseStatus(code = HttpStatus.OK)
    public Page<AccountResponse> getCollaboratorNotInCommission(CommissionFilter commissionFilter) {
        return commissionService.getAccountNotInCommission(commissionFilter);
    }

    @GetMapping("")
    @ResponseStatus(code = HttpStatus.OK)
    public Page<CommissionOutputDTO> findAll(CommissionFilter commissionFilter) {
        return commissionService.findAll(commissionFilter);
    }

    @GetMapping("/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public CommissionOutputDTO findOne(@PathVariable Long id) {
        return commissionService.findOne(id);
    }

    @GetMapping("/{id}/accounts")
    @ResponseStatus(code = HttpStatus.OK)
    public Page<AccountResponse> findCollaboratorInCommission(CommissionFilter commissionFilter, @PathVariable Long id) {
        return commissionService.getCollaboratorInCommission(commissionFilter, id);
    }
}
