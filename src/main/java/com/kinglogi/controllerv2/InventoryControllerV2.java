package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.filter.AccountInventoryOrderFilter;
import com.kinglogi.dto.filter.InventoryFilter;
import com.kinglogi.dto.filter.InventoryHistoryFilter;
import com.kinglogi.dto.inventory.AccountApplyInventoryOutput;
import com.kinglogi.dto.inventory.InventoryInputDTO;
import com.kinglogi.dto.inventory.InventoryOutputDTO;
import com.kinglogi.dto.inventory_history.InventoryHistoryOutPutDTO;
import com.kinglogi.service.InventoryService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/inventories")
public class InventoryControllerV2 {
    private final InventoryService inventoryService;

    public InventoryControllerV2(InventoryService inventoryService) {
        this.inventoryService = inventoryService;
    }

    @PostMapping("")
    public InventoryOutputDTO save(@RequestBody InventoryInputDTO input) {
        return this.inventoryService.save(input);
    }

    @PutMapping("/{id}")
    public InventoryOutputDTO update(@RequestBody InventoryInputDTO input, @PathVariable Long id) {
        return this.inventoryService.update(input, id);
    }

    @GetMapping("")
    public Page<InventoryOutputDTO> findAll(InventoryFilter filter) {
        return this.inventoryService.findAll(filter);
    }

    @GetMapping("/{id}/histories")
    public Page<InventoryHistoryOutPutDTO> findAllHistories(InventoryHistoryFilter filter, @PathVariable Long id) {
        return this.inventoryService.findAllHistoryOfInventory(filter, id);
    }

    @GetMapping("/{id}")
    public InventoryOutputDTO findOne(@PathVariable Long id) {
        return this.inventoryService.findOne(id);
    }

    @GetMapping("/validTrip")
    public InventoryOutputDTO checkValidTrip(@RequestParam String pickingUpLocation,@RequestParam String droppingOffLocation,@RequestParam Integer numberOfSeat, @RequestParam(required = false) Long tripId) {
        return this.inventoryService.checkIfInventoryValid(pickingUpLocation, droppingOffLocation, numberOfSeat, tripId);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        this.inventoryService.delete(id);
    }

    @GetMapping("/{id}/account")
    public Page<AccountApplyInventoryOutput> getAccountsApplyInventory(@PathVariable long id, AccountInventoryOrderFilter filter){
        return inventoryService.getAccountsApplyInventory(id, filter);
    }
}
