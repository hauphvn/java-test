package com.kinglogi.controllerv2.base;

import com.kinglogi.service.base.MasterDataService;
import lombok.Getter;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

public class MasterDataController<R> extends BaseController {

    @Getter
    private final MasterDataService<R> masterDataService;

    public MasterDataController(MasterDataService masterDataService) {
        this.masterDataService = masterDataService;
    }

    @GetMapping
    public List<R> getAll() {
        return masterDataService.findAll();
    }
}
