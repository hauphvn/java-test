package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.controllerv2.base.CrudController;
import com.kinglogi.dto.filter.AccountFilter;
import com.kinglogi.dto.request.AccountRequest;
import com.kinglogi.dto.response.AccountResponse;
import com.kinglogi.service.AgentAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/sub-agent-accounts")
public class SubAgentAdminControllerV2 extends CrudController<AccountRequest, AccountResponse, Long, AccountFilter> {
    @Autowired
    public SubAgentAdminControllerV2(@Qualifier("AgentAdminService") AgentAdminService service) {
        super(service);
    }

    @Override
    @GetMapping
    public Page<AccountResponse> getAll(AccountFilter baseFilter) {
        return ((AgentAdminService) getCrudService()).findSubAgent(baseFilter);
    }
}
