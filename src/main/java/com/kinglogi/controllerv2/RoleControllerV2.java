package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.controllerv2.base.MasterDataController;
import com.kinglogi.dto.response.RoleResponse;
import com.kinglogi.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/roles")
public class RoleControllerV2 extends MasterDataController<RoleResponse> {

    @Autowired
    public RoleControllerV2(RoleService service) {
        super(service);
    }
}
