package com.kinglogi.controllerv2.publish;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.request.PartnerFixedTripOrderRequest;
import com.kinglogi.dto.request.PartnerFlexibleTripOrderRequest;
import com.kinglogi.dto.request.PartnerFlexibleTripOrderRequestFromIframe;
import com.kinglogi.dto.request.PartnerOrderUpdateRequest;
import com.kinglogi.dto.response.OrderResponse;
import com.kinglogi.dto.response.PartnerOrderResponse;
import com.kinglogi.dto.response.PartnerPaymentResponse;
import com.kinglogi.service.PartnerOrderService;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.Map;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/partner-orders")
public class PartnerOrderControllerV2 {

    private final PartnerOrderService service;

    @Autowired
    public PartnerOrderControllerV2(PartnerOrderService service) {
        this.service = service;
    }

    @PostMapping("fixed-route")
    @ResponseStatus(code = HttpStatus.CREATED)
    public PartnerOrderResponse createFixedTripOrderFromApi(@Valid @RequestBody PartnerFixedTripOrderRequest request) {
        return service.createOrder(request);
    }

    @PostMapping("flexible-route")
    @ResponseStatus(code = HttpStatus.CREATED)
    public PartnerOrderResponse createFlexibleTripOrderFromApi(@Valid @RequestBody PartnerFlexibleTripOrderRequest request) {
        return service.createOrder(request);
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public PartnerOrderResponse createFlexibleTripOrderFromIframe(@Valid @RequestBody PartnerFlexibleTripOrderRequestFromIframe request) {
        return service.createOrder(request);
    }

    @PutMapping
    public PartnerPaymentResponse verifyPayment(@RequestParam Map<String, Object> allParams) {
        return service.verifyPayment(allParams);
    }

    @PutMapping("/order")
    public OrderResponse updateOrder(@RequestBody PartnerOrderUpdateRequest input){
        return service.updateOrder(input);
    }

    @GetMapping("/order")
    public Page<OrderResponse> getOrders(@RequestHeader @NotNull Long appClientId, @RequestHeader String appClientSecret, Pageable pageable){
        return service.getOrders(appClientId, appClientSecret, pageable);
    }

    @GetMapping("/order/{orderCode}")
    public OrderResponse getOrder(@RequestHeader @NotNull Long appClientId, @RequestHeader String appClientSecret, @PathVariable(name = "orderCode") String orderCode){
        return service.getOrder(appClientId, appClientSecret, orderCode);
    }

}
