package com.kinglogi.controllerv2.publish;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.request.*;
import com.kinglogi.dto.response.JwtAuthenticationToken;
import com.kinglogi.service.AuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Nullable;
import javax.validation.Valid;
import java.io.IOException;
import java.time.Instant;

@Slf4j
@RestController
@RequestMapping(value = ApiVersion.API_V2 + "/auth")
public class AuthControllerV2 {

    private final AuthService authService;

    @Autowired
    public AuthControllerV2(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("/sign-in")
    @ResponseStatus(code = HttpStatus.OK)
    public JwtAuthenticationToken login(@Valid @RequestBody LoginRequest request) {
        return authService.login(request);
    }

    @GetMapping("/sign-in/sms")
    @ResponseStatus(code = HttpStatus.OK)
    public Instant loginSendOtp(@RequestParam String phoneNumber) throws IOException {
        return authService.loginSendOtp(phoneNumber);
    }

    @PostMapping("/sign-in/sms")
    @ResponseStatus(code = HttpStatus.OK)
    public JwtAuthenticationToken loginWithOtp(@RequestParam String otp, String phoneNumber) {
        return authService.loginWithOtp(otp, phoneNumber);
    }

    @PostMapping("/sign-up")
    @ResponseStatus(code = HttpStatus.OK)
    public JwtAuthenticationToken register(@Valid @RequestBody RegisterRequest request, @Nullable @RequestParam String otp ) {
        return authService.register(request, otp);
    }

    @PostMapping("/sign-up/sms")
    @ResponseStatus(code = HttpStatus.OK)
    public Instant registerBySms(@Valid @RequestBody RegisterRequest request) throws IOException {
        return authService.registerBySms(request);
    }

    // TODO Will be removed. It's just for apple reviewing
    @PostMapping("/partners/sign-up")
    @ResponseStatus(code = HttpStatus.OK)
    public String partnerRegister(@Valid @RequestBody RegisterRequest request) {
        request.setActive(false);
        log.info("Partner registered with data={}", request);
        authService.registerByPartner(request);
        return "Đã đăng ký thành công. Vui lòng chờ kết quả phê duyệt từ KING Logi để được sử dụng.";
    }

    @PostMapping("/refresh")
    @ResponseStatus(code = HttpStatus.OK)
    public JwtAuthenticationToken refreshToken(@Valid @RequestBody RefreshTokenRequest request) {
        return authService.refreshToken(request);
    }

    @PostMapping("/forgot-password")
    @ResponseStatus(code = HttpStatus.OK)
    public Boolean forgotPassword(@Valid @RequestBody ForgotPasswordRequest request) {
        return authService.forgotPassword(request);
    }

    @PutMapping("/change-password")
    @ResponseStatus(code = HttpStatus.OK)
    public JwtAuthenticationToken changePassword(@Valid @RequestBody ChangePasswordRequest request) {
        return authService.changePassword(request);
    }

    @GetMapping("/logout")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void logout(@RequestParam String deviceId, @RequestParam String appName) {
        authService.logout(deviceId, appName);
    }

}
