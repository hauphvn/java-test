package com.kinglogi.controllerv2.publish;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.request.ContactFormRequest;
import com.kinglogi.service.SupportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/supports")
public class SupportControllerV2 {

    private final SupportService service;

    @Autowired
    public SupportControllerV2(SupportService service) {
        this.service = service;
    }

    @PostMapping("/email")
    @ResponseStatus(code = HttpStatus.CREATED)
    public Boolean createOrder(@Valid @RequestBody ContactFormRequest request) {
        return service.supportFromContactForm(request);
    }
}
