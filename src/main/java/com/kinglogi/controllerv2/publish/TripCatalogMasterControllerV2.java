package com.kinglogi.controllerv2.publish;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.controllerv2.base.MasterDataController;
import com.kinglogi.dto.response.TripCatalogResponse;
import com.kinglogi.service.TripCatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/publish/trip-catalogs")
public class TripCatalogMasterControllerV2 extends MasterDataController<TripCatalogResponse> {

    @Autowired
    public TripCatalogMasterControllerV2(TripCatalogService service) {
        super(service);
    }
}
