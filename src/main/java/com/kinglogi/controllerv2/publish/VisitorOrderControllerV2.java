package com.kinglogi.controllerv2.publish;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.request.OrderRequest;
import com.kinglogi.dto.response.OrderWithPaymentResponse;
import com.kinglogi.dto.response.PaymentResponse;
import com.kinglogi.dto.response.PaymentUrlResponse;
import com.kinglogi.service.OrderService;
import com.kinglogi.service.VisitorOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/visitor-orders")
public class VisitorOrderControllerV2 {

    private final VisitorOrderService service;

    @Autowired
    public VisitorOrderControllerV2(VisitorOrderService service) {
        this.service = service;
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public OrderWithPaymentResponse createOrder(@Valid @RequestBody OrderRequest request) {
        return service.createOrder(request);
    }

    @PutMapping
    public PaymentResponse verifyPayment(@RequestParam Map<String, Object> allParams) {
        return service.verifyPayment(allParams);
    }

    @PutMapping("/{id}/cancel")
    public void cancel(@PathVariable Long id) {
        service.cancel(id);
    }

    @PostMapping("/pay/{orderCode}")
    public OrderWithPaymentResponse generate(@PathVariable String orderCode) {
        return service.generate(orderCode);
    }
}
