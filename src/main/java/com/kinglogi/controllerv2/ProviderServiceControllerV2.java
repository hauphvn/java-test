package com.kinglogi.controllerv2;

import com.kinglogi.constant.ApiVersion;
import com.kinglogi.dto.filter.CarGroupFilter;
import com.kinglogi.dto.filter.ProviderServiceFilter;
import com.kinglogi.dto.request.CarGroupRequest;
import com.kinglogi.dto.request.ProviderServiceRequest;
import com.kinglogi.dto.response.CarGroupResponse;
import com.kinglogi.dto.response.ProviderServiceResponse;
import com.kinglogi.service.ProviderServiceService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.FileNotFoundException;
import java.net.URLConnection;
import java.util.List;

@RestController
@RequestMapping(ApiVersion.API_V2 + "/services")
public class ProviderServiceControllerV2 {
    private final ProviderServiceService providerServiceService;

    public ProviderServiceControllerV2(ProviderServiceService providerServiceService) {
        this.providerServiceService = providerServiceService;
    }


    @PostMapping("")
    @ResponseStatus(code = HttpStatus.CREATED)
    public ProviderServiceResponse save(@RequestBody ProviderServiceRequest providerServiceRequest) {
        return providerServiceService.save(providerServiceRequest);
    }

    @PutMapping("/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public ProviderServiceResponse update(@PathVariable Long id , @RequestBody ProviderServiceRequest providerServiceRequest) {
        return providerServiceService.update(id, providerServiceRequest);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        providerServiceService.deleteById(id);
    }

    @GetMapping("")
    @ResponseStatus(code = HttpStatus.OK)
    public Page<ProviderServiceResponse> getAll(ProviderServiceFilter providerServiceFilter) {
        return providerServiceService.findAll(providerServiceFilter);
    }

    @GetMapping("/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public ProviderServiceResponse getDetail(@PathVariable Long id) {
        return providerServiceService.findById(id);
    }

    @PostMapping("{id}/images")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void uploadFile(@PathVariable Long id, @RequestParam(name = "files") List<MultipartFile> multipartFiles) {
        providerServiceService.uploadImage(id, multipartFiles);
    }

    @GetMapping("/{id}/images/{fileName}")
    public ResponseEntity<InputStreamResource> viewImage(@PathVariable Long id, @PathVariable String fileName) throws FileNotFoundException {
        InputStreamResource inputStreamResource = providerServiceService.viewImage(id, fileName);
        String mimeType = URLConnection.guessContentTypeFromName(fileName);
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(mimeType))
                .body(inputStreamResource);
    }

    @DeleteMapping("/{id}/images/{fileName}")
    public void deleteImage(@PathVariable Long id, @PathVariable String fileName) {
        providerServiceService.deleteImage(id, fileName);
    }
}
