package com.kinglogi.service;

import com.kinglogi.dto.request.TransactionRequest;
import com.kinglogi.dto.response.TransactionResponse;

import java.util.List;

public interface TransactionService {

    List<TransactionResponse> getTransactions(Long targetId, Integer transactionType);

    List<TransactionResponse> findByAccountId(Long id);

    Long getBalanceByAccountId(Long id);

    TransactionResponse save(TransactionRequest request);

    TransactionResponse getDetail(Long id);
}
