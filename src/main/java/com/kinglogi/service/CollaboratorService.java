package com.kinglogi.service;

import com.kinglogi.dto.request.AccountRequest;
import com.kinglogi.dto.response.AccountResponse;

public interface CollaboratorService extends AccountService {
}
