package com.kinglogi.service;

import com.kinglogi.dto.request.PaymentRequest;
import com.kinglogi.dto.response.PaymentResponse;
import com.kinglogi.dto.response.PaymentUrlResponse;
import com.kinglogi.vnpay.VnPayRequest;

import java.util.Map;

public interface VnPayService {

    PaymentResponse verifyPayment(Map<String, Object> allParams);

    PaymentUrlResponse generateVnPayRequestUrl(PaymentRequest paymentRequest);

    VnPayRequest buildVnPayRequest(PaymentRequest paymentRequest);

    PaymentUrlResponse generateVnPayRequestUrl(VnPayRequest vnPayRequest);
}
