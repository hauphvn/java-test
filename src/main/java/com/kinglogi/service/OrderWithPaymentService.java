package com.kinglogi.service;

import com.kinglogi.dto.request.OrderRequest;
import com.kinglogi.dto.response.OrderWithPaymentResponse;
import com.kinglogi.dto.response.PaymentResponse;
import com.kinglogi.dto.response.PaymentUrlResponse;

import java.util.Map;

public interface OrderWithPaymentService {

    OrderWithPaymentResponse createOrder(OrderRequest request);

    PaymentResponse verifyPayment(Map<String, Object> allParams);

    OrderWithPaymentResponse paymentForOrder(String orderCode);

    OrderWithPaymentResponse createOrderForPartner(OrderRequest orderRequest);
}
