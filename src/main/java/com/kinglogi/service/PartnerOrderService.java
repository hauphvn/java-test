package com.kinglogi.service;

import com.kinglogi.dto.request.PartnerOrderRequest;
import com.kinglogi.dto.request.PartnerOrderUpdateRequest;
import com.kinglogi.dto.response.OrderResponse;
import com.kinglogi.dto.response.PartnerOrderResponse;
import com.kinglogi.dto.response.PartnerPaymentResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.util.Map;

public interface PartnerOrderService {

    PartnerOrderResponse createOrder(PartnerOrderRequest request);

    PartnerPaymentResponse verifyPayment(Map<String, Object> allParams);

    OrderResponse updateOrder(PartnerOrderUpdateRequest input);

    Page<OrderResponse> getOrders(Long appClientId, String appClientSecret, Pageable pageable);

    OrderResponse getOrder(Long appClientId, String appClientSecret, String orderCode);

}
