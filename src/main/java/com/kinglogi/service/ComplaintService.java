package com.kinglogi.service;

import com.kinglogi.dto.filter.base.BaseFilter;
import com.kinglogi.dto.request.AccountRequest;
import com.kinglogi.dto.request.ComplaintRequest;
import com.kinglogi.dto.response.AccountResponse;
import com.kinglogi.dto.response.ComplaintResponse;
import com.kinglogi.service.base.CrudService;
import com.kinglogi.validator.EntityExists;
import com.kinglogi.validator.FieldValueExists;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

public interface ComplaintService extends CrudService<ComplaintRequest, ComplaintResponse, Long> {
    Page<ComplaintResponse> findByUserId(String keyword, BaseFilter baseFilter);

    void uploadFiles(Long id, List<MultipartFile> multipartFiles);

    FileInputStream viewFile(Long id, String fileName) throws FileNotFoundException;

    ComplaintResponse changeStatus(Long id, ComplaintRequest complaintRequest);

    void response(Long id, ComplaintRequest complaintRequest);

    void deleteImage(Long id, String fileName);
}
