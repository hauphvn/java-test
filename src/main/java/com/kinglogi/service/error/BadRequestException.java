package com.kinglogi.service.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintDeclarationException;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class BadRequestException extends ConstraintDeclarationException {
    public BadRequestException(String message) {
        super(message);
    }
}


