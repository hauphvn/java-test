package com.kinglogi.service;

import com.kinglogi.dto.request.BankAccountRequest;
import com.kinglogi.dto.response.BankAccountResponse;
import com.kinglogi.service.base.CrudService;
import com.kinglogi.service.base.MasterDataService;

public interface BankAccountService extends MasterDataService, CrudService<BankAccountRequest, BankAccountResponse, Long> {

}
