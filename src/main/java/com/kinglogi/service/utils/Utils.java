package com.kinglogi.service.utils;

import com.kinglogi.constant.RankPoint;
import com.kinglogi.dto.PageableDTO;
import com.kinglogi.entity.Role;
import com.kinglogi.service.error.ResourceNotFoundException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Pageable;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class Utils {
    public static <T> T requireExists(Optional<T> optional, String message) {
        return optional.orElseThrow(() -> new ResourceNotFoundException(message));
    }
    public static boolean isAllSpaces(String text) {
        return StringUtils.isBlank(text) && StringUtils.isNotEmpty(text);
    }

    public static Instant truncateInstantDate(Instant date){
        return date.atZone(ZoneId.of("Asia/Ho_Chi_Minh")).truncatedTo(ChronoUnit.DAYS).toInstant();
    }

    public static Set<String> getRoleCodes(Set<Role> roles) {
        return roles.stream()
            .map(Role::getCode)
            .collect(Collectors.toSet());
    }

    public static PageableDTO getPageable(Pageable pageable, int listSize) {
        PageableDTO pageableDTO = new PageableDTO();
        final int start = (int)pageable.getOffset();
        final int end = Math.min((start + pageable.getPageSize()), listSize);

        pageableDTO.setStart(start);
        pageableDTO.setEnd(end);
        pageableDTO.setTotalSize(listSize);
        return pageableDTO;
    }

    public static RankPoint classifyRank(Integer loyaltyPoint) {
        if (loyaltyPoint >= 200) {
            return RankPoint.GOLD;
        } else if (loyaltyPoint > 100) {
            return RankPoint.SILVER;
        } else {
            return RankPoint.STANDARD;
        }
    }
}

