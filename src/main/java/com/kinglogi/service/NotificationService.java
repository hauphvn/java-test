package com.kinglogi.service;

import com.kinglogi.dto.request.NotificationRequest;
import com.kinglogi.dto.request.PushNotificationDto;
import com.kinglogi.dto.response.NotificationResponse;
import com.kinglogi.service.base.CrudService;

import java.util.List;

public interface NotificationService extends CrudService<NotificationRequest, NotificationResponse, Long> {

    boolean markAsRead(Long id);

    boolean markAllAsRead();

    int saveAll(List<NotificationRequest> notificationRequests);

    boolean deleteAll();

    boolean sendNotification(PushNotificationDto request);

    int countUnreadNotification();
}
