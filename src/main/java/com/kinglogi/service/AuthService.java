package com.kinglogi.service;

import com.kinglogi.dto.request.*;
import com.kinglogi.dto.response.JwtAuthenticationToken;

import java.io.IOException;
import java.time.Instant;

public interface AuthService {

    JwtAuthenticationToken login(LoginRequest request);

    Instant loginSendOtp(String phoneNumber) throws IOException;

    JwtAuthenticationToken loginWithOtp(String otp, String phoneNumber);

    JwtAuthenticationToken register(RegisterRequest request, String otp);

    Instant registerBySms(RegisterRequest request) throws IOException;

    Boolean registerByPartner(RegisterRequest registerRequest);

    JwtAuthenticationToken refreshToken(RefreshTokenRequest request);

    boolean forgotPassword(ForgotPasswordRequest request);

    JwtAuthenticationToken changePassword(ChangePasswordRequest request);

    void logout(String deviceId, String appName);
}
