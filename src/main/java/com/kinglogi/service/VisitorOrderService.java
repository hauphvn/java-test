package com.kinglogi.service;

import com.kinglogi.dto.request.OrderRequest;
import com.kinglogi.dto.response.PaymentResponse;
import com.kinglogi.dto.response.OrderWithPaymentResponse;
import com.kinglogi.dto.response.PaymentUrlResponse;

import java.util.Map;

public interface VisitorOrderService {

    OrderWithPaymentResponse createOrder(OrderRequest request);

    PaymentResponse verifyPayment(Map<String, Object> allParams);

    void cancel(Long id);

    OrderWithPaymentResponse generate(String orderCode);
}
