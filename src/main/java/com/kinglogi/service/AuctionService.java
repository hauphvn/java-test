package com.kinglogi.service;

import com.kinglogi.dto.request.AuctionRequest;
import com.kinglogi.dto.response.AuctionResponse;
import com.kinglogi.service.base.CrudService;

public interface AuctionService extends CrudService<AuctionRequest, AuctionResponse, Long> {

    void cancelAuctionByAdmin(Long id);
}
