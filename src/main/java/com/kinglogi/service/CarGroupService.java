package com.kinglogi.service;

import com.kinglogi.dto.request.CarGroupRequest;
import com.kinglogi.dto.response.CarGroupResponse;
import com.kinglogi.service.base.CrudService;
import org.springframework.core.io.InputStreamResource;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.util.List;

public interface CarGroupService extends CrudService<CarGroupRequest, CarGroupResponse, Long> {
    void uploadImage(Long id, List<MultipartFile> multipartFiles);

    InputStreamResource viewImage(Long id, String fileName) throws FileNotFoundException;

    void deleteImage(Long id, String fileName);
}
