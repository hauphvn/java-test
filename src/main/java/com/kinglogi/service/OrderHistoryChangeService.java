package com.kinglogi.service;

import com.google.gson.Gson;
import com.kinglogi.dto.filter.OrderHistoryChangeFilter;
import com.kinglogi.dto.order_history_change.OrderHistoryChangeOutputDTO;
import com.kinglogi.dto.request.TripRequest;
import com.kinglogi.dto.response.OrderResponse;
import com.kinglogi.entity.*;
import com.kinglogi.repository.AccountRepository;
import com.kinglogi.repository.OrderHistoryChangeRepository;
import com.kinglogi.repository.TripRepository;
import com.kinglogi.service.impl.TripServiceImpl;
import com.kinglogi.service.utils.Utils;
import com.kinglogi.specs.BaseFilterSpecs;
import com.kinglogi.utils.BeanUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
public class OrderHistoryChangeService {

    private final OrderHistoryChangeRepository orderHistoryChangeRepository;

    private final BaseFilterSpecs baseFilterSpecs;

    private final AccountRepository accountRepository;

    private final TripRepository tripRepository;

    private final TripServiceImpl tripService;

    public OrderHistoryChangeService(OrderHistoryChangeRepository orderHistoryChangeRepository, BaseFilterSpecs baseFilterSpecs, AccountRepository accountRepository, TripRepository tripRepository, TripServiceImpl tripService) {
        this.orderHistoryChangeRepository = orderHistoryChangeRepository;
        this.baseFilterSpecs = baseFilterSpecs;
        this.accountRepository = accountRepository;
        this.tripRepository = tripRepository;
        this.tripService = tripService;
    }

    public Page<OrderHistoryChangeOutputDTO> findAllOrderHistoryChange(OrderHistoryChangeFilter filter) {
        Pageable pageable = baseFilterSpecs.page(filter);
        Specification<OrderHistory> spec = baseFilterSpecs.search(filter);
        Page<OrderHistory> orderHistories = orderHistoryChangeRepository.findAll(spec, pageable);
        return orderHistories.map(orderHistory -> {
            Account account = Utils.requireExists(accountRepository.findById(orderHistory.getCreatedBy()), "error.dataNotFound");
            OrderHistoryChangeOutputDTO result = BeanUtil.copyProperties(orderHistory, OrderHistoryChangeOutputDTO.class);
            result.setModifyBy(account.getFullName());
            return result;
        });
    }

    public void changeDataOrder(OrderResponse oldOrder, OrderResponse newOrder){
        Order order = new Order();
        order.setId(oldOrder.getId());
        OrderHistory orderHistory = new OrderHistory();
        orderHistory.setOldValue(new Gson().toJson(oldOrder));
        orderHistory.setNewValue(new Gson().toJson(newOrder));
        orderHistory.setOrderCode(oldOrder.getOrderCode());
        orderHistory.setOrder(order);
        orderHistoryChangeRepository.save(orderHistory);

        // save trip when update price difference zero
        if (oldOrder.getTotalPrice() == 0 && newOrder.getTotalPrice() != 0){
            if (tripRepository.findAllByPickingUpLocationAndDroppingOffLocation(newOrder.getPickingUpLocation(), newOrder.getDroppingOffLocation()).size() == 0){
                TripRequest tripRequest = new TripRequest();
                tripRequest.setPickingUpLocation(newOrder.getPickingUpLocation());
                tripRequest.setDroppingOffLocation(newOrder.getDroppingOffLocation());
                tripRequest.setNumberOfSeat(newOrder.getNumberOfSeat());
                tripRequest.setTotalPrice(newOrder.getTotalPrice());
                tripRequest.setDistanceInKm(newOrder.getDistanceInKm());
                tripService.save(tripRequest);
            }
        }
    }

}
