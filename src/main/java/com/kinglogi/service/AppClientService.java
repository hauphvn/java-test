package com.kinglogi.service;

import com.kinglogi.constant.AppClientType;
import com.kinglogi.dto.request.AppClientRequest;
import com.kinglogi.dto.response.AppClientResponse;
import com.kinglogi.entity.AppClient;

import java.util.List;

public interface AppClientService {

    AppClientResponse save(AppClientRequest request);

    List<AppClientResponse> saveAll(List<AppClient> appClients);

    AppClient create(AppClientType type);
}
