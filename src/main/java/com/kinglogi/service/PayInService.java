package com.kinglogi.service;

import com.kinglogi.dto.request.PaymentRequest;
import com.kinglogi.dto.response.PaymentUrlResponse;

import java.util.Map;

public interface PayInService {

    PaymentUrlResponse buildPayInRequest(PaymentRequest paymentRequest);

    Boolean verifyPayIn(Map<String, Object> allParams);

}
