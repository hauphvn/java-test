package com.kinglogi.service;

import com.kinglogi.dto.request.CarRequest;
import com.kinglogi.dto.response.CarResponse;
import com.kinglogi.dto.response.file_response.FileCarResponse;
import com.kinglogi.service.base.CrudService;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface CarService extends CrudService<CarRequest, CarResponse, Long> {
    void uploadImages(Long id, List<MultipartFile> files);

    InputStreamResource viewImage(Long id, String fileName) throws IOException;

    List<FileCarResponse> getImage(Long id);
}
