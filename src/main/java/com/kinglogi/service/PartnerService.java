package com.kinglogi.service;

import com.kinglogi.dto.request.PartnerRequest;
import com.kinglogi.dto.response.PartnerResponse;
import com.kinglogi.service.base.CrudService;

public interface PartnerService extends CrudService<PartnerRequest, PartnerResponse, Long> {

}
