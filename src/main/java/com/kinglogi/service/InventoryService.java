package com.kinglogi.service;

import com.google.gson.Gson;
import com.kinglogi.constant.InventoryStatus;
import com.kinglogi.constant.InventoryTripType;
import com.kinglogi.constant.LocaleConstant;
import com.kinglogi.dto.filter.AccountInventoryOrderFilter;
import com.kinglogi.dto.filter.InventoryFilter;
import com.kinglogi.dto.filter.InventoryHistoryFilter;
import com.kinglogi.dto.inventory.AccountApplyInventoryOutput;
import com.kinglogi.dto.inventory.InventoryInputDTO;
import com.kinglogi.dto.inventory.InventoryOutputDTO;
import com.kinglogi.dto.inventory_history.InventoryHistoryOutPutDTO;
import com.kinglogi.dto.response.AccountResponse;
import com.kinglogi.entity.*;
import com.kinglogi.provider.AuthenticatedProvider;
import com.kinglogi.repository.*;
import com.kinglogi.service.error.AccessForbiddenException;
import com.kinglogi.service.error.AuthenticationException;
import com.kinglogi.service.error.BadRequestException;
import com.kinglogi.service.utils.Utils;
import com.kinglogi.specs.BaseFilterSpecs;
import com.kinglogi.utils.BeanUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class InventoryService {
    private final InventoryRepository inventoryRepository;

    private final InventoryHistoryRepository inventoryHistoryRepository;

    private final BaseFilterSpecs<Inventory> baseFilterSpecs;
    private final BaseFilterSpecs<InventoryHistory> baseFilterSpecsHistory;
    private final BaseFilterSpecs<AccountInventoryOrder> baseFilterSpecsAccountApply;
    private final AccountInventoryOrderRepository accountInventoryOrderRepository;

    private final AccountRepository accountRepository;

    private final TripRepository tripRepository;

    private final UserACL userACL;

    private final AuthenticatedProvider authenticatedProvider;

    public InventoryService(InventoryRepository inventoryRepository, InventoryHistoryRepository inventoryHistoryRepository, BaseFilterSpecs<Inventory> baseFilterSpecs, BaseFilterSpecs<InventoryHistory> baseFilterSpecsHistory, BaseFilterSpecs<AccountInventoryOrder> baseFilterSpecsAccountApply, AccountInventoryOrderRepository accountInventoryOrderRepository, AccountRepository accountRepository, TripRepository tripRepository, UserACL userACL, AuthenticatedProvider authenticatedProvider) {
        this.inventoryRepository = inventoryRepository;
        this.inventoryHistoryRepository = inventoryHistoryRepository;
        this.baseFilterSpecs = baseFilterSpecs;
        this.baseFilterSpecsHistory = baseFilterSpecsHistory;
        this.baseFilterSpecsAccountApply = baseFilterSpecsAccountApply;
        this.accountInventoryOrderRepository = accountInventoryOrderRepository;
        this.accountRepository = accountRepository;
        this.tripRepository = tripRepository;
        this.userACL = userACL;
        this.authenticatedProvider = authenticatedProvider;
    }


    // Lưu inventory mới
    public InventoryOutputDTO save(InventoryInputDTO input) {
        // Kiểm tra có phải là superadmin hoặc admin tuyến cố định hoặc admin tuyến không cố định
        if (!(userACL.isSuperAdmin() || userACL.isFixedAdmin() || userACL.isFlexibleAdmin())) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        // Tên của inventory không được rỗng
        if (StringUtils.isBlank(input.getName())) {
            throw new BadRequestException("error.notBlank");
        }
        // Kiểm tra số chỗ của chuyến không được nhỏ hơn 0
        if (input.getNumberOfSeat() <= 0) {
            throw new BadRequestException("error.numberOfSeatInvalid");
        }
        // Số lượng chuyến được tham gia inventory không được nhỏ hơn 0
        if (input.getNumberOfTrip() <= 0) {
            throw new BadRequestException("error.numberOfTripInvalid");
        }
        // Giá của chuyến không được nhỏ hơn 0
        if (input.getPrice() <= 0) {
            throw new BadRequestException("error.priceInvalid");
        }
        // Kiểm tra thời gian áp dụng < thời gian hiện tại không
        if (input.getExpireTime().getEpochSecond() < Instant.now().getEpochSecond()) {
            throw new BadRequestException("error.expireTimeInvalid");
        }
        // Kiểm tra đối tượng được tham gia inventory có trống hay không
        if (input.getTargetUsers().isEmpty()) {
            throw new BadRequestException("error,targetUserInvalid");
        }
        Inventory inventory = new Inventory();
        Trip trip = new Trip();
        // Kiểm tra tuyến cố định này đã được tạo hay chưa
        if(input.getInventoryTripType().equals(InventoryTripType.FIXED)){
            trip = Utils.requireExists(tripRepository.findById(input.getTripId()), "error.tripNotFound");
            // Nếu trip đã có inventory rồi thì ném ra lỗi
            if (trip.getInventories().size() > 0){
                throw new BadRequestException("error.tripHadInventory");
            }
            // Tuyến cố định chưa tồn tại thì tạo mới
        }else {
            trip.setPickingUpLocation(input.getPickingUpLocation());
            trip.setDroppingOffLocation(input.getDroppingOffLocation());
            trip.setLanguage(LocaleConstant.VIETNAMESE);
            trip.setTotalPrice(input.getPrice());
            trip.setNumberOfSeat(input.getNumberOfSeat());
            trip.setPickingUpDatetime(Date.from(Instant.now()));
            // Mặc đinh là 100km
            trip.setDistanceInKm(100F);
            trip = tripRepository.save(trip);

            Trip tripEn = new Trip();
            tripEn.setPickingUpLocation(input.getPickingUpLocation());
            tripEn.setDroppingOffLocation(input.getDroppingOffLocation());
            tripEn.setTotalPrice(input.getPrice());
            tripEn.setLanguage(LocaleConstant.ENGLISH);
            tripEn.setNumberOfSeat(input.getNumberOfSeat());
            tripEn.setPickingUpDatetime(Date.from(Instant.now()));
            tripEn.setDistanceInKm(100F);
            tripEn.setTrip(trip);
            tripRepository.save(tripEn);
        }

        // Lưu thông tin inventory
        inventory.setName(input.getName().trim());
        inventory.setInventoryTripType(input.getInventoryTripType());
        inventory.setPrice(input.getPrice());
        inventory.setExpireTime(input.getExpireTime());
        inventory.setTrip(trip);
        inventory.setTotalOfTrip(input.getNumberOfTrip());
        inventory.setNumberOfTrip(input.getNumberOfTrip());
        inventory.setNumberOfSeat(input.getNumberOfSeat());
        String targetUsers  = StringUtils.join(input.getTargetUsers(), ",");
        inventory.setTargetUsers(targetUsers);
        inventory.setStatus(InventoryStatus.ACTIVE);
        inventoryRepository.save(inventory);
        return new InventoryOutputDTO(inventory);
    }

    // Cập nhật inventory
    public InventoryOutputDTO update(InventoryInputDTO input, Long inventoryId) {
        // Kiểm tra có phải là superadmin hoặc admin tuyến cố định hoặc admin tuyến không cố định
        if (!(userACL.isSuperAdmin() || userACL.isFixedAdmin() || userACL.isFlexibleAdmin())) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Inventory inventory = Utils.requireExists(inventoryRepository.findById(inventoryId), "error.inventoryNotFound");
        Gson gson = new Gson();
        InventoryOutputDTO oldInventoryOutputDTO = new InventoryOutputDTO(inventory);
        InventoryHistory inventoryHistory = new InventoryHistory();
        // Lưu thông tin lịch sử chuyến đổi vào bảng inventory_history
        inventoryHistory.setInventory(inventory);
        inventoryHistory.setOldValue(gson.toJson(oldInventoryOutputDTO));
        Trip trip = Utils.requireExists(tripRepository.findById(input.getTripId()), "error.tripNotFound");
        // Tên inventory không được trống
        if (StringUtils.isBlank(input.getName())) {
            throw new BadRequestException("error.notBlank");
        }
        // Số lượng ghế không được nhỏ hơn 0
        if (input.getNumberOfSeat() <= 0) {
            throw new BadRequestException("error.numberOfSeatInvalid");
        }
        // Giá không nhỏ hơn 0
        if (input.getPrice() <= 0) {
            throw new BadRequestException("error.priceInvalid");
        }
        // Kiểm tra thời gian áp dụng của inventory
        if (input.getExpireTime().getEpochSecond() < Instant.now().getEpochSecond()) {
            throw new BadRequestException("error.expireTimeInvalid");
        }
        // Kiểm tra đối tượng áp dụng không được trống
        if (input.getTargetUsers().isEmpty()) {
            throw new BadRequestException("error,targetUserInvalid");
        }
        // Lưu thông tin mới sau khi cập nhật
        inventory.setName(input.getName().trim());
        inventory.setInventoryTripType(input.getInventoryTripType());
        inventory.setPrice(input.getPrice());
        inventory.setExpireTime(input.getExpireTime());
        inventory.setNumberOfSeat(input.getNumberOfSeat());
        inventory.setTrip(trip);
        if (input.getExpireTime().isAfter(Instant.now())){
            if (inventory.getNumberOfTrip() > 0){
                inventory.setStatus(InventoryStatus.ACTIVE);
            }
        }
        String targetUsers  = StringUtils.join(input.getTargetUsers(), ",");
        inventory.setTargetUsers(targetUsers);
        inventoryRepository.save(inventory);
        InventoryOutputDTO newInventoryOutputDTO = new InventoryOutputDTO(inventory);
        // Lưu giá trị mới vào bảng inventoryHistory
        inventoryHistory.setNewValue(gson.toJson(newInventoryOutputDTO));
        inventoryHistoryRepository.save(inventoryHistory);
        return newInventoryOutputDTO;
    }

    // Xóa inventory
    public void delete(Long inventoryId) {
        // Kiểm tra có phải là superadmin hoặc admin tuyến cố định hoặc admin tuyến không cố định
        if (!(userACL.isSuperAdmin() || userACL.isFixedAdmin() || userACL.isFlexibleAdmin())) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Inventory inventory = Utils.requireExists(inventoryRepository.findById(inventoryId), "error.inventoryNotFound");
        inventory.setDeletedAt(new Date());
        inventoryRepository.save(inventory);
    }

    // Tìm các inventory
    public Page<InventoryOutputDTO> findAll(InventoryFilter filter) {
        Pageable pageable = baseFilterSpecs.page(filter);
        Specification<Inventory> spec = baseFilterSpecs.search(filter);
        return inventoryRepository.findAllActive(spec, pageable).map(InventoryOutputDTO::new);
    }

    // Tìm lịch sử sửa đổi của inventory theo id
    public Page<InventoryHistoryOutPutDTO> findAllHistoryOfInventory(InventoryHistoryFilter filter, Long inventoryId) {
        Inventory inventory = Utils.requireExists(inventoryRepository.findById(inventoryId), "error.inventoryNotFound");
        Pageable pageable = baseFilterSpecsHistory.page(filter);
        Page<InventoryHistoryOutPutDTO> page = inventoryHistoryRepository.findAllByInventoryId(inventoryId, pageable).map(InventoryHistoryOutPutDTO::new);
        for (InventoryHistoryOutPutDTO inventoryHistoryOutPutDTO : page) {
            Account account = Utils.requireExists(accountRepository.findById(inventoryHistoryOutPutDTO.getUpdatedBy()), "error.userNotFound");
            AccountResponse accountResponse = BeanUtil.copyProperties(account, AccountResponse.class);
            inventoryHistoryOutPutDTO.setCreatedBy(accountResponse);
        }
        return page;
    }

    // Tìm 1 inventory theo id
    public InventoryOutputDTO findOne(Long inventoryId) {
        Inventory inventory = Utils.requireExists(inventoryRepository.findById(inventoryId), "error.inventoryNotFound");
        return new InventoryOutputDTO(inventory);
    }

    //TODO: use when book order and match with inventory
    public InventoryOutputDTO checkIfInventoryValid(String pickingUpLocation, String droppingOffLocation, Integer numberOfSeat, Long tripId) {
        Optional<Long> userId = authenticatedProvider.getUserId();
        Account account = Utils.requireExists(accountRepository.findById(userId.get()), "error.userNotFound");
        // Lấy role của người dùng hiện tại
        Set<String> roles = account.getRoles().stream().map(Role::getCode).collect(Collectors.toSet());
        // Nếu không có người dùng thì ném ra ngoại lệ
        if (!userId.isPresent()) {
            throw new AuthenticationException("error.notAuthorization");
        }
        // Nếu người dùng truyền vào tripid thì tìm kiếm thông tin inventory theo tripid
        if (tripId != null){
            Trip trip = Utils.requireExists(tripRepository.findById(tripId), "error.dataNotFound");
            Inventory inventory = inventoryRepository.findTopByTrip(trip);
            // Nếu tồn tại inventory thì xác thực người dùng hiện tại có được hưởng inventory không
            if (inventory != null ){
                // Người dùng được hưởng khi inventory đang được Active
                if (inventory.getStatus().equals(InventoryStatus.ACTIVE)){
                    InventoryOutputDTO inventoryOutputDTO = new InventoryOutputDTO(inventory);
                    // Lấy danh sách role người dùng được áp dụng inventory
                    List<String> targetUsers = inventoryOutputDTO.getTargetUsers().stream().map(String::valueOf).collect(Collectors.toList());
                    // Kiểm tra người dùng hiện tại có role trùng với role cho phép của inventory
                    targetUsers.retainAll(roles);
                    // Nếu tồn tại role, nghĩa là có quyền được hưởng
                    if (!targetUsers.isEmpty()) {
                        return inventoryOutputDTO;
                    }
                }
            }
        }
        Instant now = Instant.now();
        // Tìm các inventory còn hoạt động theo số chỗ và điểm đón và điểm đi, sắp xêp theo ngày tạo và giảm dần
        Page<Inventory> page = inventoryRepository.findAllByNumberOfSeatAndTripId(numberOfSeat, pickingUpLocation, droppingOffLocation, now, PageRequest.of(0, 1, Sort.by("createdAt").descending()));
        // Nếu tồn tại inventory thì xác thực người dùng hiện tại có được hưởng inventory không
        if (!page.isEmpty()) {
            Inventory inventory = page.getContent().get(0);
            InventoryOutputDTO inventoryOutputDTO = new InventoryOutputDTO(inventory);
            // Lấy danh sách role người dùng được tham gia inventory
            List<String> targetUsers = inventoryOutputDTO.getTargetUsers().stream().map(String::valueOf).collect(Collectors.toList());
            // Kiểm tra người dùng hiện tại có role trùng với role cho phép của inventory
            targetUsers.retainAll(roles);
            // Nếu tồn tại role, nghĩa là có quyền được hưởng
            if (!targetUsers.isEmpty()) {
                return inventoryOutputDTO;
            }
        }
        return null;
    }

    // Lịch chạy tự động vào 0h0m0s ở múi giờ GMT+7
    // Hàm này cập nhật trạng thái inventory từ active sang expired dự vào thời gian hiện tại
    @Scheduled(cron = "0 0 0 * * ?", zone = "GMT +7")
    public void checkStatus() {
        inventoryRepository.updateAllByExpireTimeBeforeAndStatus(InventoryStatus.EXPIRED, Instant.now(), InventoryStatus.ACTIVE);
    }

    // Lấy account được tham gia inventory
    public Page<AccountApplyInventoryOutput> getAccountsApplyInventory(long id, AccountInventoryOrderFilter filter) {
        Pageable pageable = baseFilterSpecs.page(filter);
        Specification<AccountInventoryOrder> spec = baseFilterSpecsAccountApply.search(filter);
        Specification<AccountInventoryOrder> additionalSpec = (root, query, cb) ->
                cb.equal(root.get("inventory"), id);
        spec = spec.and(additionalSpec);
        Page<AccountInventoryOrder> accountInventoryOrders = accountInventoryOrderRepository.findAll(spec, pageable);
        return accountInventoryOrders.map(accountInventoryOrder -> BeanUtil.copyProperties(accountInventoryOrder, AccountApplyInventoryOutput.class));
    }
}

