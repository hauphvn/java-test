package com.kinglogi.service;

import com.kinglogi.dto.bid.BidAuctionCreateInputDTO;
import com.kinglogi.dto.bid.BidAuctionUpdateInputDTO;
import com.kinglogi.dto.response.BidResponse;

public interface BidService {

    BidResponse bid(BidAuctionCreateInputDTO input);

    void cancelBidReserve(Long bidId);
}
