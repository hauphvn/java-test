package com.kinglogi.service;

import com.kinglogi.configuration.security.SecurityUtils;
import com.kinglogi.constant.AuthRole;
import org.springframework.stereotype.Service;

@Service
public class UserACL {

    public UserACL() {
    }

    public boolean isSuperAdmin() {
        return SecurityUtils.isCurrentUserInRole(AuthRole.SUPER_ADMIN.authority());
    }

    public boolean isFixedAdmin() {
        return SecurityUtils.isCurrentUserInRole(AuthRole.FIXED_TRIP_ADMIN.authority());
    }

    public boolean isFlexibleAdmin() {
        return SecurityUtils.isCurrentUserInRole(AuthRole.FLEXIBLE_TRIP_ADMIN.authority());
    }

    public boolean isAgentAdmin() {
        return SecurityUtils.isCurrentUserInRole(AuthRole.AGENT_ADMIN.authority());
    }

    public boolean isSubAgentAdmin() {
        return SecurityUtils.isCurrentUserInRole(AuthRole.SUB_AGENT_ADMIN.authority());
    }

    public boolean isMember() {
        return SecurityUtils.isCurrentUserInRole(AuthRole.MEMBER.authority());
    }

    public boolean isCollaborator() {
        return SecurityUtils.isCurrentUserInRole(AuthRole.COLLABORATOR.authority());
    }
}
