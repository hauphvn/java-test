package com.kinglogi.service;

import com.kinglogi.dto.request.AccountRequest;
import com.kinglogi.dto.response.AccountResponse;
import com.kinglogi.entity.Account;
import com.kinglogi.service.base.CrudService;
import com.kinglogi.validator.EntityExists;
import com.kinglogi.validator.FieldValueExists;

import java.util.Optional;

public interface AccountService extends CrudService<AccountRequest, AccountResponse, Long>, FieldValueExists, EntityExists<Long> {

    AccountResponse findByPushToken(String pushToken);


    void updateAvatar(Long accountId, String avatar);

    Optional<Account> isUsernameExisted(String phoneNumber);

    Optional<Account> isPhoneNumberExisted(String userName);

    Account getByUserNameOrPhone(String username);

    AccountResponse updatePhoneNumber(AccountRequest accountRequest, String otp);

    Account getByPhoneNumber(String phoneNumber);
}
