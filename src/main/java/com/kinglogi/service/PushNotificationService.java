package com.kinglogi.service;

import java.util.concurrent.CompletableFuture;

public interface PushNotificationService<T> {

    CompletableFuture<Boolean> sendPushNotificationAsync(T request);

}
