package com.kinglogi.service;

import com.kinglogi.dto.request.TripCatalogRequest;
import com.kinglogi.dto.response.TripCatalogResponse;
import com.kinglogi.service.base.CrudService;
import com.kinglogi.service.base.MasterDataService;

public interface TripCatalogService extends MasterDataService, CrudService<TripCatalogRequest, TripCatalogResponse, Long> {

}
