package com.kinglogi.service;

import com.kinglogi.dto.request.ContactFormRequest;

public interface SupportService {

    boolean supportFromContactForm(ContactFormRequest request);
}
