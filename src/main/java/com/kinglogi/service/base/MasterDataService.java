package com.kinglogi.service.base;

import java.util.List;

/**
 * The base service for master data
 * @param <R> The response type
 */
public interface MasterDataService<R> extends BaseService {

    List<R> findAll();
}
