package com.kinglogi.service;

import com.kinglogi.dto.filter.base.BaseFilter;
import com.kinglogi.dto.response.AccountResponse;
import org.springframework.data.domain.Page;

public interface AgentAdminService extends AccountService {
    Page<AccountResponse> findSubAgent(BaseFilter baseFilter);
}
