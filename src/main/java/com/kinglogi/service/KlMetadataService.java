package com.kinglogi.service;

import com.kinglogi.dto.KlConfiguration;

public interface KlMetadataService {

    boolean updateConfiguration(KlConfiguration configuration);

    KlConfiguration getConfiguration();
}
