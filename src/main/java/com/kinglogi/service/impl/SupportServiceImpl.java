package com.kinglogi.service.impl;

import com.kinglogi.constant.EmailTemplateType;
import com.kinglogi.dto.mail.MailInfo;
import com.kinglogi.dto.mail.MailTemplateConfig;
import com.kinglogi.dto.request.ContactFormRequest;
import com.kinglogi.dto.response.EmailTemplateResponse;
import com.kinglogi.service.EmailService;
import com.kinglogi.service.EmailTemplateService;
import com.kinglogi.service.SupportService;
import org.apache.commons.text.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SupportServiceImpl implements SupportService {

    @Value("${app.email.receiveContact}")
    private String emailReceiveContact;

    private final EmailService emailService;

    private final EmailTemplateService emailTemplateService;

    @Autowired
    public SupportServiceImpl(EmailService emailService, EmailTemplateService emailTemplateService) {
        this.emailService = emailService;
        this.emailTemplateService = emailTemplateService;
    }

    @Override
    public boolean supportFromContactForm(ContactFormRequest request) {
        ContactFormRequest safeRequest = ContactFormRequest.builder()
                .fullName(sanitize(request.getFullName()))
                .phoneNumber(sanitize(request.getPhoneNumber()))
                .subject(sanitize(request.getSubject()))
                .content(sanitize(request.getContent()))
                .email(request.getEmail())
                .build();

        EmailTemplateResponse emailTemplateResponse = emailTemplateService.findByTypeAndLanguage(EmailTemplateType.CONTACT.name(), request.getLanguage());
        MailTemplateConfig config = MailTemplateConfig.builder()
                .mailContent(emailTemplateResponse.getContent())
                .mailSubject(emailTemplateResponse.getTitle())
                .mailIsHtml(true)
                .build();

        MailInfo mailInfo = new MailInfo();
        mailInfo.addTo(emailReceiveContact);
        emailService.sendAsync(mailInfo, config, safeRequest);
        return true;
    }

    private String sanitize(String text) {
        return StringEscapeUtils.escapeHtml4(text);
    }
}
