package com.kinglogi.service.impl;

import com.kinglogi.constant.*;
import com.kinglogi.constant.Role;
import com.kinglogi.dto.UserToken;
import com.kinglogi.dto.filter.AuctionFilter;
import com.kinglogi.dto.filter.base.BaseFilter;
import com.kinglogi.dto.mail.MailInfo;
import com.kinglogi.dto.mail.MailTemplateConfig;
import com.kinglogi.dto.request.*;
import com.kinglogi.dto.response.AuctionResponse;
import com.kinglogi.dto.response.EmailTemplateResponse;
import com.kinglogi.dto.response.UserCredential;
import com.kinglogi.entity.*;
import com.kinglogi.exception.KlgResourceNotFoundException;
import com.kinglogi.provider.AuthenticatedProvider;
import com.kinglogi.repository.*;
import com.kinglogi.repository.custom.CustomRoleRepository;
import com.kinglogi.service.*;
import com.kinglogi.service.error.AccessForbiddenException;
import com.kinglogi.service.error.BadRequestException;
import com.kinglogi.service.utils.Utils;
import com.kinglogi.specs.BaseFilterSpecs;
import com.kinglogi.utils.BeanUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Qualifier("auctionService")
@Slf4j
public class AuctionServiceImpl implements AuctionService {

    private static boolean isRunning = false;
	private final AuctionRepository auctionRepository;

	private final BaseFilterSpecs<Auction> baseFilterSpecs;

	private final OrderRepository orderRepository;

	private final NotificationService notificationService;

	private final PushNotificationTokenService pushNotificationTokenService;

	private final EmailService emailService;

	private final EmailTemplateService emailTemplateService;

	private final CustomRoleRepository roleRepository;

    private final AuthenticatedProvider authenticatedProvider;

    private final BidRepository bidRepository;

    private final AccountRepository accountRepository;

    private final TransactionService transactionService;

	private final UserACL userACL;

	private final ReportService reportService;

	private final OtpService otpService;

    @Autowired
	public AuctionServiceImpl(
			AuctionRepository auctionRepository,
			BaseFilterSpecs<Auction> baseFilterSpecs,
			OrderRepository orderRepository, NotificationService notificationService,
			PushNotificationTokenService pushNotificationTokenService, EmailService emailService,
			EmailTemplateService emailTemplateService, CustomRoleRepository roleRepository, AuthenticatedProvider authenticatedProvider,
			BidRepository bidRepository, AccountRepository accountRepository, TransactionService transactionService, UserACL userACL, ReportService reportService, OtpService otpService) {
		this.auctionRepository = auctionRepository;
		this.baseFilterSpecs = baseFilterSpecs;
		this.orderRepository = orderRepository;
		this.notificationService = notificationService;
		this.pushNotificationTokenService = pushNotificationTokenService;
		this.emailService = emailService;
		this.emailTemplateService = emailTemplateService;
		this.roleRepository = roleRepository;
        this.authenticatedProvider = authenticatedProvider;
        this.bidRepository = bidRepository;
        this.accountRepository = accountRepository;
        this.transactionService = transactionService;
		this.userACL = userACL;
		this.reportService = reportService;
		this.otpService = otpService;
	}

	@Override
	public Page<AuctionResponse> findAll(BaseFilter baseFilter) {
		AuctionFilter auctionFilter = (AuctionFilter) baseFilter;
		Specification<Auction> spec = baseFilterSpecs.search(auctionFilter);
		if (auctionFilter.getStatus() != null) {
			Specification<Auction> additionalSpec = (root, query, cb) ->
					cb.equal(root.get("status"), auctionFilter.getStatus());
			spec = spec.and(additionalSpec);
		}

		if (BooleanUtils.isTrue(auctionFilter.getIsInBidding())) {
			Specification<Auction> additionalSpec = (root, query, cb) ->
					cb.greaterThan(root.get("endAt"), new Date());
			additionalSpec = additionalSpec.or((root, query, cb) -> cb.isNull(root.get("endAt")));
			spec = spec.and(additionalSpec);
		}

		Pageable pageable = baseFilterSpecs.page(baseFilter);
		Page<Auction> auctionPage = auctionRepository.findAll(spec, pageable);
		return auctionPage.map(this::convertToResponse);
	}

	@Override
	public AuctionResponse findById(Long id) {
		Auction auction = getById(id);
		return convertToResponse(auction);
	}

	@Override
    @Transactional
    public AuctionResponse save(AuctionRequest auctionRequest) {
        if (auctionRequest.getType() == null) {
            auctionRequest.setType(AuctionType.BUY);
        }
        if (auctionRequest.getOrderIds().isEmpty()){
        	throw new BadRequestException("error.ordersNotEmpty");
		}
        Auction auction = BeanUtil.copyProperties(auctionRequest, Auction.class);
		Set<Order> auctionOrders = convertOrderRequestToOrder(auctionRequest, auction);
		auctionOrders.forEach(order -> {
			if (order.getPickingUpDatetime().compareTo(auctionRequest.getEndAt()) <= 0){
				throw new BadRequestException("error.pickingUpDatetimeBeforeEndAtOfAuction");
			}
		});
		auction.setOrders(auctionOrders);
		auction.setStatus(AuctionStatus.INIT.getStatus());
		Auction savedAuction = auctionRepository.save(auction);
		return convertToResponse(savedAuction);
	}

	@Override
    @Transactional
    public AuctionResponse update(Long id, AuctionRequest auctionRequest) {
    	boolean sendSms = false;
    	List<Order> listOrderSendSms = new ArrayList<>();

		Auction auction = getById(id);

		auction.getOrders().forEach(order -> {
			if (order.getPickingUpDatetime().compareTo(auctionRequest.getEndAt()) <= 0){
				throw new BadRequestException("error.pickingUpDatetimeBeforeEndAtOfAuction");
			}
		});

		if(CollectionUtils.isNotEmpty(auctionRequest.getOrderIds())) {
			Set<Order> auctionOrders = orderRepository.findAllByIdIn(auctionRequest.getOrderIds());
			auction.setOrders(auctionOrders);
		}

		Auction savedAuction = auctionRepository.save(auction);
		BeanUtil.mergeProperties(auctionRequest, auction);
		if (AuctionStatus.START.getStatus().equals(auctionRequest.getStatus())) {
			sendNotificationAuctionStart(savedAuction);
			sendEmailStart(auction);
		}
		if (auctionRequest.getStatus().equals(AuctionStatus.FINISH.getStatus())) {
				final Long startedAuctionUserId = auction.getUpdatedBy();

				List<Bid> winningBids = bidRepository.findTopByAuctionIdAndStatusOrderByBidPriceAscCreatedAtAsc(auction.getId(), BidStatus.PENDING);
				if (winningBids.size() > 0) {
					Bid winningBid = winningBids.get(0);
					if (winningBid.getStatus().equals(BidStatus.PENDING)) {
						// Update winning bid acceptance
						winningBid.setStatus(BidStatus.SUCCESS);
						winningBid.setAuction(auction);

						if (!auction.getOrders().isEmpty()) {
							List<Order> orderListToSave = new ArrayList<>();
							for (Order order: auction.getOrders()) {
								order.setCollaboratorId(winningBid.getAccount().getId());
								order.setStatus(OrderStatus.READY_FOR_MOVING.getStatus());
								orderListToSave.add(order);
								listOrderSendSms.add(order);
								sendSms = true;
							}

							orderRepository.saveAll(orderListToSave);
						}


						// Save data
						bidRepository.save(winningBid);

						// Send notification end auction to startedAuctionUserId
						sendNotificationEndAuctionToAdmin(auction, startedAuctionUserId, winningBid.getAccount());
						sendEmailEndAuction(auction, startedAuctionUserId, winningBid.getAccount().getId());



						// Send notification to winning bidder
						sendNotificationWinningBid(winningBid.getAccount(), auction);

						// Refund money to loser bidder
						Long winningBidderId = winningBid.getAccount().getId();
						refundToLoserBidders(auction, winningBidderId);
					}
					if (winningBids.size() > 1){
						List<Bid> loseBids = winningBids.subList(1, winningBids.size() -1 );
						loseBids.forEach(bid -> {
							if (bid.getStatus().equals(BidStatus.PENDING)){
								bid.setStatus(BidStatus.FAIL);
							}
						});
						bidRepository.saveAll(loseBids);
					}
				}
		}
		if (sendSms){
			for (Order orderSendSms : listOrderSendSms) {
				Account account = accountRepository.getOne(orderSendSms.getCollaboratorId());
				Map<String, String> map = new HashMap<>();
				map.put("orderCode", orderSendSms.getOrderCode());
				if (account.getFullName() != null && !account.getFullName().isEmpty()){
					map.put("nameDriver", account.getFullName());
				}else {
					map.put("nameDriver", "N/A");
				}
				if (account.getPhoneNumber() != null && !account.getPhoneNumber().isEmpty()){
					map.put("phoneNumber", account.getPhoneNumber());
				}else {
					map.put("phoneNumber", "N/A");
				}
				Car car = account.getCars().stream().filter(c -> c.getNumberOfSeat().equals(orderSendSms.getNumberOfSeat())).findFirst().orElse(null);
				if (car != null && car.getBrand() != null && car.getBrand() != null && car.getLicensePlates() != null){
					map.put("licensePlates", car.getBrand() + " " + car.getLicensePlates());
				}else {
					map.put("licensePlates", "N/A");
				}
				try {
					otpService.sendSms(orderSendSms.getPhoneNumber(), TypeSms.INFORMATION_DRIVER, map);
				} catch (IOException e) {
					log.error(e.getMessage());
				}
			}
		}
		return convertToResponse(savedAuction);
	}

	private void sendEmailStart(Auction auction) {

		AuctionMail accountRequest = AuctionMail.builder()
				.name(auction.getName())
				.totalPrice(auction.getTotalPrice())
				.startAt(auction.getStartAt().toInstant().atZone(ZoneId.of("Asia/Ho_Chi_Minh")).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
				.build();

		EmailTemplateResponse emailTemplateResponse = emailTemplateService.findByTypeAndLanguage(EmailTemplateType.START_AUCTION.name(), "vi");
		MailTemplateConfig config = MailTemplateConfig.builder()
				.mailContent(emailTemplateResponse.getContent())
				.mailSubject(emailTemplateResponse.getTitle())
				.mailIsHtml(true)
				.build();

		MailInfo mailInfo = new MailInfo();
		com.kinglogi.entity.Role roles = Utils.requireExists(roleRepository.findByCode(Role.COLLABORATOR.getCode()), "error.dateNotFound");
		roles.getAccounts().stream().filter(account -> {
			if (account.getDeletedAt() != null) {
				return true;
			}
			if (account.getActive()) return true;
			return false;
		}).forEach(account -> {
			if (account.getActive() && account.getDeletedAt() == null){
				mailInfo.addTo(account.getEmail());
			}
		});
		emailService.sendAsync(mailInfo, config, accountRequest);
	}

	private void sendNotificationAuctionStart(Auction auction) {
		Map<String, Object> additionalData = new HashMap<>();
		additionalData.put("auctionId", auction.getId());

		String messageContentPattern = "Đấu giá \"{0}\" đang diễn ra.";
		String messageContent = MessageFormat.format(messageContentPattern, auction.getName());

		List<UserToken> userTokens = pushNotificationTokenService.getTokensByRoles(Role.COLLABORATOR.getCode());
		com.kinglogi.entity.Role role = Utils.requireExists(roleRepository.findByCode(Role.COLLABORATOR.getCode()), "error.dataNotFound");
		List<Account> accountList = new ArrayList<>(role.getAccounts()).stream().filter(account -> {
			if (account.getDeletedAt() != null) {
				return true;
			}
			if (account.getActive()) return true;
			return false;
		}).collect(Collectors.toList());
		Set<Long> accountIds = accountList.stream().map(Account::getId).collect(Collectors.toSet());

		// Save notification
		if(CollectionUtils.isNotEmpty(accountIds)) {
			List<NotificationRequest> notificationRequests = accountIds.stream().map(accountId -> NotificationRequest.builder()
					.title("Đấu giá")
					.content(messageContent)
					.type(NotificationType.AUCTION_STARTED.getType())
					.data(additionalData)
					.accountId(accountId)
					.build()
			).collect(Collectors.toList());
			notificationService.saveAll(notificationRequests);

			// Send push notification
			senPushNotification(notificationRequests.get(0), userTokens);
		}
	}

	private void senPushNotification(NotificationRequest notificationRequest, List<UserToken> userTokens) {
		PushNotificationDto pushNotificationRequest = PushNotificationDto.builder()
				.title(notificationRequest.getTitle())
				.body(notificationRequest.getContent())
				.type(notificationRequest.getType())
				.data(notificationRequest.getData())
				.userTokens(userTokens)
				.build();
		notificationService.sendNotification(pushNotificationRequest);
	}

	@Override
	public boolean deleteById(Long id) {
		Auction auction = getById(id);
		if(AuctionStatus.START.getStatus().equals(auction.getStatus())) {
			return false;
		}
		try {
			List<Bid> bids = bidRepository.findAllByAuctionId(auction.getId());
			if (!bids.isEmpty()) {
				refundToBiddersWhenCancel(auction, bids);
			}
			auction.getBids().clear();
			auctionRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
	}

	private AuctionResponse convertToResponse(Auction auction) {
		AuctionResponse auctionRes = BeanUtil.copyProperties(auction, AuctionResponse.class);
		List<Long> orderIds = new ArrayList<>();
		Optional.ofNullable(auction.getOrders()).orElse(new HashSet<>()).forEach(order -> {
			orderIds.add(order.getId());
		});
        UserCredential userCredential = Utils.requireExists(authenticatedProvider.getUser(), "error.userNotFound");
        Long userId = userCredential.getId();

        if (auction.getBids() != null) {
            List<Bid> bid = auction.getBids().stream().filter(bids -> bids.getAccount().getId().equals(userId) && bids.getStatus().equals(BidStatus.PENDING)).collect(Collectors.toList());

            if (bid.size() > 0) {
                auctionRes.setBidId(bid.get(0).getId());
                auctionRes.setBidPrice(bid.get(0).getBidPrice());
            } else {
                auctionRes.setBidPrice(0L);
            }
        }

		auctionRes.setOrderIds(orderIds);

		if (auction.getBids() != null){
			Optional<Bid> bidWinner = auction.getBids().stream().filter(bid -> bid.getStatus().equals(BidStatus.SUCCESS)).findFirst();
			bidWinner.ifPresent(bid -> auctionRes.setBidUserName(bid.getAccount().getFullName()));
		}

		return auctionRes;
	}

	private Auction getById(Long id) {
        return auctionRepository.findById(id)
                .orElseThrow(() -> new KlgResourceNotFoundException("Auction is not found with id [" + id + "]"));
    }

	private Set<Order> convertOrderRequestToOrder(AuctionRequest auctionRequest, Auction auction) {
		//Prepare init data for order
		return orderRepository.getPaidOrderByIds(auctionRequest.getOrderIds());
	}

    @Scheduled(fixedRate = 30000)
    @Transactional
    public void auctionReserveProcessing() {
        if (isRunning) {
            return;
        }

        try {
            isRunning = true;
            Date now = new Date();
            List<Auction> finishAuctions = auctionRepository.findAllByEndAtBeforeAndStatusIsAndType(now, AuctionStatus.START.getStatus(), AuctionType.RESERVE);
            if (!finishAuctions.isEmpty()) {
				boolean sendSms = false;
				List<Order> listOrderSendSms = new ArrayList<>();

                for (Auction auction: finishAuctions) {
                    final Long startedAuctionUserId = auction.getUpdatedBy();

                    List<Bid> winningBids = bidRepository.findTopByAuctionIdAndStatusOrderByBidPriceAscCreatedAtAsc(auction.getId(), BidStatus.PENDING);
                    if (winningBids.size() > 0) {
                        Bid winningBid = winningBids.get(0);
                        if (winningBid.getStatus().equals(BidStatus.PENDING)) {
                            // Update winning bid acceptance
                            winningBid.setStatus(BidStatus.SUCCESS);
                            winningBid.setAuction(auction);

                            // Update Auction status to finish
                            auction.setStatus(AuctionStatus.FINISH.getStatus());

                            if (!auction.getOrders().isEmpty()) {
                                List<Order> orderListToSave = new ArrayList<>();
                                for (Order order: auction.getOrders()) {
                                    order.setCollaboratorId(winningBid.getAccount().getId());
									order.setStatus(OrderStatus.READY_FOR_MOVING.getStatus());
                                    orderListToSave.add(order);
									listOrderSendSms.add(order);
									sendSms = true;
                                }

                                orderRepository.saveAll(orderListToSave);
                            }


                            // Save data
                            bidRepository.save(winningBid);
                            auctionRepository.save(auction);

							// Save report
							reportService.save(winningBid.getAccount(), null, winningBid, ReportType.BID, ReportActivityType.WINNER);

                            // Send notification end auction to startedAuctionUserId
							sendNotificationEndAuctionToAdmin(auction, startedAuctionUserId, winningBid.getAccount());
                            sendEmailEndAuction(auction, startedAuctionUserId, winningBid.getAccount().getId());

                            // Send notification to winning bidder
                            sendNotificationWinningBid(winningBid.getAccount(), auction);

                            // Refund money to loser bidder
                            Long winningBidderId = winningBid.getAccount().getId();
                            refundToLoserBidders(auction, winningBidderId);
                        } else {
                            // If already win, this auction status is not correct, will be FINISHED
                            auction.setStatus(AuctionStatus.FINISH.getStatus());
                            auctionRepository.save(auction);
                        }

						if (winningBids.size() > 1){
							List<Bid> loseBids = winningBids.subList(1, winningBids.size() -1 );
							loseBids.forEach(bid -> {
								if (bid.getStatus().equals(BidStatus.PENDING)){
									bid.setStatus(BidStatus.FAIL);
								}
							});
							bidRepository.saveAll(loseBids);
						}
                    } else {
                        // If can not find a winner => Data error or have some problem with system, the auction will be stopped
                        auction.setStatus(AuctionStatus.STOP.getStatus());
                        auctionRepository.save(auction);
                    }
                }

				if (sendSms){
					for (Order orderSendSms : listOrderSendSms) {
						Account account = accountRepository.getOne(orderSendSms.getCollaboratorId());
						Map<String, String> map = new HashMap<>();
						map.put("orderCode", orderSendSms.getOrderCode());
						if (account.getFullName() != null && !account.getFullName().isEmpty()){
							map.put("nameDriver", account.getFullName());
						}else {
							map.put("nameDriver", "N/A");
						}
						if (account.getPhoneNumber() != null && !account.getPhoneNumber().isEmpty()){
							map.put("phoneNumber", account.getPhoneNumber());
						}else {
							map.put("phoneNumber", "N/A");
						}
						Car car = account.getCars().stream().filter(c -> c.getNumberOfSeat().equals(orderSendSms.getNumberOfSeat())).findFirst().orElse(null);
						if (car != null && car.getBrand() != null && car.getBrand() != null && car.getLicensePlates() != null){
							map.put("licensePlates", car.getBrand() + " " + car.getLicensePlates());
						}else {
							map.put("licensePlates", "N/A");
						}
						try {
							otpService.sendSms(orderSendSms.getPhoneNumber(), TypeSms.INFORMATION_DRIVER, map);
						} catch (IOException e) {
							log.error(e.getMessage());
						}
					}
				}
            }
        } catch (Exception e) {
            log.error("Got exception during finishing auction", e);
        } finally {
            isRunning = false;
        }
    }

	private void sendNotificationEndAuctionToAdmin(Auction auction, Long startedAuctionUserId, Account winningBidder) {
		Map<String, Object> additionalData = new HashMap<>();
		additionalData.put("auctionId", auction.getId());
		String name = winningBidder.getFullName() != null ? winningBidder.getFullName() : winningBidder.getUsername();

		String messageContentPattern = "Đấu giá [{0}] kết thúc! CTV [{1}] đã thắng đấu giá";
		String messageContent = MessageFormat.format(messageContentPattern, auction.getName(), name);

		NotificationRequest notificationRequest = NotificationRequest.builder()
				.title("Đấu giá")
				.content(messageContent)
				.type(NotificationType.AUCTION_END.getType())
				.data(additionalData)
				.accountId(startedAuctionUserId) // send to admin
				.build();

		notificationService.save(notificationRequest);

		List<UserToken> userTokens = pushNotificationTokenService.getTokensByAccountId(notificationRequest.getAccountId());
		senPushNotification(notificationRequest, userTokens);
	}


	private void sendEmailEndAuction(Auction auctionResponse, Long startedAuctionUserId, Long userBidId) {
        Account account = Utils.requireExists(accountRepository.findById(startedAuctionUserId), "error.dataNotFound");
        Account accountBid = Utils.requireExists(accountRepository.findById(userBidId), "error.dataNotFound");
        AuctionMail accountRequest = AuctionMail.builder()
            .name(auctionResponse.getName())
            .bidUserName(accountBid.getFullName())
            .build();

        EmailTemplateResponse emailTemplateResponse = emailTemplateService.findByTypeAndLanguage(EmailTemplateType.FINISH_AUCTION.name(), "vi");
        MailTemplateConfig config = MailTemplateConfig.builder()
            .mailContent(emailTemplateResponse.getContent())
            .mailSubject(emailTemplateResponse.getTitle())
            .mailIsHtml(true)
            .build();

        MailInfo mailInfo = new MailInfo();
        mailInfo.addTo(account.getEmail());
        emailService.sendAsync(mailInfo, config, accountRequest);
    }

    private void sendNotificationWinningBid(Account winningBidder, Auction auction) {
        Map<String, Object> additionalData = new HashMap<>();
        additionalData.put("auctionId", auction.getId());
		String name = winningBidder.getFullName() != null ? winningBidder.getFullName() : winningBidder.getUsername();

        String messageContentPattern = "CTV [{0}] đã đấu giá thắng đấu giá [{1}]";
        String messageContent = MessageFormat.format(messageContentPattern, name, auction.getName());

        NotificationRequest notificationRequest = NotificationRequest.builder()
            .title("Đấu giá")
            .content(messageContent)
            .type(NotificationType.WINNING_A_BID.getType())
            .data(additionalData)
            .accountId(winningBidder.getId()) // send to winning bidder
            .build();

        notificationService.save(notificationRequest);

        List<UserToken> userTokens = pushNotificationTokenService.getTokensByAccountId(notificationRequest.getAccountId());
        senPushNotification(notificationRequest, userTokens);
    }

    private void sendNotificationLosingBid(Account losingBidder, Auction auction) {
        Map<String, Object> additionalData = new HashMap<>();
        additionalData.put("auctionId", auction.getId());
		String name = losingBidder.getFullName() != null ? losingBidder.getFullName() : losingBidder.getUsername();

        String messageContentPattern = "CTV [{0}] đã đấu giá thất bại đấu giá [{1}], số tiền đã đặt sẽ được hoàn trả.";
        String messageContent = MessageFormat.format(messageContentPattern, name, auction.getName());

        NotificationRequest notificationRequest = NotificationRequest.builder()
            .title("Đấu giá")
            .content(messageContent)
            .type(NotificationType.LOSING_A_BID.getType())
            .data(additionalData)
            .accountId(losingBidder.getId()) // send to losing bidder
            .build();

        notificationService.save(notificationRequest);

        List<UserToken> userTokens = pushNotificationTokenService.getTokensByAccountId(notificationRequest.getAccountId());
        senPushNotification(notificationRequest, userTokens);
    }

	private void sendNotificationCancelAuction(Account bidder, Auction auction) {
		Map<String, Object> additionalData = new HashMap<>();
		additionalData.put("auctionId", auction.getId());
		String name = bidder.getFullName() != null ? bidder.getFullName() : bidder.getUsername();

		String messageContentPattern = "Admin đã hủy cuộc đấu giá [{0}], số tiền đã đặt sẽ được hoàn trả.";
		String messageContent = MessageFormat.format(messageContentPattern, auction.getName());

		NotificationRequest notificationRequest = NotificationRequest.builder()
				.title("Đấu giá")
				.content(messageContent)
				.type(NotificationType.AUCTION_CANCEL.getType())
				.data(additionalData)
				.accountId(bidder.getId()) // send to losing bidder
				.build();

		notificationService.save(notificationRequest);

		List<UserToken> userTokens = pushNotificationTokenService.getTokensByAccountId(notificationRequest.getAccountId());
		senPushNotification(notificationRequest, userTokens);
	}

    private void refundToLoserBidders(Auction auction, Long winningBidderId) {
        List<Long> winningBids = new ArrayList<>();
        winningBids.add(winningBidderId);
        List<Bid> loserBids = bidRepository.findAllByAccountIdNotInAndStatusAndAuctionId(winningBids, BidStatus.PENDING, auction.getId());
        if (loserBids.size() > 0) {
            for (Bid bid: loserBids) {
				bid.setStatus(BidStatus.FAIL);

                Account loserBidder = bid.getAccount();

                // Refund bid to loser bidder
                performRefundBidTransaction(loserBidder.getId(), auction, bid.getBidPrice());

                // Send notification to loser bidder
                sendNotificationLosingBid(loserBidder, auction);
            }
			bidRepository.saveAll(loserBids);
        }
    }

    private void performRefundBidTransaction(Long userId, Auction auction, Long bidPrice) {
        TransactionRequest transactionRequest = new TransactionRequest();
        transactionRequest.setTargetId(userId);
        transactionRequest.setAmount(bidPrice);
        Date transactionAt = Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant());
        transactionRequest.setTransactionAt(transactionAt);
		transactionRequest.setTransactionType(PaymentType.BALANCE.name());
        transactionRequest.setContent("Đấu giá thất bại! Hoàn tiền đặt đấu giá " + auction.getName() + " lúc " +
            LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm")));
        transactionService.save(transactionRequest);
    }

	private void performRefundBidTransactionWhenCancel(Long userId, Auction auction, Long bidPrice) {
		TransactionRequest transactionRequest = new TransactionRequest();
		transactionRequest.setTargetId(userId);
		transactionRequest.setAmount(bidPrice);
		Date transactionAt = Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant());
		transactionRequest.setTransactionAt(transactionAt);
		transactionRequest.setTransactionType(PaymentType.BALANCE.name());
		transactionRequest.setContent("Đấu giá bị hủy! Hoàn tiền đặt đấu giá " + auction.getName() + " lúc " +
				LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm")));
		transactionService.save(transactionRequest);
	}

	@Override
	public void cancelAuctionByAdmin(Long id) {
		UserCredential userCredential = Utils.requireExists(authenticatedProvider.getUser(), "error.userNotFound");
		Long userId = userCredential.getId();
		Auction auction = getById(id);
		if (!(userACL.isSuperAdmin() || Objects.equals(userId, auction.getCreatedBy()))) {
			throw new AccessForbiddenException("error.notAdmin");
		}
		if (Objects.equals(auction.getStatus(), AuctionStatus.FINISH.getStatus())) {
			throw new BadRequestException("error.auctionAlreadyFinished");
		}
		auction.setStatus(AuctionStatus.STOP.getStatus());
		auctionRepository.save(auction);
		List<Bid> bids = bidRepository.findAllByAuctionId(auction.getId());
		if (!bids.isEmpty()) {
			refundToBiddersWhenCancel(auction, bids);
		}
	}

	private void refundToBiddersWhenCancel(Auction auction, List<Bid> bids) {
		if (bids.size() > 0) {
			for (Bid bid: bids) {
				Account bidder = bid.getAccount();

				// Refund bid to loser bidder
				performRefundBidTransactionWhenCancel(bidder.getId(), auction, bid.getBidPrice());

				// Send notification to loser bidder
				sendNotificationCancelAuction(bidder, auction);
			}
		}
	}
}
