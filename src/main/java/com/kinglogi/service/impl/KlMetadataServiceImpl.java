package com.kinglogi.service.impl;

import com.kinglogi.dto.KlConfiguration;
import com.kinglogi.entity.KlMetadata;
import com.kinglogi.repository.custom.CustomKlMetadataRepository;
import com.kinglogi.service.KlMetadataService;
import com.kinglogi.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class KlMetadataServiceImpl implements KlMetadataService {

    private static final String CONFIGURATION_CODE = "CONFIGURATION";

    private final CustomKlMetadataRepository repository;

    @Autowired
    public KlMetadataServiceImpl(@Qualifier("customKlMetadataRepository") CustomKlMetadataRepository repository) {
        this.repository = repository;
    }

    @Override
    public boolean updateConfiguration(KlConfiguration configuration) {
        Optional<KlMetadata> metadata = repository.findByCode(CONFIGURATION_CODE);
        KlMetadata entity = metadata.orElseGet(KlMetadata::new);
        entity.setCode(CONFIGURATION_CODE);
        entity.setContent(JsonUtils.toJson(configuration));
        repository.save(entity);
        return true;
    }

    @Override
    public KlConfiguration getConfiguration() {
        Optional<KlMetadata> metadata = repository.findByCode(CONFIGURATION_CODE);
        if(metadata.isPresent()) {
            KlMetadata entity = metadata.get();
            return JsonUtils.fromJson(entity.getContent(), KlConfiguration.class);
        }
        return new KlConfiguration();
    }
}
