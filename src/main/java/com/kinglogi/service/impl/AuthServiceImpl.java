package com.kinglogi.service.impl;

import com.kinglogi.configuration.security.JwtSetting;
import com.kinglogi.configuration.security.SmsSetting;
import com.kinglogi.constant.*;
import com.kinglogi.dto.AuthUserDetails;
import com.kinglogi.dto.mail.MailInfo;
import com.kinglogi.dto.mail.MailTemplateConfig;
import com.kinglogi.dto.request.*;
import com.kinglogi.dto.response.AccountTempResponse;
import com.kinglogi.dto.response.EmailTemplateResponse;
import com.kinglogi.dto.response.JwtAuthenticationToken;
import com.kinglogi.entity.Account;
import com.kinglogi.entity.AccountOtp;
import com.kinglogi.entity.Feature;
import com.kinglogi.entity.PushNotificationToken;
import com.kinglogi.exception.InvalidJwtToken;
import com.kinglogi.exception.KlgAuthenticationException;
import com.kinglogi.exception.KlgResourceNotFoundException;
import com.kinglogi.provider.AuthenticatedProvider;
import com.kinglogi.repository.AccountOtpRepository;
import com.kinglogi.repository.PushNotificationTokenRepository;
import com.kinglogi.repository.custom.CustomAccountRepository;
import com.kinglogi.service.*;
import com.kinglogi.service.error.BadRequestException;
import com.kinglogi.token.AccessToken;
import com.kinglogi.token.factory.JwtTokenFactory;
import com.kinglogi.token.parser.TokenParser;
import com.kinglogi.token.verifier.TokenVerifier;
import com.kinglogi.utils.BeanUtil;
import com.kinglogi.utils.EncrytedPasswordUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Service
@Slf4j
public class AuthServiceImpl implements AuthService {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenFactory tokenFactory;

    @Autowired
    private TokenVerifier tokenVerifier;

    @Autowired
    @Qualifier("customAccountRepository")
    private CustomAccountRepository userRepository;

    @Autowired
    private JwtSetting setting;

    @Autowired
    private TokenParser tokenParser;

    @Autowired
    @Qualifier("accountService")
    private AccountService accountService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private EmailTemplateService emailTemplateService;

    @Autowired
    private AccountTempService accountTempService;

    @Autowired
    private AuthenticatedProvider authenticatedProvider;

    @Autowired
    private PushNotificationTokenRepository pushNotificationTokenRepository;

    @Autowired
    private AccountHistoryService accountHistoryService;

    @Autowired
    private AccountOtpRepository accountOtpRepository;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    OtpService otpService;

    @Override
    public JwtAuthenticationToken login(LoginRequest loginRequest) {
        Account account = accountService.getByUserNameOrPhone(loginRequest.getUsername());
        if (!account.getActive() || account.getDeletedAt() != null) {
            throw new KlgAuthenticationException("error.accountInactive");
        }
        Authentication auth = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(account.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(auth);

        // Log to report
        accountHistoryService.accountLogin(account);

        return this.createResponse((AuthUserDetails) auth.getPrincipal());
    }

    @Override
    @Transactional
    public Instant loginSendOtp(String phoneNumber) throws IOException {
        Account account = accountService.getByPhoneNumber(phoneNumber);
        accountOtpRepository.deleteAllByPhoneNumber(account.getPhoneNumber());
        return otpService.sendOTP(phoneNumber);
    }

    @Transactional
    @Override
    public JwtAuthenticationToken loginWithOtp(String otp, String phoneNumber) {
        Optional<AccountOtp> accountOtp = accountOtpRepository.findByOtpAndPhoneNumber(otp, phoneNumber, Instant.now());
        if (accountOtp.isPresent()){
            Account account = accountService.getByPhoneNumber(accountOtp.get().getPhoneNumber());

            UserDetails user = userDetailsService.loadUserByUsername(account.getUsername());
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                    user,
                    null,
                    user.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            accountOtpRepository.deleteAllByPhoneNumber(account.getPhoneNumber());

            // Log to report
            accountHistoryService.accountLogin(account);

            return this.createResponse((AuthUserDetails) authenticationToken.getPrincipal());
        }else {
            throw new BadRequestException("error.otpInvalid");
        }
    }

    @Transactional
    @Override
    public JwtAuthenticationToken register(RegisterRequest registerRequest, String otp) {
        // this is register by web and collaborator app
        if (otp == null){
            if (registerRequest.getPhoneNumber() == null || StringUtils.isBlank(registerRequest.getPhoneNumber())) {
                throw new BadRequestException("error.phoneNumberEmptyOrBlank");
            }
            if (accountService.isUsernameExisted(registerRequest.getUsername().trim()).isPresent()) {
                throw new BadRequestException("error.userNameExisted");
            }
            if (accountService.isPhoneNumberExisted(registerRequest.getPhoneNumber().trim()).isPresent()) {
                throw new BadRequestException("error.phoneNumberExisted");
            }
            AccountRequest accountRequest = BeanUtil.copyProperties(registerRequest, AccountRequest.class);


            accountRequest.setActive(registerRequest.getActive() == null ? Boolean.FALSE : registerRequest.getActive());
            accountRequest.setEmail(registerRequest.getUsername());
            if (registerRequest.getRoles() == null || registerRequest.getRoles().size() == 0) {
                accountRequest.setRoles(Collections.singletonList(Role.MEMBER.getCode()));
                accountRequest.setActive(true);
            }
            accountService.save(accountRequest);

            this.sendEmailWelcome(accountRequest.getEmail(), registerRequest);

            // If login on web, set account to active and login right away
            if (accountRequest.getRoles().size() == 1 && accountRequest.getRoles().contains(Role.MEMBER.getCode())) {
                LoginRequest loginRequest = LoginRequest.builder()
                        .username(registerRequest.getUsername())
                        .password(registerRequest.getPassword())
                        .build();
                return login(loginRequest);
            }
            return null;
        }else {
            // this is register by customer app
            Optional<AccountOtp> accountOtp = accountOtpRepository.findByOtpAndPhoneNumber(otp, registerRequest.getPhoneNumber(), Instant.now());
            if (accountOtp.isPresent()){
                AccountRequest accountRequest = BeanUtil.copyProperties(registerRequest, AccountRequest.class);
                accountRequest.setActive(registerRequest.getActive() == null ? Boolean.FALSE : registerRequest.getActive());
                accountRequest.setEmail(registerRequest.getUsername());
                if (registerRequest.getRoles() == null || registerRequest.getRoles().size() == 0) {
                    accountRequest.setRoles(Collections.singletonList(Role.MEMBER.getCode()));
                    accountRequest.setActive(true);
                }
                accountService.save(accountRequest);

                this.sendEmailWelcome(accountRequest.getEmail(), registerRequest);


                Account account = accountService.getByPhoneNumber(accountOtp.get().getPhoneNumber());

                UserDetails user = userDetailsService.loadUserByUsername(account.getUsername());
                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                        user,
                        null,
                        user.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                accountOtpRepository.deleteAllByPhoneNumber(account.getPhoneNumber());

                // Log to report
                accountHistoryService.accountLogin(account);

                return this.createResponse((AuthUserDetails) authenticationToken.getPrincipal());
            }else {
                throw new BadRequestException("error.otpInvalid");
            }
        }
    }

    @Transactional
    @Override
    public Instant registerBySms(RegisterRequest registerRequest) throws IOException {
        if (registerRequest.getPhoneNumber() == null || StringUtils.isBlank(registerRequest.getPhoneNumber())) {
            throw new BadRequestException("error.phoneNumberEmptyOrBlank");
        }
        if (accountService.isUsernameExisted(registerRequest.getUsername().trim()).isPresent()) {
            throw new BadRequestException("error.userNameExisted");
        }
        if (accountService.isPhoneNumberExisted(registerRequest.getPhoneNumber().trim()).isPresent()) {
            throw new BadRequestException("error.phoneNumberExisted");
        }
        accountOtpRepository.deleteAllByPhoneNumber(registerRequest.getPhoneNumber());
        return otpService.sendOTP(registerRequest.getPhoneNumber());
    }

    @Override
    public Boolean registerByPartner(RegisterRequest registerRequest) {
        AccountRequest accountRequest = BeanUtil.copyProperties(registerRequest, AccountRequest.class);
        accountRequest.setRoles(Collections.singletonList(Role.PARTNER.getCode()));
        accountRequest.setActive(false);
        accountRequest.setEmail(registerRequest.getUsername());
        accountService.save(accountRequest);
        return true;
    }

    private void sendEmailWelcome(String email, RegisterRequest registerRequest) {
        try {
            EmailTemplateResponse emailTemplateResponse = emailTemplateService.findByTypeAndLanguage(EmailTemplateType.WELCOME.name(), registerRequest.getLanguage());
            MailTemplateConfig config = MailTemplateConfig.builder()
                    .mailContent(emailTemplateResponse.getContent())
                    .mailSubject(emailTemplateResponse.getTitle())
                    .mailIsHtml(true)
                    .build();

            MailInfo mailInfo = new MailInfo();
            mailInfo.addTo(email);
            emailService.sendAsync(mailInfo, config, registerRequest);
        } catch(KlgResourceNotFoundException e) {
            log.warn("There is no email template for sending a welcome email to a new user");
        }
    }

    private JwtAuthenticationToken createResponse(AuthUserDetails userDetail) {
        AccessToken accessToken = tokenFactory.createAccessToken(userDetail);
        return JwtAuthenticationToken.builder()
                .accessToken(accessToken.getToken())
                .refreshToken(tokenFactory.createRefreshToken(userDetail).getToken())
                .tokenType(Auth.BEARER_PREFIX)
                .build();
    }

    @Override
    public JwtAuthenticationToken refreshToken(RefreshTokenRequest request) {
        if (tokenVerifier.verify(request.getRefreshToken()) && checkRefreshTokenScope(request.getRefreshToken())) {
            Long userId = tokenParser.getUserIdFromJWT(request.getRefreshToken(), setting.getTokenSigningKey());
            Account account = userRepository.findById(userId).orElseThrow(() -> new KlgResourceNotFoundException("User is not exists by getting with id " + userId));
            AuthUserDetails userDetail = new AuthUserDetails(account, this.getFeatures(account));
            AccessToken accessToken = tokenFactory.createAccessToken(userDetail);

            return JwtAuthenticationToken.builder()
                    .accessToken(accessToken.getToken())
                    .refreshToken(request.getRefreshToken())
                    .tokenType(Auth.BEARER_PREFIX)
                    .build();
        }
        throw new InvalidJwtToken("The refresh token is invalid");
    }

    @Override
    public boolean forgotPassword(ForgotPasswordRequest request) {
        userRepository.findActivatedUserByEmail(request.getEmail()).orElseThrow(() ->
                new KlgResourceNotFoundException("User is not exists by getting with email " + request.getEmail()));

        EmailTemplateResponse emailTemplateResponse = emailTemplateService.findByTypeAndLanguage(EmailTemplateType.FORGOT_PASSWORD.name(), request.getLanguage());
        MailTemplateConfig config = MailTemplateConfig.builder()
                .mailContent(emailTemplateResponse.getContent())
                .mailSubject(emailTemplateResponse.getTitle())
                .mailIsHtml(true)
                .build();

        AccountTempResponse accountTempResponse = accountTempService.createAccountTemp(request.getEmail());

        MailInfo mailInfo = new MailInfo();
        mailInfo.addTo(request.getEmail());
        emailService.sendAsync(mailInfo, config, accountTempResponse);
        return true;
    }

    @Override
    public JwtAuthenticationToken changePassword(ChangePasswordRequest request) {
        String email = request.getEmail();
        final Account account = userRepository.findActivatedUserByEmail(email).orElseThrow(
                () -> new KlgResourceNotFoundException("User is not exists by getting with email " + email));

        if ((authenticatedProvider.getUserId().isPresent() && EncrytedPasswordUtils.isMatched(request.getPassword(), account.getPassword())) ||
                accountTempService.isValid(email, request.getPassword())) {
            // Save a new password
            account.setPassword(EncrytedPasswordUtils.encryptPassword(request.getNewPassword()));
            userRepository.save(account);

            // Delete temp account
            accountTempService.delete(email);

            // Return token
            LoginRequest loginRequest = LoginRequest.builder()
                    .username(email)
                    .password(request.getNewPassword())
                    .build();
            return login(loginRequest);
        }
        throw new KlgResourceNotFoundException("Incorrect password or password is expired");
    }

    @Override
    public void logout(String deviceId, String appName) {
        Long userId = authenticatedProvider.getUserId().get();
        List<PushNotificationToken> pushFCMTokenId = pushNotificationTokenRepository.getTokenByAccountId(userId, deviceId, appName);
        pushNotificationTokenRepository.deleteInBatch(pushFCMTokenId);
    }

    private List<Feature> getFeatures(Account account) {
        List<Feature> features = new ArrayList<>();
        account.getRoles().forEach(role -> features.addAll(role.getFeatures()));
        return features;
    }

    private boolean checkRefreshTokenScope(String refreshToken) {
        Jws<Claims> claimsJws = tokenParser.parseClaims(refreshToken, setting.getTokenSigningKey());
        Claims claims = claimsJws.getBody();
        List<String> scopes = (List<String>) claims.get(JwtTokenFactory.TOKEN_SCOPES_KEY);
        if (CollectionUtils.isEmpty(scopes)) {
            throw new InvalidJwtToken("The refresh token is invalid scope");
        }
        for (String scope : scopes) {
            if(Scope.REFRESH_TOKEN.authority().equals(scope)) {
                return true;
            }
        }
        return false;
    }
}
