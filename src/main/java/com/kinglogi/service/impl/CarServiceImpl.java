package com.kinglogi.service.impl;

import com.kinglogi.dto.filter.CarFilter;
import com.kinglogi.dto.filter.base.BaseFilter;
import com.kinglogi.dto.request.CarRequest;
import com.kinglogi.dto.response.CarResponse;
import com.kinglogi.dto.response.file_response.FileCarResponse;
import com.kinglogi.entity.Account;
import com.kinglogi.entity.Car;
import com.kinglogi.entity.CarImage;
import com.kinglogi.entity.Image;
import com.kinglogi.exception.KlgResourceNotFoundException;
import com.kinglogi.repository.CarImageRepository;
import com.kinglogi.repository.CarRepository;
import com.kinglogi.repository.ImageRepository;
import com.kinglogi.service.CarService;
import com.kinglogi.service.UserACL;
import com.kinglogi.service.error.AccessForbiddenException;
import com.kinglogi.service.error.BadRequestException;
import com.kinglogi.service.utils.Utils;
import com.kinglogi.specs.BaseFilterSpecs;
import com.kinglogi.utils.BeanUtil;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.criteria.Join;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CarServiceImpl implements CarService {

    private final UserACL userACL;

    private final CarRepository carRepository;

    private final ImageRepository imageRepository;

    private final BaseFilterSpecs<Car> baseFilterSpecs;

    public static final String FILE_DIRECTORY =  System.getProperty("user.dir") + File.separator + "storage";

    public final CarImageRepository carImageRepository;

    @Autowired
    public CarServiceImpl(UserACL userACL, CarRepository carRepository, ImageRepository imageRepository, BaseFilterSpecs<Car> baseFilterSpecs, CarImageRepository carImageRepository) {
        this.userACL = userACL;
        this.carRepository = carRepository;
        this.imageRepository = imageRepository;
        this.baseFilterSpecs = baseFilterSpecs;
        this.carImageRepository = carImageRepository;
    }

    @Override
    public Page<CarResponse> findAll(BaseFilter baseFilter) {
        CarFilter carFilter = (CarFilter) baseFilter;
        Specification<Car> searchable = baseFilterSpecs.search(carFilter);
        Specification<Car> additionalSpec = (root, query, cb) -> {
            Join<Car, Account> account = root.join("account");
            return cb.equal(account.get("id"), carFilter.getAccountId());
        };
        searchable = searchable.and(additionalSpec);
        Pageable pageable = baseFilterSpecs.page(carFilter);
        Page<Car> page = carRepository.findAll(searchable, pageable);
        return page.map(entity -> BeanUtil.copyProperties(entity, CarResponse.class));
    }

    @Override
    public CarResponse findById(Long id) {
        Car car = getById(id);
        return BeanUtil.copyProperties(car, CarResponse.class);
    }

    private Car getById(Long id) {
        return carRepository.findById(id).orElseThrow(() -> new KlgResourceNotFoundException("Car is not found with id [" + id + "]"));
    }

    @Override
    public CarResponse save(CarRequest request) {
        Account account = new Account();
        account.setId(request.getAccountId());

        Car car = BeanUtil.copyProperties(request, Car.class);
        car.setAccount(account);
        Car savedCar = carRepository.save(car);
        return BeanUtil.copyProperties(savedCar, CarResponse.class);
    }

    @Override
    public CarResponse update(Long id, CarRequest request) {
        Car car = getById(id);
        BeanUtil.mergeProperties(request, car);
        Car savedCar = carRepository.save(car);
        return BeanUtil.copyProperties(savedCar, CarResponse.class);
    }

    @Override
    public boolean deleteById(Long id) {
        try {
            carRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }

    @Override
    public void uploadImages(Long carId, List<MultipartFile> files) {
        if (!(userACL.isSuperAdmin() || userACL.isFlexibleAdmin() || userACL.isFixedAdmin())) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        if (files.size() > 4) {
            throw new BadRequestException("error.uploadMaxOnly4Images");
        }
        Car car = getById(carId);
        Long accountId = car.getAccount().getId();
        String filePath = FILE_DIRECTORY + "/collaborator/" + accountId + "/car/" + carId;
        File fileDirectory = new File(filePath);
        if (!fileDirectory.exists()) {
            fileDirectory.mkdirs();
        }
        List<File> fileList = files.stream().map(file -> {
            try {
                return convertMultiPartToFile(file, filePath);
            } catch (IOException e) {
                return null;
            }
        }).collect(Collectors.toList());
        Set<Image> images = new HashSet<>();
        for (File file : fileList) {
            Image image = new Image();
            image.setDisplayName(file.getName());
            image.setOrginal(file.getAbsolutePath());
            images.add(image);
        }
        imageRepository.saveAll(images);
        List<CarImage> carImages = images.stream().map(image -> {
            CarImage carImage = new CarImage();
            carImage.setCar(car);
            carImage.setImage(image);
            return carImage;
        }).collect(Collectors.toList());
        carImageRepository.saveAll(carImages);
    }

    @Override
    public InputStreamResource viewImage(Long imageId, String fileName) throws IOException {
        Image image = Utils.requireExists(imageRepository.findById(imageId), "imageNotFound");
        InputStream in = Files.newInputStream(Paths.get(image.getOrginal()));
        return new InputStreamResource(in);
    }


    public List<FileCarResponse> getImage(Long id) {
        Car car = getById(id);
        Page<Image> images = imageRepository.findAllByCarId(car.getId(), PageRequest.of(0, 4));
        return images.map(FileCarResponse::new).getContent();
    }

    private File convertMultiPartToFile(MultipartFile file, String filePath) throws IOException {
        String fileNameWithoutExtension = FilenameUtils.removeExtension(file.getOriginalFilename());
        File convFile = new File(filePath, fileNameWithoutExtension + ".jpg");
        int num = 0;
        while (convFile.exists()) {
            convFile = new File(filePath, fileNameWithoutExtension + "(" + (++num) + ").jpg");
        }
        FileOutputStream fos = new FileOutputStream( convFile );
        fos.write( file.getBytes() );
        fos.close();
        return convFile;
    }
}
