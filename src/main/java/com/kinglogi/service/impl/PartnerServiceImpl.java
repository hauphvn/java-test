package com.kinglogi.service.impl;

import com.kinglogi.constant.AppClientType;
import com.kinglogi.dto.filter.base.BaseFilter;
import com.kinglogi.dto.request.PartnerRequest;
import com.kinglogi.dto.response.AccountResponse;
import com.kinglogi.dto.response.AppClientResponse;
import com.kinglogi.dto.response.PartnerResponse;
import com.kinglogi.entity.Account;
import com.kinglogi.entity.AppClient;
import com.kinglogi.entity.Partner;
import com.kinglogi.exception.KlgResourceNotFoundException;
import com.kinglogi.repository.PartnerRepository;
import com.kinglogi.service.AccountService;
import com.kinglogi.service.AppClientService;
import com.kinglogi.service.PartnerService;
import com.kinglogi.service.UserACL;
import com.kinglogi.service.error.AccessForbiddenException;
import com.kinglogi.service.utils.Utils;
import com.kinglogi.specs.BaseFilterSpecs;
import com.kinglogi.utils.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Qualifier("partnerService")
public class PartnerServiceImpl implements PartnerService {

	private final PartnerRepository partnerRepository;

	private final BaseFilterSpecs<Partner> baseFilterSpecs;

	private final AccountService accountService;

	private final AppClientService appClientService;

	private final UserACL userACL;

	@Autowired
	public PartnerServiceImpl(PartnerRepository partnerRepository,
							  BaseFilterSpecs<Partner> baseFilterSpecs,
							  @Qualifier("accountService") AccountService accountService,
							  AppClientService appClientService, UserACL userACL) {
		this.partnerRepository = partnerRepository;
		this.baseFilterSpecs = baseFilterSpecs;
		this.accountService = accountService;
		this.appClientService = appClientService;
		this.userACL = userACL;
	}

	@Override
	public Page<PartnerResponse> findAll(BaseFilter baseFilter) {
		Specification<Partner> searchable = baseFilterSpecs.search(baseFilter);
		Pageable pageable = baseFilterSpecs.page(baseFilter);
		Page<Partner> page = partnerRepository.findAllActive(searchable, pageable);
		return page.map(this::buildPartnerResponse);
	}

	@Override
	public PartnerResponse findById(Long id) {
		Partner partner = getById(id);
		return buildPartnerResponse(partner);
	}

	private PartnerResponse buildPartnerResponse(Partner partner) {
		PartnerResponse response = BeanUtil.copyProperties(partner, PartnerResponse.class);
		AccountResponse accountResponse = BeanUtil.copyProperties(partner.getAccount(), AccountResponse.class);
		Integer loyaltyPoint = accountResponse.getLoyaltyPoint() != null ? accountResponse.getLoyaltyPoint() : 0;
		accountResponse.setRankPoint(Utils.classifyRank(loyaltyPoint));
		response.setAccountResponse(accountResponse);
		response.setAppClientResponses(BeanUtil.listCopyProperties(partner.getAppClients(), AppClientResponse.class));
		return response;
	}

	private Partner getById(Long id) {
		return partnerRepository.findById(id).orElseThrow(() -> new KlgResourceNotFoundException("Partner is not found with id [" + id + "]"));
	}

	@Override
	public PartnerResponse save(PartnerRequest request) {
		Partner partner = BeanUtil.copyProperties(request, Partner.class);
		if (request.getAccountRequest() != null) {
			AccountResponse accountResponse = accountService.save(request.getAccountRequest());
			Account account = new Account();
			account.setId(accountResponse.getId());
			partner.setAccount(account);
		}
		Partner savedPartner = partnerRepository.save(partner);
		appClientService.saveAll(createAppClients(partner.getId()));
		return buildPartnerResponse(savedPartner);
	}

	private List<AppClient> createAppClients(Long partnerId) {
		Partner partner = new Partner();
		partner.setId(partnerId);

		List<AppClient> appClients = new ArrayList<>();
		appClients.add(createAppClient(AppClientType.PARTNER_IFRAME, partner));
		appClients.add(createAppClient(AppClientType.PARTNER_WEB, partner));
		return appClients;
	}

	private AppClient createAppClient(AppClientType type, Partner partner) {
		AppClient appClient = appClientService.create(type);
		appClient.setPartner(partner);
		return appClient;
	}

	@Override
	public PartnerResponse update(Long id, PartnerRequest request) {
		Partner partner = getById(id);
		AccountResponse accountResponse = updateAccount(partner.getAccount().getId(), request);

		BeanUtil.mergeProperties(request, partner);
		partner.setAccount(BeanUtil.copyProperties(accountResponse, Account.class));
		Partner savedPartner = partnerRepository.save(partner);
		return buildPartnerResponse(savedPartner);
	}

	private AccountResponse updateAccount(Long accountId, PartnerRequest request) {
		request.getAccountRequest().setId(accountId);
		return accountService.update(accountId, request.getAccountRequest());
	}

	@Transactional
	@Override
	public boolean deleteById(Long id) {
		try {
			if (!userACL.isSuperAdmin()){
				throw new AccessForbiddenException("error.notPermission");
			}
			Partner partner = partnerRepository.getOne(id);
			partnerRepository.softDelete(id);
			accountService.deleteById(partner.getAccount().getId());
			return true;
		} catch (EmptyResultDataAccessException e) {
			return false;
		}
	}


}
