package com.kinglogi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.kinglogi.dto.response.RoleResponse;
import com.kinglogi.repository.custom.CustomRoleRepository;
import com.kinglogi.service.RoleService;
import com.kinglogi.utils.BeanUtil;

@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	@Qualifier("customRoleRepository")
    private CustomRoleRepository customRoleRepository;

    @Override
    public List<RoleResponse> findAll() {
        return BeanUtil.listCopyProperties(customRoleRepository.findAll(), RoleResponse.class);
    }

}
