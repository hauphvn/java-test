package com.kinglogi.service.impl;

import com.kinglogi.constant.ComplaintConstant;
import com.kinglogi.dto.filter.ComplaintFilter;
import com.kinglogi.dto.filter.base.BaseFilter;
import com.kinglogi.dto.mail.MailInfo;
import com.kinglogi.dto.mail.SimpleMailInfo;
import com.kinglogi.dto.request.ComplaintRequest;
import com.kinglogi.dto.response.ComplaintResponse;
import com.kinglogi.dto.response.FileAttachmentResponse;
import com.kinglogi.entity.Account;
import com.kinglogi.entity.Complaint;
import com.kinglogi.entity.FileAttachment;
import com.kinglogi.entity.Order;
import com.kinglogi.provider.AuthenticatedProvider;
import com.kinglogi.repository.AccountRepository;
import com.kinglogi.repository.ComplaintRepository;
import com.kinglogi.repository.FileAttachmentRepository;
import com.kinglogi.service.ComplaintService;
import com.kinglogi.service.EmailService;
import com.kinglogi.service.UserACL;
import com.kinglogi.service.error.AccessForbiddenException;
import com.kinglogi.service.error.BadRequestException;
import com.kinglogi.service.error.ResourceNotFoundException;
import com.kinglogi.service.utils.Utils;
import com.kinglogi.specs.BaseFilterSpecs;
import com.kinglogi.utils.BeanUtil;
import com.kinglogi.utils.EmailUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Qualifier("ComplaintService")
public class ComplaintServiceImpl implements ComplaintService {

    private final ComplaintRepository complaintRepository;

    private final AccountRepository accountRepository;

    private final BaseFilterSpecs<Complaint> baseFilterSpecs;

    private final AuthenticatedProvider authenticatedProvider;

    private final UserACL userACL;

    public static final String FILE_DIRECTORY =  System.getProperty("user.dir") + File.separator + "storage/";

    private final FileAttachmentRepository fIleAttachmentRepository;

    private final EmailService emailService;

    public ComplaintServiceImpl(ComplaintRepository complaintRepository,  AccountRepository accountRepository, BaseFilterSpecs<Complaint> baseFilterSpecs, AuthenticatedProvider authenticatedProvider, UserACL userACL, FileAttachmentRepository fIleAttachmentRepository, EmailService emailService) {
        this.complaintRepository = complaintRepository;
        this.accountRepository = accountRepository;
        this.baseFilterSpecs = baseFilterSpecs;
        this.authenticatedProvider = authenticatedProvider;
        this.userACL = userACL;
        this.fIleAttachmentRepository = fIleAttachmentRepository;
        this.emailService = emailService;
    }

    @Override
    public Page<ComplaintResponse> findAll(BaseFilter baseFilter) {
        ComplaintFilter complaintFilter = (ComplaintFilter) baseFilter;
        Specification<Complaint> searchable = baseFilterSpecs.search(complaintFilter);
        if (complaintFilter.getType() != null) {
            if (!(complaintFilter.getType().equals("SERVICE") || complaintFilter.getType().equals("COMPLAINT_POINT"))) {
                throw new BadRequestException("error.typeInvalid");
            }
            Specification<Complaint> additionalSpec = (root, query, cb) -> cb.equal(root.get("type"), complaintFilter.getType());
            searchable = searchable.and(additionalSpec);
        }
        if (complaintFilter.getUserId() != null) {
            Specification<Complaint> additionalSpec = (root, query, cb) -> cb.equal(root.get("user").get("id"), complaintFilter.getUserId());
            searchable = searchable.and(additionalSpec);
        }
        Pageable pageable = baseFilterSpecs.page(baseFilter);
        return complaintRepository.findAll(searchable, pageable).map(ComplaintResponse::new);
    }

    @Override
    public ComplaintResponse findById(Long aLong) {
        Complaint complaint = Utils.requireExists(complaintRepository.findById(aLong), "error.notFound");
        List<FileAttachmentResponse> fileAttachmentResponse = new ArrayList<>();
        List<FileAttachment> fileAttachments = fIleAttachmentRepository.findAllByComplaintIdOrderByCreatedAtDesc(aLong);
        if (!fileAttachments.isEmpty()) {
            fileAttachmentResponse = fileAttachments.stream().map(FileAttachmentResponse::new).collect(Collectors.toList());
        }
        ComplaintResponse complaintResponse = new ComplaintResponse(complaint);
        complaintResponse.setFileAttachment(fileAttachmentResponse);
        return complaintResponse;
    }

    @Override
    public ComplaintResponse save(ComplaintRequest complaintRequest) {
        if (StringUtils.isBlank(complaintRequest.getTitle()) || StringUtils.isBlank(complaintRequest.getDescription()) || StringUtils.isBlank(complaintRequest.getEmail()) || StringUtils.isBlank(complaintRequest.getPhoneNumber())) {
            throw new BadRequestException("error.notBlank");
        }
        if (!EmailUtils.isValidEmail(complaintRequest.getEmail())) {
            throw new BadRequestException("error.emailInvalid");
        }

        String regexPhoneNumber = "^[0-9]+$";
        if (!complaintRequest.getPhoneNumber().matches(regexPhoneNumber)) {
            throw new BadRequestException("error.phoneNumberInvalid");
        }
        if (!(complaintRequest.getType().equals("SERVICE") || complaintRequest.getType().equals("COMPLAINT_POINT"))) {
            throw new BadRequestException("error.typeInvalid");
        }
        Complaint complaint = BeanUtil.copyProperties(complaintRequest, Complaint.class);
        if (complaintRequest.getType().equals("COMPLAINT_POINT")) {
            if (complaintRequest.getPlusPoint() < 0) {
                throw new BadRequestException("error.plusPointInvalid");
            }
            complaint.setPlusPoint(complaint.getPlusPoint());
        }
        Optional<Account> accountOptional = accountRepository.findByUsernameOrPhoneNumber(complaintRequest.getEmail());
        accountOptional.ifPresent(complaint::setUser);
        complaint.setStatus(ComplaintConstant.NEW);
        complaintRepository.save(complaint);
        return new ComplaintResponse(complaint);
    }

    private Complaint getById(Long id) {
        return complaintRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("error.complaintNotFound"));
    }

    @Override
    public ComplaintResponse update(Long aLong, ComplaintRequest complaintRequest) {
        if (!(userACL.isSuperAdmin() || userACL.isFixedAdmin() || userACL.isFlexibleAdmin())) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Complaint complaint =  getById(aLong);
        if (StringUtils.isBlank(complaintRequest.getTitle()) || StringUtils.isBlank(complaintRequest.getDescription()) || StringUtils.isBlank(complaintRequest.getEmail()) || StringUtils.isBlank(complaintRequest.getPhoneNumber())) {
            throw new BadRequestException("error.notBlank");
        }
        if (!EmailUtils.isValidEmail(complaintRequest.getEmail())) {
            throw new BadRequestException("error.emailInvalid");
        }
        String regexPhoneNumber = "^[0-9]+$";
        if (!complaintRequest.getPhoneNumber().matches(regexPhoneNumber)) {
            throw new BadRequestException("error.phoneNumberInvalid");
        }
        if (!(complaintRequest.getType().equals("SERVICE") || complaintRequest.getType().equals("COMPLAINT_POINT"))) {
            throw new BadRequestException("error,typeInvalid");
        }
        if (complaintRequest.getType().equals("COMPLAINT_POINT")) {
            if (complaintRequest.getPlusPoint() < 0) {
                throw new BadRequestException("error.plusPointInvalid");
            }
            complaint.setPlusPoint(complaintRequest.getPlusPoint());
        }
        complaint.setTitle(complaintRequest.getTitle().trim());
        complaint.setDescription(complaintRequest.getDescription().trim());
        complaint.setEmail(complaintRequest.getEmail());
        complaint.setType(complaint.getType());
        complaint.setPhoneNumber(complaintRequest.getPhoneNumber());
        complaint.setStatus(complaintRequest.getStatus());
        complaintRepository.save(complaint);
        return new ComplaintResponse(complaint);
    }

    @Override
    public boolean deleteById(Long aLong) {
        Complaint complaint = getById(aLong);
        List<FileAttachment> fileAttachments = fIleAttachmentRepository.findAllByComplaintId(aLong);
        for (FileAttachment fileAttachment : fileAttachments) {
            String path = fileAttachment.getOriginalName();
            File file = new File(path);
            if (file.exists()) {
                file.delete();
            }
        }
        complaintRepository.delete(complaint);
        return true;
    }

    @Override
    public Page<ComplaintResponse> findByUserId(String keyword, BaseFilter baseFilter) {
        ComplaintFilter complaintFilter = (ComplaintFilter) baseFilter;
        Long userId = authenticatedProvider.getUserId().get();
        complaintFilter.setUserId(userId);
        return findAll(complaintFilter);
    }

    @Override
    public void uploadFiles(Long id, List<MultipartFile> multipartFile) {
        Complaint complaint = getById(id);
        String filePath = FILE_DIRECTORY  + "/complaint/" + id;
        File fileDirectory = new File(filePath);
        if (!fileDirectory.exists()) {
            fileDirectory.mkdirs();
        }
        List<File> fileList = multipartFile.stream().map(file -> {
            try {
                return convertMultiPartToFile(file, filePath);
            } catch (Exception e) {
                throw new BadRequestException("error.fileInvalid");
            }
        }).collect(Collectors.toList());
        List<FileAttachment> fileAttachments = new ArrayList<>();
        for (File file : fileList) {
            FileAttachment fileAttachment = new FileAttachment();
            fileAttachment.setFileName(file.getName());
            fileAttachment.setOriginalName(file.getAbsolutePath());
            fileAttachment.setComplaint(complaint);
            fileAttachment.setMimeType(URLConnection.guessContentTypeFromName(file.getName()));
            fileAttachments.add(fileAttachment);
        }
        fIleAttachmentRepository.saveAll(fileAttachments);

    }

    @Override
    public FileInputStream viewFile(Long id, String fileName) throws FileNotFoundException {
        Complaint complaint = getById(id);
        FileAttachment fileAttachment = fIleAttachmentRepository.findByComplaintIdAndFileName(complaint.getId(), fileName).orElseThrow(() -> new ResourceNotFoundException("error.fileNotFound"));
        File file = new File(fileAttachment.getOriginalName());
        FileInputStream fileInputStream = new FileInputStream(file);
        return fileInputStream;
    }

    @Override
    public ComplaintResponse changeStatus(Long id, ComplaintRequest complaintRequest) {
        Complaint complaint = getById(id);
        complaint.setStatus(complaintRequest.getStatus());
        complaintRepository.save(complaint);
        return new ComplaintResponse(complaint);
    }

    @Override
    public void response(Long id, ComplaintRequest complaintRequest) {
        Complaint complaint = getById(id);
        if (StringUtils.isBlank(complaintRequest.getMessage())) {
            throw new BadRequestException("error.notBlank");
        }
        if (complaint.getStatus() == ComplaintConstant.REPLIED) {
            throw new BadRequestException("error.alreadyResponse");
        }
        complaint.setStatus(ComplaintConstant.REPLIED);
        complaint.setAdminResponse(complaintRequest.getMessage().trim());
        complaintRepository.save(complaint);
        SimpleMailInfo mailInfo = new SimpleMailInfo(complaint.getEmail(), "King Logi phản hồi khiếu nại", "<p>Tiều đề: " + complaint.getTitle() + "</p><p>Mô tả: " + complaint.getDescription() + "</p><p> Trả lời: " + complaint.getAdminResponse() + "</p>");
        emailService.sendAsync(mailInfo);
    }

    @Override
    public void deleteImage(Long id, String fileName) {
        Complaint complaint = getById(id);
        FileAttachment fileAttachment = fIleAttachmentRepository.findByComplaintIdAndFileName(complaint.getId(), fileName).orElseThrow(() -> new ResourceNotFoundException("error.fileNotFound"));
        File file = new File(fileAttachment.getOriginalName());
        if (file.exists()) {
            file.delete();
        }
        fIleAttachmentRepository.delete(fileAttachment);
    }


    private File convertMultiPartToFile(MultipartFile file, String filePath) throws IOException {
        String fileNameWithoutExtension = FilenameUtils.removeExtension(file.getOriginalFilename());
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());
        File convFile = new File(filePath, fileNameWithoutExtension + "." + extension);
        int num = 0;
        while (convFile.exists()) {
            convFile = new File(filePath, fileNameWithoutExtension + "(" + (++num) + ")." + extension);
        }
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write( file.getBytes() );
        fos.close();
        return convFile;
    }



}
