package com.kinglogi.service.impl;

import com.kinglogi.constant.Role;
import com.kinglogi.dto.filter.AccountFilter;
import com.kinglogi.dto.filter.base.BaseFilter;
import com.kinglogi.dto.request.AccountRequest;
import com.kinglogi.dto.response.AccountResponse;
import com.kinglogi.entity.Account;
import com.kinglogi.exception.KlgResourceNotFoundException;
import com.kinglogi.repository.custom.CustomAccountRepository;
import com.kinglogi.repository.custom.CustomRoleRepository;
import com.kinglogi.service.AccountHistoryService;
import com.kinglogi.service.AdminService;
import com.kinglogi.service.LoyaltyPointHistoryService;
import com.kinglogi.service.UserACL;
import com.kinglogi.service.error.BadRequestException;
import com.kinglogi.specs.BaseFilterSpecs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
@Qualifier("adminService")
public class AdminServiceImpl extends AccountServiceImpl implements AdminService {

	private final UserACL userACL;

	private final CustomAccountRepository accountRepository;
    private final LoyaltyPointHistoryService loyaltyPointHistoryService;

	private final AccountHistoryService accountHistoryService;

	@Autowired
    public AdminServiceImpl(@Qualifier("customAccountRepository") CustomAccountRepository accountRepository,
							@Qualifier("customRoleRepository") CustomRoleRepository roleRepository,
							BaseFilterSpecs<Account> baseFilterSpecs, UserACL userACL, LoyaltyPointHistoryService loyaltyPointHistoryService, AccountHistoryService accountHistoryService) {
		super(accountRepository, roleRepository, baseFilterSpecs, loyaltyPointHistoryService, accountHistoryService);
		this.userACL = userACL;
		this.accountRepository = accountRepository;
        this.loyaltyPointHistoryService = loyaltyPointHistoryService;
		this.accountHistoryService = accountHistoryService;
	}

	@Override
    public Page<AccountResponse> findAll(BaseFilter baseFilter) {
    	AccountFilter accountFilter = (AccountFilter) baseFilter;
    	accountFilter.setRoleCodes(Arrays.asList(Role.FIXED_TRIP_ADMIN.getCode(), Role.FLEXIBLE_TRIP_ADMIN.getCode(), Role.SUPER_ADMIN.getCode(), Role.AGENT_ADMIN.getCode(), Role.SUB_AGENT_ADMIN.getCode()));
        return super.findAll(accountFilter);
    }

	@Override
	public AccountResponse update(Long id, AccountRequest userRequest) {
		if (!userACL.isSuperAdmin()) {
			// vì api cũ dùng status code 400 de gui message ve quyen truy cap khong duoc pap nên chúng ta sẽ giữ nguyên status code cho api này
			throw new KlgResourceNotFoundException("sorry, You're not authorized to access this resource");
		}
		Optional<Account> accountOptional = accountRepository.findById(id);
		if (!accountOptional.isPresent()){
			throw new KlgResourceNotFoundException("User is not found with id [" + id + "]");
		}
		if (accountOptional.get().getRoles().stream().anyMatch(this::isAdmin)){
			return super.update(id, userRequest);
		}else {
			throw new BadRequestException("error.userNotAdmin");
		}

	}

	private boolean isAdmin(com.kinglogi.entity.Role role){
		List<String> roles = Arrays.asList(Role.SUPER_ADMIN.getCode(), Role.FLEXIBLE_TRIP_ADMIN.getCode(), Role.FIXED_TRIP_ADMIN.getCode(), Role.AGENT_ADMIN.getCode(), Role.SUB_AGENT_ADMIN.getCode());
		if (roles.contains(role.getCode())){
			return true;
		}else {
			return false;
		}
	}

}
