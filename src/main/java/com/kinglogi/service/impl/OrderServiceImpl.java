package com.kinglogi.service.impl;

import com.kinglogi.constant.*;
import com.kinglogi.constant.Role;
import com.kinglogi.dto.PageableDTO;
import com.kinglogi.dto.UserToken;
import com.kinglogi.dto.filter.OrderFilter;
import com.kinglogi.dto.filter.base.BaseFilter;
import com.kinglogi.dto.loyalty_point_history.LoyaltyPointHistoryInputDTO;
import com.kinglogi.dto.mail.MailInfo;
import com.kinglogi.dto.mail.MailTemplateConfig;
import com.kinglogi.dto.price_range.PriceRangeOutputDTO;
import com.kinglogi.dto.promotion.PromotionResponse;
import com.kinglogi.dto.request.*;
import com.kinglogi.dto.response.EmailTemplateResponse;
import com.kinglogi.dto.response.OrderResponse;
import com.kinglogi.dto.response.OrderStopPointResponse;
import com.kinglogi.dto.response.UserCredential;
import com.kinglogi.dto.response.WinningBidderResponse;
import com.kinglogi.entity.*;
import com.kinglogi.exception.KlgResourceNotFoundException;
import com.kinglogi.provider.AuthenticatedProvider;
import com.kinglogi.provider.OrderCodeGenerator;
import com.kinglogi.repository.*;
import com.kinglogi.service.*;
import com.kinglogi.service.error.AccessForbiddenException;
import com.kinglogi.service.error.BadRequestException;
import com.kinglogi.service.utils.Utils;
import com.kinglogi.specs.BaseFilterSpecs;
import com.kinglogi.utils.BeanUtil;
import com.kinglogi.utils.DateUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

import static com.kinglogi.constant.OrderStatus.READY_FOR_MOVING;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    private final AuthenticatedProvider authenticatedProvider;

    private final BaseFilterSpecs<Order> baseFilterSpecs;

    private final OrderCodeGenerator orderCodeGenerator;

    private final EmailService emailService;

    private final EmailTemplateService emailTemplateService;

    private final UserPromotionRepository userPromotionRepository;

    private final OrderHistoryChangeService orderHistoryChangeService;


    private final AccountRepository accountRepository;


    private final LoyaltyPointHistoryRepository loyaltyPointHistoryRepository;

    private final UserACL userACL;

    private final OrderStopPointRepository orderStopPointRepository;
    private final BidRepository bidRepository;

    private final NotificationService notificationService;

    private final PushNotificationTokenService pushNotificationTokenService;

    private final TransactionService transactionService;

    private final AuctionRepository auctionRepository;

    private final PromotionRepository promotionRepository;

    private final InventoryRepository inventoryRepository;
    private final PriceRangeRepository priceRangeRepository;
    private final AccountInventoryOrderRepository accountInventoryOrderRepository;
    final long factor = 10000;

    private final ReportService reportService;

    @Value("${app.email.admin}")
    private String emailAdmin;

    public OrderServiceImpl(OrderRepository orderRepository, AuthenticatedProvider authenticatedProvider, BaseFilterSpecs<Order> baseFilterSpecs, OrderCodeGenerator orderCodeGenerator, EmailService emailService, EmailTemplateService emailTemplateService, UserPromotionRepository userPromotionRepository, OrderHistoryChangeService orderHistoryChangeService, AccountRepository accountRepository, LoyaltyPointHistoryRepository loyaltyPointHistoryRepository, UserACL userACL, OrderStopPointRepository orderStopPointRepository, BidRepository bidRepository, NotificationService notificationService, PushNotificationTokenService pushNotificationTokenService, TransactionService transactionService, AuctionRepository auctionRepository, PromotionRepository promotionRepository, InventoryRepository inventoryRepository, PriceRangeRepository priceRangeRepository, AccountInventoryOrderRepository accountInventoryOrderRepository, ReportService reportService) {
        this.orderRepository = orderRepository;
        this.authenticatedProvider = authenticatedProvider;
        this.baseFilterSpecs = baseFilterSpecs;
        this.orderCodeGenerator = orderCodeGenerator;
        this.emailService = emailService;
        this.emailTemplateService = emailTemplateService;
        this.userPromotionRepository = userPromotionRepository;
        this.orderHistoryChangeService = orderHistoryChangeService;
        this.accountRepository = accountRepository;
        this.loyaltyPointHistoryRepository = loyaltyPointHistoryRepository;
        this.userACL = userACL;
        this.orderStopPointRepository = orderStopPointRepository;
        this.bidRepository = bidRepository;
        this.notificationService = notificationService;
        this.pushNotificationTokenService = pushNotificationTokenService;
        this.transactionService = transactionService;
        this.auctionRepository = auctionRepository;
        this.promotionRepository = promotionRepository;
        this.inventoryRepository = inventoryRepository;
        this.priceRangeRepository = priceRangeRepository;
        this.accountInventoryOrderRepository = accountInventoryOrderRepository;
        this.reportService = reportService;
    }

    @Override
    public Page<OrderResponse> findAll(BaseFilter baseFilter) {
        OrderFilter orderFilter = (OrderFilter) baseFilter;
        Specification<Order> searchable = baseFilterSpecs.search(orderFilter);
        OrderType orderType = OrderType.getOrderType(orderFilter.getOrderType());
        if (orderType == OrderType.FIXED_TRIP) {
            Specification<Order> tripIdSpec = (root, query, cb) -> cb.isNotNull(root.get("trip"));
            searchable = searchable.and(tripIdSpec);
        } else if (orderType == OrderType.FLEXIBLE_TRIP) {
            Specification<Order> tripIdSpec = (root, query, cb) -> cb.isNull(root.get("trip"));
            searchable = searchable.and(tripIdSpec);
        }
        if (!orderFilter.isOfAuction()) {
            Specification<Order> auctionsIdSpec = (root, query, cb) -> {
                Join<Order, Auction> auctions = root.join("auctions", JoinType.LEFT);
                return cb.isNull(auctions.get("id"));
            };
            searchable = searchable.and(auctionsIdSpec);
        }
        if (orderFilter.getCreatedBy() != null) {
            Specification<Order> additionalSpec = (root, query, cb) -> cb.equal(root.get("account").get("id"), orderFilter.getCreatedBy());
            searchable = searchable.and(additionalSpec);
        }
        if(orderFilter.getOrderStatus() != null) {
        	Specification<Order> statusSpec = (root, query, cb) -> cb.equal(root.get("status"), orderFilter.getOrderStatus());
        	searchable = searchable.and(statusSpec);
        }
        if(!orderFilter.getOrderStatuses().isEmpty()) {
            Specification<Order> statusesSpec = (root, query, cb) -> root.get("status").in(orderFilter.getOrderStatuses());
            searchable = searchable.and(statusesSpec);
        }
        UserCredential userCredential = Utils.requireExists(authenticatedProvider.getUser(), "error.userNotFound");
        if (userCredential.getRoles().contains(Role.SUB_AGENT_ADMIN.getCode())){
            Account account = accountRepository.findById(userCredential.getId()).get();
            orderFilter.setAgentId(account.getAgentId());
        }
        if (orderFilter.getAgentId() != null) {
            List<Long> subagents = accountRepository.findAllSubAgentAdmin(orderFilter.getAgentId(), Role.SUB_AGENT_ADMIN.getCode());
            List<Long> agentIds = new ArrayList<>(subagents);
            agentIds.add(orderFilter.getAgentId());
            List<Long> collaboratorIds = accountRepository.findAllByAgentIds(agentIds);
            collaboratorIds.addAll(agentIds);

            orderFilter.setCollaboratorIds(collaboratorIds);
            orderFilter.setCreatedUser(orderFilter.getAgentId());
            Specification<Order> createdUserSpec = (root, query, cb) -> cb.equal(root.get("createdBy"), orderFilter.getCreatedUser());
            if (!orderFilter.getCollaboratorIds().isEmpty()) {
                Specification<Order> ctvSpec = (root, query, cb) -> root.get("createdBy").in(orderFilter.getCollaboratorIds());
                Specification<Order> additionalSpec = (root, query, cb) -> root.get("collaboratorId").in(orderFilter.getCollaboratorIds());
                createdUserSpec = createdUserSpec.or(ctvSpec).or(additionalSpec);
            }
            searchable = searchable.and(createdUserSpec);
        }
        Pageable pageable = baseFilterSpecs.page(baseFilter);
        Page<Order> page = orderRepository.findAllActive(searchable, pageable);
        return page.map(this::buildResponse);
    }

    @Override
    public Page<OrderResponse> getOrdersBelongToCollaborator(OrderFilter filter, Date day) {
        UserCredential userCredential = Utils.requireExists(authenticatedProvider.getUser(), "error.userNotFound");
        Integer orderStatus = filter.getOrderStatus();
        List<Integer> orderStatuses = filter.getOrderStatuses();
        if (!userCredential.getRoles().contains(Role.COLLABORATOR.getCode())) {
            throw new AccessForbiddenException("error.notCollaborator");
        }
        List<Bid> winningBids = bidRepository.findAllByAccountIdAndStatus(userCredential.getId(), BidStatus.SUCCESS);
        List<Order> orderListOutput = new ArrayList<>();

        for (Bid bid: winningBids) {
            if (bid.getAuction().getStatus().equals(AuctionStatus.FINISH.getStatus())) {
                Set<Order> winningBidOrders = bid.getAuction().getOrders();
                orderListOutput.addAll(new ArrayList<>(winningBidOrders));
            }
        }

        // Filter order by status if could
        if (orderStatus != null) {
            orderListOutput = orderListOutput.stream().filter(order -> order.getStatus().intValue() == orderStatus).collect(Collectors.toList());
        }
        // Filter order by list status if could
        if (!orderStatuses.isEmpty()) {
            orderListOutput = orderListOutput.stream().filter(order -> orderStatuses.contains(order.getStatus())).collect(Collectors.toList());
        }
        // Filter order by specific date
        if (day != null) {
            orderListOutput = orderListOutput.stream().filter(order -> convertDateToString(order.getPickingUpDatetime()).equals(convertDateToString(day))).collect(Collectors.toList());
        }

        Pageable pageable = baseFilterSpecs.page(filter);
        PageableDTO pageableDTO = Utils.getPageable(pageable, orderListOutput.size());

        if (pageableDTO.getStart() > pageableDTO.getEnd()) {
            return new PageImpl<>(new ArrayList<>());
        }
        return new PageImpl<>(orderListOutput.subList(pageableDTO.getStart(), pageableDTO.getEnd()), pageable, pageableDTO.getTotalSize()).map(this::buildResponse);
    }

    private String convertDateToString(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        return dateFormat.format(date);
    }

    @Override
    public void startOrderFromCollaborator(Long orderId) {
        UserCredential userCredential = Utils.requireExists(authenticatedProvider.getUser(), "error.userNotFound");
        if (!userCredential.getRoles().contains(Role.COLLABORATOR.getCode())) {
            throw new AccessForbiddenException("error.notCollaborator");
        }
        Order order = Utils.requireExists(orderRepository.findById(orderId), "error.dataNotFound");

        if (order.getCreatedBy() != null && !Objects.equals(order.getCollaboratorId(), userCredential.getId())) {
            throw new AccessForbiddenException("error.orderNotBeLongToYou");
        }

        if (!order.getStatus().equals(READY_FOR_MOVING.getStatus())) {
            throw new AccessForbiddenException("error.orderNotReadyForMoving");
        }
        order.setStatus(OrderStatus.ON_GOING.getStatus());
        orderRepository.save(order);
        if (order.getAccount() != null && Objects.equals(order.getAccount().getId(), userCredential.getId())) {
            // Send notification to order owner when start moving
            sendNotificationForOrderOwner(order);
        }
        // Save report
        Account account = Utils.requireExists(accountRepository.findById(userCredential.getId()), "error.dataNotFound");
        reportService.save(account, order, null, ReportType.ORDER, ReportActivityType.START);
    }

    @Override
    public void finishOrderFromCollaborator(Long orderId) {
        UserCredential userCredential = Utils.requireExists(authenticatedProvider.getUser(), "error.userNotFound");
        if (!userCredential.getRoles().contains(Role.COLLABORATOR.getCode())) {
            throw new AccessForbiddenException("error.notCollaborator");
        }
        Order order = Utils.requireExists(orderRepository.findById(orderId), "error.dataNotFound");
        Account collaborator = Utils.requireExists(accountRepository.findById(userCredential.getId()), "error.userNotFound");
        if (order.getCreatedBy() != null && !Objects.equals(order.getCollaboratorId(), userCredential.getId())) {
            throw new AccessForbiddenException("error.orderNotBeLongToYou");
        }
        // receive commission from order
        if (order.getCreatedBy() != null){
            Optional<Account> accountCommission = accountRepository.findById(order.getCreatedBy());
            if (accountCommission.isPresent()
                    && accountCommission.get().getRoles().stream().map(com.kinglogi.entity.Role::getCode).collect(Collectors.toList()).contains(Role.COLLABORATOR.getCode())
                    && accountCommission.get().getCommission() != null){
                long commissionValue;
                if (accountCommission.get().getCommission().getPriceType() == CommissionPriceType.AMOUNT) {
                    commissionValue = accountCommission.get().getCommission().getPriceValue();
                } else {
                    commissionValue = order.getEndPrice() != null ? order.getEndPrice() * accountCommission.get().getCommission().getPriceValue() / 100 : order.getTotalPrice() * accountCommission.get().getCommission().getPriceValue() / 100;
                }
                performOrderCommission(accountCommission.get().getId(), order, commissionValue);
            }
        }
        // Update order status
        order.setStatus(OrderStatus.FINISH.getStatus());
        orderRepository.save(order);

        // Check payment method if "DIRECT"
        PaymentType paymentType = PaymentType.getPaymentType(order.getPaymentType()).orElse(PaymentType.DIRECT);
        Long endPrice = order.getEndPrice() != null ? order.getEndPrice() : order.getTotalPrice();
        if (paymentType.equals(PaymentType.DIRECT)) {
            // Subtract from balance of collaborator for this order. Collaborator will receive cash from customer directly.
            if (endPrice != 0){
                performOrderTransaction(userCredential.getId(), order, ((-1) * endPrice));
            }
        }
        int loyaltyPoint = (int) (endPrice / factor);
        List<Account> accounts = new ArrayList<>();
        List<LoyaltyPointHistory> loyaltyPointHistories = new ArrayList<>();
        //Add loyaltyPoint for customer and collaborator
        collaborator.setLoyaltyPoint(collaborator.getLoyaltyPoint() != null ? collaborator.getLoyaltyPoint() + loyaltyPoint : loyaltyPoint);
        accounts.add(collaborator);

        // add loyalty point history for collaborator
        LoyaltyPointHistory inputForCollaborator = new LoyaltyPointHistory();
        inputForCollaborator.setActionType(LoyaltyActionType.PLUS);
        inputForCollaborator.setAmount(loyaltyPoint);
        inputForCollaborator.setAccount(collaborator);
        loyaltyPointHistories.add(inputForCollaborator);


        if (order.getAccount() != null) {
            Account customer = order.getAccount();
            customer.setLoyaltyPoint(customer.getLoyaltyPoint() != null ? customer.getLoyaltyPoint() + loyaltyPoint : loyaltyPoint);
            accounts.add(customer);

            // add loyalty point history for customer
            LoyaltyPointHistory inputForCustomer = new LoyaltyPointHistory();
            inputForCustomer.setActionType(LoyaltyActionType.PLUS);
            inputForCustomer.setAmount(loyaltyPoint);
            inputForCustomer.setAccount(customer);
            loyaltyPointHistories.add(inputForCustomer);
        }
        accountRepository.saveAll(accounts);
        loyaltyPointHistoryRepository.saveAll(loyaltyPointHistories);

        //send notfication
        accounts.forEach(account -> {
            LoyaltyPointHistoryInputDTO loyaltyPointHistoryInputDTO = new LoyaltyPointHistoryInputDTO();
            loyaltyPointHistoryInputDTO.setAccount(account);
            loyaltyPointHistoryInputDTO.setAmount(loyaltyPoint);
            loyaltyPointHistoryInputDTO.setActionType(LoyaltyActionType.PLUS);
            sendNotificationChangeLoyaltyPoint(loyaltyPointHistoryInputDTO);
        });
        // Check if the last order and refund bid price to collaborator
        List<Auction> auction = new ArrayList<>(order.getAuctions());
        if (!auction.isEmpty()) {
            checkLastOrderAndRefundBidPrice(auction.get(0), userCredential.getId());
        }

    }

    private void sendNotificationChangeLoyaltyPoint(LoyaltyPointHistoryInputDTO input) {
        Map<String, Object> additionalData = new HashMap<>();
        additionalData.put("account", input.getAccount().getId());

        String point = input.getActionType().equals(LoyaltyActionType.PLUS) ? "được cộng " + input.getAmount() : "bị trừ " + input.getAmount();
        String messageContentPattern = "Bạn đã {0} điểm loyalty sau khi hoàn thành chuyến đi";
        String messageContent = MessageFormat.format(messageContentPattern, point);

        NotificationRequest notificationRequest = NotificationRequest.builder()
                .title("Loyalty Point")
                .content(messageContent)
                .type(NotificationType.LOYALTY_POINT.getType())
                .data(additionalData)
                .accountId(input.getAccount().getId()) // Send to collaborator
                .build();

        notificationService.save(notificationRequest);
        senPushNotification(notificationRequest);
    }

    private void performOrderCommission(Long userId, Order order, long commissionValue) {
        TransactionRequest transactionRequest = new TransactionRequest();
        transactionRequest.setTargetId(userId);
        transactionRequest.setAmount(commissionValue);
        Date transactionAt = Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant());
        transactionRequest.setTransactionAt(transactionAt);
        transactionRequest.setTransactionType("BALANCE");
        transactionRequest.setContent("Hoa hồng nhận từ đơn hàng " + order.getOrderCode());
        transactionService.save(transactionRequest);
    }

    @Override
    public void cancelOrderFromCollaborator(Long orderId) {
        Date now = new Date();
        UserCredential userCredential = Utils.requireExists(authenticatedProvider.getUser(), "error.userNotFound");
        if (!userCredential.getRoles().contains(Role.COLLABORATOR.getCode())) {
            throw new AccessForbiddenException("error.notCollaborator");
        }
        Account account = Utils.requireExists(accountRepository.findById(userCredential.getId()),  "error.dataNotFound");
        Order order = Utils.requireExists(orderRepository.findById(orderId), "error.dataNotFound");
        List<Account> adminAccounts = accountRepository.findByUserRole(Role.SUPER_ADMIN.getCode());
        if (now.after(order.getPickingUpDatetime())) {
            throw new AccessForbiddenException("error.theOrderIsAlreadyMoving");
        }

        if (order.getCreatedBy() != null && !Objects.equals(order.getCollaboratorId(), userCredential.getId())) {
            throw new AccessForbiddenException("error.orderNotBeLongToYou");
        }

        // Change order status to cancel
        order.setStatus(OrderStatus.PAID.getStatus());
        order.setCollaboratorId(null);
        orderRepository.save(order);

        Auction auction = order.getAuctions().stream().findFirst().get();
        auction.setStatus(AuctionStatus.STOP.getStatus());
        auctionRepository.save(auction);

        Set<String> listEmailOfUser = new HashSet<>();
        Set<Order> orderInAuctions = auction.getOrders();
        for (Order orderInAuction : orderInAuctions) {
            orderInAuction.setStatus(OrderStatus.PAID.getStatus());
            orderInAuction.setCollaboratorId(null);
            if (StringUtils.isNotBlank(orderInAuction.getEmail())){
                listEmailOfUser.add(orderInAuction.getEmail());
            }
        }
        //Gửi email cho user về việc đã bị huỷ order từ ctv
        sendEmailForUserWhenCollaboratorCancelOrder(listEmailOfUser);

        Set<Bid> bids = auction.getBids();
        for (Bid bid : bids) {
            if (bid.getStatus().equals(BidStatus.SUCCESS)){
                bid.setStatus(BidStatus.FAIL);
                bidRepository.save(bid);
                //Gửi mail thông báo cho CTV
                sendEmailForCollaboratorWhenCollaboratorCancelOrder(bid);
            }
        }

        // Set penalty time for collaborator
        account.setPenaltyTime(Instant.now().plus(90, ChronoUnit.MINUTES));
        accountRepository.save(account);

        // Send notification to admin for this cancellation.
        adminAccounts.forEach(admin -> sendCancelNotificationToAdmin(userCredential, order, admin.getId()));
        // Send notification to collaborator for punishment about cancel order
        sendCancelNotificationToCollaborator(order, userCredential);
        //Send notification to users be canceled order
        sendCancelNotificationToUser(orderInAuctions);
    }

    private void sendCancelNotificationToUser(Set<Order> orderInAuctions) {
        for (Order orderInAuction : orderInAuctions) {
            if (orderInAuction.getAccount() == null){
                continue;
            }

            Map<String, Object> additionalData = new HashMap<>();
            String messageContentPattern = "CTV đã hủy đơn chuyến [{0}]. Vì vậy chúng tôi sẽ tìm tài xế khác cho bạn";
            String messageContent = MessageFormat.format(messageContentPattern, orderInAuction.getOrderCode());

            NotificationRequest notificationRequest = NotificationRequest.builder()
                .title("Đơn hàng bị ctv huỷ")
                .content(messageContent)
                .type(NotificationType.CTV_CANCEL_ORDER.getType())
                .data(additionalData)
                .accountId(orderInAuction.getAccount().getId()) // Only send notification to a user super admin
                .build();

            notificationService.save(notificationRequest);
            senPushNotification(notificationRequest);
        }
    }

    private void sendEmailForCollaboratorWhenCollaboratorCancelOrder(Bid bid) {
        EmailTemplateResponse emailTemplateResponse = emailTemplateService.findByTypeAndLanguage(EmailTemplateType.COLLABORATOR_CANCEL_ORDER.name(), "vi");
        if (StringUtils.isNotBlank(bid.getAccount().getEmail())){
            MailInfo mailInfo = new MailInfo();
            mailInfo.addTo(bid.getAccount().getEmail());
            mailInfo.setSubject(emailTemplateResponse.getTitle());
            mailInfo.setContent(emailTemplateResponse.getContent());
            emailService.sendAsync(mailInfo);
        }
    }

    private void sendEmailForUserWhenCollaboratorCancelOrder(Set<String> listEmailOfUser) {
        for (String email : listEmailOfUser) {
            EmailTemplateResponse emailTemplateResponse = emailTemplateService.findByTypeAndLanguage(EmailTemplateType.ORDER_CANCELED_BY_COLLABORATOR.name(), "vi");
            if (StringUtils.isNotBlank(email)){
                MailInfo mailInfo = new MailInfo();
                mailInfo.addTo(email);
                mailInfo.setSubject(emailTemplateResponse.getTitle());
                mailInfo.setContent(emailTemplateResponse.getContent());
                emailService.sendAsync(mailInfo);
            }
        }
    }

    private void sendCancelNotificationToAdmin(UserCredential userCredential, Order order, Long adminId) {
        Map<String, Object> additionalData = new HashMap<>();
        String name = userCredential.getFullName() != null ? userCredential.getFullName() : userCredential.getUsername();

        String messageContentPattern = "CTV [{0}] đã hủy đơn chuyến [{1}]";
        String messageContent = MessageFormat.format(messageContentPattern, name, order.getOrderCode());

        NotificationRequest notificationRequest = NotificationRequest.builder()
                .title("Đơn hàng")
                .content(messageContent)
                .type(NotificationType.JOIN_A_BID.getType())
                .data(additionalData)
                .accountId(adminId) // Only send notification to a user super admin
                .build();

        notificationService.save(notificationRequest);
        senPushNotification(notificationRequest);
    }

    private void sendCancelNotificationToCollaborator(Order order, UserCredential userCredential) {
        Map<String, Object> additionalData = new HashMap<>();
        String name = userCredential.getFullName() != null ? userCredential.getFullName() : userCredential.getUsername();

        String messageContentPattern = "CTV [{0}] đã hủy đơn chuyến [{1}] sẽ không thể tham gia đấu giá trong 90 phút";
        String messageContent = MessageFormat.format(messageContentPattern, name, order.getOrderCode());

        NotificationRequest notificationRequest = NotificationRequest.builder()
                .title("Đơn hàng")
                .content(messageContent)
                .type(NotificationType.JOIN_A_BID.getType())
                .data(additionalData)
                .accountId(userCredential.getId()) // Only send notification to a user super admin
                .build();

        notificationService.save(notificationRequest);
        senPushNotification(notificationRequest);
    }

    private void checkLastOrderAndRefundBidPrice(Auction auction, Long userId) {
        List<Order> ordersOfAuction = new ArrayList<>(auction.getOrders());
        if (!ordersOfAuction.isEmpty()) {
            List<Order> incompleteOrders = ordersOfAuction.stream().filter(order -> !order.getStatus().equals(OrderStatus.FINISH.getStatus())).collect(Collectors.toList());
            if (incompleteOrders.size() > 0) {
                // If still have incomplete orders, send notification to collaborator
                sendNotificationForCollaboratorAboutIncompleteAuction(auction, userId, incompleteOrders.size());
            } else {
                // If all orders finished, refund bid price and plus money for collaborator
                List<Bid> winningBids = bidRepository.findTopByAuctionIdAndStatusOrderByBidPriceAscCreatedAtAsc(auction.getId(), BidStatus.PENDING);
                if (winningBids.size() > 0) {
                    Long bidPrice = winningBids.get(0).getBidPrice();

                    // Refund and paid for winning bidder the complete auction
                    performOrderTransactionCompleteAuction(userId, auction, bidPrice);

                    // Save report
                    Account account = Utils.requireExists(accountRepository.findById(userId), "error.dataNotFound");
                    reportService.save(account, auction.getOrders().iterator().next(), winningBids.get(0), ReportType.ORDER, ReportActivityType.FINISH);

                    // Send notification
                    sendNotificationForCollaboratorAboutCompleteAuction(auction, userId, bidPrice);
                }
            }
        }
    }
    private void performOrderTransactionCompleteAuction(Long userId, Auction auction, Long bidPrice) {
        TransactionRequest transactionRequest = new TransactionRequest();
        transactionRequest.setTargetId(userId);
        transactionRequest.setAmount(bidPrice * 2);
        Date transactionAt = Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant());
        transactionRequest.setTransactionAt(transactionAt);
        transactionRequest.setTransactionType(PaymentType.BALANCE.name());
        transactionRequest.setContent("Đấu giá " + auction.getName() + " hoàn thành! Hoàn trả tiền đặt cọc: " + bidPrice + " và thanh toán tiền thắng: " + bidPrice);
        transactionService.save(transactionRequest);
    }

    private void performOrderTransaction(Long userId, Order order, Long orderPrice) {
        TransactionRequest transactionRequest = new TransactionRequest();
        transactionRequest.setTargetId(userId);
        transactionRequest.setAmount(orderPrice);
        Date transactionAt = Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant());
        transactionRequest.setTransactionAt(transactionAt);
        transactionRequest.setTransactionType(PaymentType.BALANCE.name());
        transactionRequest.setContent("Thu phí đơn hàng " + order.getOrderCode()  + " hình thức thanh toán trực tiếp. Vui long đảm bảo thu tiền của khách hàng khi kết thúc chuyến đi.");
        transactionService.save(transactionRequest);
    }

    private void sendNotificationForOrderOwner(Order order) {
        Map<String, Object> additionalData = new HashMap<>();

        String messageContentPattern = "Chuyến xe [{0}] đang di chuyển. Vui lòng để ý và theo dõi quãng đường từ tài xế.";
        String messageContent = MessageFormat.format(messageContentPattern, order.getPickingUpLocation() + " - " + order.getDroppingOffLocation());

        NotificationRequest notificationRequest = NotificationRequest.builder()
            .title("Đơn hàng")
            .content(messageContent)
            .type(NotificationType.JOIN_A_BID.getType())
            .data(additionalData)
            .accountId(order.getAccount().getId()) // Only send notification to a user who started a bid
            .build();

        notificationService.save(notificationRequest);
        senPushNotification(notificationRequest);
    }

    private void sendNotificationForCollaboratorAboutCompleteAuction(Auction auction, Long collaboratorId, Long bidPrice) {
        Map<String, Object> additionalData = new HashMap<>();

        String messageContentPattern = "Đấu giá [{0}] hoàn thành! Hoàn trả tiền đặt cọc: [{1}] và thanh toán tiền thắng: [{2}] ";
        String messageContent;
        messageContent = MessageFormat.format(messageContentPattern, auction.getName(), "+" + bidPrice, "+" + bidPrice);


        NotificationRequest notificationRequest = NotificationRequest.builder()
            .title("Đơn hàng")
            .content(messageContent)
            .type(NotificationType.JOIN_A_BID.getType())
            .data(additionalData)
            .accountId(collaboratorId) // Send to collaborator
            .build();

        notificationService.save(notificationRequest);
        senPushNotification(notificationRequest);
    }

    private void sendNotificationForCollaboratorAboutIncompleteAuction(Auction auction, Long collaboratorId, Integer remainOrderCount) {
        Map<String, Object> additionalData = new HashMap<>();

        String messageContentPattern = "Bạn còn [{0}] đơn hàng để hoàn thành đấu giá [{1}]. Vui lòng hoàn thành tất cả đơn hàng để nhận về số tiền thắng đấu giá và hoàn trả tiền đặt cược.";
        String messageContent = MessageFormat.format(messageContentPattern, remainOrderCount.toString(), auction.getName());

        NotificationRequest notificationRequest = NotificationRequest.builder()
            .title("Đơn hàng")
            .content(messageContent)
            .type(NotificationType.JOIN_A_BID.getType())
            .data(additionalData)
            .accountId(collaboratorId) // Send to collaborator
            .build();

        notificationService.save(notificationRequest);
        senPushNotification(notificationRequest);
    }

    private void senPushNotification(NotificationRequest notificationRequest) {
        List<UserToken> userTokens = pushNotificationTokenService.getTokensByAccountId(notificationRequest.getAccountId());
        if(CollectionUtils.isNotEmpty(userTokens)) {
            PushNotificationDto pushNotificationRequest = PushNotificationDto.builder()
                .title(notificationRequest.getTitle())
                .body(notificationRequest.getContent())
                .type(notificationRequest.getType())
                .data(notificationRequest.getData())
                .userTokens(userTokens)
                .build();
            notificationService.sendNotification(pushNotificationRequest);
        }
    }
    public List<Date> getOrdersInMonthOfCollaborator(int month, int year) {
        UserCredential userCredential = Utils.requireExists(authenticatedProvider.getUser(), "error.userNotFound");
        if (!userCredential.getRoles().contains(Role.COLLABORATOR.getCode())) {
            throw new AccessForbiddenException("error.notCollaborator");
        }
        List<Bid> winningBids = bidRepository.findAllByAccountIdAndStatus(userCredential.getId(), BidStatus.SUCCESS);
        List<Order> orderListOutput = new ArrayList<>();
        List<Integer> orderStatuses = Arrays.asList(OrderStatus.WAITING_FOR_PAY_IN.getStatus(), OrderStatus.PAID.getStatus(), READY_FOR_MOVING.getStatus());
        for (Bid bid: winningBids) {
            if (bid.getAuction().getStatus().equals(AuctionStatus.FINISH.getStatus())) {
                Set<Order> winningBidOrders = bid.getAuction().getOrders().stream().filter(order -> orderStatuses.contains(order.getStatus())).collect(Collectors.toSet());
                orderListOutput.addAll(new ArrayList<>(winningBidOrders));
            }
        }

        Date dateStart = new GregorianCalendar(year, month - 1, 1).getTime();

        // Calculate last day of month
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateStart);
        int lastDayOfMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        Date dateEnd = new GregorianCalendar(year, month - 1, lastDayOfMonth).getTime();

        List<Order> orderInMonth = orderListOutput.stream().filter(order -> order.getPickingUpDatetime().compareTo(dateStart) >= 0 && order.getPickingUpDatetime().compareTo(dateEnd) <= 0).collect(Collectors.toList());

        // Turn to set to distinct duplicate day
        Set<Date> dayHaveOrderInMonth = orderInMonth.stream().map(Order::getPickingUpDatetime).collect(Collectors.toSet());

        // Truncate day to only compare day unit, not hours to easier to distinct
        dayHaveOrderInMonth = dayHaveOrderInMonth.stream().map(date -> Date.from(date.toInstant().truncatedTo(ChronoUnit.DAYS))).collect(Collectors.toSet());

        return new ArrayList<>(dayHaveOrderInMonth);
    }

    @Override
    public List<OrderResponse> getByIds(List<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return Collections.emptyList();
        }
        return orderRepository.getByIds(ids)
                .stream()
                .map(this::buildResponse)
                .collect(Collectors.toList());
    }

    @Override
    public OrderResponse findById(Long id) {
        Order order = getById(id);
        return buildResponse(order);
    }

    private OrderResponse buildResponse(Order entity) {
        OrderResponse response = BeanUtil.copyProperties(entity, OrderResponse.class);
        if (entity.getTrip() != null) {
            response.setTripId(entity.getTrip().getId());
        }
        if (entity.getUserPromotion() != null){
            response.setDiscount(entity.getUserPromotion().getPromotion().getPrice());
            PromotionResponse promotionResponse = BeanUtil.copyProperties(entity.getUserPromotion().getPromotion(), PromotionResponse.class);
            response.setPromotionResponse(promotionResponse);
        }
        if (entity.getOrderStopPoints().size() != 0){
            List<OrderStopPointResponse> orderStopPointResponses = entity.getOrderStopPoints().stream().sorted(Comparator.comparing(OrderStopPoint::getId)).map(orderStopPoint -> {
                OrderStopPointResponse orderStopPointResponse = new OrderStopPointResponse();
                orderStopPointResponse.setStopPoint(orderStopPoint.getName());
                orderStopPointResponse.setPrice(orderStopPoint.getPrice() == null ? 0L : orderStopPoint.getPrice());
                return orderStopPointResponse;
            }).collect(Collectors.toList());
            response.setOrderStopPoints(orderStopPointResponses);
        }

        if (entity.getPriceRange() != null){
            response.setPriceRange(BeanUtil.copyProperties(entity.getPriceRange(), PriceRangeOutputDTO.class));
        }

        return response;
    }

    private Order getById(Long id) {
        return orderRepository.findByIdAndDeletedAtIsNull(id).orElseThrow(() -> new KlgResourceNotFoundException("Order catalog is not found with id [" + id + "]"));
    }

    @Override
    public OrderResponse save(OrderRequest request) {
        Order order = BeanUtil.copyProperties(request, Order.class);
        convertRequestToOrder(request, order);
        // Tạo orderCode có nếu nó đang trống
        if (StringUtils.isBlank(order.getOrderCode())) {
            order.setOrderCode(orderCodeGenerator.generate());
        }
        // Nếu status là Null thì cho là chờ thanh toán
        if (order.getStatus() == null) {
            order.setStatus(OrderStatus.WAITING_FOR_PAY_IN.getStatus());
        }
        // Nếu trạng thái là finish thì ném ra ngoại lệ
        if (Objects.equals(order.getStatus(), OrderStatus.FINISH.getStatus())) {
            throw new BadRequestException("error.nonPermissionStatus");
        }

        // KIểm tra xem có dùng mã giảm giá không
        if (request.getUserPromotionId() != null){
            UserPromotion userPromotion = Utils.requireExists(userPromotionRepository.findById(request.getUserPromotionId()), "error.dataNotFound");
            order.setUserPromotion(userPromotion);
        }

        Inventory inventory = new Inventory();
        boolean canUseInventory = false;
        // Kiểm tra có sử dụng inventory còn active hay không và thực hiện cập nhật lại số lượng inventory
        if (request.getInventoryId() != null) {
            inventory = Utils.requireExists(inventoryRepository.findById(request.getInventoryId()), "error.inventoryNotFound");
            // Cập nhật lại inventory sau khi sử dụng
            if (inventory.getStatus().equals(InventoryStatus.ACTIVE)){
                inventory.setNumberOfTrip(inventory.getNumberOfTrip() - 1);
                canUseInventory = true;
                // Cập nhất trạng thái nếu hết lượt sử dụng inventory
                if (inventory.getNumberOfTrip() <= 0){
                    inventory.setStatus(InventoryStatus.EXPIRED);
                }
                //save inventory
                inventoryRepository.save(inventory);

                order.setTotalPrice(inventory.getPrice());
                order.setInventory(inventory);
            }
            // Sử dụng giá của vùng giá cho chuyến hiện tại
        }else if (request.getPriceRangeId() != null){
            PriceRange priceRange = Utils.requireExists(priceRangeRepository.findById(request.getPriceRangeId()), "error.dataNotFound");
            order.setTotalPrice(priceRange.getPrice());
            order.setPriceRange(priceRange);
        }
        order = orderRepository.save(order);

        // save account user inventory for order
        if (order.getAccount() != null && canUseInventory){
            Account account = Utils.requireExists(accountRepository.findById(order.getAccount().getId()), "error.dataNotFound");
            AccountInventoryOrder accountInventoryOrder = new AccountInventoryOrder();
            accountInventoryOrder.setUsername(account.getUsername());
            accountInventoryOrder.setInventory(inventory);
            accountInventoryOrderRepository.save(accountInventoryOrder);
        }

        // Lưu điểm dừng và giá của chuyến
        if (request.getOrderStopPoints() != null){
            List<OrderStopPointRequest> stopPoints = request.getOrderStopPoints();
            Order finalOrder = order;
            List<OrderStopPoint> orderStopPoints = stopPoints.stream().map(orderStopPointRequest -> {
                OrderStopPoint orderStopPoint = new OrderStopPoint();
                orderStopPoint.setName(orderStopPointRequest.getStopPoint());
                orderStopPoint.setPrice(orderStopPointRequest.getPrice());
                orderStopPoint.setOrder(finalOrder);
                return orderStopPoint;
            }).collect(Collectors.toList());
            orderStopPointRepository.saveAll(orderStopPoints);
        } else {
            OrderStopPoint orderStopPoint = new OrderStopPoint();
            orderStopPoint.setName(order.getDroppingOffLocation());
            orderStopPoint.setPrice(order.getTotalPrice());
            orderStopPoint.setOrder(order);
            orderStopPointRepository.save(orderStopPoint);
        }
        // Gửi email cho các Superadmin nếu người dùng là CTV
        if (userACL.isCollaborator()) {
            List<Account> adminAccounts = accountRepository.findByUserRole(Role.SUPER_ADMIN.getCode());
            UserCredential userCredential = Utils.requireExists(authenticatedProvider.getUser(), "error.notFound");
            Order finalOrder1 = order;
            adminAccounts.forEach(admin -> sendOrderCollaboratorCreateNotificationToAdmin(finalOrder1, userCredential, admin));
        }
        if (request.isSendMail()){
            sendEmail(request);
            sendEmailForAdmin(request);
        }

        // Save report
        Long id = authenticatedProvider.getUserId().orElse(null);
        if (id != null){
            Account account = Utils.requireExists(accountRepository.findById(id), "error.dataNotFound");
            reportService.save(account, order, null, ReportType.ORDER, ReportActivityType.CREATE);
        }else {
            reportService.save(order.getAccount(), order, null, ReportType.ORDER, ReportActivityType.CREATE);
        }
        return BeanUtil.copyProperties(order, OrderResponse.class);
    }

    // Cập nhật đơn hàng
    @Override
    public OrderResponse update(Long id, OrderRequest request) {
        Order order = getById(id);
        Optional<Long> optionalAccountId = authenticatedProvider.getUserId();
        // Lấy thông tin các người liên quan tới order hiện tại: createUser, orderUserId, agentAdminId
        if (optionalAccountId.isPresent()) {
            Set<Long> accountIds = new HashSet<>();
            // user belong to order
            Account orderUser = order.getAccount();
            if (orderUser != null) {
                Long orderUserId = orderUser.getId();
                accountIds.add(orderUserId);
            }
            // user create order
            Long createUser = order.getCreatedBy();
            if (createUser != null){
                accountIds.add(createUser);
                // if collaborator create order
                Account collaboratorCreateOrder = Utils.requireExists(accountRepository.findById(order.getCreatedBy()), "error.notFound");
                if (collaboratorCreateOrder.getAgentId() != null) {
                    Long agentAdminId = collaboratorCreateOrder.getAgentId();
                    //get agent admin of collaborator
                    accountIds.add(agentAdminId);
                }
            }
            // if collaborator work order
            if (order.getCollaboratorId() != null) {
                Account collaborator = Utils.requireExists(accountRepository.findById(order.getCollaboratorId()), "error.notFound");
                if (collaborator.getAgentId() != null) {
                    Long agentAdminId = collaborator.getAgentId();
                    //get agent of collaborator
                    accountIds.add(agentAdminId);
                }
            }
            // Chỉ liên quan tới order mới cập nhật được order
            if (!(userACL.isSuperAdmin() || userACL.isFlexibleAdmin() || userACL.isFixedAdmin() || accountIds.contains(optionalAccountId.get()))) {
                throw new AccessForbiddenException("error.notPermission");
            }
        }
        // Nếu order đã được trả tiền và hoàn thành chuyến thì không cập nhật được
        if (order.getStatus() != OrderStatus.PAID.getStatus()) {
            if (Objects.equals(request.getStatus(), OrderStatus.FINISH.getStatus())) {
                throw new BadRequestException("error.nonPermissionStatus");
            }
        }
        // Get information before updating
        OrderResponse oldOrderResponse = BeanUtil.copyProperties(order, OrderResponse.class);
        // begin updating
        BeanUtil.mergeProperties(request, order);
        Promotion promotion = null;
        // Lấy thông tin promotion trước đây
        if (order.getUserPromotion() != null) {
            promotion = order.getUserPromotion().getPromotion();
        }
        // Cập nhật thông tin promotion trước đây sử dụng, và cập nhật thông tin promoton đang sử dụng
        if (request.getPromotionCode() != null){
            List<UserPromotion> userPromotions = new ArrayList<>();
            // promotion trước đây tăng lên 1 lần, coi như chưa sử dụng
            if (order.getUserPromotion() != null) {
                UserPromotion oldUserPromotion = order.getUserPromotion();
                oldUserPromotion.setUsageRemain(oldUserPromotion.getUsageRemain() + 1);
                userPromotions.add(oldUserPromotion);
            }
            Account account = order.getAccount();
            promotion = Utils.requireExists(promotionRepository.findByCodeAndDeletedAtIsNullAndStatus(request.getPromotionCode(), PromotionStatus.ACTIVE.getStatus()), "error.dataNotFound");
            UserPromotion userPromotion = Utils.requireExists(userPromotionRepository.findByPromotionAndAccount(promotion, account), "error.dataNotFound");
            // promotion hiện tại đang sử dụng thì giảm đi 1 lần
            if (userPromotion.getUsageRemain() == 0){
                throw new BadRequestException("error.promotionEndOfUse");
            }
            userPromotion.setUsageRemain(userPromotion.getUsageRemain() - 1);
            userPromotions.add(userPromotion);

            order.setUserPromotion(userPromotion);
            // Tiến hành giảm EndPrice khi sử dụng promotion
            if (order.getTotalPrice() == 0) {
                order.setEndPrice(0L);
            } else {
                order.setEndPrice(order.getTotalPrice() - promotion.getPrice());
            }
            userPromotionRepository.saveAll(userPromotions);
        }
        // Giảm TotalPrice khi sử dụng promotion
        if (request.getTotalPrice() != null) {
            if (promotion != null) {
                order.setEndPrice(request.getTotalPrice() - promotion.getPrice());
                if (order.getEndPrice() <= 0) {
                    order.setEndPrice(0L);
                }
            } else {
                order.setEndPrice(request.getTotalPrice());
            }
        }
        convertRequestToOrder(request, order);
        order = orderRepository.save(order);

        // log history change order
        OrderResponse newOrderResponse = BeanUtil.copyProperties(order, OrderResponse.class);
        orderHistoryChangeService.changeDataOrder(oldOrderResponse, newOrderResponse);

        sendEmail(request);

        // Lấy thoogn tin giảm giá khi sử dụng promotion
        if (promotion != null) {
            newOrderResponse.setDiscount(promotion.getPrice());
            PromotionResponse promotionResponse = BeanUtil.copyProperties(promotion, PromotionResponse.class);
            newOrderResponse.setPromotionResponse(promotionResponse);
        }

        // save report
        Account account = Utils.requireExists(accountRepository.findById(optionalAccountId.get()), "error.dataNotFound");
        reportService.save(account, order, null, ReportType.ORDER, ReportActivityType.UPDATE);

        return newOrderResponse;
    }

    private void sendOrderCollaboratorCreateNotificationToAdmin(Order order, UserCredential userCredential, Account admin) {
        Map<String, Object> additionalData = new HashMap<>();
        String name = userCredential.getFullName() != null ? userCredential.getFullName() : userCredential.getUsername();

        String messageContentPattern = "CTV [{0}] đã tạo đơn hàng [{1}]";
        String messageContent = MessageFormat.format(messageContentPattern, name, order.getOrderCode());

        NotificationRequest notificationRequest = NotificationRequest.builder()
                .title("Đơn hàng")
                .content(messageContent)
                .type(NotificationType.JOIN_A_BID.getType())
                .data(additionalData)
                .accountId(admin.getId()) // Only send notification to a user super admin
                .build();

        notificationService.save(notificationRequest);
        senPushNotification(notificationRequest);
    }

    private void sendEmail(OrderRequest request) {
        if(StringUtils.isEmpty(request.getEmail())) {
            return;
        }

        ConfirmOrderMailDto dto = ConfirmOrderMailDto.builder()
                .fullName(request.getFullName())
                .email(request.getEmail())
                .phoneNumber(request.getPhoneNumber())
                .orderCode(request.getOrderCode())
                .pickingUpLocation(request.getPickingUpLocation())
                .droppingOffLocation(request.getDroppingOffLocation())
                .totalPrice(request.getTotalPrice())
                .endPrice(request.getEndPrice())
                .brand(request.getBrand())
                .numberOfSeat(request.getNumberOfSeat())
                .note(request.getNote())
                .pickingUpDatetime(DateUtils.toDateTimeString(request.getPickingUpDatetime()))
                .verifyComment(request.getVerifyComment())
                .status("Đơn hàng được chấp nhận")
                .build();

        EmailTemplateResponse emailTemplateResponse = null;
        if (OrderStatus.PAID.getStatus().equals(request.getStatus())) {
            emailTemplateResponse = emailTemplateService.findByTypeAndLanguage(EmailTemplateType.PAID.name(), request.getLanguage());
        } else if (OrderStatus.WAITING_FOR_PAY_IN.getStatus().equals(request.getStatus())) {
            emailTemplateResponse = emailTemplateService.findByTypeAndLanguage(EmailTemplateType.UNPAID.name(), request.getLanguage());
        }

        if (emailTemplateResponse == null) {
            return;
        }

        MailTemplateConfig config = MailTemplateConfig.builder()
                .mailContent(emailTemplateResponse.getContent())
                .mailSubject(emailTemplateResponse.getTitle())
                .mailIsHtml(true)
                .build();

        MailInfo mailInfo = new MailInfo();
        mailInfo.addTo(request.getEmail());
        emailService.sendAsync(mailInfo, config, dto);
    }

    public void sendEmailInvoice(Long orderId) {
        Order order = getById(orderId);
        if(order.getStatus() != OrderStatus.FINISH.getStatus()) {
            throw new BadRequestException("error.orderNotFinished");
        }
        LocalDateTime now = LocalDateTime.ofInstant(Instant.now(), ZoneId.of("Asia/Ho_Chi_Minh"));
        if (userACL.isSuperAdmin() || userACL.isFixedAdmin() || userACL.isFlexibleAdmin()) {
            if (order.getUpdatedAt().plus(10, ChronoUnit.DAYS).isBefore(now)) {
                throw new BadRequestException("error.dayExpired");
            }
        }
        if (userACL.isCollaborator() || userACL.isMember()) {
            if (order.getUpdatedAt().plus(3, ChronoUnit.DAYS).isBefore(now)) {
                throw new BadRequestException("error.dayExpired");
            }
        }
        InvoiceMail invoiceMail = InvoiceMail.builder()
                .orderCode(order.getOrderCode())
                .address(order.getAccount().getAddress())
                .phoneNumber(order.getPhoneNumber())
                .email(order.getEmail())
                .fullName(order.getFullName()).build();

        EmailTemplateResponse emailTemplateResponse = emailTemplateService.findByTypeAndLanguage(EmailTemplateType.INVOICE.name(), "vi");
        MailTemplateConfig config = MailTemplateConfig.builder()
                .mailContent(emailTemplateResponse.getContent())
                .mailSubject(emailTemplateResponse.getTitle())
                .mailIsHtml(true)
                .build();
        MailInfo mailInfo = new MailInfo();
        mailInfo.addTo(order.getEmail());
        emailService.sendAsync(mailInfo, config, invoiceMail);

    }

    private void convertRequestToOrder(OrderRequest request, Order order) {
        // lấy thông tin account và trip gán vào order
        if (request.getAccountId() != null) {
            Account account = new Account();
            account.setId(request.getAccountId());
            order.setAccount(account);
        } else {
            Optional<Account> account = accountRepository.findByUsernameOrPhoneNumber(request.getPhoneNumber());
            account.ifPresent(order::setAccount);
        }
    	if (request.getTripId() != null) {
    		Trip trip = new Trip();
    		trip.setId(request.getTripId());
    		order.setTrip(trip);
    	}
    }

    // Xóa đơn hàng
    @Override
    public boolean deleteById(Long id) {
        try {
            Order order = getById(id);
            Set<Auction> auctions = orderRepository.getAuctionByOrderId(id);
            // Nếu có đấu giá thì xóa các đấu giá rồi xóa mềm order
            if (!auctions.isEmpty()) {
                for (Auction auction : auctions) {
                    auction.getOrders().remove(order);
                    auction.setOrders(auction.getOrders());
                }
                auctionRepository.saveAll(auctions);
            }
            order.setDeletedAt(Date.from(Instant.now()));
            orderRepository.save(order);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }

    @Override
    public OrderResponse getOrderByOrderCode(String code) {
        Order order = orderRepository.getOrderByOrderCode(code).orElseThrow(() ->
                new KlgResourceNotFoundException("Order is not found with order code [" + code + "]"));
        return BeanUtil.copyProperties(order, OrderResponse.class);
    }

    @Override
    public WinningBidderResponse getWinningBidder(Long orderId) {
        Optional<Bid> winningBid = orderRepository.findOneActive(orderId)
                .map(Order::getAuctions)
                .orElseGet(HashSet::new)
                .stream()
                .map(Auction::getBids)
                .flatMap(Collection::stream)
                .filter(Objects::nonNull)
                .filter(bid -> bid.getStatus().equals(BidStatus.SUCCESS))
                .findFirst()
                ;
        if (winningBid.isPresent()) {
            Optional<Account> account = winningBid.map(Bid::getAccount);
            if(account.isPresent()) {
                WinningBidderResponse response = BeanUtil.copyProperties(account.get(), WinningBidderResponse.class);
                response.setBidAt(winningBid.get().getCreatedAt());
                if (account.get().getAvatar() != null) {
                    String avatar = "/profile/avatars/" + account.get().getId();
                    response.setAvatar(avatar);
                }
                Order order = orderRepository.findOneActive(orderId).get();
                Car car = account.get().getCars().stream().filter(c -> c.getNumberOfSeat().equals(order.getNumberOfSeat())).findFirst().orElse(null);
                if (car != null && car.getBrand() != null && car.getBrand() != null && car.getLicensePlates() != null){
                    response.setLicensePlates(car.getLicensePlates());
                }
                return response;
            }
        }
        return  null;
    }

    @Scheduled(cron = "0 * * ? * *")
    public void cancelOrderExpire(){
        List<Integer> statuses = new ArrayList<>();
        statuses.add(OrderStatus.WAITING_FOR_PAY_IN.getStatus());
        statuses.add(OrderStatus.PAID.getStatus());
        List<Order> orderExpires = orderRepository.findAllByStatusInAndPickingUpDatetimeAndDeletedAtNull(statuses, new Date(System.currentTimeMillis()));
        for (Order orderExpire : orderExpires) {
            Integer status = orderExpire.getStatus();
            orderExpire.setStatus(OrderStatus.CANCELLED.getStatus());
            orderRepository.save(orderExpire);
            if (status.equals(OrderStatus.PAID.getStatus())){
                this.sendNotificationForOrderOfCustomerExpire(orderExpire);
            }
        }
    }

    @Async
    public void sendNotificationForOrderOfCustomerExpire(Order order) {
        Map<String, Object> additionalData = new HashMap<>();

        String messageContentPattern = "Chuyến xe {0} của bạn đã bị hủy do thời gian đón đã bị quá hạn. Vui lòng liên hệ admin để được hỗ trợ";
        String messageContent = MessageFormat.format(messageContentPattern, order.getOrderCode());

        NotificationRequest notificationRequest = NotificationRequest.builder()
                .title("Đơn hàng")
                .content(messageContent)
                .type(NotificationType.CANCEL_ORDER.getType())
                .data(additionalData)
                .accountId(order.getAccount() != null ? order.getAccount().getId() : null)
                .build();

        notificationService.save(notificationRequest);
        senPushNotification(notificationRequest);
    }

    private void sendEmailForAdmin(OrderRequest request) {
        OrderMailDto dto = OrderMailDto.builder()
            .orderCode(request.getOrderCode())
            .pickingUpLocation(request.getPickingUpLocation())
            .droppingOffLocation(request.getDroppingOffLocation())
            .pickingUpDatetime(DateUtils.toDateTimeString(request.getPickingUpDatetime()))
            .build();

        EmailTemplateResponse emailTemplateResponse = emailTemplateService.findByTypeAndLanguage(EmailTemplateType.NEW_ORDER.name(), "vi");

        if (emailTemplateResponse == null) {
            return;
        }

        MailTemplateConfig config = MailTemplateConfig.builder()
            .mailContent(emailTemplateResponse.getContent())
            .mailSubject(emailTemplateResponse.getTitle())
            .mailIsHtml(true)
            .build();

        MailInfo mailInfo = new MailInfo();
        mailInfo.addTo(emailAdmin);
        emailService.sendAsync(mailInfo, config, dto);
    }

}
