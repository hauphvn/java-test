package com.kinglogi.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kinglogi.configuration.mail.MailSetting;
import com.kinglogi.dto.mail.MailAttachment;
import com.kinglogi.dto.mail.MailInfo;
import com.kinglogi.dto.mail.MailTemplateConfig;
import com.kinglogi.dto.mail.SimpleMailInfo;
import com.kinglogi.exception.NotificationException;
import com.kinglogi.service.EmailService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import sendinblue.ApiClient;
import sendinblue.Configuration;
import sendinblue.auth.ApiKeyAuth;
import sibApi.TransactionalEmailsApi;
import sibModel.*;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Component
@Slf4j
public class EmailServiceImpl implements EmailService {

	public static final String DEFAULT_SUBJECT_CHARSET = StandardCharsets.UTF_8.name();

	private String apiKey;

	@Autowired
	private SpringTemplateEngine templateEngine;

	@Autowired
	private MailSetting mailSetting;

	public EmailServiceImpl(@Value("${app.email.apiKey}") String apiKey) {
		this.apiKey = apiKey;
		this.configSendInBlue();
	}

	@Override
	public boolean send(SimpleMailInfo mailInfo) {
		try {
			return this.sendAsync(mailInfo).get();
		} catch (InterruptedException | ExecutionException e) {
			throw new NotificationException("Can't send a simple email with the given info " + mailInfo, e);
		}
	}

	@Override
	public boolean send(MailInfo mailInfo) {
		try {
			return this.sendAsync(mailInfo).get();
		} catch (InterruptedException | ExecutionException e) {
			throw new NotificationException("Can't send an email with the given info " + mailInfo, e);
		}
	}

	@Override
	public <T> boolean send(MailInfo mailInfo, String mailTemplate, T data) {
		try {
			return this.sendAsync(mailInfo, mailTemplate, data).get();
		} catch (InterruptedException | ExecutionException e) {
			String message = MessageFormat.format(
					"Cannot send an email with mailInfo {0}, mailTemplate {1}, data {2}",
					mailInfo, mailTemplate, data);
			throw new NotificationException(message, e);
		}
	}

	@Override
	public <T> boolean send(MailInfo mailInfo, MailTemplateConfig mailTemplateConfig, T data) {
		try {
			return this.sendAsync(mailInfo, mailTemplateConfig, data).get();
		} catch (InterruptedException | ExecutionException e) {
			String message = MessageFormat.format(
					"Cannot send an email with mailInfo {0}, mailTemplateConfig {1}, data {2}",
					mailInfo, mailTemplateConfig, data);
			throw new NotificationException(message, e);
		}
	}

	@Async
	@Override
	public Future<Boolean> sendAsync(SimpleMailInfo mailInfo) {
		log.debug("Send a simple email " + mailInfo);
		try {
			TransactionalEmailsApi transactionalEmailsApi = new TransactionalEmailsApi();
			SendSmtpEmailSender sender = new SendSmtpEmailSender();
			sender.setEmail(mailSetting.getSendFrom());
			sender.setName(mailSetting.getSendFrom());
			List<SendSmtpEmailTo> toList = new ArrayList<>();
			for (String emailTo : mailInfo.getTo()) {
                SendSmtpEmailTo to = new SendSmtpEmailTo();
                to.setEmail(emailTo);
				toList.add(to);
			}
			SendSmtpEmail sendSmtpEmail = new SendSmtpEmail();
			sendSmtpEmail.setSender(sender);
			sendSmtpEmail.setSubject(mailInfo.getSubject());
			sendSmtpEmail.setHtmlContent(mailInfo.getContent());
			sendSmtpEmail.to(toList);
			transactionalEmailsApi.sendTransacEmail(sendSmtpEmail);
			return new AsyncResult<>(true);
		} catch (Exception e) {
			throw new NotificationException("Can't send an email async with the given info " + mailInfo, e);
		}
	}

	@Async
	@Override
	public Future<Boolean> sendAsync(@NonNull MailInfo mailInfo) {
		log.debug("Send an email mailInfo " + mailInfo);
		try {
			TransactionalEmailsApi transactionalEmailsApi = new TransactionalEmailsApi();
			SendSmtpEmailSender sender = new SendSmtpEmailSender();
			sender.setEmail(mailSetting.getSendFrom());
			sender.setName(mailSetting.getSendFrom());
			SendSmtpEmail sendSmtpEmail = new SendSmtpEmail();
			List<SendSmtpEmailTo> toList = new ArrayList<>();
			for (String emailTo : mailInfo.getTo()) {
                SendSmtpEmailTo to = new SendSmtpEmailTo();
                to.setEmail(emailTo);
				toList.add(to);
			}
			List<SendSmtpEmailCc> ccList = new ArrayList<>();
			if (mailInfo.getCc().length > 0) {
				for (String ccUser : mailInfo.getCc()) {
					SendSmtpEmailCc cc = new SendSmtpEmailCc();
					cc.setEmail(ccUser);
					ccList.add(cc);
				}
				sendSmtpEmail.cc(ccList);
			}
			List<SendSmtpEmailBcc> bccList = new ArrayList<>();
			if (mailInfo.getBcc().length > 0) {
				for (String bccUser : mailInfo.getBcc()) {
					SendSmtpEmailBcc bcc = new SendSmtpEmailBcc();
					bcc.setEmail(bccUser);
					bccList.add(bcc);
				}
				sendSmtpEmail.bcc(bccList);
			}
			sendSmtpEmail.setSender(sender);
			sendSmtpEmail.setSubject(mailInfo.getSubject());
			sendSmtpEmail.setHtmlContent(mailInfo.getContent());
			sendSmtpEmail.to(toList);
			transactionalEmailsApi.sendTransacEmail(sendSmtpEmail);
			return new AsyncResult<>(true);
		} catch (Exception e) {
			throw new NotificationException("Can't send an email async with the given info " + mailInfo, e);
		}
	}

	@Async
	@Override
	public <T> Future<Boolean> sendAsync(@NonNull MailInfo mailInfo, @NonNull String mailTemplate, T data) {
		log.debug(MessageFormat.format("Send an email mailInfo {0}, mailTemplate {1}, data {2}", mailInfo, mailTemplate, data));
		try {
			TransactionalEmailsApi transactionalEmailsApi = new TransactionalEmailsApi();
			SendSmtpEmailSender sender = new SendSmtpEmailSender();
			sender.setEmail(mailSetting.getSendFrom());
			sender.setName(mailSetting.getSendFrom());
			List<SendSmtpEmailTo> toList = new ArrayList<>();
			for (String emailTo : mailInfo.getTo()) {
                SendSmtpEmailTo to = new SendSmtpEmailTo();
                to.setEmail(emailTo);
				toList.add(to);
			}
			Locale locale = LocaleContextHolder.getLocale();
			Context context = new Context(locale);
			Map<String, Object> mailVariables = this.convertObjectToMap(data);
			String mailContent = this.replaceThymeleafMarker(mailTemplate, context, mailVariables);
			File file = new File(mailContent);
			Document document = Jsoup.parse(file, "UTF-8");
			SendSmtpEmail sendSmtpEmail = new SendSmtpEmail();
			sendSmtpEmail.setSender(sender);
			sendSmtpEmail.setSubject(mailInfo.getSubject());
			sendSmtpEmail.setHtmlContent(document.html());
			sendSmtpEmail.to(toList);
			transactionalEmailsApi.sendTransacEmail(sendSmtpEmail);
			return new AsyncResult<>(true);
		} catch (Exception e) {
			throw new NotificationException("Can't send an email async with the given info " + mailInfo, e);
		}
	}

	@Async
	@Override
	public <T> Future<Boolean> sendAsync(@NonNull MailInfo mailInfo, @NonNull MailTemplateConfig mailTemplateConfig, T data) {
		log.debug(MessageFormat.format("Send an email mailInfo {0}, mailTemplateConfig {1}, data {2}", mailInfo, mailTemplateConfig, data));
		Locale locale = LocaleContextHolder.getLocale();
		Context context = new Context(locale);

		Map<String, Object> mailVariables = this.convertObjectToMap(data);
		if (StringUtils.isNotBlank(mailTemplateConfig.getMailSubject())) {
			String mailSubject = this.replaceThymeleafMarker(mailTemplateConfig.getMailSubject(), context, mailVariables);
			mailInfo.setSubject(mailSubject);
		}

		if (StringUtils.isNotBlank(mailTemplateConfig.getMailContent())) {
			String mailContent = this.replaceThymeleafMarker(mailTemplateConfig.getMailContent(), context, mailVariables);
			mailInfo.setContent(mailContent);
			mailInfo.setHtml(mailTemplateConfig.getMailIsHtml());
		}

		return this.sendAsync(mailInfo);
	}

	private MimeBodyPart getMimeBodyPart(MailAttachment att) {
		try {
			MimeBodyPart attachment = new MimeBodyPart();
			DataSource dataSource = new ByteArrayDataSource(att.getFileIss().getInputStream(), att.getMimeType());
			DataHandler dataHandler = new DataHandler(dataSource);
			attachment.setDataHandler(dataHandler);
			attachment.setFileName(att.getName());
			attachment.setDisposition(MimeBodyPart.ATTACHMENT);
			return attachment;
		} catch (IOException | MessagingException e) {
			throw new NotificationException("Can't send an email with attachment " + att.getName(), e);
		}
	}

	/**
	 * Convert java object to map
	 *
	 * @param data The object data
	 * @return the map with key value of the given object data
	 */
	private <T> Map<String, Object> convertObjectToMap(T data) {
		if (data == null) {
			return new HashMap<>();
		}
		ObjectMapper mapper = new ObjectMapper();
		return mapper.convertValue(data, new TypeReference<Map<String, Object>>() {
		});
	}

	/**
	 * Replace mail marker by thymeleaf
	 *
	 * @param thymeleafMarker The thymeleaf template with markers
	 * @param context The thymeleaf process context
	 * @param vars The parameters for replacing the markers
	 * @return The string after replacing values for the markers
	 */
	private String replaceThymeleafMarker(String thymeleafMarker, Context context, Map<String, Object> vars) {
		context.setVariables(vars);
		return templateEngine.process(thymeleafMarker, context);
	}

	private void configSendInBlue() {
		ApiClient defaultClient = Configuration.getDefaultApiClient();
		// Configure API key authorization: api-key
		ApiKeyAuth apiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("api-key");
		apiKeyAuth.setApiKey(apiKey);
	}
}
