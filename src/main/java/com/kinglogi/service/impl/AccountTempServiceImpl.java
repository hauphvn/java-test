package com.kinglogi.service.impl;

import com.kinglogi.dto.response.AccountTempResponse;
import com.kinglogi.entity.AccountTemp;
import com.kinglogi.repository.AccountTempRepository;
import com.kinglogi.service.AccountTempService;
import com.kinglogi.utils.EncrytedPasswordUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
@Transactional
public class AccountTempServiceImpl implements AccountTempService {

    private final AccountTempRepository accountTempRepository;

    @Value("${app.password.length}")
    private Integer passwordLength;

    @Value("${app.password.expiredInMinutes}")
    private Integer passwordExpiredInMinutes;

    @Autowired
    public AccountTempServiceImpl(AccountTempRepository accountTempRepository) {
        this.accountTempRepository = accountTempRepository;
    }

    @Override
    public AccountTempResponse createAccountTemp(String username) {
        String rawPassword = RandomStringUtils.randomAlphanumeric(passwordLength);
        String encryptedPassword = EncrytedPasswordUtils.encryptPassword(rawPassword);
        Date expiredAt = Date.from(LocalDateTime.now().plusMinutes(passwordExpiredInMinutes).atZone(ZoneId.systemDefault()).toInstant());

        AccountTemp accountTemp = new AccountTemp();
        accountTemp.setUsername(username);
        accountTemp.setPassword(encryptedPassword);
        accountTemp.setExpiredAt(expiredAt);
        accountTempRepository.save(accountTemp);

        return AccountTempResponse.builder()
                .expiredAt(expiredAt)
                .passwordInPlainText(rawPassword)
                .username(username)
                .expiredInMinutes(passwordExpiredInMinutes)
                .build();
    }

    @Override
    public boolean isValid(String username, String password) {
        List<AccountTemp> accountTemps = accountTempRepository
                .findAccountTempsByUsernameAndExpiredAtAfter(username, new Date())
                .orElseGet(ArrayList::new);
        return accountTemps.stream().anyMatch(accountTemp -> EncrytedPasswordUtils.isMatched(password, accountTemp.getPassword()));
    }

    @Override
    public boolean delete(String username) {
        accountTempRepository.deleteByUsername(username);
        return true;
    }
}
