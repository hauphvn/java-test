package com.kinglogi.service.impl;

import com.kinglogi.constant.TransactionReason;
import com.kinglogi.converter.VnPayMoneyConverter;
import com.kinglogi.dto.request.PaymentRequest;
import com.kinglogi.dto.request.TransactionRequest;
import com.kinglogi.dto.response.PaymentResponse;
import com.kinglogi.dto.response.PaymentUrlResponse;
import com.kinglogi.dto.response.TransactionResponse;
import com.kinglogi.entity.VnPayTransaction;
import com.kinglogi.provider.AuthenticatedProvider;
import com.kinglogi.provider.OrderCodeGenerator;
import com.kinglogi.repository.VnPayTransactionRepository;
import com.kinglogi.service.PayInService;
import com.kinglogi.service.TransactionService;
import com.kinglogi.service.VnPayService;
import com.kinglogi.utils.JsonUtils;
import com.kinglogi.vnpay.VnPayRequest;
import com.kinglogi.vnpay.constant.VnPayResponseCode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@RequiredArgsConstructor
public class PayInServiceImpl implements PayInService {

    private final TransactionService transactionService;

    private final VnPayService vnPayService;

    private final OrderCodeGenerator orderCodeGenerator;

    private final AuthenticatedProvider authenticatedProvider;

    private final VnPayTransactionRepository vnPayTransactionRepository;

    @Override
    public PaymentUrlResponse buildPayInRequest(PaymentRequest paymentRequest) {
        String txnRef = orderCodeGenerator.generate();
        paymentRequest.setTxnRef(txnRef);

        VnPayRequest vnPayRequest = vnPayService.buildVnPayRequest(paymentRequest);
        return vnPayService.generateVnPayRequestUrl(vnPayRequest);
    }

    @Override
    public Boolean verifyPayIn(Map<String, Object> allParams) {
        PaymentResponse paymentResponse = vnPayService.verifyPayment(allParams);
        if (VnPayResponseCode.SUCCESS.getCode().equals(paymentResponse.getResponseCode())) {
            performPayInTransaction(paymentResponse);
            saveVnPayTransactionHistory(null, paymentResponse);
            return true;
        }
        return false;
    }
    // Lưu thông tin giao dịch VNPay
    private void saveVnPayTransactionHistory(Long refId, PaymentResponse paymentResponse) {
        VnPayTransaction transaction = new VnPayTransaction();
        transaction.setType(TransactionReason.PAY_IN.getType());
        transaction.setRefId(refId);
        transaction.setContent(JsonUtils.toJson(paymentResponse));
        vnPayTransactionRepository.save(transaction);
    }

    // Lưu thông tin giao dịch nạp tiền
    private TransactionResponse performPayInTransaction(PaymentResponse paymentResponse) {
        TransactionRequest transactionRequest = new TransactionRequest();
        transactionRequest.setTargetId(authenticatedProvider.getUserId().get());
        transactionRequest.setAmount(new VnPayMoneyConverter().fromVnPay(paymentResponse.getAmount()));
        transactionRequest.setTransactionAt(paymentResponse.getPayDate());
        transactionRequest.setContent(paymentResponse.getOrderInfo());
        transactionRequest.setTransactionType("VNPAY");
        return transactionService.save(transactionRequest);
    }
}
