package com.kinglogi.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kinglogi.converter.VnPayMoneyConverter;
import com.kinglogi.dto.request.PaymentRequest;
import com.kinglogi.dto.response.PaymentResponse;
import com.kinglogi.dto.response.PaymentUrlResponse;
import com.kinglogi.provider.WebProvider;
import com.kinglogi.service.VnPayService;
import com.kinglogi.utils.BeanUtil;
import com.kinglogi.vnpay.VnPayRequest;
import com.kinglogi.vnpay.VnPayResponse;
import com.kinglogi.vnpay.VnPaySetting;
import com.kinglogi.vnpay.constant.VnPayCommandType;
import com.kinglogi.vnpay.constant.VnPayCurrency;
import com.kinglogi.vnpay.constant.VnPayLocale;
import com.kinglogi.vnpay.provider.VnPayProvider;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
@Slf4j
@RequiredArgsConstructor
public class VnPayServiceImpl implements VnPayService {

    private static final int MAX_LENGTH_ORDER_INFO = 255;

    private static final String ORDER_TYPE = "billpayment";

    private final VnPayProvider vnPayProvider;

    private final WebProvider webProvider;

    private final VnPaySetting vnPaySetting;

    @Override
    public PaymentResponse verifyPayment(Map<String, Object> allParams) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        VnPayResponse response = mapper.convertValue(allParams, VnPayResponse.class);
        String secureHash = response.getSecureHash();
        response.setSecureHash(null);
        response.setSecureHashType(null);
        Map<String, Object> orderedFields = mapper.convertValue(response, new TypeReference<LinkedHashMap<String, Object>>(){});
        if (StringUtils.equals(secureHash, vnPayProvider.hashAllFields(orderedFields))) {
            PaymentResponse paymentResponse = BeanUtil.copyProperties(response, PaymentResponse.class);
            Date date = addHoursToJavaUtilDate(paymentResponse.getPayDate(), (-7));
            paymentResponse.setPayDate(date);
            return paymentResponse;
        }
        log.warn("Can not hash response " + allParams);
        return new PaymentResponse();
    }

    public Date addHoursToJavaUtilDate(Date date, int hours) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR_OF_DAY, hours);
        return calendar.getTime();
    }

    @Override
    public PaymentUrlResponse generateVnPayRequestUrl(PaymentRequest paymentRequest) {
        VnPayRequest vnPayRequest = buildVnPayRequest(paymentRequest);
        return generateVnPayRequestUrl(vnPayRequest);
    }

    @Override
    public VnPayRequest buildVnPayRequest(PaymentRequest paymentRequest) {

        return VnPayRequest.builder()
                .version(vnPaySetting.getVersion())
                .command(VnPayCommandType.PAYMENT.getValue())
                .tmnCode(vnPaySetting.getTmnCode())
                .bankCode(null)
                .locale(VnPayLocale.VIETNAM.getValue())
                .currCode(VnPayCurrency.VND.name())
                .txnRef(paymentRequest.getTxnRef())
                .orderInfo(StringUtils.substring(paymentRequest.getContent(), 0, MAX_LENGTH_ORDER_INFO))
                .orderType(ORDER_TYPE)
                .amount(new VnPayMoneyConverter().toVnPay(paymentRequest.getAmount()))
                .returnUrl(paymentRequest.getReturnUrl())
                .ipAddr(webProvider.getClientIp())
                .createDate(vnPayProvider.currentDate())
                .secureHash(null)
                .build();
    }

    @Override
    public PaymentUrlResponse generateVnPayRequestUrl(VnPayRequest vnPayRequest) {
        PaymentUrlResponse response = new PaymentUrlResponse();
        response.setUrl(vnPayProvider.buildRequestUrl(vnPayRequest));
        response.setCreatedAt(new Date());
        return response;
    }
}
