package com.kinglogi.service.impl;

import com.kinglogi.dto.filter.CarGroupFilter;
import com.kinglogi.dto.filter.base.BaseFilter;
import com.kinglogi.dto.request.CarGroupRequest;
import com.kinglogi.dto.response.CarGroupResponse;
import com.kinglogi.dto.response.file_response.FileCarGroupResponse;
import com.kinglogi.entity.CarGroup;
import com.kinglogi.entity.CarGroupFile;
import com.kinglogi.entity.File;
import com.kinglogi.repository.CarGroupFileRepository;
import com.kinglogi.repository.CarGroupRepository;
import com.kinglogi.repository.FileRepository;
import com.kinglogi.service.CarGroupService;
import com.kinglogi.service.UserACL;
import com.kinglogi.service.error.AccessForbiddenException;
import com.kinglogi.service.error.BadRequestException;
import com.kinglogi.service.error.ResourceNotFoundException;
import com.kinglogi.service.utils.Utils;
import com.kinglogi.specs.BaseFilterSpecs;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CarGroupServiceImpl implements CarGroupService {

    private final CarGroupRepository carGroupRepository;

    private final FileRepository fileRepository;

    public String FILE_DIRECTORY;

    private final BaseFilterSpecs<CarGroup> baseFilterSpecs;

    private final CarGroupFileRepository carGroupFileRepository;

    private final UserACL userACL;

    public CarGroupServiceImpl(CarGroupRepository carGroupRepository, FileRepository fileRepository, BaseFilterSpecs<CarGroup> baseFilterSpecs, CarGroupFileRepository carGroupFileRepository, UserACL userACL) {
        this.carGroupRepository = carGroupRepository;
        this.fileRepository = fileRepository;
        this.baseFilterSpecs = baseFilterSpecs;
        this.carGroupFileRepository = carGroupFileRepository;
        this.userACL = userACL;
        this.FILE_DIRECTORY = System.getProperty("user.dir") + java.io.File.separator + "storage/";
    }

    @Override
    public Page<CarGroupResponse> findAll(BaseFilter baseFilter) {
        CarGroupFilter carGroupFilter = (CarGroupFilter) baseFilter;
        Pageable pageable = baseFilterSpecs.page(carGroupFilter);
        Specification<CarGroup> searchable = baseFilterSpecs.search(carGroupFilter);
        if (carGroupFilter.getNumberOfSeat() != 0) {
            Specification<CarGroup> additionalSpec = (root, query, cb) -> cb.equal(root.get("numberOfSeat"), carGroupFilter.getNumberOfSeat());
            searchable = searchable.and(additionalSpec);
        }
        Page<CarGroupResponse> page = carGroupRepository.findAll(searchable, pageable).map(CarGroupResponse::new);
        for (CarGroupResponse carGroupResponse : page) {
            List<FileCarGroupResponse> fileResponses = carGroupFileRepository.findAllByCarGroupId(carGroupResponse.getId()).stream().map(file -> new FileCarGroupResponse(file, carGroupResponse.getId())).collect(Collectors.toList());
            carGroupResponse.setFiles(fileResponses);
        }
        return page;
    }

    @Override
    public CarGroupResponse findById(Long aLong) {
        CarGroup carGroup = Utils.requireExists(carGroupRepository.findById(aLong), "error.carGroupNotFound");;
        List<FileCarGroupResponse> fileResponses = carGroup.getFiles().stream().map(file -> new FileCarGroupResponse(file, aLong)).collect(Collectors.toList());
        CarGroupResponse carGroupResponse = new CarGroupResponse(carGroup);
        carGroupResponse.setFiles(fileResponses);
        return carGroupResponse;
    }

    @Override
    public CarGroupResponse save(CarGroupRequest carGroupRequest) {
        if (!userACL.isSuperAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        if (StringUtils.isBlank(carGroupRequest.getTitle())) {
            throw new BadRequestException("error.notBlank");
        }
        Optional<CarGroup> carGroupOptional = carGroupRepository.findByNumberOfSeat(carGroupRequest.getNumberOfSeat());
        if (carGroupOptional.isPresent()) {
            throw new BadRequestException("error.numberOfSeatExisted");
        }
        CarGroup carGroup = new CarGroup();
        carGroup.setTitle(carGroupRequest.getTitle().trim());
        carGroup.setPriceNegotiate(carGroupRequest.isPriceNegotiate());
        if (!carGroupRequest.isPriceNegotiate()) {
            if (carGroupRequest.getPrice() <= 0) {
                throw new BadRequestException("error.priceInvalid");
            }
            carGroup.setPrice(carGroupRequest.getPrice());
        }
        carGroup.setNumberOfSeat(carGroupRequest.getNumberOfSeat());
        carGroupRepository.save(carGroup);
        return new CarGroupResponse(carGroup);
    }

    @Override
    public CarGroupResponse update(Long aLong, CarGroupRequest carGroupRequest) {
        if (!userACL.isSuperAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        CarGroup carGroup = Utils.requireExists(carGroupRepository.findById(aLong), "error.carGroupNotFound");;
        if (StringUtils.isBlank(carGroupRequest.getTitle())) {
            throw new BadRequestException("error.notBlank");
        }
        if (carGroupRequest.getNumberOfSeat() < 0) {
            throw new BadRequestException("error.numberOfSeatInvalid");
        }
        Optional<CarGroup> carGroupOptional = carGroupRepository.findByNumberOfSeat(carGroupRequest.getNumberOfSeat());
        if (carGroupOptional.isPresent() && carGroupOptional.get().getId() != aLong) {
            throw new BadRequestException("error.numberOfSeatExisted");
        }
        carGroup.setTitle(carGroupRequest.getTitle().trim());
        carGroup.setPriceNegotiate(carGroupRequest.isPriceNegotiate());
        if (!carGroupRequest.isPriceNegotiate()) {
            if (carGroupRequest.getPrice() <= 0) {
                throw new BadRequestException("error.priceInvalid");
            }
            carGroup.setPrice(carGroupRequest.getPrice());
        }
        carGroup.setNumberOfSeat(carGroupRequest.getNumberOfSeat());
        carGroupRepository.save(carGroup);
        return new CarGroupResponse(carGroup);
    }

    @Override
    public boolean deleteById(Long aLong) {
        if (!userACL.isSuperAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        CarGroup carGroup = Utils.requireExists(carGroupRepository.findById(aLong), "error.carGroupNotFound");
        List<File> files = carGroup.getFiles();
        for (File file : files) {
            java.io.File newFile = new java.io.File(file.getUrl());
            if (newFile.exists()) {
                newFile.delete();
            }
        }
        carGroupRepository.delete(carGroup);
        return true;
    }

    @Override
    public void uploadImage(Long id, List<MultipartFile> multipartFiles) {
        if (!userACL.isSuperAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        CarGroup carGroup = Utils.requireExists(carGroupRepository.findById(id), "error.carGroupNotFound");;
        if (multipartFiles.size() > 5) {
            throw new BadRequestException("error.uploadMax5Image");
        }
        String filePath = FILE_DIRECTORY + "carGroup/" + id;
        java.io.File fileDirectory = new java.io.File(filePath);
        if (!fileDirectory.exists()) {
            fileDirectory.mkdirs();
        }
        List<java.io.File> fileList = multipartFiles.stream().map(file -> {
            try {
                return convertMultiPartToFile(file, filePath);
            } catch (Exception e) {
                throw new BadRequestException(e.getMessage());
            }
        }).collect(Collectors.toList());
        List<File> files = new ArrayList<>();
        for (java.io.File newFile : fileList) {
            File file = new File();
            file.setName(newFile.getName());
            file.setUrl(newFile.getAbsolutePath());
            files.add(file);
        }
        fileRepository.saveAll(files);
        List<CarGroupFile> carGroupFiles = files.stream().map(file -> {
            CarGroupFile carGroupFile = new CarGroupFile();
            carGroupFile.setCarGroup(carGroup);
            carGroupFile.setFile(file);
            return carGroupFile;
        }).collect(Collectors.toList());
        carGroupFileRepository.saveAll(carGroupFiles);
    }

    @Override
    public InputStreamResource viewImage(Long id, String fileName) throws FileNotFoundException {
        CarGroup carGroup = Utils.requireExists(carGroupRepository.findById(id), "error.carGroupNotFound");;
        File file = fileRepository.findByNameAndGroupId(id, fileName).orElseThrow(() -> new ResourceNotFoundException("error.fileNotFound"));
        java.io.File newFile = new java.io.File(file.getUrl());
        FileInputStream fileInputStream = new FileInputStream(newFile);
        return new InputStreamResource(fileInputStream);
    }

    @Override
    public void deleteImage(Long id, String fileName) {
        if (!userACL.isSuperAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        CarGroup carGroup = Utils.requireExists(carGroupRepository.findById(id), "error.carGroupNotFound");;
        File file = fileRepository.findByNameAndGroupId(id, fileName).orElseThrow(() -> new ResourceNotFoundException("error.fileNotFound"));
        java.io.File newFile = new java.io.File(file.getUrl());
        if (newFile.exists()) {
            newFile.delete();
        }
        fileRepository.delete(file);
    }

    private java.io.File convertMultiPartToFile(MultipartFile file, String filePath) throws IOException {
        String fileNameWithoutExtension = FilenameUtils.removeExtension(file.getOriginalFilename());
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());
        java.io.File convFile = new java.io.File(filePath, fileNameWithoutExtension + "." + extension);
        int num = 0;
        while (convFile.exists()) {
            convFile = new java.io.File(filePath, fileNameWithoutExtension + "(" + (++num) + ")." + extension);
        }
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write( file.getBytes() );
        fos.close();
        return convFile;
    }

}
