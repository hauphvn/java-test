package com.kinglogi.service.impl;

import com.kinglogi.constant.LocaleConstant;
import com.kinglogi.constant.OrderStatus;
import com.kinglogi.dto.filter.TripFilter;
import com.kinglogi.dto.filter.base.BaseFilter;
import com.kinglogi.dto.price_range.PriceRangeOutputDTO;
import com.kinglogi.dto.request.MetaData;
import com.kinglogi.dto.request.TripRequest;
import com.kinglogi.dto.response.SimpleTripResponse;
import com.kinglogi.dto.response.TripResponse;
import com.kinglogi.entity.*;
import com.kinglogi.exception.KlgResourceNotFoundException;
import com.kinglogi.repository.OrderRepository;
import com.kinglogi.repository.PriceRangeRepository;
import com.kinglogi.repository.TripCatalogRepository;
import com.kinglogi.repository.TripRepository;
import com.kinglogi.service.TripService;
import com.kinglogi.service.UserACL;
import com.kinglogi.service.error.AccessForbiddenException;
import com.kinglogi.service.error.BadRequestException;
import com.kinglogi.service.utils.Utils;
import com.kinglogi.specs.BaseFilterSpecs;
import com.kinglogi.utils.BeanUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class TripServiceImpl implements TripService {

    public static final int COLUMN_INDEX_PICKING_UP_LOCATION = 0;
    public static final int COLUMN_INDEX_DROPPING_OFF_LOCATION = 1;
    public static final int COLUMN_INDEX_PICKING_UP_DATETIME = 2;
    public static final int COLUMN_INDEX_DISTANCE_IN_KM = 3;
    public static final int COLUMN_INDEX_TOTAL_PRICE = 4;
    public static final int COLUMN_INDEX_BRAND = 5;
    public static final int COLUMN_INDEX_NUMBER_OF_SEAT = 6;
    public static final int COLUMN_INDEX_DESCRIPTION = 7;

    private final UserACL userACL;

    private final TripRepository tripRepository;

    private final BaseFilterSpecs<Trip> baseFilterSpecs;

    private final OrderRepository orderRepository;

    private final PriceRangeRepository priceRangeRepository;

    private final TripCatalogRepository tripCatalogRepository;

    @Autowired
    public TripServiceImpl(UserACL userACL, TripRepository tripRepository, BaseFilterSpecs<Trip> baseFilterSpecs, OrderRepository orderRepository, PriceRangeRepository priceRangeRepository, TripCatalogRepository tripCatalogRepository) {
        this.userACL = userACL;
        this.tripRepository = tripRepository;
        this.baseFilterSpecs = baseFilterSpecs;
        this.orderRepository = orderRepository;
        this.priceRangeRepository = priceRangeRepository;
        this.tripCatalogRepository = tripCatalogRepository;
    }

    @Override
    public List<TripResponse> findAll() {
        TripFilter tripFilter = new TripFilter();
        tripFilter.setLimit(200);
        return findAll(tripFilter).getContent();
    }

    @Override
    public Page<TripResponse> findAll(BaseFilter baseFilter) {
    	TripFilter tripFilter = (TripFilter) baseFilter;
        Specification<Trip> searchable = buildSpecForSearchTrips(tripFilter);
        Pageable pageable = baseFilterSpecs.page(tripFilter);
        Page<Trip> page = tripRepository.findAllActive(searchable, pageable);
        return page.map(this::buildResponse);
    }

    @Override
    public Page<SimpleTripResponse> findAllSimpleTrip(BaseFilter baseFilter) {
        TripFilter tripFilter = (TripFilter) baseFilter;
        Specification<Trip> searchable = buildSpecForSearchTrips(tripFilter);
        Pageable pageable = baseFilterSpecs.page(tripFilter);
        Page<Trip> page = tripRepository.findAllActive(searchable, pageable);
        return page.map(trip -> BeanUtil.copyProperties(trip, SimpleTripResponse.class));
    }

    private Specification<Trip> buildSpecForSearchTrips(TripFilter tripFilter) {
    	Specification<Trip> searchByKeyword = baseFilterSpecs.search(tripFilter);

    	if (StringUtils.isBlank(tripFilter.getLanguage())) {
    	    tripFilter.setLanguage(LocaleConstant.VIETNAMESE);
    	}

        String language = tripFilter.getLanguage();
        Specification<Trip> searchByLanguage = (root, query, cb) -> {
            query.distinct(true);
            return cb.equal(root.get("language"), language);
        };
        searchByKeyword = Specification.where(searchByKeyword).and(searchByLanguage);

        return searchByKeyword;
    }

    @Override
    public TripResponse findById(Long id) {
        Trip trip = getById(id);
        return buildResponse(trip);
    }

    private TripResponse buildResponse(Trip entity) {
        TripResponse tripResponse = BeanUtil.copyProperties(entity, TripResponse.class);

        Calendar calPickingDateTime = Calendar.getInstance();
        if(tripResponse.getPickingUpDatetime() != null) {
            Calendar pickingUpDateTime = Calendar.getInstance();
            pickingUpDateTime.setTime(tripResponse.getPickingUpDatetime());

            calPickingDateTime.set(Calendar.HOUR_OF_DAY, pickingUpDateTime.get(Calendar.HOUR_OF_DAY));
            calPickingDateTime.set(Calendar.MINUTE, pickingUpDateTime.get(Calendar.MINUTE));
            tripResponse.setPickingUpDatetime(calPickingDateTime.getTime());
        }

        if (entity.getPriceRange() != null){
            PriceRangeOutputDTO priceRangeOutputDTO = BeanUtil.copyProperties(entity.getPriceRange(), PriceRangeOutputDTO.class);
            tripResponse.setPriceRangeOutputDTO(priceRangeOutputDTO);
        }
        return tripResponse;
    }

    private Trip getById(Long id) {
        return tripRepository.findById(id).orElseThrow(() -> new KlgResourceNotFoundException("Trip catalog is not found with id [" + id + "]"));
    }

    @Override
    public TripResponse save(TripRequest request) {
        if (request.getPriceRangeId() == null){
            Trip viTrip = BeanUtil.copyProperties(request, Trip.class);
            viTrip.setLanguage(LocaleConstant.VIETNAMESE);
            viTrip = tripRepository.save(viTrip);

            Trip enTrip = BeanUtil.copyProperties(request, Trip.class);
            enTrip.setLanguage(LocaleConstant.ENGLISH);
            enTrip.setTrip(viTrip);
            enTrip = tripRepository.save(enTrip);
            return this.buildResponse(viTrip);
        }else {
            PriceRange priceRange = Utils.requireExists(priceRangeRepository.findById(request.getPriceRangeId()), "error.dataNotFound");
            if (matchPickUpWithPriceRange(request.getGoMetaData(), priceRange.getGoAreaMetadata())){
                Trip viTrip = BeanUtil.copyProperties(request, Trip.class);
                viTrip.setLanguage(LocaleConstant.VIETNAMESE);
                viTrip.setDroppingOffLocation(priceRange.getDestinationArea());
                viTrip.setPriceRange(priceRange);
                viTrip.setTotalPrice(priceRange.getPrice());
                viTrip = tripRepository.save(viTrip);

                Trip enTrip = BeanUtil.copyProperties(request, Trip.class);
                enTrip.setLanguage(LocaleConstant.ENGLISH);
                enTrip.setTrip(viTrip);
                enTrip.setPriceRange(priceRange);
                enTrip.setDroppingOffLocation(priceRange.getDestinationArea());
                enTrip.setTotalPrice(priceRange.getPrice());
                enTrip = tripRepository.save(enTrip);

                return this.buildResponse(viTrip);
            }else {
                TripCatalog tripCatalog = BeanUtil.copyProperties(request, TripCatalog.class);
                tripCatalog.setPricePerKm((long) (request.getTotalPrice() / request.getDistanceInKm()));
                tripCatalog = tripCatalogRepository.save(tripCatalog);
                return BeanUtil.copyProperties(tripCatalog, TripResponse.class);
            }
        }
    }

    private boolean matchPickUpWithPriceRange(MetaData goMetaData, String goArea){
        String provinceGo = "";
        String districtGo = "";
        if (goMetaData.getProvince() != null){
            provinceGo = "\"province\":\""+ goMetaData.getProvince() + "\"";
        }
        if (goMetaData.getDistrict() != null){
            districtGo = "\"district\":\""+ goMetaData.getDistrict() + "\"";
        }
        return goArea.contains(provinceGo) && goArea.contains(districtGo);
    }

    @Override
    public TripResponse update(Long id, TripRequest request) {
        Trip trip = getById(id);
        BeanUtil.mergeProperties(request, trip);
        Trip savedTrip = tripRepository.save(trip);
        return this.buildResponse(savedTrip);
    }

    @Override
    public boolean deleteById(Long id) {
        try {
            Trip trip = getById(id);
            Set<Order> orders = trip.getOrders();
            List<Integer> orderStatuses = Arrays.asList(OrderStatus.PAID.getStatus(), OrderStatus.ON_GOING.getStatus(), OrderStatus.READY_FOR_MOVING.getStatus());
            if (!orders.isEmpty()) {
                Set<Order> ordersOnGoing = trip.getOrders().stream().filter(order -> orderStatuses.contains(order.getStatus())).collect(Collectors.toSet());
                if (!ordersOnGoing.isEmpty()) {
                    throw new BadRequestException("error.tripBeingUsed");
                }
                for (Order order : orders) {
                    order.setTrip(null);
                }
                orderRepository.saveAll(orders);
            }

            trip.setDeletedAt(Date.from(Instant.now()));
            tripRepository.save(trip);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }

    @Override
	public TripResponse getTripByParentTripId(Long parentTripId) {
		Trip trip = tripRepository.getTripByParentId(parentTripId);
        return buildResponse(trip);
	}

    @Override
    public Page<String> getPickingUpLocations(String pickingUpLocation, String language, Pageable pageable) {
        Page<String> pickingUpLocations = tripRepository.getPickingUpLocations(pickingUpLocation, language, pageable);
        return pickingUpLocations;
    }

    @Override
    public Page<TripResponse> getDroppingOffLocations(String pickingUpLocation, String droppingOffLocation, String language, Integer numberOfSeat, Pageable pageable) {
        if (numberOfSeat == null){
            Page<Trip> trips = tripRepository.getDroppingOffLocations(pickingUpLocation, droppingOffLocation, language, pageable);
            return trips.map(trip -> BeanUtil.copyProperties(trip, TripResponse.class));
        }else {
            Page<Trip> trips = tripRepository.getDroppingOffLocations(pickingUpLocation, droppingOffLocation, language, numberOfSeat, pageable);
            return trips.map(trip -> BeanUtil.copyProperties(trip, TripResponse.class));
        }

    }

    @Override
    public void importTrip(MultipartFile file) throws IOException, ParseException {
        if (!userACL.isSuperAdmin() && !userACL.isFixedAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        InputStream inputStream = file.getInputStream();

        List<TripRequest> tripRequestList = new ArrayList<>();

        try (Workbook workbook = WorkbookFactory.create(inputStream)){
            //Sheet
            Sheet sheet = workbook.getSheetAt(0);

            //remove row empty
            for (int i = 0; i <= sheet.getLastRowNum(); i++) {
                if (sheet.getRow(i) == null) continue;
                Row row = sheet.getRow(i);
                int emptyNumber = 0;
                for (int j = 0; j <= 7; j++){
                    Cell cell = row.getCell(j);
                    if ((cell == null || getCellValue(cell).equals("")) && (j != 5 && j != 7)) {
                        emptyNumber++;
                    }
                }
                if (emptyNumber >= 7){
                    sheet.removeRow(row);
                }
            }

            // Get all rows
            Iterator<Row> iterator = sheet.iterator();
            while (iterator.hasNext()) {
                Row nextRow = iterator.next();
                if (nextRow.getRowNum() == 0) {
                    // Ignore header
                    continue;
                }

                // Read cells and set value for trip request object
                TripRequest tripRequest = new TripRequest();
                for (int i = 0; i < 8; i++){
                    //Read cell
                    Cell cell = nextRow.getCell(i);
                    Object cellValue = null;
                    if (i != 5 && i != 7){
                        if (cell == null){
                            throw new BadRequestException("error.cannotNullContent");
                        }
                        cellValue = getCellValue(cell);
                        if (cellValue == null){
                            throw new BadRequestException("error.cannotBlankContent");
                        }
                    }else {
                        if (cell != null){
                            cellValue = getCellValue(cell);
                        }else {
                            continue;
                        }
                    }

                    // Set value for trip request object
                    int columnIndex = i;
                    switch (columnIndex) {
                        case COLUMN_INDEX_PICKING_UP_LOCATION:
                            tripRequest.setPickingUpLocation(cellValue.toString());
                            break;
                        case COLUMN_INDEX_DROPPING_OFF_LOCATION:
                            tripRequest.setDroppingOffLocation(cellValue.toString());
                            break;
                        case COLUMN_INDEX_PICKING_UP_DATETIME:
                            tripRequest.setPickingUpDatetime(new SimpleDateFormat("yyyyMMddHHmmss").parse(String.valueOf(BigDecimal.valueOf((double) cellValue).longValue())));
                            break;
                        case COLUMN_INDEX_DISTANCE_IN_KM:
                            tripRequest.setDistanceInKm(BigDecimal.valueOf((double) cellValue).floatValue());
                            break;
                        case COLUMN_INDEX_TOTAL_PRICE:
                            tripRequest.setTotalPrice(BigDecimal.valueOf((double) cellValue).longValue());
                            break;
                        case COLUMN_INDEX_BRAND:
                            tripRequest.setBrand(getCellValue(cell).toString());
                            break;
                        case COLUMN_INDEX_NUMBER_OF_SEAT:
                            tripRequest.setNumberOfSeat(BigDecimal.valueOf((double) cellValue).intValue());
                            if (tripRequest.getNumberOfSeat() == 2 || tripRequest.getNumberOfSeat() == 3 || tripRequest.getNumberOfSeat() >= 46 || tripRequest.getNumberOfSeat() < 0){
                                tripRequest.setNumberOfSeat(0);
                            }
                            break;
                        case COLUMN_INDEX_DESCRIPTION:
                            tripRequest.setDescription(getCellValue(cell).toString());
                            break;
                        default:
                            break;
                    }

                }
                tripRequestList.add(tripRequest);
            }

            workbook.close();
            inputStream.close();
        }

        if (tripRequestList.isEmpty()){
            throw new BadRequestException("error.importFailed");
        }

        tripRequestList.forEach(tripRequest -> save(tripRequest));
    }

    // Get cell value
    private static Object getCellValue(Cell cell) {
        CellType cellType = cell.getCellType();
        Object cellValue = null;
        switch (cellType) {
            case BOOLEAN:
                cellValue = cell.getBooleanCellValue();
                break;
            case FORMULA:
                Workbook workbook = cell.getSheet().getWorkbook();
                FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
                cellValue = evaluator.evaluate(cell).getNumberValue();
                break;
            case NUMERIC:
                cellValue = cell.getNumericCellValue();
                break;
            case STRING:
            case BLANK:
                cellValue = cell.getStringCellValue();
                break;
            case _NONE:
            case ERROR:
                break;
            default:
                break;
        }

        return cellValue;
    }

}
