package com.kinglogi.service.impl;

import com.kinglogi.dto.filter.base.BaseFilter;
import com.kinglogi.dto.request.BankAccountRequest;
import com.kinglogi.dto.response.BankAccountResponse;
import com.kinglogi.entity.BankAccount;
import com.kinglogi.exception.KlgResourceNotFoundException;
import com.kinglogi.repository.BankAccountRepository;
import com.kinglogi.service.BankAccountService;
import com.kinglogi.specs.BaseFilterSpecs;
import com.kinglogi.utils.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BankAccountServiceImpl implements BankAccountService {

    private final BankAccountRepository bankAccountRepository;

    private final BaseFilterSpecs<BankAccount> baseFilterSpecs;

    @Autowired
    public BankAccountServiceImpl(BankAccountRepository bankAccountRepository, BaseFilterSpecs<BankAccount> baseFilterSpecs) {
        this.bankAccountRepository = bankAccountRepository;
        this.baseFilterSpecs = baseFilterSpecs;
    }

    @Override
    public List<BankAccountResponse> findAll() {
        return BeanUtil.listCopyProperties(bankAccountRepository.findAll(), BankAccountResponse.class);
    }

    @Override
    public Page<BankAccountResponse> findAll(BaseFilter baseFilter) {
        Specification<BankAccount> searchable = baseFilterSpecs.search(baseFilter);
        Pageable pageable = baseFilterSpecs.page(baseFilter);
        Page<BankAccount> page = bankAccountRepository.findAll(searchable, pageable);
        return page.map((entity) -> BeanUtil.copyProperties(entity, BankAccountResponse.class));
    }

    @Override
    public BankAccountResponse findById(Long id) {
        BankAccount bankAccount = getById(id);
        return BeanUtil.copyProperties(bankAccount, BankAccountResponse.class);
    }

    private BankAccount getById(Long id) {
        return bankAccountRepository.findById(id).orElseThrow(() -> new KlgResourceNotFoundException("Bank account is not found with id [" + id + "]"));
    }

    @Override
    public BankAccountResponse save(BankAccountRequest request) {
        BankAccount bankAccount = BeanUtil.copyProperties(request, BankAccount.class);
        BankAccount savedBankAccount = bankAccountRepository.save(bankAccount);
        return BeanUtil.copyProperties(savedBankAccount, BankAccountResponse.class);
    }

    @Override
    public BankAccountResponse update(Long id, BankAccountRequest request) {
        BankAccount bankAccount = getById(id);
        BeanUtil.mergeProperties(request, bankAccount);
        BankAccount savedBankAccount = bankAccountRepository.save(bankAccount);
        return BeanUtil.copyProperties(savedBankAccount, BankAccountResponse.class);
    }

    @Override
    public boolean deleteById(Long id) {
        try {
            bankAccountRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }
}
