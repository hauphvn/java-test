package com.kinglogi.service.impl;

import com.kinglogi.constant.Auth;
import com.kinglogi.dto.AuthUserDetails;
import com.kinglogi.entity.Account;
import com.kinglogi.entity.Feature;
import com.kinglogi.entity.Role;
import com.kinglogi.exception.KlgResourceNotFoundException;
import com.kinglogi.repository.custom.CustomAccountRepository;
import com.kinglogi.service.utils.Utils;
import com.kinglogi.utils.EncrytedPasswordUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	private final CustomAccountRepository userRepository;

	@Autowired
	public UserDetailsServiceImpl(@Qualifier("customAccountRepository") CustomAccountRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public UserDetails loadUserByUsername(String email) {
        Account account = userRepository.findByUsernameOrPhoneNumber(email).orElseThrow(() -> new UsernameNotFoundException("Invalid username or password."));
		return this.getUserDetails(account);
	}

	/**
	 * Get the user based on the username
	 *
	 * @param id The id of user
	 */
	public UserDetails loadUserById(Long id) {
		Account account = userRepository.findOneActive(id).orElseThrow(() -> new KlgResourceNotFoundException("User with id [" + id + "] is not exists"));
		return this.getUserDetails(account);
	}

	/**
	 * Get user detail if user is valid
	 */
	private UserDetails getUserDetails(Account account) {
		if (StringUtils.isBlank(account.getPassword())) {
			String pwd = EncrytedPasswordUtils.encryptPassword(Auth.GOOGLE_PASSWORD_TOKEN);
			account.setPassword(pwd);
		}
		return new AuthUserDetails(account, this.getFeatures(account));
	}

	private List<Feature> getFeatures(Account account) {
		List<Feature> features = new ArrayList<>();
		account.getRoles().forEach(role -> features.addAll(role.getFeatures()));
		return features;
	}
}
