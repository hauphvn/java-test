package com.kinglogi.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kinglogi.configuration.security.SmsSetting;
import com.kinglogi.constant.*;
import com.kinglogi.constant.Role;
import com.kinglogi.dto.event.EventLogOtp;
import com.kinglogi.dto.filter.OrderFilter;
import com.kinglogi.dto.request.AccountRequest;
import com.kinglogi.dto.request.UserProfileRequest;
import com.kinglogi.dto.response.AccountResponse;
import com.kinglogi.dto.response.OrderResponse;
import com.kinglogi.dto.response.TransactionResponse;
import com.kinglogi.dto.sms.SmsRequest;
import com.kinglogi.dto.sms.SmsResponse;
import com.kinglogi.entity.*;
import com.kinglogi.exception.KlgAuthenticationException;
import com.kinglogi.provider.AuthenticatedProvider;
import com.kinglogi.repository.AccountOtpRepository;
import com.kinglogi.repository.ImageRepository;
import com.kinglogi.repository.OrderRepository;
import com.kinglogi.repository.custom.CustomAccountRepository;
import com.kinglogi.service.*;
import com.kinglogi.service.error.AccessForbiddenException;
import com.kinglogi.service.error.BadRequestException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserProfileServiceImpl implements UserProfileService {

    private final AuthenticatedProvider authenticatedProvider;
    private final TransactionService transactionService;
    private final AccountService accountService;

    private final ImageRepository imageRepository;

    public static final String FILE_DIRECTORY =  System.getProperty("user.dir") + File.separator + "documents/";
    private final OrderService orderService;
    private final AccountOtpRepository accountOtpRepository;

    @Autowired
    ApplicationEventPublisher applicationEventPublisher;
    @Autowired
    private CustomAccountRepository accountRepository;
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    OtpService otpService;

    @Autowired
    public UserProfileServiceImpl(
            AuthenticatedProvider authenticatedProvider,
            TransactionService transactionService,
            @Qualifier("accountService") AccountService accountService,
            ImageRepository imageRepository, OrderService orderService, AccountOtpRepository accountOtpRepository) {
        this.authenticatedProvider = authenticatedProvider;
        this.transactionService = transactionService;
        this.accountService = accountService;
        this.imageRepository = imageRepository;
        this.orderService = orderService;
        this.accountOtpRepository = accountOtpRepository;
    }

    @Override
    public AccountResponse getProfile() {
        Long userId = getLoggedInUserId();
        AccountResponse response = accountService.findById(userId);
        response.setBalance(transactionService.getBalanceByAccountId(userId));
        return response;
    }

    @Override
    public AccountResponse updateProfile(UserProfileRequest request) {
        Long userId = getLoggedInUserId();
        AccountResponse accountResponse = accountService.findById(userId);
        AccountRequest accountRequest = AccountRequest.builder()
                .id(userId)
                .fullName(request.getFullName())
                .email(request.getEmail())
                .gender(request.getGender())
                .birthday(request.getBirthday())
                .phoneNumber(request.getPhoneNumber())
                .address(request.getAddress())
                .roles(new ArrayList<>(accountResponse.getRoles()))
                .allowNotification(request.getAllowNotification())
                .build();

        AccountResponse response = accountService.update(accountRequest.getId(), accountRequest);
        response.setBalance(transactionService.getBalanceByAccountId(userId));
        return response;
    }

    @Override
    public List<TransactionResponse> getTransactionsByAccountId() {
        return transactionService.findByAccountId(getLoggedInUserId());
    }

    @Override
    public Page<OrderResponse> getOrdersByAccountId(OrderFilter orderFilter) {
        orderFilter.setOrderType(OrderType.ALL.getType());
        orderFilter.setCreatedBy(getLoggedInUserId());
        return orderService.findAll(orderFilter);
    }

    @Override
    public void upLoadAvatar(MultipartFile multipartFile) throws IOException {
        Long userId = getLoggedInUserId();
        String filePath = FILE_DIRECTORY + "/account/avatar/" + userId;
        File fileDirectory = new File(filePath);
        if (!fileDirectory.exists()) {
            fileDirectory.mkdirs();
        }
        File file = convertMultiPartToFile(multipartFile, filePath);
        Image image = new Image();
        image.setOrginal(file.getAbsolutePath());
        image.setDisplayName(file.getName());
        imageRepository.save(image);

        accountService.updateAvatar(userId, String.valueOf(image.getId()));
    }

    @Override
    public InputStreamResource viewImage() {
        InputStreamResource inputStreamResource = null;
        Long userId = getLoggedInUserId();
        String avatar = accountService.findById(userId).getAvatar();
        if (avatar != null) {
            Long avatarId = Long.parseLong(avatar);
            Image image = imageRepository.findById(avatarId).get();
            try {
                InputStream inputStream = Files.newInputStream(Paths.get(image.getOrginal()));
                inputStreamResource = new InputStreamResource(inputStream);
            } catch (IOException e) {
                return null;
            }
        }
        return inputStreamResource;
    }

    @Override
    public AccountResponse updatePhoneNumber(UserProfileRequest request, String otp) {
        Long userId = getLoggedInUserId();
        AccountRequest accountRequest = AccountRequest.builder()
                .id(userId)
                .phoneNumber(request.getPhoneNumber())
                .build();
        return accountService.updatePhoneNumber(accountRequest, otp);
    }

    @Transactional
    @Override
    public Instant sendOtpForUpdatePhoneNumber(UserProfileRequest request) throws IOException {
        Long userId = getLoggedInUserId();
        AccountResponse account = accountService.findById(userId);
        if (account.getPhoneNumber().trim().equals(request.getPhoneNumber())){
            throw new BadRequestException("error.duplicatePhoneNumber");
        }
        if (accountRepository.findByPhoneNumber(request.getPhoneNumber().trim()).isPresent()) {
            throw new BadRequestException("error.phoneNumberExisted");
        }
        accountOtpRepository.deleteAllByPhoneNumber(request.getPhoneNumber());
        return otpService.sendOTP(request.getPhoneNumber());
    }

    @Override
    public void deleteAccount() {
        Long userId = getLoggedInUserId();
        Account account = accountRepository.getOne(userId);
        if (account.getDeletedAt() != null){
            throw new BadRequestException("error.accountAlreadyDeleted");
        }
        accountService.deleteById(userId);
    }

    @Override
    public InputStreamResource viewImage(Long id) {
        InputStreamResource inputStreamResource = null;
        AccountResponse account = accountService.findById(id);
        String avatar = account.getAvatar();
        if (avatar != null) {
            Long avatarId = Long.parseLong(avatar);
            Image image = imageRepository.findById(avatarId).get();
            try {
                InputStream inputStream = Files.newInputStream(Paths.get(image.getOrginal()));
                inputStreamResource = new InputStreamResource(inputStream);
            } catch (IOException e) {
                return null;
            }
        }
        return inputStreamResource;
    }


    public Long getLoggedInUserId() {
        return authenticatedProvider.getUserId().get();
    }

    private File convertMultiPartToFile(MultipartFile file, String filePath) throws IOException {
        String fileNameWithoutExtension = FilenameUtils.removeExtension(file.getOriginalFilename());
        File convFile = new File(filePath, fileNameWithoutExtension + ".jpg");
        int num = 0;
        while (convFile.exists()) {
            convFile = new File(filePath, fileNameWithoutExtension + "(" + (++num) + ").jpg");
        }
        FileOutputStream fos = new FileOutputStream( convFile );
        fos.write( file.getBytes() );
        fos.close();
        return convFile;
    }

    @Override
    public String exportOrder() throws IOException {
        Optional<Long> idUser = authenticatedProvider.getUserId();
        if (!idUser.isPresent()){
            throw new AccessForbiddenException("error.notPermission");
        }
        Account account = accountRepository.getOne(idUser.get());
        Set<String> roles = account.getRoles().stream().map(com.kinglogi.entity.Role::getCode).collect(Collectors.toSet());
        if (!roles.contains(Role.PARTNER.getCode())){
            throw new KlgAuthenticationException("error.notPartner");
        }
        List<Order> orders = orderRepository.findAllByPartner(account.getId());
        String path = FILE_DIRECTORY + File.separator  + "orders-"+ account.getId() +".xlsx";
        writeToExcel(orders, path);
        return path;
    }

    private void writeToExcel(List<Order> orders, String excelFilePath) throws IOException {
        // Create Workbook
        Workbook workbook = getWorkbook(excelFilePath);

        // Create sheet
        Sheet sheet = workbook.createSheet("Orders"); // Create sheet with sheet name

        int rowIndex = 0;

        // Write header
        Row row = sheet.createRow(rowIndex);
        Cell cell = row.createCell(0);
        cell.setCellValue("Order code");

        cell = row.createCell(1);
        cell.setCellValue("Email");

        cell = row.createCell(2);
        cell.setCellValue("Full name");

        cell = row.createCell(3);
        cell.setCellValue("Phone number");

        cell = row.createCell(4);
        cell.setCellValue("Pick up localtion");

        cell = row.createCell(5);
        cell.setCellValue("Pick up time");

        cell = row.createCell(6);
        cell.setCellValue("Drop off localtion");

        cell = row.createCell(7);
        cell.setCellValue("number of seat");

        cell = row.createCell(8);
        cell.setCellValue("Brand");

        cell = row.createCell(9);
        cell.setCellValue("Distance in km");

        cell = row.createCell(10);
        cell.setCellValue("Payment type");

        cell = row.createCell(11);
        cell.setCellValue("Status");

        cell = row.createCell(12);
        cell.setCellValue("Price");

        // Write data
        rowIndex++;
        for (Order order : orders) {
            // Create row
            row = sheet.createRow(rowIndex);
            // Write data on row
            writeOrder(order, row);
            rowIndex++;
        }

        // Auto resize column witdth
        int numberOfColumn = sheet.getRow(0).getPhysicalNumberOfCells();
        autosizeColumn(sheet, numberOfColumn);

        // Create file excel
        createOutputFile(workbook, excelFilePath);
    }

    // Create output file
    private static void createOutputFile(Workbook workbook, String excelFilePath) throws IOException {
        try (OutputStream os = new FileOutputStream(excelFilePath)) {
            workbook.write(os);
        }
    }

    private void writeOrder(Order order, Row row) {
        Cell cell = row.createCell(0);
        if (order.getOrderCode() != null) {
            cell.setCellValue(order.getOrderCode());
        }

        cell = row.createCell(1);
        if (order.getEmail() != null) {
            cell.setCellValue(order.getEmail());
        }

        cell = row.createCell(2);
        if (order.getFullName() != null) {
            cell.setCellValue(order.getFullName());
        }

        cell = row.createCell(3);
        if (order.getPhoneNumber() != null) {
            cell.setCellValue(order.getPhoneNumber());
        }

        cell = row.createCell(4);
        if (order.getPickingUpLocation() != null) {
            cell.setCellValue(order.getPickingUpLocation());
        }

        cell = row.createCell(5);
        if (order.getPickingUpDatetime() != null) {
            cell.setCellValue(converTimeToString(order.getPickingUpDatetime()));
        }

        cell = row.createCell(6);
        if (order.getDroppingOffLocation() != null) {
            cell.setCellValue(order.getDroppingOffLocation());
        }

        cell = row.createCell(7);
        if (order.getNumberOfSeat() != null) {
            cell.setCellValue(order.getNumberOfSeat());
        }

        cell = row.createCell(8);
        if (order.getBrand() != null) {
            cell.setCellValue(order.getBrand());
        }

        cell = row.createCell(9);
        if (order.getDistanceInKm() != null){
            cell.setCellValue(order.getDistanceInKm());
        }

        cell = row.createCell(10);
        if (order.getPaymentType() != null) {
            cell.setCellValue(order.getPaymentType());
        }

        cell = row.createCell(11);
        if (order.getStatus() != null) {
            cell.setCellValue(OrderStatus.getOrderStatus(order.getStatus().intValue()).get().toString());
        }

        cell = row.createCell(12);
        if (order.getEndPrice() != null){
            cell.setCellValue(order.getEndPrice());
        }


    }

    // Auto resize column width
    private static void autosizeColumn(Sheet sheet, int lastColumn) {
        for (int columnIndex = 0; columnIndex < lastColumn; columnIndex++) {
            sheet.autoSizeColumn(columnIndex);
        }
    }

    // Create workbook
    private static Workbook getWorkbook(String excelFilePath) throws IOException {
        Workbook workbook = null;

        if (excelFilePath.endsWith("xlsx")) {
            workbook = new XSSFWorkbook();
        } else if (excelFilePath.endsWith("xls")) {
            workbook = new HSSFWorkbook();
        } else {
            throw new IllegalArgumentException("The specified file is not Excel file");
        }

        return workbook;
    }

    private String converTimeToString(Date date){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = dateFormat.format(date);
        return dateString;
    }
}
