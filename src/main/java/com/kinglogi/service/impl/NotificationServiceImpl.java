package com.kinglogi.service.impl;

import com.kinglogi.constant.NotificationProvider;
import com.kinglogi.constant.NotificationType;
import com.kinglogi.dto.UserToken;
import com.kinglogi.dto.filter.base.BaseFilter;
import com.kinglogi.dto.request.*;
import com.kinglogi.dto.response.NotificationResponse;
import com.kinglogi.entity.Account;
import com.kinglogi.entity.Notification;
import com.kinglogi.exception.KlgResourceNotFoundException;
import com.kinglogi.factory.PushNotificationServiceFactory;
import com.kinglogi.provider.AuthenticatedProvider;
import com.kinglogi.repository.NotificationRepository;
import com.kinglogi.service.NotificationService;
import com.kinglogi.specs.BaseFilterSpecs;
import com.kinglogi.utils.BeanUtil;
import com.kinglogi.utils.JsonUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Join;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class NotificationServiceImpl implements NotificationService {

    private final AuthenticatedProvider authenticatedProvider;

    private final NotificationRepository notificationRepository;

    private final BaseFilterSpecs<Notification> baseFilterSpecs;

    @Autowired
    public NotificationServiceImpl(
            AuthenticatedProvider authenticatedProvider,
            NotificationRepository notificationRepository,
            BaseFilterSpecs<Notification> baseFilterSpecs) {
        this.authenticatedProvider = authenticatedProvider;
        this.notificationRepository = notificationRepository;
        this.baseFilterSpecs = baseFilterSpecs;
    }

    @Override
    public Page<NotificationResponse> findAll(BaseFilter baseFilter) {
        Specification<Notification> searchable = baseFilterSpecs.search(baseFilter);
        Specification<Notification> additionalSpec = (root, query, cb) -> {
            Join<Notification, Account> account = root.join("account");
            return cb.equal(account.get("id"), authenticatedProvider.getUserId().get());
        };
        searchable = searchable.and(additionalSpec);
        Pageable pageable = baseFilterSpecs.page(baseFilter);
        Page<Notification> page = notificationRepository.findAll(searchable, pageable);
        return page.map((entity) -> BeanUtil.copyProperties(entity, NotificationResponse.class));
    }

    @Override
    public NotificationResponse findById(Long id) {
        Notification tripCatalog = getById(id);
        return BeanUtil.copyProperties(tripCatalog, NotificationResponse.class);
    }

    private Notification getById(Long id) {
        return notificationRepository.findById(id).orElseThrow(() -> new KlgResourceNotFoundException("Trip catalog is not found with id [" + id + "]"));
    }

    @Override
    public NotificationResponse save(NotificationRequest request) {
        Account account = new Account();
        account.setId(request.getAccountId());

        Notification notification = BeanUtil.copyProperties(request, Notification.class);
        if (request.getData() != null) {
            notification.setAdditionalData(JsonUtils.toJson(request.getData()));
        }
        notification.setAccount(account);
        Notification savedNotification = notificationRepository.save(notification);
        return BeanUtil.copyProperties(savedNotification, NotificationResponse.class);
    }

    @Override
    public NotificationResponse update(Long id, NotificationRequest request) {
        Notification notification = getById(id);
        if (request.getData() != null) {
            notification.setAdditionalData(JsonUtils.toJson(request.getData()));
        }
        BeanUtil.mergeProperties(request, notification);
        Notification savedNotification = notificationRepository.save(notification);
        return BeanUtil.copyProperties(savedNotification, NotificationResponse.class);
    }

    @Override
    public boolean deleteById(Long id) {
        try {
            notificationRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }

    @Override
    public boolean markAsRead(Long id) {
        Notification notification = getById(id);
        notification.setMarkAsRead(true);
        notificationRepository.save(notification);
        return true;
    }

    @Override
    public boolean markAllAsRead() {
        Long userId = authenticatedProvider.getUserId().get();
        notificationRepository.markAllAsReadByAccountId(userId, LocalDateTime.now());
        return true;
    }

    @Override
    public int saveAll(List<NotificationRequest> notificationRequests) {
        return ChunkServiceExecutor.execute(notificationRequests, subNotificationRequests -> {
            subNotificationRequests.forEach(this::save);
            notificationRepository.flush();
            return subNotificationRequests.size();
        });
    }

    @Override
    public boolean deleteAll() {
        Long userId = authenticatedProvider.getUserId().get();
        notificationRepository.deleteAllByAccountId(userId);
        return true;
    }

    @Override
    public boolean sendNotification(PushNotificationDto request) {
        sendPushNotification(request);
        return true;
    }

    @Override
    public int countUnreadNotification() {
        Long userId = authenticatedProvider.getUserId().get();
        return notificationRepository.countUnreadNotification(userId);
    }

    private void sendPushNotification(PushNotificationDto request) {
        NotificationType.getByType(request.getType()).ifPresent(notificationType -> {
            switch (notificationType.getProvider()){
                case EXPO:
                    sendViaExpoPush(request, notificationType.getProvider());
                    break;
                case FIREBASE:
                    sendViaFcm(request, notificationType.getProvider());
                    break;
                default:
                    break;
            }
        });
    }

    private void sendViaExpoPush(PushNotificationDto request, NotificationProvider provider) {
        String []tokens = request.getUserTokens().stream().map(UserToken::getToken).distinct().toArray(String[]::new);
        ExpoPushNotificationRequest pushNotificationRequest = ExpoPushNotificationRequest.builder()
                .to(tokens)
                .title(request.getTitle())
                .body(request.getBody())
                .data(request.getData())
                .build();
        PushNotificationServiceFactory.getService(provider).sendPushNotificationAsync(pushNotificationRequest);
    }

    private void sendViaFcm(PushNotificationDto request, NotificationProvider provider) {
        List<String> tokens = request.getUserTokens().stream().map(UserToken::getToken).distinct().collect(Collectors.toList());
        FcmPushNotificationRequest.FcmPushNotificationRequestBuilder notificationRequest = FcmPushNotificationRequest.builder()
                .tokens(tokens)
                .title(request.getTitle())
                .body(request.getBody())
                .data(request.getData());
        if (StringUtils.isNotBlank(request.getTopicName())) {
            FcmSubscriptionRequest subscriptionRequest = FcmSubscriptionRequest.builder()
                    .topicName(request.getTopicName())
                    .build();
            subscriptionRequest.withParentBuilder(notificationRequest);
            PushNotificationServiceFactory.getService(provider).sendPushNotificationAsync(subscriptionRequest);
        } else {
            PushNotificationServiceFactory.getService(provider).sendPushNotificationAsync(notificationRequest.build());
        }
    }
}
