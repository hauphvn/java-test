package com.kinglogi.service.impl;

import com.kinglogi.constant.NotificationType;
import com.kinglogi.constant.PromotionStatus;
import com.kinglogi.constant.UserSelectOption;
import com.kinglogi.dto.UserToken;
import com.kinglogi.dto.commission.CommissionOutputDTO;
import com.kinglogi.dto.filter.AccountFilter;
import com.kinglogi.dto.filter.PromotionFillter;
import com.kinglogi.dto.filter.base.BaseFilter;
import com.kinglogi.dto.promotion.PromotionRequest;
import com.kinglogi.dto.promotion.PromotionResponse;
import com.kinglogi.dto.request.NotificationRequest;
import com.kinglogi.dto.request.PushNotificationDto;
import com.kinglogi.dto.response.AccountResponse;
import com.kinglogi.dto.user_promotion.UserPromotionResponse;
import com.kinglogi.entity.Account;
import com.kinglogi.entity.Promotion;
import com.kinglogi.entity.Role;
import com.kinglogi.entity.UserPromotion;
import com.kinglogi.exception.KlgResourceNotFoundException;
import com.kinglogi.provider.AuthenticatedProvider;
import com.kinglogi.repository.AccountRepository;
import com.kinglogi.repository.PromotionRepository;
import com.kinglogi.repository.UserPromotionRepository;
import com.kinglogi.repository.custom.CustomAccountRepository;
import com.kinglogi.repository.custom.CustomRoleRepository;
import com.kinglogi.repository.impl.PromotionRepositoryImpl;
import com.kinglogi.service.NotificationService;
import com.kinglogi.service.PromotionService;
import com.kinglogi.service.PushNotificationTokenService;
import com.kinglogi.service.UserACL;
import com.kinglogi.service.error.AccessForbiddenException;
import com.kinglogi.service.error.BadRequestException;
import com.kinglogi.service.utils.Utils;
import com.kinglogi.specs.BaseFilterSpecs;
import com.kinglogi.utils.BeanUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class PromotionServiceImpl implements PromotionService {

    private final UserACL userACL;
    private final PromotionRepository promotionRepository;
    private final BaseFilterSpecs<Promotion> baseFilterSpecs;
    private final AccountRepository accountRepository;
    private final UserPromotionRepository userPromotionRepository;
    private final CustomRoleRepository roleRepository;
    private final PushNotificationTokenService pushNotificationTokenService;
    private final NotificationService notificationService;
    private final PromotionRepositoryImpl customPromotionRepository;
    private final AuthenticatedProvider authenticatedProvider;
    private final CustomAccountRepository customAccountRepository;

    @Autowired
    public PromotionServiceImpl(UserACL userACL, PromotionRepository promotionRepository, BaseFilterSpecs<Promotion> baseFilterSpecs, AccountRepository accountRepository, UserPromotionRepository userPromotionRepository, CustomRoleRepository customRoleRepository, PushNotificationTokenService pushNotificationTokenService, NotificationService notificationService, PromotionRepositoryImpl customPromotionRepository, AuthenticatedProvider authenticatedProvider, CustomAccountRepository customAccountRepository) {
        this.userACL = userACL;
        this.promotionRepository = promotionRepository;
        this.baseFilterSpecs = baseFilterSpecs;
        this.accountRepository = accountRepository;
        this.userPromotionRepository = userPromotionRepository;
        this.roleRepository = customRoleRepository;
        this.pushNotificationTokenService = pushNotificationTokenService;
        this.notificationService = notificationService;
        this.customPromotionRepository = customPromotionRepository;
        this.authenticatedProvider = authenticatedProvider;
        this.customAccountRepository = customAccountRepository;
    }

    private PromotionResponse buildResponse(Promotion promotion, List<UserPromotion> userPromotions, UserSelectOption selectOption) {
        PromotionResponse response = BeanUtil.copyProperties(promotion, PromotionResponse.class);
        response.setExpireTime(promotion.getExpireTime());
        List<Long> ids = new ArrayList<>();
        userPromotions.stream().forEach(userPromotion -> ids.add(userPromotion.getAccount().getId()));
        response.setListUserIds(ids);
        response.setUserSelectOption(selectOption);
        return response;
    }

    private PromotionResponse convertToResponse(Promotion promotion){
        PromotionResponse promotionResponse = BeanUtil.copyProperties(promotion, PromotionResponse.class);
        promotionResponse.setExpireTime(promotion.getExpireTime());
        promotionResponse.setUserPromotionResponses(promotion.getUserPromotions().stream().map(userPromotion -> {
            UserPromotionResponse userPromotionResponse = new UserPromotionResponse();
            userPromotionResponse.setId(userPromotion.getAccount().getId());
            userPromotionResponse.setUsername(userPromotion.getAccount().getUsername());
            userPromotionResponse.setPhoneNumber(userPromotion.getAccount().getPhoneNumber());
            userPromotionResponse.setUsageRemain(userPromotion.getUsageRemain());
            userPromotionResponse.setFullName(userPromotion.getAccount().getFullName());
            return userPromotionResponse;
        }).collect(Collectors.toList()));
        return promotionResponse;
    }

    private PromotionResponse buildResponseDetail(Promotion promotion, List<UserPromotion> userPromotions){
        PromotionResponse promotionResponse = BeanUtil.copyProperties(promotion, PromotionResponse.class);
        promotionResponse.setExpireTime(promotion.getExpireTime());
        List<UserPromotionResponse> userPromotionResponses = userPromotions.stream().map(userPromotion -> {
            UserPromotionResponse userPromotionResponse = new UserPromotionResponse();
            userPromotionResponse.setId(userPromotion.getAccount().getId());
            userPromotionResponse.setUsername(userPromotion.getAccount().getUsername());
            userPromotionResponse.setFullName(userPromotion.getAccount().getFullName());
            userPromotionResponse.setPhoneNumber(userPromotion.getAccount().getPhoneNumber());
            userPromotionResponse.setUsageRemain(userPromotion.getUsageRemain());
            return userPromotionResponse;
        }).collect(Collectors.toList());
        promotionResponse.setUserPromotionResponses(userPromotionResponses);
        return promotionResponse;
    }

    @Override
    public Page<PromotionResponse> findAll(BaseFilter baseFilter) {
        if (!(userACL.isSuperAdmin() || userACL.isFixedAdmin() || userACL.isFlexibleAdmin())){
            throw new AccessForbiddenException("error.notAdmin");
        }
        PromotionFillter promotionFillter = (PromotionFillter) baseFilter;
        Specification<Promotion> spec = baseFilterSpecs.search(promotionFillter);

        Specification<Promotion> additionalSpec = (root, query, cb) ->
                cb.isNull(root.get("deletedAt"));
        spec = spec.and(additionalSpec);

        Pageable pageable = baseFilterSpecs.page(baseFilter);
        Page<Promotion> promotionPage = promotionRepository.findAll(spec, pageable);
        return promotionPage.map(this::convertToResponse);
    }

    @Override
    public PromotionResponse findById(Long aLong) {
        if (!(userACL.isSuperAdmin() || userACL.isFixedAdmin() || userACL.isFlexibleAdmin())) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Promotion promotion = Utils.requireExists(promotionRepository.findByIdAndDeletedAtIsNull(aLong), "error.dataNotFound");
        List<UserPromotion> userPromotions = userPromotionRepository.findAllByPromotion(promotion);
        return buildResponseDetail(promotion, userPromotions);
    }

    @Override
    public PromotionResponse save(PromotionRequest promotionRequest) {
        if (!(userACL.isSuperAdmin() || userACL.isFixedAdmin() || userACL.isFlexibleAdmin())){
            throw new AccessForbiddenException("error.notAdmin");
        }
        if (Utils.truncateInstantDate(Instant.now()).compareTo(Utils.truncateInstantDate(promotionRequest.getExpireTime())) >= 0) {
            throw new BadRequestException("error.expireTimeMustBeGreaterThanCurrentTime");
        }
        if (promotionRequest.getNumberOfUsage() <= 0){
            throw new BadRequestException("error.numberOfUsageInvalid");
        }
        if (promotionRequest.getPrice() <= 0){
            throw new BadRequestException("error.priceInvalid");
        }
        if (promotionRequest.getUserSelectOption().equals(UserSelectOption.OPTIONAL)){
            if (promotionRequest.getListUserIds().isEmpty()){
                throw new BadRequestException("error.listUserIdsEmpty");
            }
        }

        Optional<Promotion> promotionOptional = promotionRepository.findByCodeAndDeletedAtIsNull(promotionRequest.getCode());
        if (promotionOptional.isPresent()){
            throw new BadRequestException("error.promotionCodeExist");
        }

        Promotion promotion = BeanUtil.copyProperties(promotionRequest, Promotion.class);
        promotion.setExpireTime(Utils.truncateInstantDate(promotionRequest.getExpireTime()));
        promotion.setStatus(PromotionStatus.ACTIVE.getStatus());
        promotionRepository.save(promotion);
        // Get the list of accounts to apply the promo code
        List<Account> accountList = new ArrayList<>();
        if (promotionRequest.getUserSelectOption().equals(UserSelectOption.OPTIONAL)){
            accountList = accountRepository.findAllByIdIn(promotionRequest.getListUserIds());
        }else if (promotionRequest.getUserSelectOption().equals(UserSelectOption.ALL)){
            accountList = accountRepository.findAllByActiveIsTrueAndDeletedAtIsNull();
        } else if (promotionRequest.getUserSelectOption().equals(UserSelectOption.COLLABORATOR)) {
            Role role = Utils.requireExists(roleRepository.findByCode(UserSelectOption.COLLABORATOR.getCode()), "error.dataNotFound");
            accountList.addAll(role.getAccounts());
        }else if (promotionRequest.getUserSelectOption().equals(UserSelectOption.MEMBER)){
            Role role = Utils.requireExists(roleRepository.findByCode(UserSelectOption.MEMBER.getCode()), "error.dataNotFound");
            accountList.addAll(role.getAccounts());
        }else if (promotionRequest.getUserSelectOption().equals(UserSelectOption.PARTNER)){
            Role role = Utils.requireExists(roleRepository.findByCode(UserSelectOption.PARTNER.getCode()), "error.dataNotFound");
            accountList.addAll(role.getAccounts());
        }
        accountList = accountList.stream().filter(account -> {
            if (account.getActive() && account.getDeletedAt() == null){
                return true;
            }
            return false;
        }).collect(Collectors.toList());

        List<UserPromotion> userPromotionList = accountList.stream().map(account -> {
            UserPromotion userPromotion = new UserPromotion();
            userPromotion.setPromotion(promotion);
            userPromotion.setAccount(account);
            userPromotion.setUsageRemain(promotionRequest.getNumberOfUsage());
            return userPromotion;
        }).collect(Collectors.toList());
        userPromotionRepository.saveAll(userPromotionList);
        sendNotification(promotion.getId());
        return buildResponse(promotion, userPromotionList, promotionRequest.getUserSelectOption());
    }

    @Override
    public PromotionResponse update(Long aLong, PromotionRequest s) {
        if (!(userACL.isSuperAdmin() || userACL.isFixedAdmin() || userACL.isFlexibleAdmin())) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        if (Utils.truncateInstantDate(Instant.now()).compareTo(Utils.truncateInstantDate(s.getExpireTime())) >= 0) {
            throw new BadRequestException("error.expireTimeMustBeGreaterThanCurrentTime");
        }
        if (s.getNumberOfUsage() <= 0){
            throw new BadRequestException("error.numberOfUsageInvalid");
        }
        if (s.getPrice() <= 0){
            throw new BadRequestException("error.priceInvalid");
        }
        Promotion promotion = Utils.requireExists(promotionRepository.findByIdAndDeletedAtIsNull(aLong), "error.dataNotFound");
        if (promotion.getStatus().equals(PromotionStatus.EXPIRE.getStatus())){
            throw new BadRequestException("error.promotionExpired");
        }

        if (!s.getCode().equals(promotion.getCode())){
            Optional<Promotion> promotionOptional = promotionRepository.findByCodeAndDeletedAtIsNull(s.getCode());
            if (promotionOptional.isPresent()){
                throw new BadRequestException("error.promotionCodeExist");
            }
        }

        // update promotion
        BeanUtil.mergeProperties(s, promotion);
        promotion.setExpireTime(Utils.truncateInstantDate(s.getExpireTime()));
        // Get the list of accounts to apply the promo code
        List<Account> accountList = new ArrayList<>();
        if (s.getUserSelectOption().equals(UserSelectOption.OPTIONAL)){
            accountList = accountRepository.findAllByIdIn(s.getListUserIds());
        }else if (s.getUserSelectOption().equals(UserSelectOption.ALL)){
            accountList = accountRepository.findAllByActiveIsTrueAndDeletedAtIsNull();
        } else if (s.getUserSelectOption().equals(UserSelectOption.COLLABORATOR)) {
            Role role = Utils.requireExists(roleRepository.findByCode(UserSelectOption.COLLABORATOR.getCode()), "error.dataNotFound");
            accountList.addAll(role.getAccounts());
        }else if (s.getUserSelectOption().equals(UserSelectOption.MEMBER)){
            Role role = Utils.requireExists(roleRepository.findByCode(UserSelectOption.MEMBER.getCode()), "error.dataNotFound");
            accountList.addAll(role.getAccounts());
        }else if (s.getUserSelectOption().equals(UserSelectOption.PARTNER)){
            Role role = Utils.requireExists(roleRepository.findByCode(UserSelectOption.PARTNER.getCode()), "error.dataNotFound");
            accountList.addAll(role.getAccounts());
        }
        accountList = accountList.stream().filter(account -> {
            if (account.getActive() && account.getDeletedAt() == null){
                return true;
            }
            return false;
        }).collect(Collectors.toList());

        // update list user promotion
        List<UserPromotion> userPromotions = userPromotionRepository.findAllByPromotion(promotion);
        Set<Account> accountInPromotion = userPromotions.stream().map(UserPromotion::getAccount).collect(Collectors.toSet());
        List<UserPromotion> userPromotionList = accountList.stream().filter(account -> {
            if (accountInPromotion.contains(account)){
                return false;
            }
            return true;
        }).map(account -> {
            UserPromotion userPromotion = new UserPromotion();
            userPromotion.setPromotion(promotion);
            userPromotion.setAccount(account);
            userPromotion.setUsageRemain(s.getNumberOfUsage());
            return userPromotion;
        }).collect(Collectors.toList());
        userPromotionRepository.saveAll(userPromotionList);
        return buildResponse(promotion, userPromotionList, s.getUserSelectOption());
    }

    @Override
    public boolean deleteById(Long aLong) {
        if (!(userACL.isSuperAdmin() || userACL.isFixedAdmin() || userACL.isFlexibleAdmin())) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Promotion promotion = Utils.requireExists(promotionRepository.findById(aLong), "error.dataNotFound");
        promotionRepository.softDelete(promotion);
        return true;
    }

    private void sendNotification(Long promotionId){
        Promotion promotion = Utils.requireExists(promotionRepository.findById(promotionId), "error.dataNotFound");
        List<UserPromotion> userPromotionList = userPromotionRepository.findAllByPromotion(promotion);
        Map<String, Object> additionalData = new HashMap<>();
        additionalData.put("promotionId", promotion.getId());
        String messageContentPattern = "Khuyến mãi \"{0}\" đang diễn ra.";
        String messageContent = MessageFormat.format(messageContentPattern, promotion.getName());
        List<UserToken> userTokens = pushNotificationTokenService.getTokensByAccountIds(userPromotionList.stream().map(UserPromotion::getAccount).map(Account::getId).collect(Collectors.toList()));
        Set<Long> accountIds = userPromotionList.stream().map(UserPromotion::getAccount).filter(account -> {
            if (account.getDeletedAt() != null) {
                return true;
            }
            if (account.getActive()) return true;
            return false;
        }).map(Account::getId).collect(Collectors.toSet());

        //save notification
        if (CollectionUtils.isNotEmpty(accountIds)){
            List<NotificationRequest> notificationRequests = accountIds.stream().map(accountId -> NotificationRequest.builder()
                    .title("Khuyến mãi")
                    .content(messageContent)
                    .type(NotificationType.PROMOTION_STARTED.getType())
                    .data(additionalData)
                    .accountId(accountId)
                    .build()
            ).collect(Collectors.toList());
            notificationService.saveAll(notificationRequests);

            //send push notification
            sendPushNotification(notificationRequests.get(0), userTokens);
        }
    }

    private void sendPushNotification(NotificationRequest notificationRequest, List<UserToken> userTokens) {
        PushNotificationDto pushNotificationRequest = PushNotificationDto.builder()
                .title(notificationRequest.getTitle())
                .body(notificationRequest.getContent())
                .type(notificationRequest.getType())
                .data(notificationRequest.getData())
                .userTokens(userTokens)
                .build();
        notificationService.sendNotification(pushNotificationRequest);
    }

    @Scheduled(cron = "0 0 0 * * ?", zone = "Asia/Ho_Chi_Minh")
    public void expiredPromotion(){
        List<Promotion> promotions = promotionRepository.findAllByStatusAndExpireTimeBefore(PromotionStatus.ACTIVE.getStatus(), Instant.now());
        promotions.stream().forEach(promotion -> {
            promotion.setStatus(PromotionStatus.EXPIRE.getStatus());
            List<UserPromotion> userPromotionList = userPromotionRepository.findAllByPromotion(promotion);
            userPromotionList.forEach(userPromotion -> {
                userPromotion.setUsageRemain(0);
            });
            userPromotionRepository.saveAll(userPromotionList);
        });
        promotionRepository.saveAll(promotions);
    }

    public Page<PromotionResponse> getPromotionOfUser(PromotionFillter promotionFillter) {
        Long id = Utils.requireExists(authenticatedProvider.getUserId(), "error.dataNotFound");
        Account account = getById(id);
        Pageable pageable = baseFilterSpecs.page(promotionFillter);
        Page<Promotion> promotions = customPromotionRepository.findAllByUser(account, promotionFillter, pageable);
        return promotions.map(promotion -> BeanUtil.copyProperties(promotion, PromotionResponse.class));
    }

    protected Account getById(Long id) {
        return accountRepository.findOneActive(id)
                .orElseThrow(() -> new KlgResourceNotFoundException("User is not found with id [" + id + "]"));
    }

    public PromotionResponse reactivePromotion(long id, long userId) {
        if (!(userACL.isSuperAdmin() || userACL.isFixedAdmin() || userACL.isFlexibleAdmin())) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Account account = Utils.requireExists(accountRepository.findAllByIdAndActiveIsTrueAndDeletedAtNull(userId), "error.dataNotFound");
        Promotion promotion = Utils.requireExists(promotionRepository.findByIdAndDeletedAtIsNullAndStatus(id, PromotionStatus.ACTIVE.getStatus()), "error.dataNotFound");
        UserPromotion userPromotion = Utils.requireExists(userPromotionRepository.findByPromotionAndAccount(promotion, account), "error.dataNotFound");
        userPromotion.setUsageRemain(promotion.getNumberOfUsage());
        userPromotionRepository.save(userPromotion);
        return BeanUtil.copyProperties(promotion, PromotionResponse.class);
    }

    public Boolean deleteUserPromotion(long id, long userId) {
        if (!(userACL.isSuperAdmin() || userACL.isFixedAdmin() || userACL.isFlexibleAdmin())) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Account account = Utils.requireExists(accountRepository.findAllByIdAndActiveIsTrueAndDeletedAtNull(userId), "error.dataNotFound");
        Promotion promotion = Utils.requireExists(promotionRepository.findByIdAndDeletedAtIsNullAndStatus(id, PromotionStatus.ACTIVE.getStatus()), "error.dataNotFound");
        UserPromotion userPromotion = Utils.requireExists(userPromotionRepository.findByPromotionAndAccount(promotion, account), "error.dataNotFound");
        userPromotion.setUsageRemain(0);
        userPromotionRepository.save(userPromotion);
        return true;
    }

    public PromotionResponse findByCode(String code) {
        Optional<Promotion> promotionOptional = promotionRepository.findByCodeAndDeletedAtIsNull(code);
        PromotionResponse promotionResponse = null;
        if (promotionOptional.isPresent()) {
            List<UserPromotion> userPromotions = userPromotionRepository.findAllByPromotion(promotionOptional.get());
            promotionResponse = buildResponseDetail(promotionOptional.get(), userPromotions);
        }
        return promotionResponse;
    }

    public Page<AccountResponse> getAccounts(AccountFilter accountFilter) {
        Pageable pageable = baseFilterSpecs.page(accountFilter);

        Page<Account> page = customAccountRepository.getAllAccountsInActive(accountFilter, pageable);
        return page.map(account -> BeanUtil.copyProperties(account, AccountResponse.class));
    }

}
