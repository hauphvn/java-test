package com.kinglogi.service.impl;

import com.kinglogi.dto.filter.CarGroupFilter;
import com.kinglogi.dto.filter.ProviderServiceFilter;
import com.kinglogi.dto.filter.base.BaseFilter;
import com.kinglogi.dto.request.ProviderServiceRequest;
import com.kinglogi.dto.response.CarGroupResponse;
import com.kinglogi.dto.response.ProviderServiceResponse;
import com.kinglogi.dto.response.file_response.FileCarGroupResponse;
import com.kinglogi.dto.response.file_response.FileProviderServiceResponse;
import com.kinglogi.entity.CarGroup;
import com.kinglogi.entity.Commission;
import com.kinglogi.entity.File;
import com.kinglogi.entity.ProviderService;
import com.kinglogi.repository.FileRepository;
import com.kinglogi.repository.ProviderServiceRepository;
import com.kinglogi.service.ProviderServiceService;
import com.kinglogi.service.UserACL;
import com.kinglogi.service.error.AccessForbiddenException;
import com.kinglogi.service.error.BadRequestException;
import com.kinglogi.service.error.ResourceNotFoundException;
import com.kinglogi.service.utils.Utils;
import com.kinglogi.specs.BaseFilterSpecs;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProviderServiceServiceImpl implements ProviderServiceService {

    private final ProviderServiceRepository providerServiceRepository;

    private final UserACL userACL;

    private final FileRepository fileRepository;

    public String FILE_DIRECTORY = System.getProperty("user.dir") + java.io.File.separator + "storage/";;

    private final BaseFilterSpecs<ProviderService> baseFilterSpecs;

    public ProviderServiceServiceImpl(ProviderServiceRepository providerServiceRepository, UserACL userACL, FileRepository fileRepository, BaseFilterSpecs<ProviderService> baseFilterSpecs) {
        this.providerServiceRepository = providerServiceRepository;
        this.userACL = userACL;
        this.fileRepository = fileRepository;
        this.baseFilterSpecs = baseFilterSpecs;
    }

    @Override
    public Page<ProviderServiceResponse> findAll(BaseFilter baseFilter) {
        ProviderServiceFilter providerServiceFilter = (ProviderServiceFilter) baseFilter;
        Pageable pageable = baseFilterSpecs.page(providerServiceFilter);
        Specification<ProviderService> spec = baseFilterSpecs.search(providerServiceFilter);
        return providerServiceRepository.findAll(spec, pageable).map(ProviderServiceResponse::new);
    }

    @Override
    public ProviderServiceResponse findById(Long aLong) {
        ProviderService providerService = Utils.requireExists(providerServiceRepository.findById(aLong), "error.serviceNotFound");
        return new ProviderServiceResponse(providerService);
    }

    @Override
    public ProviderServiceResponse save(ProviderServiceRequest providerServiceRequest) {
        if (!userACL.isSuperAdmin()) {
            throw new BadRequestException("error.notAdmin");
        }
        if (StringUtils.isBlank(providerServiceRequest.getTitle())) {
            throw new BadRequestException("error.titleNotBlank");
        }
        if (StringUtils.isBlank(providerServiceRequest.getDescription())) {
            throw new BadRequestException("error.descriptionNotBlank");
        }
        ProviderService providerService = new ProviderService();
        providerService.setTitle(providerServiceRequest.getTitle().trim());
        providerService.setDescription(providerServiceRequest.getDescription().trim());
        providerServiceRepository.save(providerService);
        return new ProviderServiceResponse(providerService);
    }

    @Override
    public ProviderServiceResponse update(Long aLong, ProviderServiceRequest providerServiceRequest) {
        if (!userACL.isSuperAdmin()) {
            throw new BadRequestException("error.notAdmin");
        }
        ProviderService providerService = Utils.requireExists(providerServiceRepository.findById(aLong), "error.serviceNotFound");
        if (StringUtils.isBlank(providerServiceRequest.getTitle())) {
            throw new BadRequestException("error.titleNotBlank");
        }
        if (StringUtils.isBlank(providerServiceRequest.getDescription())) {
            throw new BadRequestException("error.descriptionNotBlank");
        }
        providerService.setTitle(providerServiceRequest.getTitle().trim());
        providerService.setDescription(providerServiceRequest.getDescription().trim());
        providerServiceRepository.save(providerService);
        return new ProviderServiceResponse(providerService);
    }

    @Override
    public boolean deleteById(Long aLong) {
        if (!userACL.isSuperAdmin()) {
            throw new BadRequestException("error.notAdmin");
        }
        ProviderService providerService = Utils.requireExists(providerServiceRepository.findById(aLong), "error.serviceNotFound");
        List<File> files = providerService.getFiles();
        for (File file : files) {
            java.io.File newFile = new java.io.File(file.getUrl());
            if (newFile.exists()) {
                newFile.delete();
            }
        }
        providerServiceRepository.delete(providerService);
        return true;
    }

    @Override
    public void uploadImage(Long id, List<MultipartFile> multipartFiles) {
        if (!userACL.isSuperAdmin()) {
            throw new BadRequestException("error.notAdmin");
        }
        ProviderService providerService = Utils.requireExists(providerServiceRepository.findById(id), "error.serviceNotFound");
        String filePath = FILE_DIRECTORY + "service/" + id;
        java.io.File fileDirectory = new java.io.File(filePath);
        if (!fileDirectory.exists()) {
            fileDirectory.mkdirs();
        }
        List<java.io.File> fileList = multipartFiles.stream().map(file -> {
            try {
                return convertMultiPartToFile(file, filePath);
            } catch (Exception e) {
                throw new BadRequestException(e.getMessage());
            }
        }).collect(Collectors.toList());
        List<File> files = new ArrayList<>();
        for (java.io.File newFile : fileList) {
            File file = new File();
            file.setName(newFile.getName());
            file.setUrl(newFile.getAbsolutePath());
            file.setMimeType(FilenameUtils.getExtension(newFile.getName()));
            files.add(file);
        }
        fileRepository.saveAll(files);
        providerService.setFiles(files);
        providerServiceRepository.save(providerService);
    }

    private java.io.File convertMultiPartToFile(MultipartFile file, String filePath) throws IOException {
        String fileNameWithoutExtension = FilenameUtils.removeExtension(file.getOriginalFilename());
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());
        java.io.File convFile = new java.io.File(filePath, fileNameWithoutExtension + "." + extension);
        int num = 0;
        while (convFile.exists()) {
            convFile = new java.io.File(filePath, fileNameWithoutExtension + "(" + (++num) + ")." + extension);
        }
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write( file.getBytes() );
        fos.close();
        return convFile;
    }

    @Override
    public InputStreamResource viewImage(Long id, String fileName) throws FileNotFoundException {
        ProviderService providerService = Utils.requireExists(providerServiceRepository.findById(id), "error.serviceNotFound");
        File file = fileRepository.findByNameAndServiceId(id, fileName).orElseThrow(() -> new ResourceNotFoundException("error.fileNotFound"));
        java.io.File newFile = new java.io.File(file.getUrl());
        FileInputStream fileInputStream = new FileInputStream(newFile);
        return new InputStreamResource(fileInputStream);
    }

    @Override
    public void deleteImage(Long id, String fileName) {
        if (!userACL.isSuperAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        ProviderService providerService = Utils.requireExists(providerServiceRepository.findById(id), "error.serviceNotFound");
        File file = fileRepository.findByNameAndServiceId(id, fileName).orElseThrow(() -> new ResourceNotFoundException("error.fileNotFound"));
        java.io.File newFile = new java.io.File(file.getUrl());
        if (newFile.exists()) {
            newFile.delete();
        }
        fileRepository.delete(file);
    }
}
