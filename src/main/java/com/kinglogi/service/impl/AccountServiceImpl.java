package com.kinglogi.service.impl;

import com.kinglogi.constant.LoyaltyActionType;
import com.kinglogi.dto.AuthUserDetails;
import com.kinglogi.dto.collaborator_group.CollaboratorGroupResponse;
import com.kinglogi.dto.commission.CommissionOutputDTO;
import com.kinglogi.dto.filter.AccountFilter;
import com.kinglogi.dto.filter.base.BaseFilter;
import com.kinglogi.dto.loyalty_point_history.LoyaltyPointHistoryInputDTO;
import com.kinglogi.dto.request.AccountRequest;
import com.kinglogi.dto.response.AccountResponse;
import com.kinglogi.entity.*;
import com.kinglogi.exception.KlgResourceNotFoundException;
import com.kinglogi.repository.AccountOtpRepository;
import com.kinglogi.repository.PushNotificationTokenRepository;
import com.kinglogi.repository.custom.CustomAccountRepository;
import com.kinglogi.repository.custom.CustomRoleRepository;
import com.kinglogi.service.AccountHistoryService;
import com.kinglogi.service.AccountService;
import com.kinglogi.service.LoyaltyPointHistoryService;
import com.kinglogi.service.error.BadRequestException;
import com.kinglogi.service.error.ResourceNotFoundException;
import com.kinglogi.service.utils.Utils;
import com.kinglogi.specs.BaseFilterSpecs;
import com.kinglogi.utils.BeanUtil;
import com.kinglogi.utils.EncrytedPasswordUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Qualifier("accountService")
@Transactional
public class AccountServiceImpl implements AccountService {

    protected CustomAccountRepository accountRepository;

    protected CustomRoleRepository roleRepository;

    protected BaseFilterSpecs<Account> baseFilterSpecs;

    private final LoyaltyPointHistoryService loyaltyPointHistoryService;

    private final AccountHistoryService accountHistoryService;

    @Autowired
    private PushNotificationTokenRepository pushNotificationTokenRepository;

    @Autowired
    private AccountOtpRepository accountOtpRepository;

    @Autowired
    private UserDetailsService userDetailsService;


    @Autowired
    public AccountServiceImpl(@Qualifier("customAccountRepository") CustomAccountRepository accountRepository,
                              @Qualifier("customRoleRepository") CustomRoleRepository roleRepository,
                              BaseFilterSpecs<Account> baseFilterSpecs,
                              LoyaltyPointHistoryService loyaltyPointHistoryService,
                              AccountHistoryService accountHistoryService) {
        this.accountRepository = accountRepository;
        this.roleRepository = roleRepository;
        this.baseFilterSpecs = baseFilterSpecs;
        this.loyaltyPointHistoryService = loyaltyPointHistoryService;
        this.accountHistoryService = accountHistoryService;
    }

    @Override
    public Page<AccountResponse> findAll(BaseFilter baseFilter) {
    	AccountFilter accountFilter = (AccountFilter) baseFilter;
        Pageable pageable = baseFilterSpecs.page(accountFilter);
        Specification<Account> spec = baseFilterSpecs.search(accountFilter);

        Page<Account> page = accountRepository.getAllAccounts(accountFilter, pageable);
        return page.map(this::convertToResponse);
    }

    private CollaboratorGroupResponse buildCollaboratorGroupResponse(CollaboratorGroup collaboratorGroup) {
        CollaboratorGroupResponse response = BeanUtil.copyProperties(collaboratorGroup, CollaboratorGroupResponse.class);
        response.setName(collaboratorGroup.getName());
        return response;
    }

    @Override
    public AccountResponse findById(Long id) {
        Account account = getById(id);
        return convertToResponse(account);
    }

    protected Account getById(Long id) {
        return accountRepository.findOneActive(id)
                .orElseThrow(() -> new ResourceNotFoundException("User is not found with id [" + id + "]"));
    }

    @Override
    public AccountResponse save(AccountRequest userRequest) {
        if (userRequest.getPhoneNumber() == null || StringUtils.isBlank(userRequest.getPhoneNumber())) {
            throw new BadRequestException("error.phoneNumberEmptyOrBlank");
        }
        if (accountRepository.findByUsername(userRequest.getUsername().trim()).isPresent()) {
            throw new BadRequestException("error.userNameExisted");
        }
        if (accountRepository.findByPhoneNumber(userRequest.getPhoneNumber().trim()).isPresent()) {
            throw new BadRequestException("error.phoneNumberExisted");
        }
        Account user = BeanUtil.copyProperties(userRequest, Account.class);
        user.setEmail(user.getUsername());
        user.setPassword(EncrytedPasswordUtils.encryptPassword(userRequest.getPassword()));
        user.setRoles(this.getRoles(userRequest.getRoles()));
        if (user.getAllowNotification() == null) {
            user.setAllowNotification(true);
        }
        Account savedUser = accountRepository.save(user);
        return BeanUtil.copyProperties(savedUser, AccountResponse.class);
    }

    private Set<Role> getRoles(List<String> roleCodes) {
        return Optional.ofNullable(roleCodes)
                .orElseGet(ArrayList::new)
                .stream().map(roleRepository::findByCode)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());
    }

    @Override
    public AccountResponse update(Long id, AccountRequest userRequest) {
        Account user = getById(id);

        if (userRequest.getEmail() != null && !userRequest.getEmail().equals(user.getEmail())){
            userRequest.setUsername(userRequest.getEmail());
        }else if (userRequest.getUsername() != null &&  !userRequest.getUsername().equals(user.getUsername())){
            userRequest.setEmail(userRequest.getUsername());
        }

        Optional<Account> accountOptionalUserName = accountRepository.findByUsername(userRequest.getUsername());
        if (accountOptionalUserName.isPresent() && !accountOptionalUserName.get().getId().equals(user.getId())) {
            throw new BadRequestException("error.userNameExisted");
        }
        Optional<Account> accountOptionalPhoneNumber = accountRepository.findByPhoneNumber(userRequest.getPhoneNumber());
        if (accountOptionalPhoneNumber.isPresent() && !accountOptionalPhoneNumber.get().getId().equals(user.getId())) {
            throw new BadRequestException("error.phoneNumberExisted");
        }

        Optional<Account> accountOptionalEmail = accountRepository.findAccountByEmail(userRequest.getEmail());
        if (accountOptionalEmail.isPresent() && !accountOptionalEmail.get().getId().equals(user.getId())) {
            throw new BadRequestException("error.emailExisted");
        }

        // Record the value before changing
        AccountResponse oldAccountResponse = BeanUtil.copyProperties(user, AccountResponse.class);
        oldAccountResponse.setRoles(Utils.getRoleCodes(user.getRoles()));

        // Loyalty Point save
        if (userRequest.getLoyaltyPointActionType() != null) { // If update loyalty point for user
            LoyaltyPointHistoryInputDTO input = new LoyaltyPointHistoryInputDTO();

            if (userRequest.getLoyaltyPointActionType() == 0) { // plus
                int amount = (user.getLoyaltyPoint() != null ? user.getLoyaltyPoint() : 0) + userRequest.getLoyaltyPointActionAmount();
                user.setLoyaltyPoint(amount);
                input.setActionType(LoyaltyActionType.PLUS);
                input.setAmount(userRequest.getLoyaltyPointActionAmount());
            } else if (userRequest.getLoyaltyPointActionType() == 1) { // minus
                int amount = (user.getLoyaltyPoint() != null ? user.getLoyaltyPoint() : 0) - userRequest.getLoyaltyPointActionAmount();
                user.setLoyaltyPoint(amount);
                user.setLoyaltyPoint(Math.max(amount, 0));
                input.setActionType(LoyaltyActionType.MINUS);
                input.setAmount(userRequest.getLoyaltyPointActionAmount());
            }

            input.setAccount(user);
            // save loyalty point history
            this.loyaltyPointHistoryService.save(input);
        }

        BeanUtil.mergeProperties(userRequest, user);
        user.setRoles(this.getRoles(userRequest.getRoles()));
        if (StringUtils.isNotBlank(userRequest.getPassword())) {
            user.setPassword(EncrytedPasswordUtils.encryptPassword(userRequest.getPassword()));
        }
        Account savedUser = accountRepository.save(user);

        // log change data of user
        AccountResponse newAccountResponse = BeanUtil.copyProperties(user, AccountResponse.class);
        newAccountResponse.setRoles(Utils.getRoleCodes(user.getRoles()));
        accountHistoryService.changeDataOfUser(oldAccountResponse, newAccountResponse);

        return convertToResponse(savedUser);
    }

    @Override
    public boolean deleteById(Long id) {
        try {
            Account account = Utils.requireExists(accountRepository.findById(id), "error.notFound");
            account.setDeletedAt(Date.from(Instant.now()));
            long timestamp = Instant.now().toEpochMilli();
            account.setUsername("deleteAt_"+ timestamp);
            account.setPhoneNumber("deleteAt_"+ timestamp);
            account.setEmail("deleteAt_"+ timestamp);
            accountRepository.save(account);
            pushNotificationTokenRepository.deleteByAccountId(account);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }

    private boolean isFieldValueExists(Object value) {
        if (value == null || StringUtils.isBlank(value.toString())) {
            return false;
        }
        return accountRepository.findByUsername(value.toString().trim()).isPresent();
    }

    @Override
    public boolean isEntityExists(Long id, Map<String, Object> uniqueValues) {
        final String uniqueFieldName = "username";
        if (uniqueValues == null || uniqueValues.isEmpty() || uniqueValues.get(uniqueFieldName) == null) {
            return false;
        }
        String username = uniqueValues.get(uniqueFieldName).toString();
        if(id == null) {
            return isFieldValueExists(username);
        }
        return accountRepository.findByUsername(username)
                .map(Account::getId)
                .map(existsId -> existsId.longValue() != id.longValue())
                .orElse(Boolean.FALSE);
    }

    @Override
    public boolean isFieldValueExists(String fieldName, Object value) {
        if (value == null || StringUtils.isBlank(value.toString())) {
            return false;
        }
        return accountRepository.findByUsername(value.toString().trim()).isPresent();
    }

    private Set<String> getRoleCodes(Set<Role> roles) {
    	return roles.stream()
    			.map(Role::getCode)
    			.collect(Collectors.toSet());
    }

    private AccountResponse convertToResponse(Account account) {
    	AccountResponse res = BeanUtil.copyProperties(account, AccountResponse.class);
        res.setRoles(getRoleCodes(account.getRoles()));
        if (account.getCollaboratorGroup() != null) {
            res.setCollaboratorGroup(buildCollaboratorGroupResponse(account.getCollaboratorGroup()));
        }
        if (account.getCommission() != null) {
            res.setCommissionOutputDTO(new CommissionOutputDTO(account.getCommission()));
        }
        Integer loyaltyPoint = res.getLoyaltyPoint() != null ? res.getLoyaltyPoint() : 0;
        res.setRankPoint(Utils.classifyRank(loyaltyPoint));
        return res;
    }

    @Override
    public AccountResponse findByPushToken(String pushToken) {
        Account account = accountRepository.findByPushToken(pushToken)
                .orElseThrow(() -> new KlgResourceNotFoundException("User is not found with push token [" + pushToken + "]"));
        return convertToResponse(account);
    }

    @Override
    public void updateAvatar(Long accountId, String avatar) {
        Account account = getById(accountId);
        account.setAvatar(avatar);
        accountRepository.save(account);
    }


    public Optional<Account> isUsernameExisted(String username) {
        return accountRepository.findByUsername(username);
    }

    @Override
    public Optional<Account> isPhoneNumberExisted(String phoneNumber) {
        return accountRepository.findByPhoneNumber(phoneNumber);
    }

    @Override
    public Account getByUserNameOrPhone(String username) {
        return accountRepository.findByUsernameOrPhoneNumber(username)
            .orElseThrow(() -> new ResourceNotFoundException("User is not found with username [" + username + "]"));
    }

    @Override
    public AccountResponse updatePhoneNumber(AccountRequest accountRequest, String otp) {
        if (accountRequest.getPhoneNumber() == null || StringUtils.isBlank(accountRequest.getPhoneNumber())) {
            throw new BadRequestException("error.phoneNumberEmptyOrBlank");
        }
        if (isPhoneNumberExisted(accountRequest.getPhoneNumber().trim()).isPresent()) {
            throw new BadRequestException("error.phoneNumberExisted");
        }
        Account account = getById(accountRequest.getId());
        account.setPhoneNumber(accountRequest.getPhoneNumber().trim());
        Account savedUser = accountRepository.save(account);

        Optional<AccountOtp> accountOtp = accountOtpRepository.findByOtpAndPhoneNumber(otp, accountRequest.getPhoneNumber(), Instant.now());
        if (accountOtp.isPresent()){
            account.setPhoneNumber(accountRequest.getPhoneNumber());
            accountRepository.save(account);

            UserDetails user = userDetailsService.loadUserByUsername(account.getUsername());
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                    user,
                    null,
                    user.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            accountOtpRepository.deleteAllByPhoneNumber(accountRequest.getPhoneNumber());

            return convertToResponse(savedUser);
        }else {
            throw new BadRequestException("error.otpInvalid");
        }
    }


    @Override
    public Account getByPhoneNumber(String phoneNumber) {
        return accountRepository.findByPhoneNumber(phoneNumber).orElseThrow(() -> new ResourceNotFoundException("error.phoneNumberNotExist"));
    }
}
