package com.kinglogi.service.impl;

import com.kinglogi.constant.*;
import com.kinglogi.dto.request.*;
import com.kinglogi.dto.response.*;
import com.kinglogi.entity.Account;
import com.kinglogi.entity.AppClient;
import com.kinglogi.entity.Order;
import com.kinglogi.exception.KlgAuthenticationException;
import com.kinglogi.exception.KlgResourceNotFoundException;
import com.kinglogi.exception.ValidatingException;
import com.kinglogi.provider.AuthenticatedProvider;
import com.kinglogi.repository.AppClientRepository;
import com.kinglogi.repository.OrderRepository;
import com.kinglogi.service.OrderWithPaymentService;
import com.kinglogi.service.PartnerOrderService;
import com.kinglogi.service.TripService;
import com.kinglogi.service.error.BadRequestException;
import com.kinglogi.utils.BeanUtil;
import com.kinglogi.vnpay.constant.VnPayResponseCode;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PartnerOrderServiceImpl implements PartnerOrderService {

    private final OrderWithPaymentService orderWithPaymentService;
    private final AuthenticatedProvider authenticatedProvider;
    private final AppClientRepository appClientRepository;
    private final TripService tripService;
    private final OrderRepository orderRepository;

    @Override
    public PartnerOrderResponse createOrder(PartnerOrderRequest request) {
        OrderRequest orderRequest = BeanUtil.copyProperties(request, OrderRequest.class);
        orderRequest.setAccountId(getAccountIdIfRequestValid(request));
        // Nếu yêu cầu là loại PartnerFixedTripOrderRequest
        if (request instanceof PartnerFixedTripOrderRequest) {
            PartnerFixedTripOrderRequest fixedTripOrderRequest = (PartnerFixedTripOrderRequest) request;
            // Kiểm tra kiểu thanh toán là VNpay và url trả về của thanh toán có rỗng không
            if (PaymentType.VNPAY.name().equals(fixedTripOrderRequest.getPaymentType()) && StringUtils.isBlank(fixedTripOrderRequest.getPaymentCallbackUrl())) {
                throw new ValidatingException("paymentCallbackUrl không được phép rỗng");
            }
            try {
                // Lưu thông tin tuyến
                TripResponse tripResponse = tripService.findById(fixedTripOrderRequest.getTripId());
                orderRequest.setBrand(tripResponse.getBrand());
                orderRequest.setDistanceInKm(tripResponse.getDistanceInKm());
            } catch(KlgResourceNotFoundException e) {
                throw new ValidatingException("Id tuyến không tồn tại");
            }
        }
        orderRequest.setSendMail(request.isSendEmail());
        orderRequest.setPaymentType(request.getPaymentType() != null ? request.getPaymentType().name() : null);
        return BeanUtil.copyProperties(orderWithPaymentService.createOrderForPartner(orderRequest), PartnerOrderResponse.class);
    }

    private Long getAccountIdIfRequestValid(PartnerOrderRequest request) {
        // Kiểm tra yêu câu là loại PartnerFlexibleTripOrderRequestFromIframe
        if (request instanceof PartnerFlexibleTripOrderRequestFromIframe) {
            // Create order via iFrame
            // Tìm optAppClient của yêu cầu
            PartnerFlexibleTripOrderRequestFromIframe iframeRequest = (PartnerFlexibleTripOrderRequestFromIframe) request;
            Optional<AppClient> optAppClient = appClientRepository.findById(iframeRequest.getAppClientId());
            // Nếu optAppClient không tồn tại thì ném ngoại lệ
            if (!optAppClient.isPresent()
                    || !StringUtils.equals(optAppClient.get().getSecretKey(), iframeRequest.getAppClientSecret())) {
                throw new KlgAuthenticationException("Key không hợp lệ, vui lòng kiểm tra lại");
            }
            Account account = optAppClient.get().getPartner().getAccount();
            // Nếu account không hoạt động thì ném ra ngoại lệ
            if (BooleanUtils.isNotTrue(account.getActive())) {
                throw new KlgAuthenticationException("Tài khoản đăng nhập không hợp lệ");
            }
            return account.getId();
        }

        Optional<UserCredential> optUser = authenticatedProvider.getUser();
        // Kiểm tra người đó có phải đối tác không
        if (optUser.isPresent() && optUser.get().getRoles().contains(Role.PARTNER.getCode())) {
            // Create order via API
            return optUser.get().getId();
        }

        throw new KlgAuthenticationException("Bạn không có quyền truy cập hệ thống");
    }

    // Xác thực thanh toán
    @Override
    public PartnerPaymentResponse verifyPayment(Map<String, Object> allParams) {
        PaymentResponse paymentResponse = orderWithPaymentService.verifyPayment(allParams);
        PartnerPaymentResponse partnerPaymentResponse = BeanUtil.copyProperties(paymentResponse, PartnerPaymentResponse.class);
        partnerPaymentResponse.setOrderCode(paymentResponse.getTxnRef());
        // Thanh toán thành công
        if (VnPayResponseCode.SUCCESS.getCode().equals(paymentResponse.getResponseCode())) {
            partnerPaymentResponse.setStatus(ResponseStatus.SUCCESS.getStatus());
            partnerPaymentResponse.setMessage("Thanh toán thành công");
            // Thanh toán thất bại
        } else {
            partnerPaymentResponse.setStatus(ResponseStatus.ERROR.getStatus());
            partnerPaymentResponse.setMessage("Thanh toán không thành công");
        }
        return partnerPaymentResponse;
    }

    @Override
    public OrderResponse updateOrder(PartnerOrderUpdateRequest input) {
        Optional<AppClient> optAppClient = appClientRepository.findById(input.getAppClientId());
        // Nếu optAppClient không tồn tại thì ném ngoại lệ
        if (!optAppClient.isPresent()
                || !AppClientType.PARTNER_WEB.getType().equals(optAppClient.get().getType())
                || !StringUtils.equals(optAppClient.get().getSecretKey(), input.getAppClientSecret())) {
            throw new KlgAuthenticationException("Key không hợp lệ, vui lòng kiểm tra lại");
        }
        Account account = optAppClient.get().getPartner().getAccount();
        Set<String> roles = account.getRoles().stream().map(com.kinglogi.entity.Role::getCode).collect(Collectors.toSet());
        if (!roles.contains(Role.PARTNER.getCode())){
            throw new KlgAuthenticationException("Bạn không phải đối tác");
        }
        Order order = orderRepository.getOrderByOrderCode(input.getOrderCode()).orElseThrow(() -> new BadRequestException("error.orderNotFound"));
        Long accountId = account.getId();
        if (order.getAccount() == null || !order.getAccount().getId().equals(accountId)){
            throw new KlgAuthenticationException("Đơn hàng này không phải của bạn");
        }
        if (input.getStatus() != null && !order.getStatus().equals(input.getStatus().getStatus())) {
            if (order.getStatus().equals(OrderStatus.FINISH.getStatus())) {
                throw new BadRequestException("Đơn hàng đã hoàn thành");
            } else if (order.getStatus().equals(OrderStatus.CANCELLED.getStatus())) {
                throw new BadRequestException("Đơn hàng đã bị huỷ");
            } else if (order.getStatus().equals(OrderStatus.WAITING_FOR_PAY_IN.getStatus()) || order.getStatus().equals(OrderStatus.PAID.getStatus())) {
                if (input.getStatus().equals(OrderStatus.WAITING_FOR_PAY_IN) || input.getStatus().equals(OrderStatus.PAID) || input.getStatus().equals(OrderStatus.CANCELLED)) {
                    order.setStatus(input.getStatus().getStatus());
                }
            } else {
                throw new BadRequestException("Bạn không thể cập nhật trạng thái của đơn hàng này");
            }
        }

        if(input.getPrice() != null){
            order.setEndPrice(input.getPrice());
        }

        if (input.getPaymentType() != null){
            order.setPaymentType(input.getPaymentType().name());
        }

        orderRepository.save(order);

        return BeanUtil.copyProperties(order, OrderResponse.class);
    }

    @Override
    public Page<OrderResponse> getOrders(Long appClientId, String appClientSecret, Pageable pageable) {
        Optional<AppClient> optAppClient = appClientRepository.findById(appClientId);
        // Nếu optAppClient không tồn tại thì ném ngoại lệ
        if (!optAppClient.isPresent()
                || !AppClientType.PARTNER_WEB.getType().equals(optAppClient.get().getType())
                || !StringUtils.equals(optAppClient.get().getSecretKey(), appClientSecret)) {
            throw new KlgAuthenticationException("Key không hợp lệ, vui lòng kiểm tra lại");
        }
        Account account = optAppClient.get().getPartner().getAccount();
        Set<String> roles = account.getRoles().stream().map(com.kinglogi.entity.Role::getCode).collect(Collectors.toSet());
        if (!roles.contains(Role.PARTNER.getCode())){
            throw new KlgAuthenticationException("Bạn không phải đối tác");
        }
        Page<Order> orders = orderRepository.findAllByAccountId(account.getId(), pageable);
        return orders.map(order -> BeanUtil.copyProperties(order, OrderResponse.class));
    }

    @Override
    public OrderResponse getOrder(Long appClientId, String appClientSecret, String orderCode) {
        Optional<AppClient> optAppClient = appClientRepository.findById(appClientId);
        // Nếu optAppClient không tồn tại thì ném ngoại lệ
        if (!optAppClient.isPresent()
                || !AppClientType.PARTNER_WEB.getType().equals(optAppClient.get().getType())
                || !StringUtils.equals(optAppClient.get().getSecretKey(), appClientSecret)) {
            throw new KlgAuthenticationException("Key không hợp lệ, vui lòng kiểm tra lại");
        }
        Account account = optAppClient.get().getPartner().getAccount();
        Set<String> roles = account.getRoles().stream().map(com.kinglogi.entity.Role::getCode).collect(Collectors.toSet());
        if (!roles.contains(Role.PARTNER.getCode())){
            throw new KlgAuthenticationException("Bạn không phải đối tác");
        }
        Order order = orderRepository.getOrderByOrderCode(orderCode).orElseThrow(() -> new BadRequestException("error.orderNotFound"));
        Long accountId = account.getId();
        if (order.getAccount() == null || !order.getAccount().getId().equals(accountId)){
            throw new KlgAuthenticationException("Đơn hàng này không phải của bạn");
        }
        return BeanUtil.copyProperties(order, OrderResponse.class);
    }

}
