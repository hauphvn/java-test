package com.kinglogi.service.impl;

import com.kinglogi.constant.AppClientType;
import com.kinglogi.dto.request.AppClientRequest;
import com.kinglogi.dto.response.AppClientResponse;
import com.kinglogi.entity.AppClient;
import com.kinglogi.repository.AppClientRepository;
import com.kinglogi.service.AppClientService;
import com.kinglogi.utils.BeanUtil;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class AppClientServiceImpl implements AppClientService {

    private static final int SECRET_KEY_LENGTH = 32;

    private final AppClientRepository appClientRepository;

    @Autowired
    public AppClientServiceImpl(AppClientRepository appClientRepository) {
        this.appClientRepository = appClientRepository;
    }

    @Override
    public AppClientResponse save(AppClientRequest request) {
        AppClient appClient = buildAppClient(Objects.requireNonNull(AppClientType.getByType(request.getType()).orElseGet(() -> null)));
        AppClient requestAppClient = BeanUtil.copyProperties(request, AppClient.class);
        BeanUtil.mergeProperties(requestAppClient, appClient);
        return BeanUtil.copyProperties(appClientRepository.save(appClient), AppClientResponse.class);
    }

    @Override
    public List<AppClientResponse> saveAll(List<AppClient> appClients) {
        return appClients.stream()
                .map(appClientRepository::save)
                .map(appClient -> BeanUtil.copyProperties(appClient, AppClientResponse.class))
                .collect(Collectors.toList());
    }

    @Override
    public AppClient create(AppClientType type) {
        return buildAppClient(type);
    }

    private AppClient buildAppClient(AppClientType type) {
        AppClient appClient = new AppClient();
        appClient.setActive(true);
        appClient.setType(type.getType());
        appClient.setSecretKey(RandomStringUtils.randomAlphanumeric(SECRET_KEY_LENGTH));

        switch (type) {
            case PARTNER_IFRAME:
                appClient.setName("IFRAME");
                appClient.setDescription("Sử dụng khi cần nhúng web King Logi trong IFRAME");
                break;
            case PARTNER_WEB:
                appClient.setName("PARTNER API");
                appClient.setDescription("Sử dụng để chứng thực đối tác gọi API của King Logi");
                break;
            case SYS_MOBILE:
                appClient.setName("MOBILE API");
                appClient.setDescription("Sử dụng để chứng thực ứng dụng MOBILE của King Logi.");
                break;
            case SYS_WEB:
                appClient.setName("WEB API");
                appClient.setDescription("Sử dụng để chứng thực ứng dụng WEB của King Logi.");
                break;
            default:
                break;
        }
        return appClient;
    }
}
