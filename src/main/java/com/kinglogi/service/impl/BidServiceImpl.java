package com.kinglogi.service.impl;

import com.kinglogi.constant.*;
import com.kinglogi.dto.UserToken;
import com.kinglogi.dto.bid.BidAuctionCreateInputDTO;
import com.kinglogi.dto.request.AuctionRequest;
import com.kinglogi.dto.request.PushNotificationDto;
import com.kinglogi.dto.request.NotificationRequest;
import com.kinglogi.dto.request.TransactionRequest;
import com.kinglogi.dto.response.AuctionResponse;
import com.kinglogi.dto.response.BidResponse;
import com.kinglogi.dto.response.UserCredential;
import com.kinglogi.constant.NotificationType;
import com.kinglogi.dto.mail.MailInfo;
import com.kinglogi.dto.mail.MailTemplateConfig;
import com.kinglogi.dto.request.*;
import com.kinglogi.dto.response.*;
import com.kinglogi.entity.*;
import com.kinglogi.exception.TransactionException;
import com.kinglogi.provider.AuthenticatedProvider;
import com.kinglogi.repository.AccountRepository;
import com.kinglogi.repository.AuctionRepository;
import com.kinglogi.repository.BidRepository;
import com.kinglogi.repository.OrderRepository;
import com.kinglogi.service.*;
import com.kinglogi.service.error.AccessForbiddenException;
import com.kinglogi.service.error.BadRequestException;
import com.kinglogi.service.utils.Utils;
import com.kinglogi.utils.BeanUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
@Slf4j
public class BidServiceImpl implements BidService {

    private final AuthenticatedProvider authenticatedProvider;

    private final BidRepository bidRepository;

    private final TransactionService transactionService;

    private final AuctionService auctionService;

    private final NotificationService notificationService;

    private final PushNotificationTokenService pushNotificationTokenService;

    private final EmailTemplateService emailTemplateService;

    private final AccountRepository accountRepository;

    private final EmailService emailService;

    private final OrderRepository orderRepository;

    private final AuctionRepository auctionRepository;

    private final ReportService reportService;

    private final OtpService otpService;

    @Autowired
    public BidServiceImpl(AuthenticatedProvider authenticatedProvider,
                          BidRepository bidRepository, TransactionService transactionService,
                          AuctionService auctionService, NotificationService notificationService,
                          PushNotificationTokenService pushNotificationTokenService, EmailTemplateService emailTemplateService, AccountRepository accountRepository,
                          EmailService emailService,
                          OrderRepository orderRepository,
                          AuctionRepository auctionRepository, ReportService reportService, OtpService otpService) {
        this.authenticatedProvider = authenticatedProvider;
        this.bidRepository = bidRepository;
        this.transactionService = transactionService;
        this.auctionService = auctionService;
        this.notificationService = notificationService;
        this.pushNotificationTokenService = pushNotificationTokenService;
        this.emailTemplateService = emailTemplateService;
        this.accountRepository = accountRepository;
        this.emailService = emailService;
        this.orderRepository = orderRepository;
        this.auctionRepository = auctionRepository;
        this.reportService = reportService;
        this.otpService = otpService;
    }

    @Override
    @Transactional
    public BidResponse bid(BidAuctionCreateInputDTO input) {
        UserCredential userCredential = authenticatedProvider.getUser().get();
        Long userId = userCredential.getId();
        Long balance = transactionService.getBalanceByAccountId(userId);
        Account account = Utils.requireExists(accountRepository.findById(userId), "error.userNotFound");
        if (account.getMinBalanceCollaborator() != null) {
            if (balance < account.getMinBalanceCollaborator()) {
                throw new BadRequestException("error.collaboratorBalanceHasNotReachedTheMinimumBalance");
            }
        }
        AuctionResponse auctionResponse = auctionService.findById(input.getAuctionId());
        final Long startedAuctionUserId = auctionResponse.getUpdatedBy();

        AuctionRequest auctionRequest = BeanUtil.copyProperties(auctionResponse, AuctionRequest.class);
        if (auctionRequest.getStatus() == null ||
                AuctionStatus.START.getStatus().intValue() != auctionRequest.getStatus().intValue()) {
            throw new TransactionException("Cuộc đấu giá đã kết thúc.");
        }

        if (!auctionResponse.getType().equals(input.getType())) {
            throw new BadRequestException("Đấu giá không đúng kiểu.");
        }

        if (auctionResponse.getEndAt() != null && new Date().after(auctionResponse.getEndAt())) {
            throw new TransactionException("Cuộc đấu giá đã kết thúc.");
        }

        if (input.getType().equals(AuctionType.BUY)) {
            boolean sendSms = false;
            List<Order> listOrderSendSms = new ArrayList<>();

            if (auctionResponse.getTotalPrice() > balance) {
                throw new TransactionException("Số dư trong tài khoản không đủ để tham gia đấu giá này.");
            }

            // Create Bid
            Bid bid = saveBid(userId, auctionRequest.getId(), auctionResponse.getTotalPrice());

            // Create Transaction
            performWithDrawTransaction(userId, auctionRequest);

            // Update Auction status
            auctionRequest.setStatus(AuctionStatus.FINISH.getStatus());
            auctionResponse = auctionService.update(input.getAuctionId(), auctionRequest);

            sendNotification(userCredential, auctionResponse, startedAuctionUserId);
            sendEmailEndAuction(auctionResponse, startedAuctionUserId, userId);

            // Add collaboratorId of winning bidder to order
            Auction auction = Utils.requireExists(auctionRepository.findById(input.getAuctionId()), "error.dataNotFound");
            if (!auction.getOrders().isEmpty()) {
                List<Order> orderListToSave = new ArrayList<>();
                for (Order order: auction.getOrders()) {
                    order.setCollaboratorId(userId);
                    order.setStatus(OrderStatus.READY_FOR_MOVING.getStatus());
                    orderListToSave.add(order);
                    listOrderSendSms.add(order);
                    sendSms = true;
                }

                orderRepository.saveAll(orderListToSave);
            }

            if (sendSms){
                for (Order orderSendSms : listOrderSendSms) {
                    Account collaborator = accountRepository.getOne(orderSendSms.getCollaboratorId());
                    Map<String, String> map = new HashMap<>();
                    map.put("orderCode", orderSendSms.getOrderCode());
                    if (collaborator.getFullName() != null && !collaborator.getFullName().isEmpty()){
                        map.put("nameDriver", collaborator.getFullName());
                    }else {
                        map.put("nameDriver", "N/A");
                    }
                    if (collaborator.getPhoneNumber() != null && !collaborator.getPhoneNumber().isEmpty()){
                        map.put("phoneNumber", collaborator.getPhoneNumber());
                    }else {
                        map.put("phoneNumber", "N/A");
                    }
                    Car car = collaborator.getCars().stream().filter(c -> c.getNumberOfSeat().equals(orderSendSms.getNumberOfSeat())).findFirst().orElse(null);
                    if (car != null && car.getBrand() != null && car.getBrand() != null && car.getLicensePlates() != null){
                        map.put("licensePlates", car.getBrand() + " " + car.getLicensePlates());
                    }else {
                        map.put("licensePlates", "N/A");
                    }
                    try {
                        otpService.sendSms(orderSendSms.getPhoneNumber(), TypeSms.INFORMATION_DRIVER, map);
                    } catch (IOException e) {
                        log.error(e.getMessage());
                    }
                }
            }

            return BeanUtil.copyProperties(bid, BidResponse.class);
        } else {
            if (input.getBidPrice() > auctionResponse.getTotalPrice()) {
                throw new TransactionException("Giá đặt không được cao hơn gía khởi điểm.");
            }

            if (input.getBidPrice() > balance) {
                throw new TransactionException("Số dư trong tài khoản không đủ để tham gia đấu giá này.");
            }

            // Check bậc giá 10k, 20k, ...
            if (input.getBidPrice() % 10000 > 0) {
                throw new TransactionException("Bậc giá không hợp lệ.");
            }

            /// Create Bid
            Bid bid = saveBidReserve(userId, auctionRequest.getId(), input.getBidPrice());

            // send notification
            sendNotification(userCredential, auctionResponse, startedAuctionUserId);

            // Create Transaction
            performWithDrawTransactionReserve(userId, auctionRequest, input.getBidPrice());

            return BeanUtil.copyProperties(bid, BidResponse.class);
        }
    }

    @Override
    @Transactional
    public void cancelBidReserve(Long bidId) {
        Bid bid = Utils.requireExists(bidRepository.findById(bidId), "error.dataNotFound");
        UserCredential userCredential = Utils.requireExists(authenticatedProvider.getUser(), "error.userNotFound");
        Long userId = userCredential.getId();
        AuctionResponse auctionResponse = auctionService.findById(bid.getAuction().getId());
        AuctionRequest auctionRequest = BeanUtil.copyProperties(auctionResponse, AuctionRequest.class);

        if (!Objects.equals(bid.getAccount().getId(), userId)) {
            throw new AccessForbiddenException("error.bidNotBelongToUser");
        }

        Long bidPrice = bid.getBidPrice();

        bidRepository.delete(bid);

        performCancelTransactionBid(userId, auctionRequest, bidPrice);
    }

    private void sendEmailEndAuction(AuctionResponse auctionResponse, Long startedAuctionUserId, Long userBidId) {
        Account account = Utils.requireExists(accountRepository.findById(startedAuctionUserId), "error.dataNotFound");
        Account accountBid = Utils.requireExists(accountRepository.findById(userBidId), "error.dataNotFound");
        AuctionMail accountRequest = AuctionMail.builder()
                .name(auctionResponse.getName())
                .bidUserName(accountBid.getFullName())
                .build();

        EmailTemplateResponse emailTemplateResponse = emailTemplateService.findByTypeAndLanguage(EmailTemplateType.FINISH_AUCTION.name(), "vi");
        MailTemplateConfig config = MailTemplateConfig.builder()
                .mailContent(emailTemplateResponse.getContent())
                .mailSubject(emailTemplateResponse.getTitle())
                .mailIsHtml(true)
                .build();

        MailInfo mailInfo = new MailInfo();
        mailInfo.addTo(account.getEmail());
        emailService.sendAsync(mailInfo, config, accountRequest);
    }

    private void sendNotification(UserCredential userCredential, AuctionResponse auctionResponse, Long startedAuctionUserId) {
        Map<String, Object> additionalData = new HashMap<>();
        additionalData.put("auctionId", auctionResponse.getId());
        String name = userCredential.getFullName() != null ? userCredential.getFullName() : userCredential.getUsername();

        String messageContentPattern = "CTV [{0}] đã đấu giá cho [{1}]";
        String messageContent = MessageFormat.format(messageContentPattern, name, auctionResponse.getName());

        NotificationRequest notificationRequest = NotificationRequest.builder()
                .title("Đấu giá")
                .content(messageContent)
                .type(NotificationType.JOIN_A_BID.getType())
                .data(additionalData)
                .accountId(startedAuctionUserId) // Only send notification to a user who started a bid
                .build();

        notificationService.save(notificationRequest);
        senPushNotification(notificationRequest);
    }

    private void senPushNotification(NotificationRequest notificationRequest) {
        List<UserToken> userTokens = pushNotificationTokenService.getTokensByAccountId(notificationRequest.getAccountId());
        if(CollectionUtils.isNotEmpty(userTokens)) {
            PushNotificationDto pushNotificationRequest = PushNotificationDto.builder()
                    .title(notificationRequest.getTitle())
                    .body(notificationRequest.getContent())
                    .type(notificationRequest.getType())
                    .data(notificationRequest.getData())
                    .userTokens(userTokens)
                    .build();
            notificationService.sendNotification(pushNotificationRequest);
        }
    }

    private Bid saveBid(Long accountId, Long auctionId, Long totalPrice) {
        Account account = new Account();
        account.setId(accountId);

        Auction auction = new Auction();
        auction.setId(auctionId);

        Bid bid = new Bid();
        bid.setAccount(account);
        bid.setAuction(auction);
        bid.setBidPrice(totalPrice);
        bid.setStatus(BidStatus.SUCCESS);
        bid = bidRepository.save(bid);
        // save report
        reportService.save(account, null, bid, ReportType.BID, ReportActivityType.WINNER);

        return bid;
    }

    private Bid saveBidReserve(Long accountId, Long auctionId, Long bidPrice) {
        Account account = new Account();
        account.setId(accountId);

        Auction auction = new Auction();
        auction.setId(auctionId);

        Bid bid = new Bid();
        bid.setAccount(account);
        bid.setAuction(auction);
        bid.setStatus(BidStatus.PENDING); // need to check in schedule to valid this
        bid.setBidPrice(bidPrice);

        // save report
        reportService.save(account, null, bid, ReportType.BID, ReportActivityType.BID);

        return bidRepository.save(bid);
    }

    private void performWithDrawTransaction(Long userId, AuctionRequest auctionRequest) {
        TransactionRequest transactionRequest = new TransactionRequest();
        transactionRequest.setTargetId(userId);
        transactionRequest.setAmount((-1) * auctionRequest.getTotalPrice());
        transactionRequest.setTransactionType("BALANCE");
        Date transactionAt = Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant());
        transactionRequest.setTransactionAt(transactionAt);
        transactionRequest.setContent("Thanh toán đấu giá " + auctionRequest.getName() + " lúc " +
                LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm")));
        transactionService.save(transactionRequest);
    }

    private void performWithDrawTransactionReserve(Long userId, AuctionRequest auctionRequest, Long bidPrice) {
        TransactionRequest transactionRequest = new TransactionRequest();
        transactionRequest.setTargetId(userId);
        transactionRequest.setTransactionType("BALANCE");
        transactionRequest.setAmount((-1) * bidPrice);
        Date transactionAt = Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant());
        transactionRequest.setTransactionAt(transactionAt);
        transactionRequest.setContent("Thanh toán đấu giá " + auctionRequest.getName() + " lúc " +
            LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm")));
        transactionService.save(transactionRequest);
    }

    private void performCancelTransactionBid(Long userId, AuctionRequest auctionRequest, Long bidPrice) {
        TransactionRequest transactionRequest = new TransactionRequest();
        transactionRequest.setTargetId(userId);
        transactionRequest.setAmount(bidPrice);
        transactionRequest.setTransactionType("BALANCE");
        Date transactionAt = Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant());
        transactionRequest.setTransactionAt(transactionAt);
        transactionRequest.setContent("Hủy đặt đấu giá " + auctionRequest.getName() + " lúc " +
            LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm")));
        transactionService.save(transactionRequest);
    }
}
