package com.kinglogi.service.impl;

import com.kinglogi.constant.NotificationType;
import com.kinglogi.constant.TransactionReason;
import com.kinglogi.dto.UserToken;
import com.kinglogi.dto.request.NotificationRequest;
import com.kinglogi.dto.request.PushNotificationDto;
import com.kinglogi.dto.request.TransactionRequest;
import com.kinglogi.dto.response.TransactionResponse;
import com.kinglogi.entity.Account;
import com.kinglogi.entity.Order;
import com.kinglogi.entity.Transaction;
import com.kinglogi.exception.ValidatingException;
import com.kinglogi.repository.AccountRepository;
import com.kinglogi.repository.TransactionRepository;
import com.kinglogi.service.NotificationService;
import com.kinglogi.service.PushNotificationTokenService;
import com.kinglogi.service.TransactionService;
import com.kinglogi.service.error.ResourceNotFoundException;
import com.kinglogi.service.utils.Utils;
import com.kinglogi.utils.BeanUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class TransactionServiceImpl implements TransactionService {

    private final TransactionRepository transactionRepository;

    private final PushNotificationTokenService pushNotificationTokenService;

    private final NotificationService notificationService;

    private final AccountRepository accountRepository;

    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository, PushNotificationTokenService pushNotificationTokenService, NotificationService notificationService, AccountRepository accountRepository) {
        this.transactionRepository = transactionRepository;
        this.pushNotificationTokenService = pushNotificationTokenService;
        this.notificationService = notificationService;
        this.accountRepository = accountRepository;
    }

    @Override
    public List<TransactionResponse> getTransactions(Long targetId, Integer transactionType) {
        if (TransactionReason.PAY_IN.getType().equals(transactionType)) {
            return findByAccountId(targetId);
        } else if (TransactionReason.PAYMENT_ORDER.getType().equals(transactionType)) {
            return findByOrderId(targetId);
        }else if (transactionType == null){
            List<TransactionResponse> result = findByAccountId(targetId);
            return result.stream().map(transactionResponse -> {
                Account account = Utils.requireExists(accountRepository.findById(transactionResponse.getCreatedBy()), "error.dataNotFound");
                transactionResponse.setModifyBy(account.getFullName());
                return transactionResponse;
            }).collect(Collectors.toList());
        }
        throw new ValidatingException("Invalid transaction type");
    }

    private List<TransactionResponse> findByOrderId(Long id) {
        return BeanUtil.listCopyProperties(transactionRepository.findByOrderIdOrderByTransactionAtDesc(id), TransactionResponse.class);
    }

    @Override
    public List<TransactionResponse> findByAccountId(Long id) {
        return BeanUtil.listCopyProperties(transactionRepository.findByAccountIdOrderByTransactionAtDesc(id), TransactionResponse.class);
    }

    // Lấy số dư
    @Override
    public Long getBalanceByAccountId(Long id) {
        // Sắp xếp theo thời gian giảm dần, loại bỏ các phần tử null
        // Cộng tổng các giao dịch thì ra số dư
        return transactionRepository.findByAccountIdOrderByTransactionAtDesc(id)
                .parallelStream()
                .map(Transaction::getAmount)
                .filter(Objects::nonNull)
                .reduce(0l, Long::sum);
    }

    @Override
    public  TransactionResponse save(TransactionRequest request) {
        // Lưu thông tin giao dịch nạp tiền
        if (TransactionReason.PAY_IN.getType().equals(request.getTransactionReason())) {
            Transaction transaction = BeanUtil.copyProperties(request, Transaction.class);
            // Lưu thông tin account vào thông tin giao dịch
            if (request.getTargetId() != null) {
                Account account = new Account();
                account.setId(request.getTargetId());
                transaction.setAccount(account);

                // Gửi thông báo
                sendNotification(account.getId(), transaction.getAmount());
            }
            Transaction savedTransaction = transactionRepository.save(transaction);
            return BeanUtil.copyProperties(savedTransaction, TransactionResponse.class);
            // Lưu thông tin giao dịch cho việc đặt chuyến
        } else if (TransactionReason.PAYMENT_ORDER.getType().equals(request.getTransactionReason())) {
            Transaction transaction = BeanUtil.copyProperties(request, Transaction.class);
            //Tạo thông tin chuyến vào thông tin giao dịch
            if (request.getTargetId() != null) {
                Order order = new Order();
                order.setId(request.getTargetId());

                Set<Order> orders = new HashSet<>();
                orders.add(order);
                transaction.setOrders(orders);
            }

            Transaction savedTransaction = transactionRepository.save(transaction);
            return BeanUtil.copyProperties(savedTransaction, TransactionResponse.class);
        }
        throw new ValidatingException("Invalid transaction type");
    }

    protected Transaction getById(Long id) {
        return transactionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User is not found with id [" + id + "]"));
    }

    @Override
    public TransactionResponse getDetail(Long id) {
        // Lấy thông tin giao dịch theo id
        Transaction transaction = getById(id);
        return BeanUtil.copyProperties(transaction, TransactionResponse.class);
    }
    // Gửi thông báo giao dịch
    public void sendNotification(Long accountId, Long amount){
        Map<String, Object> additionalData = new HashMap<>();
        additionalData.put("accountId", accountId);
        String messageContentPattern = "Thời gian {0} \nGiao dịch: {1} VND";
        String messageContent = "";
        if (amount > 0){
            messageContent = MessageFormat.format(messageContentPattern, Instant.now().atZone(ZoneId.of("Asia/Ho_Chi_Minh")).format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")), "+" + amount);
        }else {
            messageContent = MessageFormat.format(messageContentPattern, Instant.now().atZone(ZoneId.of("Asia/Ho_Chi_Minh")).format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")), amount);
        }

        List<UserToken> userTokens = pushNotificationTokenService.getTokensByAccountIds(Collections.singletonList(accountId));
        List<Account> accountList = Collections.singletonList(Utils.requireExists(accountRepository.findById(accountId), "error.dataNotFound"));
        Set<Long> accountIds = accountList.stream().filter(account -> {
            if (account.getDeletedAt() != null) {
                return true;
            }
            if (account.getActive()) return true;
            return false;
        }).map(Account::getId).collect(Collectors.toSet());

        //save notification
        if (CollectionUtils.isNotEmpty(accountIds)){
            String finalMessageContent = messageContent;
            List<NotificationRequest> notificationRequests = accountIds.stream().map(account -> NotificationRequest.builder()
                    .title("Giao dịch")
                    .content(finalMessageContent)
                    .type(NotificationType.NEW_TRANSACTION.getType())
                    .data(additionalData)
                    .accountId(account)
                    .build()
            ).collect(Collectors.toList());
            notificationService.saveAll(notificationRequests);

            //send push notification
            sendPushNotification(notificationRequests.get(0), userTokens);
        }
    }

    private void sendPushNotification(NotificationRequest notificationRequest, List<UserToken> userTokens) {
        PushNotificationDto pushNotificationRequest = PushNotificationDto.builder()
                .title(notificationRequest.getTitle())
                .body(notificationRequest.getContent())
                .type(notificationRequest.getType())
                .data(notificationRequest.getData())
                .userTokens(userTokens)
                .build();
        notificationService.sendNotification(pushNotificationRequest);
    }
}
