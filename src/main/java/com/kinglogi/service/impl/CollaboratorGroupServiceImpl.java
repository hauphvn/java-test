package com.kinglogi.service.impl;

import com.kinglogi.dto.collaborator_group.CollaboratorGroupRequest;
import com.kinglogi.dto.collaborator_group.CollaboratorGroupResponse;
import com.kinglogi.dto.commission.CommissionOutputDTO;
import com.kinglogi.dto.filter.CollaboratorGroupFilter;
import com.kinglogi.dto.filter.base.BaseFilter;
import com.kinglogi.dto.response.AccountResponse;
import com.kinglogi.entity.Account;
import com.kinglogi.entity.CollaboratorGroup;
import com.kinglogi.entity.Role;
import com.kinglogi.provider.AuthenticatedProvider;
import com.kinglogi.repository.AccountRepository;
import com.kinglogi.repository.CollaboratorGroupRepository;
import com.kinglogi.service.CollaboratorGroupService;
import com.kinglogi.service.UserACL;
import com.kinglogi.service.error.AccessForbiddenException;
import com.kinglogi.service.error.BadRequestException;
import com.kinglogi.service.utils.Utils;
import com.kinglogi.specs.BaseFilterSpecs;
import com.kinglogi.utils.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Qualifier("collaboratorGroupService")
@Transactional
public class CollaboratorGroupServiceImpl implements CollaboratorGroupService {
    private final UserACL userACL;
    private final CollaboratorGroupRepository collaboratorGroupRepository;
    private final BaseFilterSpecs<CollaboratorGroup> baseFilterSpecs;
    private final AccountRepository accountRepository;
    private final AuthenticatedProvider authenticatedProvider;

    @Autowired
    public CollaboratorGroupServiceImpl(UserACL userACL, CollaboratorGroupRepository collaboratorGroupRepository, BaseFilterSpecs<CollaboratorGroup> baseFilterSpecs,
                                        AccountRepository accountRepository, AuthenticatedProvider authenticatedProvider) {
        this.userACL = userACL;
        this.collaboratorGroupRepository = collaboratorGroupRepository;
        this.baseFilterSpecs = baseFilterSpecs;
        this.accountRepository = accountRepository;
        this.authenticatedProvider = authenticatedProvider;
    }

    private CollaboratorGroupResponse buildResponse(CollaboratorGroup collaboratorGroup) {
        CollaboratorGroupResponse response = BeanUtil.copyProperties(collaboratorGroup, CollaboratorGroupResponse.class);
        response.setName(collaboratorGroup.getName());
        return response;
    }

    private AccountResponse buildAccountResponse(Account account) {
        AccountResponse res = BeanUtil.copyProperties(account, AccountResponse.class);
        res.setRoles(getRoleCodes(account.getRoles()));
        if (account.getCollaboratorGroup() != null) {
            res.setCollaboratorGroup(buildResponse(account.getCollaboratorGroup()));
        }
        if (account.getCommission() != null) {
            res.setCommissionOutputDTO(new CommissionOutputDTO(account.getCommission()));
        }
        return res;
    }

    private Set<String> getRoleCodes(Set<Role> roles) {
        return roles.stream()
            .map(Role::getCode)
            .collect(Collectors.toSet());
    }

    @Override
    public Page<CollaboratorGroupResponse> findAll(BaseFilter baseFilter) {
        if (!(userACL.isSuperAdmin() || userACL.isFixedAdmin() || userACL.isFlexibleAdmin())) {
            throw new AccessForbiddenException("error.notAdmin");
        }

        CollaboratorGroupFilter collaboratorGroupFilter = (CollaboratorGroupFilter) baseFilter;
        Pageable pageable = baseFilterSpecs.page(baseFilter);
        Specification<CollaboratorGroup> spec = baseFilterSpecs.search(collaboratorGroupFilter);

        Page<CollaboratorGroup> page = collaboratorGroupRepository.findAll(spec, pageable);
        return page.map(this::buildResponse);
    }

    @Override
    public CollaboratorGroupResponse findById(Long aLong) {
        if (!(userACL.isSuperAdmin() || userACL.isFixedAdmin() || userACL.isFlexibleAdmin())) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        CollaboratorGroup result = Utils.requireExists(collaboratorGroupRepository.findById(aLong), "error.dataNotFound");
        return buildResponse(result);
    }

    @Override
    public CollaboratorGroupResponse save(CollaboratorGroupRequest collaboratorGroupRequest) {
        if (!(userACL.isSuperAdmin() || userACL.isFixedAdmin() || userACL.isFlexibleAdmin())) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        if (collaboratorGroupRequest.getName() == null || collaboratorGroupRequest.getName().isEmpty() || Utils.isAllSpaces(collaboratorGroupRequest.getName())) {
            throw new BadRequestException("error.nameEmptyOrBlank");
        }

        boolean collaboratorGroupNameExist = collaboratorGroupRepository.findExistByName(collaboratorGroupRequest.getName());

        if (collaboratorGroupNameExist) {
            throw new BadRequestException("error.collaboratorNameExisted");
        }

        List<Account> listUser = accountRepository.findAllByIdIn(collaboratorGroupRequest.getListUserId());
        List<Account> listUserToSave = new ArrayList<>();

        CollaboratorGroup collaboratorGroup = new CollaboratorGroup();
        collaboratorGroup.setName(collaboratorGroupRequest.getName());
        collaboratorGroupRepository.save(collaboratorGroup);
        for (Account account: listUser) {
            // If account is not Collaborator role
            Set<String> roles = getRoleCodes(account.getRoles());
            if (!roles.contains(com.kinglogi.constant.Role.COLLABORATOR.getCode())) {
                continue;
            }
            // If account already in another collaborator group, will skipp
            if (account.getCollaboratorGroup() != null && !account.getCollaboratorGroup().getId().equals(collaboratorGroup.getId())) {
                continue;
            }
            account.setCollaboratorGroup(collaboratorGroup);
            listUserToSave.add(account);
        }
        accountRepository.saveAll(listUserToSave);

        return buildResponse(collaboratorGroup);
    }

    @Override
    public CollaboratorGroupResponse update(Long aLong, CollaboratorGroupRequest collaboratorGroupRequest) {
        if (!(userACL.isSuperAdmin() || userACL.isFixedAdmin() || userACL.isFlexibleAdmin())) {
            throw new AccessForbiddenException("error.notAdmin");
        }

        CollaboratorGroup collaboratorGroup = Utils.requireExists(collaboratorGroupRepository.findById(aLong), "error.dataNotFound");

        if (collaboratorGroupRequest.getName() == null || collaboratorGroupRequest.getName().isEmpty() || Utils.isAllSpaces(collaboratorGroupRequest.getName())) {
            throw new BadRequestException("error.nameEmptyOrBlank");
        }

        if (!collaboratorGroup.getName().equals(collaboratorGroupRequest.getName())) {
            boolean collaboratorGroupNameExist = collaboratorGroupRepository.findExistByName(collaboratorGroupRequest.getName());
            if (collaboratorGroupNameExist) {
                throw new BadRequestException("error.collaboratorNameExisted");
            }
            collaboratorGroup.setName(collaboratorGroupRequest.getName());
        }
        collaboratorGroupRepository.save(collaboratorGroup);

        List<Account> listUser = accountRepository.findAllByIdIn(collaboratorGroupRequest.getListUserId());
        List<Account> listUserInGroup = accountRepository.findAllByCollaboratorGroupId(collaboratorGroup.getId());
        List<Account> listUserToSave = new ArrayList<>();
        for (Account account: listUser) {
            // If account is not Collaborator role
            Set<String> roles = getRoleCodes(account.getRoles());
            if (!roles.contains(com.kinglogi.constant.Role.COLLABORATOR.getCode())) {
                continue;
            }

            // If account already in another collaborator group, will skipp
            if (account.getCollaboratorGroup() != null && !account.getCollaboratorGroup().getId().equals(collaboratorGroup.getId())) {
                continue;
            }

            account.setCollaboratorGroup(collaboratorGroup);
            listUserToSave.add(account);
        }

        for (Account userInGroup: listUserInGroup) {
            // If user in group but not exist in list input => Will remove
            if (!collaboratorGroupRequest.getListUserId().contains(userInGroup.getId())) {
                userInGroup.setCollaboratorGroup(null);
                listUserToSave.add(userInGroup);
            }
        }

        accountRepository.saveAll(listUserToSave);

        return buildResponse(collaboratorGroup);
    }

    @Override
    public boolean deleteById(Long aLong) {
        if (!(userACL.isSuperAdmin() || userACL.isFixedAdmin() || userACL.isFlexibleAdmin())) {
            throw new AccessForbiddenException("error.notAdmin");
        }

        CollaboratorGroup collaboratorGroup = Utils.requireExists(collaboratorGroupRepository.findById(aLong), "error.dataNotFound");
        List<Account> listUserInGroup = accountRepository.findAllByCollaboratorGroupId(collaboratorGroup.getId());
        List<Account> listUserToSave = new ArrayList<>();
        if (!listUserInGroup.isEmpty()) {
            for (Account userInGroup: listUserInGroup) {
                // If user in group but not exist in list input => Will remove
                userInGroup.setCollaboratorGroup(null);
                listUserToSave.add(userInGroup);
            }

            accountRepository.saveAll(listUserToSave);
        }

        collaboratorGroupRepository.delete(collaboratorGroup);
        return true;
    }

    public Page<AccountResponse> getCollaboratorInGroup(Long groupId, BaseFilter baseFilter) {
        Pageable pageable = baseFilterSpecs.page(baseFilter);
        Page<Account> accountPage = accountRepository.findAllByCollaboratorGroupId(groupId, pageable, baseFilter.getKeyword());
        return accountPage.map(this::buildAccountResponse);
    }

    public Page<AccountResponse> getCollaboratorNotInGroup(BaseFilter baseFilter) {
        Pageable pageable = baseFilterSpecs.page(baseFilter);
        Page<Account> accountPage = accountRepository.findAllByCollaboratorGroupNull(pageable, baseFilter.getKeyword());
        return accountPage.map(this::buildAccountResponse);
    }

        public void removeCollaboratorFromGroup(Long accountId, Long collaboratorGroupId) {
        if (!(userACL.isSuperAdmin() || userACL.isFixedAdmin() || userACL.isFlexibleAdmin())) {
            throw new AccessForbiddenException("error.notAdmin");
        }

        Account account = Utils.requireExists(accountRepository.findById(accountId), "error.userNotFound");
        CollaboratorGroup collaboratorGroup = Utils.requireExists(collaboratorGroupRepository.findById(collaboratorGroupId), "error.userCollaboratorGroupNotFound");
        if (account.getCollaboratorGroup() == null || !account.getCollaboratorGroup().getId().equals(collaboratorGroup.getId())) {
            throw new AccessForbiddenException("error.collaboratorNotInGroup");
        }

        account.setCollaboratorGroup(null);
        accountRepository.save(account);
    }
}
