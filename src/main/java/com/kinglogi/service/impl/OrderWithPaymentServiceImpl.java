package com.kinglogi.service.impl;

import com.kinglogi.constant.*;
import com.kinglogi.converter.VnPayMoneyConverter;
import com.kinglogi.dto.request.OrderRequest;
import com.kinglogi.dto.request.PaymentRequest;
import com.kinglogi.dto.request.TransactionRequest;
import com.kinglogi.dto.response.*;
import com.kinglogi.entity.*;
import com.kinglogi.exception.KlgAuthenticationException;
import com.kinglogi.exception.KlgResourceNotFoundException;
import com.kinglogi.provider.AuthenticatedProvider;
import com.kinglogi.provider.OrderCodeGenerator;
import com.kinglogi.repository.*;
import com.kinglogi.service.OrderService;
import com.kinglogi.service.OrderWithPaymentService;
import com.kinglogi.service.TransactionService;
import com.kinglogi.service.VnPayService;
import com.kinglogi.service.error.AuthenticationException;
import com.kinglogi.service.error.BadRequestException;
import com.kinglogi.service.utils.Utils;
import com.kinglogi.utils.BeanUtil;
import com.kinglogi.utils.JsonUtils;
import com.kinglogi.vnpay.VnPayRequest;
import com.kinglogi.vnpay.VnPaySetting;
import com.kinglogi.vnpay.constant.VnPayResponseCode;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
@RequiredArgsConstructor
@Transactional
public class OrderWithPaymentServiceImpl implements OrderWithPaymentService {

    private final AuthenticatedProvider authenticatedProvider;

    private final OrderCodeGenerator orderCodeGenerator;

    private final VnPayService vnPayService;

    private final OrderService orderService;

    private final TransactionService transactionService;

    private final VnPaySetting vnPaySetting;

    private final VnPayTransactionRepository vnPayTransactionRepository;

    private final AccountRepository accountRepository;

    private final PromotionRepository promotionRepository;

    private final UserPromotionRepository userPromotionRepository;

    private final TransactionServiceImpl transactionServiceImpl;

    private final InventoryRepository inventoryRepository;

    private final TripRepository tripRepository;

    @Override
    public OrderWithPaymentResponse createOrder(OrderRequest orderRequest) {
        OrderWithPaymentResponse orderResponse = new OrderWithPaymentResponse();
        if (orderRequest.getAccountId() == null) {
            authenticatedProvider.getUserId().ifPresent(orderRequest::setAccountId);
        }

        // Kiểm tra có trong inventory, nếu có thì lưu TotalPrice bằng giá của inventory
        if (orderRequest.getInventoryId() != null) {
            Inventory inventory = Utils.requireExists(inventoryRepository.findById(orderRequest.getInventoryId()), "error.inventoryNotFound");
            orderRequest.setTotalPrice(inventory.getPrice());
        }

        // Kiểm tra trip có tồn tại hay không
        if (orderRequest.getTripId() != null){
            tripRepository.findById(orderRequest.getTripId()).ifPresent(trip -> {
                if (trip.getDeletedAt() != null){
                    throw new KlgResourceNotFoundException("error.notFoundTrip");
                }
            });
        }

        orderRequest.setOrderCode(orderCodeGenerator.generate());
        orderRequest.setStatus(OrderStatus.WAITING_FOR_PAY_IN.getStatus());
        // Nếu totalprice trống thì lưu nó là 0
        if (orderRequest.getTotalPrice() == null) {
            orderRequest.setTotalPrice(0L);
        }
        orderRequest.setEndPrice(orderRequest.getTotalPrice());

        // order use promotion
        // Kiểm tra xem người dùng hiện tại được sử dụng promotion hay không và cập nhật promotion sau khi sử dụng và giá đơn hàng sau khi sử dụng promotion
        if (orderRequest.getPromotionCode() != null){
            if (orderRequest.getAccountId() != null){
                Account account = Utils.requireExists(accountRepository.findAllByIdAndActiveIsTrueAndDeletedAtNull(orderRequest.getAccountId()), "error.dataNotFound");
                Promotion promotion = Utils.requireExists(promotionRepository.findByCodeAndDeletedAtIsNullAndStatus(orderRequest.getPromotionCode(), PromotionStatus.ACTIVE.getStatus()), "error.dataNotFound");
                UserPromotion userPromotion = Utils.requireExists(userPromotionRepository.findByPromotionAndAccount(promotion, account), "error.dataNotFound");
                // Kiểm tra giảm giá hết hạn chưa
                if (userPromotion.getUsageRemain() == 0){
                    orderResponse.setStatus(ResponseStatus.ERROR.getStatus());
                    orderResponse.setCode(OrderPhase.PAY_IN.getPhase());
                    orderResponse.setMessage("Khuyến mãi đã hết lượt sử dụng");
                    return orderResponse;
                }
                // Cập nhật promotion
                userPromotion.setUsageRemain(userPromotion.getUsageRemain() - 1);
                userPromotionRepository.save(userPromotion);
                orderRequest.setUserPromotionId(userPromotion.getId());
                // Cập nhật giá sau khi áp dụng promotion
                orderRequest.setEndPrice(orderRequest.getEndPrice() - promotion.getPrice());
            }
        }

        if (orderRequest.getEndPrice() < 0){
            orderRequest.setEndPrice(0L);
        }
        // Lấy kiểu thanh toán mặc định là thanh toán trực tiếp
        PaymentType paymentType = PaymentType.getPaymentType(orderRequest.getPaymentType()).orElse(PaymentType.DIRECT);
        orderRequest.setPaymentType(paymentType.name());
        // Thực hiện thao tác thanh toán bằng số dư
        if (paymentType == PaymentType.BALANCE) {
            if (orderRequest.getAccountId() == null) {
                throw new KlgAuthenticationException("Can not create an order with payment type is balance without logged in.");
            }
            // Lấy số dư của người dùng
            Long balance = transactionService.getBalanceByAccountId(orderRequest.getAccountId());
            // Kiểm tra số dư có đủ để thanh toán không
            if (orderRequest.getEndPrice() > balance) {
                orderResponse.setStatus(ResponseStatus.ERROR.getStatus());
                orderResponse.setCode(OrderPhase.PAY_IN.getPhase());
                orderResponse.setMessage("Số dư trong tài khoản không đủ để thực hiện giao dịch.");
                return orderResponse;
            }
            // Trừ tiền trong tài khoản
            TransactionResponse transactionResponse = performWithDrawTransaction(orderRequest);
            orderRequest.setTransactionId(transactionResponse.getId());
            orderRequest.setStatus(OrderStatus.PAID.getStatus());

            orderResponse.setStatus(ResponseStatus.SUCCESS.getStatus());
            orderResponse.setCode(OrderPhase.FINISH.getPhase());
            orderResponse.setMessage("Đã tạo đơn hàng thành công.");

            // Thực hiện thao tác thanh toán bằng VNPay
        } else if (paymentType == PaymentType.VNPAY) {
            PaymentRequest paymentRequest = buildPaymentRequest(orderRequest);
            VnPayRequest vnPayRequest = vnPayService.buildVnPayRequest(paymentRequest);
            PaymentUrlResponse paymentUrlResponse = vnPayService.generateVnPayRequestUrl(vnPayRequest);
            orderResponse.setPaymentInfo(paymentUrlResponse);
            orderResponse.setStatus(ResponseStatus.SUCCESS.getStatus());
            orderResponse.setCode(OrderPhase.REDIRECT_TO_PAYMENT.getPhase());
            orderResponse.setMessage("Đã tạo đơn hàng thành công. Chuyển sang thanh toán.");
            // Thanh toán trực tiếp
        } else {
            orderResponse.setStatus(ResponseStatus.SUCCESS.getStatus());
            orderResponse.setCode(OrderPhase.FINISH.getPhase());
            orderResponse.setMessage("Đã tạo đơn hàng thành công.");
        }

        OrderResponse savedOrderResponse = orderService.save(orderRequest);
        orderResponse.setOrderCode(savedOrderResponse.getOrderCode());
        orderResponse.setDiscount(savedOrderResponse.getDiscount());
        return orderResponse;
    }

    // Tạo request tới VNPay
    private PaymentRequest buildPaymentRequest(OrderRequest orderRequest) {
        String[] contents = {
                "Dat xe King Logi",
                LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm"))
        };
        String orderInfo = StringUtils.join(contents, " _ ");

        String txnRef = orderRequest.getOrderCode();

        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setContent(orderInfo);
        // Lưu giá
        paymentRequest.setAmount(orderRequest.getEndPrice() != null ? orderRequest.getEndPrice() : orderRequest.getTotalPrice());
        paymentRequest.setTxnRef(txnRef);
        paymentRequest.setReturnUrl(StringUtils.defaultIfBlank(orderRequest.getPaymentCallbackUrl(), MessageFormat.format(vnPaySetting.getReturnUrl(), txnRef)));
        return paymentRequest;
    }

    // Tiến hàng trừ tiền trong tài khoản người dùng
    private TransactionResponse performWithDrawTransaction(OrderRequest orderRequest) {
        TransactionRequest transactionRequest = new TransactionRequest();
        transactionRequest.setTransactionType(orderRequest.getPaymentType());
        transactionRequest.setTargetId(orderRequest.getAccountId());
        // Trừ tiền
        transactionRequest.setAmount((-1) * orderRequest.getEndPrice());
        Date transactionAt = Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant());
        transactionRequest.setTransactionAt(transactionAt);
        transactionRequest.setContent("Đặt xe King Logi " + "[" + orderRequest.getOrderCode() + "] " +
                LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm")));
        transactionRequest.setTransactionReason(0);
        return transactionService.save(transactionRequest);
    }

    @Override
    public PaymentResponse verifyPayment(Map<String, Object> allParams) {
        if (!allParams.containsKey("vnp_TxnRef")) {
            throw new BadRequestException("error.failRequestParam");
        }
        String code = allParams.get("vnp_TxnRef").toString();
        PaymentResponse paymentResponse = new PaymentResponse();
        OrderResponse orderResponse = orderService.getOrderByOrderCode(code);
        // Kiểm tra đã thanh toán trước đó chưa
        if (!Objects.equals(orderResponse.getStatus(), OrderStatus.WAITING_FOR_PAY_IN.getStatus())) {
            paymentResponse.setMessage("Đã thanh toán trước đó");
            return paymentResponse;
        }
        paymentResponse = vnPayService.verifyPayment(allParams);
        // KIểm tra thanh toán thành công
        if (VnPayResponseCode.SUCCESS.getCode().equals(paymentResponse.getResponseCode())) {

            OrderRequest orderRequest = BeanUtil.copyProperties(orderResponse, OrderRequest.class);
            orderRequest.setStatus(OrderStatus.PAID.getStatus());
            orderService.update(orderResponse.getId(), orderRequest);

            TransactionResponse transactionResponse = performPayInTransaction(paymentResponse, orderResponse.getId());

            saveVnPayTransactionHistory(orderResponse.getId(), paymentResponse);
        }
        return paymentResponse;
    }

    @Override
    public OrderWithPaymentResponse paymentForOrder(String orderCode) {
        Optional<Long> accountId = authenticatedProvider.getUserId();
        if (!accountId.isPresent()) {
            throw new AuthenticationException("error.notAuthorization");
        }
        OrderResponse orderResponse = orderService.getOrderByOrderCode(orderCode);
        OrderWithPaymentResponse paymentResponse = new OrderWithPaymentResponse();
        // Nếu không phải chờ thanh toán thì trả về là đã thanh toán
        if (!Objects.equals(orderResponse.getStatus(), OrderStatus.WAITING_FOR_PAY_IN.getStatus())) {
            throw new BadRequestException("error.orderHasBeenPaid");
        }
        OrderRequest orderRequest = BeanUtil.copyProperties(orderResponse, OrderRequest.class);
        Long endPrice = orderResponse.getEndPrice() != null || orderResponse.getEndPrice() >= 0 ? orderResponse.getEndPrice() : orderRequest.getTotalPrice();
        orderRequest.setEndPrice(endPrice);
        // Nếu EndPrice là 0 thì báo đã thanh toán thành công
        if (orderRequest.getEndPrice() == 0) {
            orderRequest.setStatus(OrderStatus.PAID.getStatus());
            orderService.update(orderResponse.getId(), orderRequest);
            paymentResponse.setMessage("Đã thanh toán thành công.");
            return paymentResponse;
        }
        // Thanh toán bằng VNPay
        if (Objects.equals(orderResponse.getPaymentType(), PaymentType.VNPAY.name())) {
            //Tạo request tới VNPay
            PaymentRequest paymentRequest = buildPaymentRequest(orderRequest);
            VnPayRequest vnPayRequest = vnPayService.buildVnPayRequest(paymentRequest);
            PaymentUrlResponse paymentUrlResponse = vnPayService.generateVnPayRequestUrl(vnPayRequest);
            paymentResponse.setPaymentInfo(paymentUrlResponse);
            return paymentResponse;

            // Kiểm tra thanh toán bằng số dư
        } else if (Objects.equals(orderResponse.getPaymentType(), PaymentType.BALANCE.name())) {
            orderRequest.setAccountId(accountId.get());
            Long balance = transactionService.getBalanceByAccountId(orderRequest.getAccountId());
            // So sánh số dư và giá chuyến đi
            if (orderRequest.getEndPrice() > balance) {
                throw new BadRequestException("error.balanceIsNotEnough");
            }
            TransactionResponse transactionResponse = performWithDrawTransaction(orderRequest);

            orderRequest.setStatus(OrderStatus.PAID.getStatus());
            orderRequest.setTransactionId(transactionResponse.getId());
            orderService.update(orderResponse.getId(), orderRequest);
            paymentResponse.setMessage("Đã thanh toán thành công.");
            return paymentResponse;
        }
        throw new BadRequestException("error.paymentFailed");
    }

    // Lưu lịch sử thanh toán VNpay
    private void saveVnPayTransactionHistory(Long refId, PaymentResponse paymentResponse) {
        VnPayTransaction transaction = new VnPayTransaction();
        transaction.setType(TransactionReason.PAYMENT_ORDER.getType());
        transaction.setRefId(refId);
        transaction.setContent(JsonUtils.toJson(paymentResponse));
        vnPayTransactionRepository.save(transaction);
    }

    // Lưu thông tin nạp tiền VNPay
    private TransactionResponse performPayInTransaction(PaymentResponse paymentResponse, Long id) {
        TransactionRequest transactionRequest = new TransactionRequest();
        transactionRequest.setTransactionType(PaymentType.VNPAY.name());
        transactionRequest.setTransactionReason(TransactionReason.PAYMENT_ORDER.getType());
        transactionRequest.setTargetId(id);
        transactionRequest.setAmount(new VnPayMoneyConverter().fromVnPay(paymentResponse.getAmount()));
        transactionRequest.setTransactionAt(paymentResponse.getPayDate());
        transactionRequest.setContent(paymentResponse.getOrderInfo());
        return transactionService.save(transactionRequest);
    }

    public OrderWithPaymentResponse createOrderForPartner(OrderRequest orderRequest) {
        OrderWithPaymentResponse orderResponse = new OrderWithPaymentResponse();
        if (orderRequest.getAccountId() == null) {
            authenticatedProvider.getUserId().ifPresent(orderRequest::setAccountId);
        }

        // Kiểm tra có trong inventory, nếu có thì lưu TotalPrice bằng giá của inventory
        if (orderRequest.getInventoryId() != null) {
            Inventory inventory = Utils.requireExists(inventoryRepository.findById(orderRequest.getInventoryId()), "error.inventoryNotFound");
            orderRequest.setTotalPrice(inventory.getPrice());
        }

        orderRequest.setOrderCode(orderCodeGenerator.generate());
        orderRequest.setStatus(OrderStatus.WAITING_FOR_PAY_IN.getStatus());
        // Nếu totalprice trống thì lưu nó là 0
        if (orderRequest.getTotalPrice() == null) {
            orderRequest.setTotalPrice(0L);
        }
        orderRequest.setEndPrice(orderRequest.getTotalPrice());

        // order use promotion
        // Kiểm tra xem người dùng hiện tại được sử dụng promotion hay không và cập nhật promotion sau khi sử dụng và giá đơn hàng sau khi sử dụng promotion
        if (orderRequest.getPromotionCode() != null){
            if (orderRequest.getAccountId() != null){
                Account account = Utils.requireExists(accountRepository.findAllByIdAndActiveIsTrueAndDeletedAtNull(orderRequest.getAccountId()), "error.dataNotFound");
                Promotion promotion = Utils.requireExists(promotionRepository.findByCodeAndDeletedAtIsNullAndStatus(orderRequest.getPromotionCode(), PromotionStatus.ACTIVE.getStatus()), "error.dataNotFound");
                UserPromotion userPromotion = Utils.requireExists(userPromotionRepository.findByPromotionAndAccount(promotion, account), "error.dataNotFound");
                // Kiểm tra giảm giá hết hạn chưa
                if (userPromotion.getUsageRemain() == 0){
                    orderResponse.setStatus(ResponseStatus.ERROR.getStatus());
                    orderResponse.setCode(OrderPhase.PAY_IN.getPhase());
                    orderResponse.setMessage("Khuyến mãi đã hết lượt sử dụng");
                    return orderResponse;
                }
                // Cập nhật promotion
                userPromotion.setUsageRemain(userPromotion.getUsageRemain() - 1);
                userPromotionRepository.save(userPromotion);
                orderRequest.setUserPromotionId(userPromotion.getId());
                // Cập nhật giá sau khi áp dụng promotion
                orderRequest.setEndPrice(orderRequest.getEndPrice() - promotion.getPrice());
            }
        }

        if (orderRequest.getEndPrice() < 0){
            orderRequest.setEndPrice(0L);
        }
        // Lấy kiểu thanh toán mặc định là thanh toán trực tiếp
        PaymentType paymentType = PaymentType.getPaymentType(orderRequest.getPaymentType()).orElse(PaymentType.DIRECT);
        orderRequest.setPaymentType(paymentType.name());
        orderResponse.setStatus(ResponseStatus.SUCCESS.getStatus());
        orderResponse.setCode(OrderPhase.FINISH.getPhase());
        orderResponse.setMessage("Đã tạo đơn hàng thành công.");

        OrderResponse savedOrderResponse = orderService.save(orderRequest);
        orderResponse.setOrderCode(savedOrderResponse.getOrderCode());
        orderResponse.setDiscount(savedOrderResponse.getDiscount());
        return orderResponse;
    }
}
