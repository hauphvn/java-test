package com.kinglogi.service.impl;

import com.google.firebase.messaging.*;
import com.kinglogi.dto.request.FcmPushNotificationRequest;
import com.kinglogi.dto.request.FcmSubscriptionRequest;
import com.kinglogi.service.FcmPushNotificationService;
import com.kinglogi.service.FcmService;
import com.kinglogi.service.PushNotificationService;
import com.kinglogi.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Service
@Qualifier("fcmPushNotificationService")
@Slf4j
public class FcmPushNotificationServiceImpl implements FcmPushNotificationService, PushNotificationService<FcmPushNotificationRequest> {

    private final FcmService fcmService;

    @Autowired
    public FcmPushNotificationServiceImpl(FcmService fcmService) {
        this.fcmService = fcmService;
    }

    @Override
    public boolean sendPnsToDevice(FcmPushNotificationRequest notificationRequestDto) {
        if (CollectionUtils.isEmpty(notificationRequestDto.getTokens())) {
            return false;
        }

        final Notification notification = buildNotification(notificationRequestDto);
        final WebpushConfig webpushConfig = buildWebpushConfig(notificationRequestDto);
        final String jsonContent = JsonUtils.toJson(notificationRequestDto.getData());

        int numberOfNotifications = ChunkServiceExecutor.execute(notificationRequestDto.getTokens(), tokens -> {
            tokens.forEach(token -> {
                try {
                    Message message = Message.builder()
                            .setToken(token)
                            .setNotification(notification)
                            .putData("content", jsonContent)
                            .setWebpushConfig(webpushConfig)
                            .build();
                    FirebaseMessaging.getInstance().send(message);
                } catch (FirebaseMessagingException e) {
                    log.error("Fail to send firebase notification to device ", e);
                }
            });
            return tokens.size();
        });
        return numberOfNotifications > 0;
    }

    private Notification buildNotification(FcmPushNotificationRequest notificationRequestDto) {
        return Notification.builder()
                .setTitle(notificationRequestDto.getTitle())
                .setBody(notificationRequestDto.getBody())
                .build();
    }

    private WebpushConfig buildWebpushConfig(FcmPushNotificationRequest notificationRequestDto) {
        WebpushConfig webpushConfig = null;
        if (StringUtils.isNotBlank(notificationRequestDto.getLink())) {
            WebpushFcmOptions fcmOptions = WebpushFcmOptions.builder()
                    .setLink(notificationRequestDto.getLink())
                    .build();

            webpushConfig = WebpushConfig.builder().setFcmOptions(fcmOptions).build();
        }
        return webpushConfig;
    }

    @Override
    public void subscribeToTopic(FcmSubscriptionRequest subscriptionRequestDto) {
        if (CollectionUtils.isEmpty(subscriptionRequestDto.getTokens())) {
            return;
        }
        try {
            FirebaseMessaging.getInstance(fcmService.getFirebaseApp())
                    .subscribeToTopic(subscriptionRequestDto.getTokens(), subscriptionRequestDto.getTopicName());
        } catch (FirebaseMessagingException e) {
            log.error("Firebase subscribe to topic fail: " + subscriptionRequestDto.getTopicName(), e);
        }
    }

    @Override
    public void unsubscribeFromTopic(FcmSubscriptionRequest subscriptionRequestDto) {
        if (CollectionUtils.isEmpty(subscriptionRequestDto.getTokens())) {
            return;
        }
        try {
            FirebaseMessaging.getInstance(fcmService.getFirebaseApp())
                    .unsubscribeFromTopic(subscriptionRequestDto.getTokens(), subscriptionRequestDto.getTopicName());
        } catch (FirebaseMessagingException e) {
            log.error("Firebase unsubscribe from topic fail: " + subscriptionRequestDto.getTopicName(), e);
        }
    }

    @Override
    public boolean sendPnsToTopic(FcmSubscriptionRequest subscriptionRequestDto) {
        if (CollectionUtils.isEmpty(subscriptionRequestDto.getTokens())) {
            return false;
        }

        subscribeToTopic(subscriptionRequestDto);

        final Notification notification = buildNotification(subscriptionRequestDto);
        final WebpushConfig webpushConfig = buildWebpushConfig(subscriptionRequestDto);
        final String jsonContent = JsonUtils.toJson(subscriptionRequestDto.getData());

        int numberOfNotifications = ChunkServiceExecutor.execute(subscriptionRequestDto.getTokens(), tokens -> {
            tokens.forEach(token -> {
                try {
                    Message message = Message.builder()
                            .setTopic(subscriptionRequestDto.getTopicName())
                            .setNotification(notification)
                            .putData("content", jsonContent)
                            .setWebpushConfig(webpushConfig)
                            .build();
                    FirebaseMessaging.getInstance().send(message);
                } catch (FirebaseMessagingException e) {
                    log.error("Fail to send firebase subscription to topic " + subscriptionRequestDto.getTopicName(), e);
                }
            });
            return tokens.size();
        });
        return numberOfNotifications > 0;
    }

    @Async
    @Override
    public CompletableFuture<Boolean> sendPushNotificationAsync(FcmPushNotificationRequest request) {
        if (CollectionUtils.isEmpty(request.getTokens())) {
            return CompletableFuture.completedFuture(false);
        }

        if (request instanceof FcmSubscriptionRequest) {
            sendPnsToTopic((FcmSubscriptionRequest) request);
        } else {
            sendPnsToDevice(request);
        }

        return CompletableFuture.completedFuture(true);
    }
}
