package com.kinglogi.service.impl;

import com.kinglogi.constant.*;
import com.kinglogi.dto.UserToken;
import com.kinglogi.dto.request.NotificationRequest;
import com.kinglogi.dto.request.OrderRequest;
import com.kinglogi.dto.request.PushNotificationDto;
import com.kinglogi.dto.request.TransactionRequest;
import com.kinglogi.dto.response.OrderWithPaymentResponse;
import com.kinglogi.dto.response.PaymentResponse;
import com.kinglogi.dto.response.PaymentUrlResponse;
import com.kinglogi.dto.response.UserCredential;
import com.kinglogi.entity.Account;
import com.kinglogi.entity.Order;
import com.kinglogi.provider.AuthenticatedProvider;
import com.kinglogi.repository.OrderRepository;
import com.kinglogi.repository.custom.CustomRoleRepository;
import com.kinglogi.service.*;
import com.kinglogi.service.error.AccessForbiddenException;
import com.kinglogi.service.utils.Utils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class VisitorOrderServiceImpl implements VisitorOrderService {

    private final OrderWithPaymentService orderWithPaymentService;

    private final NotificationService notificationService;

    private final PushNotificationTokenService pushNotificationTokenService;

    private final CustomRoleRepository roleRepository;

    private final OrderRepository orderRepository;

    private final TransactionService transactionService;

    private final AuthenticatedProvider authenticatedProvider;

    private final OtpService otpService;

    @Override
    public OrderWithPaymentResponse createOrder(OrderRequest orderRequest) {
        OrderWithPaymentResponse orderResponse = orderWithPaymentService.createOrder(orderRequest);

        // Nếu trạng thái là thành công thì gửi thông báo
        if (ResponseStatus.SUCCESS.getStatus().equals(orderResponse.getStatus())) {
            sendNotification(orderRequest);
            Order order = orderRepository.getOrderByOrderCode(orderResponse.getOrderCode()).orElse(null);
            if (order != null && order.getPhoneNumber() != null && !order.getPhoneNumber().isEmpty()){
                try {
                    Map<String, String> map = new HashMap<>();
                    if (order.getPaymentType().equals(PaymentType.BALANCE.name()) && order.getEndPrice() != null && order.getEndPrice() > 0){
                        map.put("amount", order.getEndPrice().toString());
                        map.put("orderCode", order.getOrderCode());
                        otpService.sendSms(order.getPhoneNumber(), TypeSms.ORDER_BALANCE, map);
                    }else {
                        map.put("orderCode", order.getOrderCode());
                        otpService.sendSms(order.getPhoneNumber(), TypeSms.CREATE_ORDER, map);
                    }
                } catch (IOException e) {
                    log.error(e.getMessage());
                }
            }
        }

        return orderResponse;
    }

    private void sendNotification(OrderRequest orderRequest) {
        Map<String, Object> additionalData = new HashMap<>();
        additionalData.put("orderCode", orderRequest.getOrderCode());

        String messageContentPattern = "Khách hàng {0} đã đặt xe với mã đơn hàng là {1}";
        String messageContent = MessageFormat.format(messageContentPattern, orderRequest.getFullName(), orderRequest.getOrderCode());

        // Send notification for all admin based on role which correspondence for a order type
        String role = Optional.ofNullable(orderRequest.getTripId()).map(tripId -> Role.FIXED_TRIP_ADMIN.getCode()).orElse(Role.FLEXIBLE_TRIP_ADMIN.getCode());
        List<UserToken> userTokens = pushNotificationTokenService.getTokensByRoles(role);

        // Save notification to database
        // Tìm các account là cộng tác viên đang còn hoạt động
        List<Account> accountList = new ArrayList<>(Utils.requireExists(roleRepository.findByCode(Role.COLLABORATOR.getCode()), "error.dataNotFound").getAccounts().stream().filter(account -> {
            if (account.getDeletedAt() != null) {
                return true;
            }
            if (account.getActive()) return true;
            return false;
        }).collect(Collectors.toList()));
        List<Long> accountIds = accountList.stream().map(Account::getId).distinct().collect(Collectors.toList());
        // Nếu Token rỗng thì tạo danh sách thông báo mới cho các ctv
        if (CollectionUtils.isEmpty(userTokens)) {
            List<NotificationRequest> notifications = accountIds.stream()
                    .map(accountId -> NotificationRequest.builder()
                            .title("Đơn hàng mới")
                            .content(messageContent)
                            .type(NotificationType.NEW_ORDER.getType())
                            .data(additionalData)
                            .accountId(accountId)
                            .build())
                    .collect(Collectors.toList());
            notificationService.saveAll(notifications);

            // Send push notification
            PushNotificationDto pushNotificationRequest = PushNotificationDto.builder()
                    .title("Đơn hàng mới")
                    .body(messageContent)
                    .type(NotificationType.NEW_ORDER.getType())
                    .data(additionalData)
                    .userTokens(userTokens)
                    .topicName(NotificationType.NEW_ORDER.name() + "_" + OrderType.FIXED_TRIP.getType())
                    .build();
            notificationService.sendNotification(pushNotificationRequest);
        }
    }

    // Xác thực thanh toán
    @Override
    public PaymentResponse verifyPayment(Map<String, Object> allParams) {
        return orderWithPaymentService.verifyPayment(allParams);
    }

    // Hủy chuyến
    @Override
    public void cancel(Long id) {
        UserCredential userCredential = Utils.requireExists(authenticatedProvider.getUser(), "error.userNotFound");
        Order order = Utils.requireExists(orderRepository.findById(id), "error.orderNotFound");
        if (order.getCreatedBy() != null && !Objects.equals(order.getCreatedBy(), userCredential.getId())) {
            throw new AccessForbiddenException("error.orderNotBeLongToYou");
        }
        // Lấy kiểu thanh toán, mặc đinh là thanh toán trực tiếp
        PaymentType paymentType = PaymentType.getPaymentType(order.getPaymentType()).orElse(PaymentType.DIRECT);
        // Nếu không phải thanh toán trực tiếp và không dùng giảm giá thì hoàn tiền
        if (paymentType != PaymentType.DIRECT) {
            if (order.getUserPromotion() == null) {
                //Tiến hành hoàn tiền
                performWithDrawTransaction(order.getCreatedBy(), order);
            }
        }
        order.setStatus(OrderStatus.CANCELLED.getStatus());
        orderRepository.save(order);
    }

    @Override
    public OrderWithPaymentResponse generate(String orderCode) {
        return orderWithPaymentService.paymentForOrder(orderCode);
    }


    // Hoàn tiền khi hủy chuyến
    private void performWithDrawTransaction(Long userId, Order order) {
        TransactionRequest transactionRequest = new TransactionRequest();
        transactionRequest.setTargetId(userId);
        // Hoàn tiền lại 85%
        Long amount = order.getEndPrice() != null ? order.getEndPrice() / 100 * 85 : order.getTotalPrice() / 100 * 85;
        transactionRequest.setAmount(amount);
        Date transactionAt = Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant());
        transactionRequest.setTransactionAt(transactionAt);
        transactionRequest.setContent("Hoàn tiền đơn hàng " + order.getOrderCode() + " lúc " +
                LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm")));
        transactionService.save(transactionRequest);
    }
}
