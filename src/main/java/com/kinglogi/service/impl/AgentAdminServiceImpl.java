package com.kinglogi.service.impl;

import com.kinglogi.dto.filter.AccountFilter;
import com.kinglogi.dto.filter.base.BaseFilter;
import com.kinglogi.dto.request.AccountRequest;
import com.kinglogi.dto.response.AccountResponse;
import com.kinglogi.entity.Account;
import com.kinglogi.entity.Role;
import com.kinglogi.repository.custom.CustomAccountRepository;
import com.kinglogi.repository.custom.CustomRoleRepository;
import com.kinglogi.service.AccountHistoryService;
import com.kinglogi.service.AgentAdminService;
import com.kinglogi.service.LoyaltyPointHistoryService;
import com.kinglogi.service.UserACL;
import com.kinglogi.service.error.AccessForbiddenException;
import com.kinglogi.service.error.BadRequestException;
import com.kinglogi.specs.BaseFilterSpecs;
import com.kinglogi.utils.BeanUtil;
import com.kinglogi.utils.EncrytedPasswordUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Qualifier("AgentAdminService")
public class AgentAdminServiceImpl extends AccountServiceImpl implements AgentAdminService {
    private final UserACL userACL;

    private final UserProfileServiceImpl userProfileService;
    private final LoyaltyPointHistoryService loyaltyPointHistoryService;

    private final AccountHistoryService accountHistoryService;

    @Autowired
    public AgentAdminServiceImpl(@Qualifier("customAccountRepository") CustomAccountRepository accountRepository,
                                 @Qualifier("customRoleRepository") CustomRoleRepository roleRepository,
                                 BaseFilterSpecs<Account> baseFilterSpecs, UserACL userACL, UserProfileServiceImpl userProfileService, LoyaltyPointHistoryService loyaltyPointHistoryService, AccountHistoryService accountHistoryService) {
        super(accountRepository, roleRepository, baseFilterSpecs, loyaltyPointHistoryService, accountHistoryService);
        this.userACL = userACL;
        this.userProfileService = userProfileService;
        this.loyaltyPointHistoryService = loyaltyPointHistoryService;
        this.accountHistoryService = accountHistoryService;
    }

    @Override
    public AccountResponse save(AccountRequest userRequest) {
        if (!(userACL.isAgentAdmin() || userACL.isSubAgentAdmin())) {
            throw new AccessForbiddenException("error.notAgentAdmin");
        }
        Account agentAdmin = getById(userProfileService.getLoggedInUserId());
        Optional<Account> accountOptionalUserName = accountRepository.findByUsername(userRequest.getUsername());
        if (userRequest.getPhoneNumber() == null || StringUtils.isBlank(userRequest.getPhoneNumber())) {
            throw new BadRequestException("error.phoneNumberEmptyOrBlank");
        }
        if (accountOptionalUserName.isPresent()) {
            throw new BadRequestException("error.userNameExisted");
        }
        Optional<Account> accountOptionalPhoneNumber = accountRepository.findByPhoneNumber(userRequest.getPhoneNumber());
        if (accountOptionalPhoneNumber.isPresent()) {
            throw new BadRequestException("error.phoneNumberExisted");
        }
        Account userCTV = BeanUtil.copyProperties(userRequest, Account.class);
        userCTV.setEmail(userCTV.getUsername());
        userCTV.setPassword(EncrytedPasswordUtils.encryptPassword(userRequest.getPassword()));
        userCTV.setMinBalanceCollaborator(userRequest.getMinBalance());
        userCTV.setRoles(this.getRoles(userRequest.getRoles()));
        if (userCTV.getAllowNotification() == null) {
            userCTV.setAllowNotification(true);
        }
        userCTV.setAgentId(agentAdmin.getId());
        Account savedUser = accountRepository.save(userCTV);

        return BeanUtil.copyProperties(savedUser, AccountResponse.class);
    }

    @Override
    public AccountResponse update(Long id, AccountRequest userRequest) {
        if (!(userACL.isAgentAdmin() || userACL.isSubAgentAdmin())) {
            throw new AccessForbiddenException("error.notAgentAdmin");
        }
        Account user = getById(id);
        Account agentAdmin = getById(userProfileService.getLoggedInUserId());
        if (!user.getAgentId().equals(agentAdmin.getId())) {
            throw new AccessForbiddenException("error.NotPermissionUpdate");
        }
        if (userRequest.getPhoneNumber() == null || StringUtils.isBlank(userRequest.getPhoneNumber())) {
            throw new BadRequestException("error.phoneNumberEmptyOrBlank");
        }
        Optional<Account> accountOptionalUserName = accountRepository.findByUsername(userRequest.getUsername());
        if (accountOptionalUserName.isPresent() && !accountOptionalUserName.get().getId().equals(user.getId())) {
            throw new BadRequestException("error.userNameExisted");
        }
        Optional<Account> accountOptionalPhoneNumber = accountRepository.findByPhoneNumber(userRequest.getPhoneNumber());
        if (accountOptionalPhoneNumber.isPresent() && !accountOptionalPhoneNumber.get().getId().equals(user.getId())) {
            throw new BadRequestException("error.phoneNumberExisted");
        }
        user.setMinBalanceCollaborator(userRequest.getMinBalance());
        return super.update(id, userRequest);
    }

    @Override
    public boolean deleteById(Long id) {
        if (!(userACL.isAgentAdmin() || userACL.isSubAgentAdmin())) {
            throw new AccessForbiddenException("error.notAgentAdmin");
        }
        Account user = getById(id);
        Account agentAdmin = getById(userProfileService.getLoggedInUserId());
        if (!user.getAgentId().equals(agentAdmin.getId())) {
            throw new AccessForbiddenException("error.NotPermissionDelete");
        }
        return super.deleteById(id);
    }

    private Set<Role> getRoles(List<String> roleCodes) {
        return Optional.ofNullable(roleCodes)
                .orElseGet(ArrayList::new)
                .stream().map(roleRepository::findByCode)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());
    }

    private AccountResponse convertToResponse(Account account) {
        AccountResponse res = BeanUtil.copyProperties(account, AccountResponse.class);
        res.setRoles(getRoleCodes(account.getRoles()));
        res.setMinBalance(account.getMinBalanceCollaborator());
        return res;
    }

    private Set<String> getRoleCodes(Set<com.kinglogi.entity.Role> roles) {
        return roles.stream()
                .map(com.kinglogi.entity.Role::getCode)
                .collect(Collectors.toSet());
    }

    @Override
    public Page<AccountResponse> findAll(BaseFilter baseFilter) {
        Account agentAdmin = getById(userProfileService.getLoggedInUserId());
        AccountFilter accountFilter = (AccountFilter) baseFilter;
        accountFilter.setRoleCodes(Collections.singletonList(com.kinglogi.constant.Role.COLLABORATOR.getCode()));
        Pageable pageable = baseFilterSpecs.page(accountFilter);
        Page<Account> page = accountRepository.getAllAccountsByAgent(accountFilter, pageable, agentAdmin.getId());
        return page.map(this::convertToResponse);
    }

    @Override
    public Page<AccountResponse> findSubAgent(BaseFilter baseFilter) {
        Account agentAdmin = getById(userProfileService.getLoggedInUserId());
        AccountFilter accountFilter = (AccountFilter) baseFilter;
        accountFilter.setRoleCodes(Collections.singletonList(com.kinglogi.constant.Role.SUB_AGENT_ADMIN.getCode()));
        Pageable pageable = baseFilterSpecs.page(accountFilter);
        Page<Account> page = accountRepository.getAllAccountsByAgent(accountFilter, pageable, agentAdmin.getId());
        return page.map(this::convertToResponse);
    }
}
