package com.kinglogi.service.impl;

import com.kinglogi.dto.UserToken;
import com.kinglogi.dto.request.SubscribePushNotificationRequest;
import com.kinglogi.entity.Account;
import com.kinglogi.entity.PushNotificationToken;
import com.kinglogi.provider.AuthenticatedProvider;
import com.kinglogi.repository.PushNotificationTokenRepository;
import com.kinglogi.service.PushNotificationTokenService;
import com.kinglogi.utils.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class PushNotificationTokenServiceImpl implements PushNotificationTokenService {

    private final AuthenticatedProvider authenticatedProvider;

    private final PushNotificationTokenRepository pushNotificationTokenRepository;

    @Autowired
    public PushNotificationTokenServiceImpl(
            AuthenticatedProvider authenticatedProvider,
            PushNotificationTokenRepository pushNotificationTokenRepository) {
        this.authenticatedProvider = authenticatedProvider;
        this.pushNotificationTokenRepository = pushNotificationTokenRepository;
    }

    @Override
    public boolean subscribe(SubscribePushNotificationRequest request) {
        Account account = new Account();
        account.setId(authenticatedProvider.getUserId().get());
        Optional<PushNotificationToken> pushNotificationTokenOptional = pushNotificationTokenRepository.findByDeviceIdAndAppNameAndAccount(request.getDeviceId(), request.getAppName(), account);
        if (pushNotificationTokenOptional.isPresent()){
            pushNotificationTokenRepository.delete(pushNotificationTokenOptional.get());
        }

        PushNotificationToken pushNotificationToken = BeanUtil.copyProperties(request, PushNotificationToken.class);
        pushNotificationToken.setAccount(account);
        pushNotificationTokenRepository.save(pushNotificationToken);
        return true;
    }

    @Override
    public List<UserToken> getTokensByAccountId(Long accountId) {
        return pushNotificationTokenRepository.getTokensByAccountId(accountId);
    }

    @Override
    public List<UserToken> getTokensByRoles(String ...roles) {
        return pushNotificationTokenRepository.getTokensByRoleCodes(Arrays.asList(roles));
    }

    @Override
    public List<UserToken> getTokensByAccountIds(List<Long> accountIds) {
        return pushNotificationTokenRepository.getTokensByAccountIds(accountIds);
    }


}
