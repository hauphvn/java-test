package com.kinglogi.service.impl;

import com.kinglogi.constant.LocaleConstant;
import com.kinglogi.dto.mail.MailInfo;
import com.kinglogi.dto.mail.MailTemplateConfig;
import com.kinglogi.dto.request.EmailTemplateRequest;
import com.kinglogi.dto.response.EmailTemplateResponse;
import com.kinglogi.entity.EmailTemplate;
import com.kinglogi.exception.KlgApplicationException;
import com.kinglogi.exception.KlgResourceNotFoundException;
import com.kinglogi.exception.ValidatingException;
import com.kinglogi.provider.OrderCodeGenerator;
import com.kinglogi.repository.EmailTemplateRepository;
import com.kinglogi.service.EmailService;
import com.kinglogi.service.EmailTemplateService;
import com.kinglogi.utils.BeanUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
@AllArgsConstructor
public class EmailTemplateServiceImpl implements EmailTemplateService {

    private final EmailTemplateRepository emailTemplateRepository;
    private final OrderCodeGenerator orderCodeGenerator;
    private final EmailService emailService;

    @Override
    public List<EmailTemplateResponse> findAll() {
        return emailTemplateRepository.findAll().stream().map(this::convertToResponse).collect(Collectors.toList());
    }


    @Override
    public EmailTemplateResponse findById(Long id) {
        EmailTemplate emailTemplate = getById(id);
        return convertToResponse(emailTemplate);
    }

    private EmailTemplate getById(Long id) {
        return emailTemplateRepository.findById(id).orElseThrow(
                () -> new KlgResourceNotFoundException("Email template is not found with id [" + id + "]"));
    }

    @Override
    public EmailTemplateResponse update(Long id, EmailTemplateRequest request) {
        EmailTemplate emailTemplate = getById(id);
        BeanUtil.mergeProperties(request, emailTemplate);
        EmailTemplate savedEmailTemplate = emailTemplateRepository.save(emailTemplate);
        return convertToResponse(savedEmailTemplate);
    }

    @Override
    public EmailTemplateResponse findByTypeAndLanguage(String type, String language) {
        if (StringUtils.isBlank(language)) {
            language = LocaleConstant.VIETNAMESE;
        }
        EmailTemplate emailTemplate = emailTemplateRepository.findByTypeAndLanguage(type, language).orElseThrow(
                () -> new KlgResourceNotFoundException("Email template is not found with type [" + type + "]"));
        return convertToResponse(emailTemplate);
    }

    @Override
    public String dryRun(Long id, Map<String, Object> data) {
        if (data.get("email") == null || StringUtils.isBlank(data.get("email").toString())) {
            throw new ValidatingException("Missing a received email");
        }
        try {
            EmailTemplateResponse emailTemplateResponse = findById(id);
            MailTemplateConfig config = MailTemplateConfig.builder()
                    .mailContent(emailTemplateResponse.getContent())
                    .mailSubject(emailTemplateResponse.getTitle())
                    .mailIsHtml(true)
                    .build();

            MailInfo mailInfo = new MailInfo();
            mailInfo.addTo(String.valueOf(data.get("email")));
            emailService.send(mailInfo, config, data);
            return "Send successfully";
        } catch(Exception e) {
            throw new KlgApplicationException("Can not dry-run for sending an email with id=" + id + " and data=" + data, e);
        }
    }

    private EmailTemplateResponse convertToResponse(EmailTemplate emailTemplate) {
        EmailTemplateResponse response = BeanUtil.copyProperties(emailTemplate, EmailTemplateResponse.class);
        EmailTemplate originalEmailTemplate = emailTemplate.getEmailTemplate();
        if (originalEmailTemplate != null) {
            response.setOriginalId(originalEmailTemplate.getId());
        }
        return response;
    }
}
