package com.kinglogi.service.impl;

import com.kinglogi.constant.BidStatus;
import com.kinglogi.dto.filter.AuctionBidFilter;
import com.kinglogi.dto.response.AuctionBidResponse;
import com.kinglogi.dto.response.SimpleOrderResponse;
import com.kinglogi.entity.Auction;
import com.kinglogi.entity.Order;
import com.kinglogi.provider.AuthenticatedProvider;
import com.kinglogi.repository.AuctionRepository;
import com.kinglogi.repository.BidRepository;
import com.kinglogi.repository.OrderRepository;
import com.kinglogi.service.AuctionBidService;
import com.kinglogi.service.AuctionService;
import com.kinglogi.service.utils.Utils;
import com.kinglogi.specs.BaseFilterSpecs;
import com.kinglogi.utils.BeanUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AuctionBidServiceImpl implements AuctionBidService {

    private final AuthenticatedProvider authenticatedProvider;

    private final AuctionRepository auctionRepository;

    private final OrderRepository orderRepository;

    private final BaseFilterSpecs<Auction> baseFilterSpecs;

    private final BidRepository bidRepository;

    private final AuctionService auctionService;

    @Override
    public List<AuctionBidResponse> getAuctionBidsByLoggedInUser(AuctionBidFilter filter) {
        filter.setUserId(authenticatedProvider.getUserId().get());
        return getAuctionBids(filter);
    }

    @Override
    public List<AuctionBidResponse> getAuctionBids(AuctionBidFilter filter) {
        BidStatus status = null;
        if (filter.getStatus() != null) {
            if (filter.getStatus() == 1) {
                status = BidStatus.SUCCESS;
            } else if (filter.getStatus() == 0) {
                status = BidStatus.FAIL;
            }
        }
        Pageable pageable = baseFilterSpecs.page(filter);
        if (status == null){
            return auctionRepository.getAllAuctionBids(filter.getUserId(), filter.getAuctionId(), pageable);
        }
        return auctionRepository.getAuctionBids(filter.getUserId(), filter.getAuctionId(), status, pageable);
    }

    @Override
    public AuctionBidResponse getDetail(Long auctionId) {
        AuctionBidFilter filter = AuctionBidFilter.builder()
                .auctionId(auctionId)
                .userId(authenticatedProvider.getUserId().get())
                .build();
        List<AuctionBidResponse> auctionBidResponses = getAuctionBids(filter);
        Optional<AuctionBidResponse> optionalAuctionBidResponse = auctionBidResponses.stream().filter(AuctionBidResponse::getAcceptance).findFirst();

        Auction auction = Utils.requireExists(auctionRepository.findById(auctionId), "error.dataNotFound");
        List<Order> orders = orderRepository.getAllByAuctions(auction);
        List<SimpleOrderResponse> simpleOrderResponses = orders.stream().map(order -> {
            SimpleOrderResponse simpleOrderResponseMap = BeanUtil.copyProperties(order, SimpleOrderResponse.class);
            if (!optionalAuctionBidResponse.isPresent()) {
                simpleOrderResponseMap.setEmail("");
                simpleOrderResponseMap.setFullName("");
                simpleOrderResponseMap.setPhoneNumber("");
            }
            return simpleOrderResponseMap;
        }).collect(Collectors.toList());

        AuctionBidResponse auctionBidResponse = BeanUtil.copyProperties(auction, AuctionBidResponse.class);
        auctionBidResponse.setOrders(simpleOrderResponses);
        auctionBidResponse.setAcceptance(optionalAuctionBidResponse.isPresent());
        return auctionBidResponse;
    }
}
