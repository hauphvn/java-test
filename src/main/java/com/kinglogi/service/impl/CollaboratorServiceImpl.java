package com.kinglogi.service.impl;

import com.kinglogi.constant.Role;
import com.kinglogi.dto.filter.AccountFilter;
import com.kinglogi.dto.filter.base.BaseFilter;
import com.kinglogi.dto.request.AccountRequest;
import com.kinglogi.dto.response.AccountResponse;
import com.kinglogi.entity.Account;
import com.kinglogi.exception.KlgResourceNotFoundException;
import com.kinglogi.repository.custom.CustomAccountRepository;
import com.kinglogi.repository.custom.CustomRoleRepository;
import com.kinglogi.service.AccountHistoryService;
import com.kinglogi.service.CollaboratorService;
import com.kinglogi.service.LoyaltyPointHistoryService;
import com.kinglogi.service.UserACL;
import com.kinglogi.specs.BaseFilterSpecs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Qualifier("collaboratorService")
public class CollaboratorServiceImpl extends AccountServiceImpl implements CollaboratorService {

	private final UserACL userACL;

    private final LoyaltyPointHistoryService loyaltyPointHistoryService;

	private final AccountHistoryService accountHistoryService;

	@Autowired
    public CollaboratorServiceImpl(@Qualifier("customAccountRepository") CustomAccountRepository accountRepository,
								   @Qualifier("customRoleRepository") CustomRoleRepository roleRepository,
								   BaseFilterSpecs<Account> baseFilterSpecs, UserACL userACL, LoyaltyPointHistoryService loyaltyPointHistoryService, AccountHistoryService accountHistoryService) {
		super(accountRepository, roleRepository, baseFilterSpecs, loyaltyPointHistoryService, accountHistoryService);
		this.userACL = userACL;
        this.loyaltyPointHistoryService = loyaltyPointHistoryService;
		this.accountHistoryService = accountHistoryService;
	}

	@Override
    public Page<AccountResponse> findAll(BaseFilter baseFilter) {
    	AccountFilter accountFilter = (AccountFilter) baseFilter;
    	accountFilter.setRoleCodes(Arrays.asList(Role.COLLABORATOR.getCode()));
        return super.findAll(accountFilter);
    }

	@Override
	public AccountResponse update(Long id, AccountRequest userRequest) {
		if (!userACL.isSuperAdmin()) {
			// vì api cũ dùng status code 400 de gui message ve quyen truy cap khong duoc pap nên chúng ta sẽ giữ nguyên status code cho api này
			throw new KlgResourceNotFoundException("sorry, You're not authorized to access this resource");
		}
		return super.update(id, userRequest);
	}
}
