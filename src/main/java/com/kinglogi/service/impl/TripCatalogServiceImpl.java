package com.kinglogi.service.impl;

import com.kinglogi.dto.filter.base.BaseFilter;
import com.kinglogi.dto.request.TripCatalogRequest;
import com.kinglogi.dto.response.TripCatalogResponse;
import com.kinglogi.entity.TripCatalog;
import com.kinglogi.exception.KlgResourceNotFoundException;
import com.kinglogi.repository.TripCatalogRepository;
import com.kinglogi.service.TripCatalogService;
import com.kinglogi.specs.BaseFilterSpecs;
import com.kinglogi.utils.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TripCatalogServiceImpl implements TripCatalogService {

    private final TripCatalogRepository tripCatalogRepository;

    private final BaseFilterSpecs<TripCatalog> baseFilterSpecs;

    @Autowired
    public TripCatalogServiceImpl(TripCatalogRepository tripCatalogRepository, BaseFilterSpecs<TripCatalog> baseFilterSpecs) {
        this.tripCatalogRepository = tripCatalogRepository;
        this.baseFilterSpecs = baseFilterSpecs;
    }

    @Override
    public List<TripCatalogResponse> findAll() {
        return BeanUtil.listCopyProperties(tripCatalogRepository.findAll(), TripCatalogResponse.class);
    }

    @Override
    public Page<TripCatalogResponse> findAll(BaseFilter baseFilter) {
        Specification<TripCatalog> searchable = baseFilterSpecs.search(baseFilter);
        Pageable pageable = baseFilterSpecs.page(baseFilter);
        Page<TripCatalog> page = tripCatalogRepository.findAll(searchable, pageable);
        return page.map(entity -> BeanUtil.copyProperties(entity, TripCatalogResponse.class));
    }

    @Override
    public TripCatalogResponse findById(Long id) {
        TripCatalog tripCatalog = getById(id);
        return BeanUtil.copyProperties(tripCatalog, TripCatalogResponse.class);
    }

    private TripCatalog getById(Long id) {
        return tripCatalogRepository.findById(id).orElseThrow(() -> new KlgResourceNotFoundException("Trip catalog is not found with id [" + id + "]"));
    }

    @Override
    public TripCatalogResponse save(TripCatalogRequest request) {
        TripCatalog tripCatalog = BeanUtil.copyProperties(request, TripCatalog.class);
        TripCatalog savedTripCatalog = tripCatalogRepository.save(tripCatalog);
        return BeanUtil.copyProperties(savedTripCatalog, TripCatalogResponse.class);
    }

    @Override
    public TripCatalogResponse update(Long id, TripCatalogRequest request) {
        TripCatalog tripCatalog = getById(id);
        BeanUtil.mergeProperties(request, tripCatalog);
        TripCatalog savedTripCatalog = tripCatalogRepository.save(tripCatalog);
        return BeanUtil.copyProperties(savedTripCatalog, TripCatalogResponse.class);
    }

    @Override
    public boolean deleteById(Long id) {
        try {
            tripCatalogRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }
}
