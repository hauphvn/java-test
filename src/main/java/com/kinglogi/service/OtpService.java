package com.kinglogi.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kinglogi.configuration.security.SmsSetting;
import com.kinglogi.constant.SmsResponseCode;
import com.kinglogi.constant.TypeSms;
import com.kinglogi.dto.event.EventLogOtp;
import com.kinglogi.dto.sms.SmsRequest;
import com.kinglogi.dto.sms.SmsResponse;
import com.kinglogi.entity.AccountOtp;
import com.kinglogi.entity.SmsTemplate;
import com.kinglogi.repository.AccountOtpRepository;
import com.kinglogi.repository.SmsTemplateRepository;
import com.kinglogi.service.error.BadRequestException;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.MessageFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class OtpService {

    @Autowired
    SmsSetting smsSetting;

    @Autowired
    ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    AccountOtpRepository accountOtpRepository;

    @Autowired
    SmsTemplateRepository smsTemplateRepository;

    public Instant sendOTP(String phoneNumber) throws IOException {
        int randomOtp = (int) ((Math.random()*900000)+100000);
        while (accountOtpRepository.findByOtpAndPhoneNumber(String.valueOf(randomOtp), phoneNumber).isPresent()){
            randomOtp = (int) ((Math.random()*900000)+100000);
        }
        Instant expireTime = Instant.now().plus(2, ChronoUnit.MINUTES);

        AccountOtp accountOtp = new AccountOtp();
        accountOtp.setPhoneNumber(phoneNumber);
        accountOtp.setOtp(String.valueOf(randomOtp));
        accountOtp.setExpiredAt(expireTime);
        accountOtpRepository.save(accountOtp);

        ObjectMapper mapper = new ObjectMapper();
        SmsRequest smsRequest = new SmsRequest();
        smsRequest.setUser(smsSetting.getUser());
        smsRequest.setPass(smsSetting.getPass());
        smsRequest.setBrandName(smsSetting.getBrandName());
        smsRequest.setPhone(phoneNumber);
        smsRequest.setTranId("");
        smsRequest.setDataEncode(0);
        if (smsSetting.isOwnTemplate()){
            smsRequest.setMess("KINGLOGI Chuc mung nam moi, an khang thinh vuong");
        }else {
            smsRequest.setMess(randomOtp + " la ma bao mat cua ban. Khong chia se ma cua ban voi bat ky ai.");
        }
        smsRequest.setSendTime("");
        smsRequest.setTelcoCode("");

        // Create Json String body
        String json = mapper.writeValueAsString(smsRequest);

        Map<String, Object> data = new HashMap<>();
        data.put("mode", smsSetting.isEnableSendSms() ? "Có gửi sms" : "Không gửi sms");
        data.put("phoneNumber", phoneNumber);
        data.put("otp", randomOtp);
        data.put("smsContent", smsRequest.getMess());
        data.put("expireTime", expireTime.atZone(ZoneId.of("Asia/Ho_Chi_Minh")).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

        // Create request
        if (smsSetting.isEnableSendSms()){
            CloseableHttpClient client = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost(smsSetting.getUrlApi());
            StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Content-type", "application/json;charset=UTF-8");

            CloseableHttpResponse response = client.execute(httpPost);
            String body = EntityUtils.toString(response.getEntity());
            SmsResponse smsResponse = mapper.readValue(body, SmsResponse.class);
            client.close();

            data.put("message", SmsResponseCode.getMessageByCode(smsResponse.getCode()));
            applicationEventPublisher.publishEvent(new EventLogOtp(this, "sendOtp", data));

            if (smsResponse.getCode() != SmsResponseCode.SUCCESS.getCode()){
                log.error("Sms error: " + SmsResponseCode.getMessageByCode(smsResponse.getCode()));
                throw new BadRequestException(SmsResponseCode.getMessageByCode(smsResponse.getCode()));
            }
            return expireTime;
        }
        applicationEventPublisher.publishEvent(new EventLogOtp(this, "sendOtp", data));
        return expireTime;
    }

    @Async
    public void sendSms(String phoneNumber, TypeSms type, Map<String, String> map) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        SmsRequest smsRequest = new SmsRequest();
        smsRequest.setUser(smsSetting.getUser());
        smsRequest.setPass(smsSetting.getPass());
        smsRequest.setBrandName(smsSetting.getBrandName());
        smsRequest.setPhone(phoneNumber);
        smsRequest.setTranId("");
        smsRequest.setDataEncode(0);
        SmsTemplate smsTemplate = smsTemplateRepository.findByType(type);
        if (smsTemplate == null){
            return;
        }
        String messageContentPattern = smsTemplate.getContent();
        String messageContent = "";
        if (smsTemplate.getType().equals(TypeSms.CREATE_ORDER)){
            messageContent = MessageFormat.format(messageContentPattern, map.get("orderCode"));
        }else if (smsTemplate.getType().equals(TypeSms.ORDER_BALANCE)){
            messageContent = MessageFormat.format(messageContentPattern, map.get("amount"), map.get("orderCode"));
        }else if (smsTemplate.getType().equals(TypeSms.INFORMATION_DRIVER)){
            messageContent = MessageFormat.format(messageContentPattern, map.get("orderCode"), map.get("nameDriver"), map.get("phoneNumber"), map.get("licensePlates"));
        }else {
            return;
        }


        smsRequest.setMess(messageContent);
        smsRequest.setSendTime("");
        smsRequest.setTelcoCode("");

        // Create Json String body
        String json = mapper.writeValueAsString(smsRequest);

        Map<String, Object> data = new HashMap<>();
        data.put("mode", smsSetting.isEnableSendSms() ? "Có gửi sms" : "Không gửi sms");
        data.put("phoneNumber", phoneNumber);
        data.put("smsContent", smsRequest.getMess());

        // Create request
        if (smsSetting.isEnableSendSms()){
            CloseableHttpClient client = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost(smsSetting.getUrlApi());
            StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Content-type", "application/json;charset=UTF-8");

            CloseableHttpResponse response = client.execute(httpPost);
            String body = EntityUtils.toString(response.getEntity());
            SmsResponse smsResponse = mapper.readValue(body, SmsResponse.class);
            client.close();

            data.put("message", SmsResponseCode.getMessageByCode(smsResponse.getCode()));
            applicationEventPublisher.publishEvent(new EventLogOtp(this, "sendSms", data));

            if (smsResponse.getCode() != SmsResponseCode.SUCCESS.getCode()){
                log.error("Sms error: " + SmsResponseCode.getMessageByCode(smsResponse.getCode()));
                data.put("error", SmsResponseCode.getMessageByCode(smsResponse.getCode()));
            }
        }
        applicationEventPublisher.publishEvent(new EventLogOtp(this, "sendSms", data));
    }

}
