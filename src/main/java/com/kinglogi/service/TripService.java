package com.kinglogi.service;

import com.kinglogi.dto.filter.base.BaseFilter;
import com.kinglogi.dto.request.TripRequest;
import com.kinglogi.dto.response.SimpleTripResponse;
import com.kinglogi.dto.response.TripResponse;
import com.kinglogi.entity.Trip;
import com.kinglogi.service.base.CrudService;
import com.kinglogi.service.base.MasterDataService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;

public interface TripService extends MasterDataService, CrudService<TripRequest, TripResponse, Long> {

	Page<SimpleTripResponse> findAllSimpleTrip(BaseFilter baseFilter);

	TripResponse getTripByParentTripId(Long parentTripId);

    Page<String> getPickingUpLocations(String pickingUpLocation, String language, Pageable pageable);

	Page<TripResponse> getDroppingOffLocations(String pickingUpLocation, String droppingOffLocation, String language, Integer numberOfSeat, Pageable pageable);

    void importTrip(MultipartFile file) throws IOException, ParseException;
}
