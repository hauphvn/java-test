package com.kinglogi.service;


import com.kinglogi.constant.PartnerLinkPosition;
import com.kinglogi.dto.filter.PartnerLinkFilter;
import com.kinglogi.dto.partner_link.PartnerLinkInputDTO;
import com.kinglogi.dto.partner_link.PartnerLinkOutputDTO;
import com.kinglogi.entity.Commission;
import com.kinglogi.entity.PartnerLink;
import com.kinglogi.repository.PartnerLinkRepository;
import com.kinglogi.service.error.AccessForbiddenException;
import com.kinglogi.service.error.BadRequestException;
import com.kinglogi.service.error.ResourceNotFoundException;
import com.kinglogi.service.utils.Utils;
import com.kinglogi.specs.BaseFilterSpecs;
import com.kinglogi.utils.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PartnerLinkService {
    private final PartnerLinkRepository partnerLinkRepository;

    public String FILE_DIRECTORY = System.getProperty("user.dir") + java.io.File.separator + "storage/";
    private final BaseFilterSpecs<PartnerLink> baseFilterSpecs;

    private final UserACL userACL;

    public PartnerLinkService(PartnerLinkRepository partnerLinkRepository,
                              BaseFilterSpecs<PartnerLink> baseFilterSpecs, UserACL userACL) {
        this.partnerLinkRepository = partnerLinkRepository;
        this.baseFilterSpecs = baseFilterSpecs;
        this.userACL = userACL;
    }

    //TODO
    public PartnerLinkOutputDTO save(PartnerLinkInputDTO input) {
        PartnerLink partnerLink = new PartnerLink();
        if (StringUtils.isBlank(input.getName())) {
            throw new BadRequestException("error.nameNotBlank");
        }
        if (StringUtils.isBlank(input.getUrl())) {
            throw new BadRequestException("error.urlNotBlank");
        }
        if (input.getPosition() == PartnerLinkPosition.LEFT) {
            Optional<PartnerLink> optionalPartnerLinkLeft = partnerLinkRepository.findByPosition(PartnerLinkPosition.LEFT);
            optionalPartnerLinkLeft.ifPresent(link -> link.setPosition(PartnerLinkPosition.FOOTER));
        }
        if (input.getPosition() == PartnerLinkPosition.RIGHT) {
            Optional<PartnerLink> optionalPartnerLinkRight = partnerLinkRepository.findByPosition(PartnerLinkPosition.RIGHT);
            optionalPartnerLinkRight.ifPresent(link -> link.setPosition(PartnerLinkPosition.FOOTER));
        }
        partnerLink.setName(input.getName().trim());
        partnerLink.setUrl(input.getUrl().trim());
        partnerLink.setPosition(input.getPosition());
        partnerLinkRepository.save(partnerLink);
        return new PartnerLinkOutputDTO(partnerLink);
    }

    //TODO
    public PartnerLinkOutputDTO update(PartnerLinkInputDTO input, Long partnerLinkId) {
        if (!userACL.isSuperAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        PartnerLink partnerLink = Utils.requireExists(partnerLinkRepository.findById(partnerLinkId), "error.partnerLinkNotfound");
        if (StringUtils.isBlank(input.getName())) {
            throw new BadRequestException("error.nameNotBlank");
        }
        if (StringUtils.isBlank(input.getUrl())) {
            throw new BadRequestException("error.urlNotBlank");
        }
        if (input.getPosition() == PartnerLinkPosition.LEFT) {
            Optional<PartnerLink> optionalPartnerLinkLeft = partnerLinkRepository.findByPosition(PartnerLinkPosition.LEFT);
            if (optionalPartnerLinkLeft.isPresent() && optionalPartnerLinkLeft.get().getId() != partnerLinkId) {
                optionalPartnerLinkLeft.get().setPosition(PartnerLinkPosition.FOOTER);
            }
        }
        if (input.getPosition() == PartnerLinkPosition.RIGHT) {
            Optional<PartnerLink> optionalPartnerLinkRight = partnerLinkRepository.findByPosition(PartnerLinkPosition.RIGHT);
            if (optionalPartnerLinkRight.isPresent() && optionalPartnerLinkRight.get().getId() != partnerLinkId) {
                optionalPartnerLinkRight.get().setPosition(PartnerLinkPosition.FOOTER);
            }
        }
        partnerLink.setName(input.getName().trim());
        partnerLink.setUrl(input.getUrl().trim());
        partnerLink.setPosition(input.getPosition());
        partnerLinkRepository.save(partnerLink);
        return new PartnerLinkOutputDTO(partnerLink);
    }

    //TODO
    public void delete(Long partnerLinkId) {
        if (!userACL.isSuperAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        PartnerLink partnerLink = Utils.requireExists(partnerLinkRepository.findById(partnerLinkId), "error.partnerLinkNotfound");
        List<String> urls = new ArrayList<>();
        if (partnerLink.getAdImageUrl() != null) {
            urls.add(partnerLink.getAdImageUrl());
        }
        if (partnerLink.getLogoUrl() != null) {
            urls.add(partnerLink.getLogoUrl());
        }
        if (!urls.isEmpty()) {
            for (String url : urls) {
                java.io.File newFile = new java.io.File(url);
                if (newFile.exists()) {
                    newFile.delete();
                }
            }
        }
        partnerLinkRepository.delete(partnerLink);
    }

    //TODO
    public void updateLogo(MultipartFile logo, Long partnerLinkId) throws IOException {
        if (!userACL.isSuperAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        PartnerLink partnerLink = Utils.requireExists(partnerLinkRepository.findById(partnerLinkId), "error.partnerLinkNotfound");
        if (partnerLink.getLogoUrl() != null) {
            File file = new File(partnerLink.getLogoUrl());
            if (file.exists()) {
                file.delete();
            }
        }
        String filePath = FILE_DIRECTORY + "partnerLink/" + partnerLinkId;
        java.io.File fileDirectory = new java.io.File(filePath);
        if (!fileDirectory.exists()) {
            fileDirectory.mkdirs();
        }
        java.io.File logoFile = FileUtils.convertMultiPartToFile(logo, filePath);
        partnerLink.setLogoUrl(logoFile.getAbsolutePath());
        partnerLinkRepository.save(partnerLink);
    }

    //TODO
    public void updateAdImage(MultipartFile adImage, Long partnerLinkId) throws IOException {
        if (!userACL.isSuperAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        PartnerLink partnerLink = Utils.requireExists(partnerLinkRepository.findById(partnerLinkId), "error.partnerLinkNotfound");
        if (partnerLink.getAdImageUrl() != null) {
            File file = new File(partnerLink.getAdImageUrl());
            if (file.exists()) {
                file.delete();
            }
        }
        String filePath = FILE_DIRECTORY + "partnerLink/" + partnerLink.getId();
        java.io.File fileDirectory = new java.io.File(filePath);
        if (!fileDirectory.exists()) {
            fileDirectory.mkdirs();
        }
        java.io.File imageFile = FileUtils.convertMultiPartToFile(adImage, filePath);
        partnerLink.setAdImageUrl(imageFile.getAbsolutePath());
        partnerLinkRepository.save(partnerLink);
    }

    //TODO
    public Page<PartnerLinkOutputDTO> findAll(PartnerLinkFilter partnerLinkFilter) {
        Pageable pageable = baseFilterSpecs.page(partnerLinkFilter);
        Specification<PartnerLink> spec = baseFilterSpecs.search(partnerLinkFilter);
        return partnerLinkRepository.findAll(spec, pageable).map(PartnerLinkOutputDTO::new);
    }

    //Todo
    public PartnerLinkOutputDTO findOne(Long partnerLinkId) {
        PartnerLink partnerLink = Utils.requireExists(partnerLinkRepository.findById(partnerLinkId), "error.partnerLinkNotfound");
        return new PartnerLinkOutputDTO(partnerLink);
    }


    public InputStreamResource viewImage(Long id, String fileName) throws FileNotFoundException {
        PartnerLink partnerLink = Utils.requireExists(partnerLinkRepository.findById(id), "error.partnerLinkNotfound");
        File newFile;
        if (Objects.equals(fileName, FilenameUtils.getName(partnerLink.getAdImageUrl()))) {
            newFile = new File(partnerLink.getAdImageUrl());
        } else {
            newFile = new File(partnerLink.getLogoUrl());
        }
        FileInputStream fileInputStream = new FileInputStream(newFile);
        return new InputStreamResource(fileInputStream);
    }

    public List<PartnerLinkOutputDTO> ads() {
        return partnerLinkRepository.findAllByPositionIn(PartnerLinkPosition.LEFT, PartnerLinkPosition.RIGHT).stream().map(PartnerLinkOutputDTO::new).collect(Collectors.toList());
    }
}
