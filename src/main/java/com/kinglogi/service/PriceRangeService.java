package com.kinglogi.service;

import com.google.gson.Gson;
import com.kinglogi.dto.filter.PriceRangeFilter;
import com.kinglogi.dto.price_range.AreaMetadata;
import com.kinglogi.dto.price_range.PriceRangeInputDTO;
import com.kinglogi.dto.price_range.PriceRangeOutputDTO;
import com.kinglogi.entity.PriceRange;
import com.kinglogi.repository.PriceRangeRepository;
import com.kinglogi.service.error.AccessForbiddenException;
import com.kinglogi.service.utils.Utils;
import com.kinglogi.specs.BaseFilterSpecs;
import com.kinglogi.utils.BeanUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PriceRangeService {

    private final UserACL userACL;

    private final BaseFilterSpecs baseFilterSpecs;

    private final PriceRangeRepository priceRangeRepository;

    public PriceRangeService(UserACL userACL, BaseFilterSpecs baseFilterSpecs, PriceRangeRepository priceRangeRepository) {
        this.userACL = userACL;
        this.baseFilterSpecs = baseFilterSpecs;
        this.priceRangeRepository = priceRangeRepository;
    }

    // TODO: Need to update API when create fixed trip to be able to have price range as dropping off location.[
    // TODO: Need to update API book order to have apply priceRange

    // Tạo vùng giá
    public PriceRangeOutputDTO create(PriceRangeInputDTO input) {
        // Kiểm tra có phải là admin hoặc admin tuyến cố định không
        if (!userACL.isSuperAdmin() && !userACL.isFixedAdmin()){
            throw new AccessForbiddenException("error.notAdmin");
        }
        // Copy thuộc tính vào biến priceRange từ input người dùng
        PriceRange priceRange = BeanUtil.copyProperties(input, PriceRange.class);
        // Lưu vào bảng vùng giá
        priceRange = priceRangeRepository.save(priceRange);
        // Tạo outut return cho người dùng
        PriceRangeOutputDTO priceRangeOutputDTO = BeanUtil.copyProperties(priceRange, PriceRangeOutputDTO.class);
        return priceRangeOutputDTO;
    }

    // Cập nhật vùng giá
    public PriceRangeOutputDTO update(Long priceRangeId, PriceRangeInputDTO input) {
        // Kiểm tra có phải là admin hoặc admin tuyến cố định không
        if (!userACL.isSuperAdmin() && !userACL.isFixedAdmin()){
            throw new AccessForbiddenException("error.notAdmin");
        }
        // Tìm kiếm vùng giá chỉnh sửa theo id vùng giá
        PriceRange priceRange = Utils.requireExists(priceRangeRepository.findById(priceRangeId), "error.dataNotFound");
        // Sao chép thuộc tính của input vào pricerange
        // input là cái cần cập nhật
        // pricerange là vùng giá ban đầu, trước khi cập nhật
        BeanUtil.mergeProperties(input, priceRange);
        // Lưu vùng giá mới lại
        priceRange = priceRangeRepository.save(priceRange);
        // tạo output return
        PriceRangeOutputDTO priceRangeOutputDTO = BeanUtil.copyProperties(priceRange, PriceRangeOutputDTO.class);
        return priceRangeOutputDTO;
    }

    // Xóa theo id vùng giá
    public void delete(Long priceRangeId) {
        // Kiểm tra có phải là admin hoặc admin tuyến cố định không
        if (!userACL.isSuperAdmin() && !userACL.isFixedAdmin()){
            throw new AccessForbiddenException("error.notAdmin");
        }
        // tìm vùng giá theo id
        PriceRange priceRange = Utils.requireExists(priceRangeRepository.findById(priceRangeId), "error.dataNotFound");
        // xóa mềm vùng giá
        priceRangeRepository.softDelete(priceRange);
    }

    // tìm 1 vùng giá trong bảng theo id
    public PriceRangeOutputDTO findOne(Long priceRangeId) {
        PriceRange priceRange = Utils.requireExists(priceRangeRepository.findByIdAndDeletedAtIsNull(priceRangeId), "error.dataNotFound");
        PriceRangeOutputDTO priceRangeOutputDTO = BeanUtil.copyProperties(priceRange, PriceRangeOutputDTO.class);
        return priceRangeOutputDTO;
    }

    // Lấy tất cả vùng giá
    public Page<PriceRangeOutputDTO> findAll(PriceRangeFilter filter) {
        Specification<PriceRange> spec = baseFilterSpecs.search(filter);
        if (!filter.getKeyword().equals("")) {
            if (filter.getKeyword().matches("\\d+")){
                Specification<PriceRange> additionalSpec = (root, query, cb) ->
                        cb.equal(root.get("price"), filter.getKeyword());
                spec = spec.or(additionalSpec);
            }
        }
        Specification<PriceRange> additionalSpec = (root, query, cb) ->
                cb.isNull(root.get("deletedAt"));
        spec = spec.and(additionalSpec);
        Pageable pageable = baseFilterSpecs.page(filter);
        Page<PriceRange> page = priceRangeRepository.findAll(spec, pageable);
        return page.map(priceRange -> BeanUtil.copyProperties(priceRange, PriceRangeOutputDTO.class));
    }

    // tìm kiếm qua địa điểm trả khách
    public PriceRangeOutputDTO findByDroppingOffLocation(String goAreaMetadata, String destinationAreaMetadata) {
        Gson gson = new Gson();

        // Go area
        // Lưu trữ từ json sang 1 object AreaMetadata
        AreaMetadata areaGo = gson.fromJson(goAreaMetadata, AreaMetadata.class);
        String provinceGo = "";
        String districtGo = "";
        if (areaGo.getProvince() != null){
            // Tạo chuỗi String có chứa province
            provinceGo = "\"province\":\""+ areaGo.getProvince() + "\"";
        }
        if (areaGo.getDistrict() != null){
            // Tạo chuỗi string có chứa district
            districtGo = "\"district\":\""+ areaGo.getDistrict() + "\"";
        }

        // Destination area
        // Lưu trữ từ json sang 1 object AreaMetadata
        AreaMetadata areaDestination = gson.fromJson(destinationAreaMetadata, AreaMetadata.class);
        String provinceDestination = "";
        String districtDestination = "";
        if (areaDestination.getProvince() != null){
            // Tạo chuỗi String có chứa province
            provinceDestination = "\"province\":\""+ areaDestination.getProvince() + "\"";
        }
        if (areaDestination.getDistrict() != null){
            // Tạo chuỗi string có chứa district
            districtDestination = "\"district\":\""+ areaDestination.getDistrict() + "\"";
        }

        // tìm kiếm các vùng giá khớp với các chuỗi proviceGo và DistrictGo mới ghép ở trên từ input người dùng
        List<PriceRange> priceRanges = priceRangeRepository.findAllByMatchingDestinationArea(provinceGo, districtGo, provinceDestination, districtDestination);
        // Nếu tồn tại vùng giá thì return cho người dùng cái đầu tiên
        if (priceRanges.size() > 0){
            return BeanUtil.copyProperties(priceRanges.get(0), PriceRangeOutputDTO.class);
        }
        return null;
    }
}
