package com.kinglogi.service;

import com.kinglogi.dto.filter.OrderFilter;
import com.kinglogi.dto.filter.base.BaseFilter;
import com.kinglogi.dto.request.OrderRequest;
import com.kinglogi.dto.response.OrderResponse;
import com.kinglogi.dto.response.WinningBidderResponse;
import com.kinglogi.service.base.CrudService;
import org.springframework.data.domain.Page;

import java.util.Date;
import java.util.List;

public interface OrderService extends CrudService<OrderRequest, OrderResponse, Long> {

    OrderResponse getOrderByOrderCode(String code);

    WinningBidderResponse getWinningBidder(Long orderId);

    List<OrderResponse> getByIds(List<Long> ids);

    void sendEmailInvoice(Long orderId);

    Page<OrderResponse> getOrdersBelongToCollaborator(OrderFilter filter, Date day);

    List<Date> getOrdersInMonthOfCollaborator(int year, int month);

    void startOrderFromCollaborator(Long orderId);

    void finishOrderFromCollaborator(Long orderId);

    void cancelOrderFromCollaborator(Long orderId);
}
