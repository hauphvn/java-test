package com.kinglogi.service;

import com.kinglogi.dto.event.EventLogOtp;
import com.kinglogi.dto.log.LogOutput;
import com.kinglogi.entity.Log;
import com.kinglogi.repository.LogRepository;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

@Service
public class LogService {

    private final LogRepository logRepository;

    public LogService(LogRepository logRepository) {
        this.logRepository = logRepository;
    }

    @EventListener
    public void logEventOtp(EventLogOtp event){
            Log log = new Log();
            log.setEventName(event.getEvent());
            log.setData(event.getData());
            log.setEventDate(Instant.now());
            logRepository.save(log);

    }

    public Page<LogOutput> getLogs(Pageable pageable) {
        Page<Log> logPage = logRepository.findAll(pageable);
        return logPage.map(LogOutput::new);
    }
}
