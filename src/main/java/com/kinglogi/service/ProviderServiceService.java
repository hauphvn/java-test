package com.kinglogi.service;

import com.kinglogi.dto.filter.ProviderServiceFilter;
import com.kinglogi.dto.request.ProviderServiceRequest;
import com.kinglogi.dto.response.ProviderServiceResponse;
import com.kinglogi.service.base.CrudService;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.util.List;

public interface ProviderServiceService extends CrudService<ProviderServiceRequest, ProviderServiceResponse, Long> {

    void uploadImage(Long id, List<MultipartFile> multipartFiles);

    InputStreamResource viewImage(Long id, String fileName) throws FileNotFoundException;

    void deleteImage(Long id, String fileName);
}
