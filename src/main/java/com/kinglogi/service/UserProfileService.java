package com.kinglogi.service;

import com.kinglogi.dto.filter.OrderFilter;
import com.kinglogi.dto.request.UserProfileRequest;
import com.kinglogi.dto.response.AccountResponse;
import com.kinglogi.dto.response.OrderResponse;
import com.kinglogi.dto.response.TransactionResponse;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.Instant;
import java.util.List;

public interface UserProfileService {

    AccountResponse getProfile();

    AccountResponse updateProfile(UserProfileRequest request);

    List<TransactionResponse> getTransactionsByAccountId();

    Page<OrderResponse> getOrdersByAccountId(OrderFilter orderFilter);

    void upLoadAvatar(MultipartFile multipartFile) throws IOException;

    InputStreamResource viewImage();

    AccountResponse updatePhoneNumber(UserProfileRequest request, String otp);

    InputStreamResource viewImage(Long id);

    Instant sendOtpForUpdatePhoneNumber(UserProfileRequest request) throws IOException;

    void deleteAccount();

    String exportOrder() throws IOException;
}
