package com.kinglogi.service;

import com.kinglogi.dto.request.FcmPushNotificationRequest;
import com.kinglogi.dto.request.FcmSubscriptionRequest;

public interface FcmPushNotificationService {

    boolean sendPnsToDevice(FcmPushNotificationRequest notificationRequestDto);

    void subscribeToTopic(FcmSubscriptionRequest subscriptionRequestDto);

    void unsubscribeFromTopic(FcmSubscriptionRequest subscriptionRequestDto);

    boolean sendPnsToTopic(FcmSubscriptionRequest notificationRequestDto);
}
