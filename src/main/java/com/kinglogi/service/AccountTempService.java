package com.kinglogi.service;

import com.kinglogi.dto.response.AccountTempResponse;

public interface AccountTempService {

    AccountTempResponse createAccountTemp(String username);

    boolean isValid(String username, String password);

    boolean delete(String username);
}
