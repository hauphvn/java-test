package com.kinglogi.service;

import com.google.gson.Gson;
import com.kinglogi.dto.account_history.AccountActivityOutputDTO;
import com.kinglogi.dto.account_history.AccountHistoryOutputDTO;
import com.kinglogi.dto.filter.AccountHistoryFilter;
import com.kinglogi.dto.filter.AccountLoginFilter;
import com.kinglogi.dto.response.AccountResponse;
import com.kinglogi.entity.Account;
import com.kinglogi.entity.AccountHistory;
import com.kinglogi.entity.Auction;
import com.kinglogi.repository.AccountHistoryRepository;
import com.kinglogi.repository.AccountRepository;
import com.kinglogi.service.utils.Utils;
import com.kinglogi.specs.BaseFilterSpecs;
import com.kinglogi.utils.BeanUtil;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;

@Service
public class AccountHistoryService {
    private final AccountHistoryRepository accountHistoryRepository;

    private final AccountRepository accountRepository;

    private final BaseFilterSpecs<Auction> baseFilterSpecs;

    public AccountHistoryService(AccountHistoryRepository accountHistoryRepository, AccountRepository accountRepository, BaseFilterSpecs<Auction> baseFilterSpecs) {
        this.accountHistoryRepository = accountHistoryRepository;
        this.accountRepository = accountRepository;
        this.baseFilterSpecs = baseFilterSpecs;
    }

    //TODO
    public Page<AccountHistoryOutputDTO> findAllAccountHistoryOfAccount(AccountHistoryFilter filter, Long accountId) {
        Account account = Utils.requireExists(accountRepository.findById(accountId), "error.dataNotFound");
        Pageable pageable = baseFilterSpecs.page(filter);
        Page<AccountHistory> page = accountHistoryRepository.findAllByAccount(account, pageable);
        return page.map(accountHistory -> {
            Account modifyAccount = Utils.requireExists(accountRepository.findById(accountHistory.getCreatedBy()), "error.dataNotFound");
            AccountHistoryOutputDTO accountHistoryOutputDTO = new AccountHistoryOutputDTO(accountHistory);
            AccountResponse accountResponse = BeanUtil.copyProperties(modifyAccount, AccountResponse.class);
            accountHistoryOutputDTO.setCreatedBy(accountResponse);
            return accountHistoryOutputDTO;
        });
    }

    //TODO
    public InputStreamResource exportAccountHistory(Long accountId) {
        return null;
    }

    public void accountLogin(Account account){
        account.setLastLogin(Instant.now());
        accountRepository.save(account);
    }

    public void changeDataOfUser(AccountResponse oldAccount, AccountResponse newAccount){
        AccountHistory accountHistory = new AccountHistory();
        String valueOld = new Gson().toJson(oldAccount);
        String valueNew = new Gson().toJson(newAccount);
        Account account = new Account();
        account.setId(oldAccount.getId());
        accountHistory.setAccount(account);
        accountHistory.setOldValue(valueOld);
        accountHistory.setNewValue(valueNew);
        accountHistoryRepository.save(accountHistory);
    }

    public Page<AccountActivityOutputDTO> findAllActivityOfAccounts(AccountLoginFilter filter) {
        Pageable pageable = baseFilterSpecs.page(filter);
        Page<Account> page = accountRepository.findByUsernameContaining(filter.getKeyword(), pageable);
        return page.map(AccountActivityOutputDTO::new);
    }
}
