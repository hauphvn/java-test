package com.kinglogi.service;

import com.kinglogi.constant.ReportActivityType;
import com.kinglogi.constant.ReportType;
import com.kinglogi.dto.report.ReportOutputDTO;
import com.kinglogi.entity.*;
import com.kinglogi.repository.AccountRepository;
import com.kinglogi.repository.AuctionRepository;
import com.kinglogi.repository.BidRepository;
import com.kinglogi.repository.ReportRepository;
import com.kinglogi.service.utils.Utils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.*;
import java.io.File;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Service
public class ReportService {

    public static final String FILE_DIRECTORY =  System.getProperty("user.dir") + File.separator + "documents";
    static {
        File file  = new File(FILE_DIRECTORY);
        if(!file.exists()) {
            file.mkdirs();
        }
    }

    private final ReportRepository reportRepository;

    private final AccountRepository accountRepository;

    private final AuctionRepository auctionRepository;

    private static CellStyle cellStyleFormatNumber = null;

    private final BidRepository bidRepository;

    public ReportService(ReportRepository reportRepository, AccountRepository accountRepository, AuctionRepository auctionRepository, BidRepository bidRepository) {
        this.reportRepository = reportRepository;
        this.accountRepository = accountRepository;
        this.auctionRepository = auctionRepository;
        this.bidRepository = bidRepository;
    }

    @Async
    public void save(Account account, Order order, Bid bid, ReportType reportType, ReportActivityType reportActivityType){
        if (reportType.equals(ReportType.ORDER)){
            Report report = new Report();
            report.setDate(Instant.now());
            report.setAccount(account);
            report.setCode(order.getOrderCode());
            report.setActivity(reportActivityType);
            report.setPickUp(order.getPickingUpLocation());
            report.setDropOff(order.getDroppingOffLocation());
            report.setPrice(order.getTotalPrice() == null ? 0 : order.getTotalPrice());
            report.setTotalCharged(order.getEndPrice() == null ? 0 : order.getEndPrice());
            report.setCar(order.getBrand());
            report.setCurrency("VND");
            report.setCustomer(order.getFullName());
            report.setCustomerPhone(order.getPhoneNumber());
            if (order.getAccount() != null){
                report.setLoyaltyNumber(order.getAccount().getId());
            }

            if (order.getCollaboratorId() != null){
                Account collaborator = Utils.requireExists(accountRepository.findById(order.getCollaboratorId()), "error.dataNotFound");
                report.setDriverPhone(collaborator.getPhoneNumber() == null ? null : collaborator.getPhoneNumber());
                if (reportActivityType.equals(ReportActivityType.START) || reportActivityType.equals(ReportActivityType.FINISH)){
                    if (order.getAuctions().size() > 0){
                        Auction auction = order.getAuctions().iterator().next();
                        List<Bid> bids = bidRepository.findAllByAuctionAndAccount(auction, account);
                        if (bids.size() > 0){
                            report.setDriverRevenue(bids.get(0).getBidPrice());
                            report.setKlgRevenue(order.getEndPrice() - bids.get(0).getBidPrice());
                        }
                    }
                }
            }
            reportRepository.save(report);
        }else if (reportType.equals(ReportType.BID)){
            Auction auction = Utils.requireExists(auctionRepository.findById(bid.getAuction().getId()), "error.dataNotFound");
            Account collaborator = Utils.requireExists(accountRepository.findById(bid.getAccount().getId()), "error.dataNotFound");
            for (Order orderAuction : auction.getOrders()){
                Report report = new Report();
                report.setDate(Instant.now());
                report.setAccount(account);
                report.setCode(orderAuction.getOrderCode());
                report.setActivity(reportActivityType);
                report.setPickUp(orderAuction.getPickingUpLocation());
                report.setDropOff(orderAuction.getDroppingOffLocation());
                report.setPrice(orderAuction.getTotalPrice() == null ? 0 : orderAuction.getTotalPrice());
                report.setTotalCharged(orderAuction.getEndPrice() == null ? 0 : orderAuction.getEndPrice());
                report.setCar(orderAuction.getBrand());
                report.setCurrency("VND");
                report.setTotalCharged(orderAuction.getEndPrice());
                report.setCustomer(orderAuction.getFullName());
                report.setCustomerPhone(orderAuction.getPhoneNumber());
                report.setDriverPhone(collaborator.getPhoneNumber());
                report.setDriverRevenue(bid.getBidPrice());
                report.setKlgRevenue(orderAuction.getEndPrice() - bid.getBidPrice());
                if (orderAuction.getAccount() != null){
                    report.setLoyaltyNumber(orderAuction.getAccount().getId());
                }
                reportRepository.save(report);
            }
        }
    }

    public InputStreamResource exportReport() throws IOException {
        // Create Workbook
        Workbook workbook = getWorkbook(FILE_DIRECTORY + File.separator  + "report.xlsx");

        // Create sheet
        Sheet sheet = workbook.createSheet("Reports"); // Create sheet with sheet name

        List<Report> reports = reportRepository.findAll();

        int rowIndex = 0;

        // Write header
        writeHeader(sheet, rowIndex);

        // Write data
        rowIndex++;
        for (Report book : reports) {
            // Create row
            Row row = sheet.createRow(rowIndex);
            // Write data on row
            writeReport(book, row);
            rowIndex++;
        }

        // Auto resize column witdth
        int numberOfColumn = sheet.getRow(0).getPhysicalNumberOfCells();
        autosizeColumn(sheet, numberOfColumn);

        // Create file excel
        createOutputFile(workbook, FILE_DIRECTORY + File.separator +  "report.xlsx");
        InputStream inputStream = new FileInputStream(FILE_DIRECTORY + File.separator  + "report.xlsx");

        File file = new File(FILE_DIRECTORY + File.separator  + "report.xlsx");
        if (file.exists()){
            file.delete();
        }
        return new InputStreamResource(inputStream);
    }

    // Create workbook
    private static Workbook getWorkbook(String excelFilePath) throws IOException {
        Workbook workbook = null;

        if (excelFilePath.endsWith("xlsx")) {
            workbook = new XSSFWorkbook();
        } else if (excelFilePath.endsWith("xls")) {
            workbook = new HSSFWorkbook();
        } else {
            throw new IllegalArgumentException("The specified file is not Excel file");
        }
        return workbook;
    }

    // Write header with format
    private static void writeHeader(Sheet sheet, int rowIndex) {
        // create CellStyle
        CellStyle cellStyle = createStyleForHeader(sheet);

        // Create row
        Row row = sheet.createRow(rowIndex);

        // Create cells
        Cell cell = row.createCell(0);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Date & time");

        cell = row.createCell(1);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("User");

        cell = row.createCell(2);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Agent");

        cell = row.createCell(3);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Code");

        cell = row.createCell(4);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Activity");

        cell = row.createCell(5);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Pickup");

        cell = row.createCell(6);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Dropoff");

        cell = row.createCell(7);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Price");

        cell = row.createCell(8);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Car");

        cell = row.createCell(9);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Currency");

        cell = row.createCell(10);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Total charged");

        cell = row.createCell(11);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("KLG Revenue");

        cell = row.createCell(12);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Driver revenue");

        cell = row.createCell(13);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Customer");

        cell = row.createCell(14);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Customer phone");

        cell = row.createCell(15);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Driver phone");

        cell = row.createCell(16);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Loyalty Nbr");

    }

    // Create CellStyle for header
    private static CellStyle createStyleForHeader(Sheet sheet) {
        // Create font
        Font font = sheet.getWorkbook().createFont();
        font.setFontName("Times New Roman");
        font.setBold(true);
        font.setFontHeightInPoints((short) 14); // font size

        // Create CellStyle
        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        cellStyle.setFont(font);
        return cellStyle;
    }

    // Write data
    private static void writeReport(Report report, Row row) {
        if (cellStyleFormatNumber == null) {
            // Format number
            short format = (short)BuiltinFormats.getBuiltinFormat("#,##0");
            // DataFormat df = workbook.createDataFormat();
            // short format = df.getFormat("#,##0");

            //Create CellStyle
            Workbook workbook = row.getSheet().getWorkbook();
            cellStyleFormatNumber = workbook.createCellStyle();
            cellStyleFormatNumber.setDataFormat(format);
        }

        Cell cell = row.createCell(0);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss")
                .withZone(ZoneId.of("Asia/Ho_Chi_Minh"));
        String date = formatter.format(report.getDate());
        cell.setCellValue(date);

        cell = row.createCell(1);
        if (report.getAccount() != null){
            cell.setCellValue(report.getAccount().getFullName());
        }

        cell = row.createCell(2);
        if (report.getAccount() != null){
            if (report.getAccount().getAgentId() != null){
                cell.setCellValue(report.getAccount().getAgentId().toString());
            }
        }

        cell = row.createCell(3);
        if (report.getCode() != null) {
            cell.setCellValue(report.getCode());
        }

        cell = row.createCell(4);
        if (report.getActivity() != null) {
            cell.setCellValue(report.getActivity().toString());
        }

        cell = row.createCell(5);
        if (report.getPickUp() != null) {
            cell.setCellValue(report.getPickUp());
        }

        cell = row.createCell(6);
        if (report.getDropOff() != null) {
            cell.setCellValue(report.getDropOff());
        }

        cell = row.createCell(7);
        if (report.getPrice() != null) {
            cell.setCellValue(report.getPrice().toString());
        }

        cell = row.createCell(8);
        if (report.getCar() != null) {
            cell.setCellValue(report.getCar());
        }

        cell = row.createCell(9);
        if (report.getCurrency() != null) {
            cell.setCellValue(report.getCurrency());
        }

        cell = row.createCell(10);
        if (report.getTotalCharged() != null) {
            cell.setCellValue(report.getTotalCharged().toString());
        }

        cell = row.createCell(11);
        if (report.getKlgRevenue() != null){
            cell.setCellValue(report.getKlgRevenue().toString());
        }

        cell = row.createCell(12);
        if (report.getDriverRevenue() != null) {
            cell.setCellValue(report.getDriverRevenue().toString());
        }

        cell = row.createCell(13);
        if (report.getCustomer() != null) {
            cell.setCellValue(report.getCustomer());
        }

        cell = row.createCell(14);
        if (report.getCustomerPhone() != null) {
            cell.setCellValue(report.getCustomerPhone());
        }

        cell = row.createCell(15);
        if (report.getDriverPhone() != null){
            cell.setCellValue(report.getDriverPhone());
        }

        cell = row.createCell(16);
        if (report.getLoyaltyNumber() != null){
            cell.setCellValue(report.getLoyaltyNumber().toString());
        }

    }

    // Auto resize column width
    private static void autosizeColumn(Sheet sheet, int lastColumn) {
        for (int columnIndex = 0; columnIndex < lastColumn; columnIndex++) {
            sheet.autoSizeColumn(columnIndex);
        }
    }

    // Create output file
    private static void createOutputFile(Workbook workbook, String excelFilePath) throws IOException {
        try (OutputStream os = new FileOutputStream(excelFilePath)) {
            workbook.write(os);
        }
    }

    public Page<ReportOutputDTO> getReports(Instant date, Pageable pageable) {
        Page<Report> reports;
        if (date == null){
            reports = reportRepository.findAll(pageable);
        }else {
            Instant to = date.truncatedTo(ChronoUnit.DAYS);
            Instant from = to.plus(86399, ChronoUnit.SECONDS);
            reports = reportRepository.findAllByDateAfterAndDateBefore(to, from, pageable);
        }
        return reports.map(ReportOutputDTO::new);
    }

}
