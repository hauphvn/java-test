package com.kinglogi.service;

import com.kinglogi.constant.LoyaltyActionType;
import com.kinglogi.constant.NotificationType;
import com.kinglogi.dto.UserToken;
import com.kinglogi.dto.filter.LoyaltyPointHistoryFilter;
import com.kinglogi.dto.loyalty_point_history.LoyaltyPointHistoryInputDTO;
import com.kinglogi.dto.loyalty_point_history.LoyaltyPointHistoryOutputDTO;
import com.kinglogi.dto.request.NotificationRequest;
import com.kinglogi.dto.request.PushNotificationDto;
import com.kinglogi.entity.Account;
import com.kinglogi.entity.LoyaltyPointHistory;
import com.kinglogi.provider.AuthenticatedProvider;
import com.kinglogi.repository.AccountRepository;
import com.kinglogi.repository.LoyaltyPointHistoryRepository;
import com.kinglogi.service.utils.Utils;
import com.kinglogi.specs.BaseFilterSpecs;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.*;

@Service
public class LoyaltyPointHistoryService {
    private final LoyaltyPointHistoryRepository loyaltyPointHistoryRepository;
    private final BaseFilterSpecs<LoyaltyPointHistory> baseFilterSpecs;

    private final AuthenticatedProvider authenticatedProvider;

    private final PushNotificationTokenService pushNotificationTokenService;

    private final NotificationService notificationService;

    private final AccountRepository accountRepository;

    public LoyaltyPointHistoryService(LoyaltyPointHistoryRepository loyaltyPointHistoryRepository, BaseFilterSpecs<LoyaltyPointHistory> baseFilterSpecs, AuthenticatedProvider authenticatedProvider, PushNotificationTokenService pushNotificationTokenService, NotificationService notificationService, AccountRepository accountRepository) {
        this.loyaltyPointHistoryRepository = loyaltyPointHistoryRepository;
        this.baseFilterSpecs = baseFilterSpecs;
        this.authenticatedProvider = authenticatedProvider;
        this.pushNotificationTokenService = pushNotificationTokenService;
        this.notificationService = notificationService;
        this.accountRepository = accountRepository;
    }

    public void save(LoyaltyPointHistoryInputDTO input) {
        LoyaltyPointHistory loyaltyPointHistory = new LoyaltyPointHistory();
        loyaltyPointHistory.setAmount(input.getAmount());
        loyaltyPointHistory.setActionType(input.getActionType());
        loyaltyPointHistory.setAccount(input.getAccount());
        loyaltyPointHistoryRepository.save(loyaltyPointHistory);

        // send notification when change loyalty point
        this.sendNotificationChangeLoyaltyPoint(input);
    }

    public Page<LoyaltyPointHistoryOutputDTO> findAll(LoyaltyPointHistoryFilter filter) {
        Long id = Utils.requireExists(this.authenticatedProvider.getUserId(), "error.dataNotFound");
        Pageable pageable = baseFilterSpecs.page(filter);
        return loyaltyPointHistoryRepository.findAllByAccountId(pageable, id).map(LoyaltyPointHistoryOutputDTO::new);
    }

    private void sendNotificationChangeLoyaltyPoint(LoyaltyPointHistoryInputDTO input) {
        Long userId = authenticatedProvider.getUserId().get();
        String changeUserName = "";
        Optional<Account> accountOptional = accountRepository.findById(userId);
        if (accountOptional.isPresent()){
            changeUserName = accountOptional.get().getFullName();
        }

        Map<String, Object> additionalData = new HashMap<>();
        additionalData.put("account", input.getAccount().getId());

        String point = input.getActionType().equals(LoyaltyActionType.PLUS) ? "được cộng " + input.getAmount() : "bị trừ " + input.getAmount();
        String messageContentPattern = "Bạn đã {0} điểm loyalty bởi \"{1}\"";
        String messageContent = MessageFormat.format(messageContentPattern, point, changeUserName);

        List<UserToken> userTokens = pushNotificationTokenService.getTokensByAccountId(input.getAccount().getId());

        List<NotificationRequest> notificationRequests = Collections.singletonList(NotificationRequest.builder()
                .title("Loyalty Point")
                .content(messageContent)
                .type(NotificationType.LOYALTY_POINT.getType())
                .data(additionalData)
                .accountId(input.getAccount().getId())
                .build());

        // Save notification
        notificationService.saveAll(notificationRequests.subList(0,1));

        if(CollectionUtils.isNotEmpty(userTokens)) {
            // Send push notification
            senPushNotification(notificationRequests.get(0), userTokens);
        }
    }

    private void senPushNotification(NotificationRequest notificationRequest, List<UserToken> userTokens) {
        PushNotificationDto pushNotificationRequest = PushNotificationDto.builder()
                .title(notificationRequest.getTitle())
                .body(notificationRequest.getContent())
                .type(notificationRequest.getType())
                .data(notificationRequest.getData())
                .userTokens(userTokens)
                .build();
        notificationService.sendNotification(pushNotificationRequest);
    }

}
