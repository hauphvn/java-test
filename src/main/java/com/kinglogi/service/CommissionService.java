package com.kinglogi.service;

import com.kinglogi.constant.Role;
import com.kinglogi.dto.commission.CommissionCreateInputDTO;
import com.kinglogi.dto.commission.CommissionOutputDTO;
import com.kinglogi.dto.commission.CommissionUpdateInputDTO;
import com.kinglogi.dto.filter.CommissionFilter;
import com.kinglogi.dto.response.AccountResponse;
import com.kinglogi.entity.Account;
import com.kinglogi.entity.Auction;
import com.kinglogi.entity.CollaboratorGroup;
import com.kinglogi.entity.Commission;
import com.kinglogi.repository.AccountRepository;
import com.kinglogi.repository.CommissionRepository;
import com.kinglogi.service.error.AccessForbiddenException;
import com.kinglogi.service.error.BadRequestException;
import com.kinglogi.service.utils.Utils;
import com.kinglogi.specs.BaseFilterSpecs;
import com.kinglogi.utils.BeanUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class CommissionService {
    private final CommissionRepository commissionRepository;
    private final UserACL userACL;
    private final AccountRepository accountRepository;
    private final BaseFilterSpecs<Commission> baseFilterSpecs;

    public CommissionService(CommissionRepository commissionRepository, UserACL userACL, AccountRepository accountRepository, BaseFilterSpecs<Commission> baseFilterSpecs) {
        this.commissionRepository = commissionRepository;
        this.userACL = userACL;
        this.accountRepository = accountRepository;
        this.baseFilterSpecs = baseFilterSpecs;
    }

    /*
    Save a commission
     */
    public CommissionOutputDTO save(CommissionCreateInputDTO createInput) {
        if (!userACL.isSuperAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }

        if (createInput.getTitle().isEmpty() || Utils.isAllSpaces(createInput.getTitle())
            || createInput.getDescription().isEmpty() || Utils.isAllSpaces(createInput.getDescription())) {
            throw new BadRequestException("error.notBlank");
        }

        if (createInput.getCollaboratorIds().isEmpty()) {
            throw new BadRequestException("error.listEmpty");
        }

        List<Account> accountList = accountRepository.findAllByIdIn(createInput.getCollaboratorIds());
        accountList = accountList.stream().filter(account -> Utils.getRoleCodes(account.getRoles()).contains(Role.COLLABORATOR.getCode())).collect(Collectors.toList());

        Commission commission = new Commission();
        commission.setTitle(createInput.getTitle());
        commission.setDescription(createInput.getDescription());
        commission.setPriceType(createInput.getPriceType());
        commission.setPriceValue(createInput.getPriceValue());
        commissionRepository.save(commission);

        List<Account> accountToSave = new ArrayList<>();
        if (!accountList.isEmpty()) {
            for (Account account: accountList) {
                if (account.getCommission() == null) {
                    account.setCommission(commission);
                    accountToSave.add(account);
                }
            }

            if (accountToSave.size() > 0) {
                accountRepository.saveAll(accountToSave);
            }
        }
        return new CommissionOutputDTO(commission);
    }

    /*
    Update a commission
     */
    public CommissionOutputDTO update(Long id, CommissionUpdateInputDTO updateInput) {
        if (!userACL.isSuperAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }

        Commission commission = Utils.requireExists(commissionRepository.findById(id), "error.dataNotFound");
        List<Account> accountToSave = new ArrayList<>();

        if (updateInput.getTitle() != null) {
            if (updateInput.getTitle().isEmpty() || Utils.isAllSpaces(updateInput.getTitle())) {
                throw new BadRequestException("error.notBlank");
            }

            commission.setTitle(updateInput.getTitle());
        }

        if (updateInput.getDescription() != null) {
            if (updateInput.getDescription().isEmpty() || Utils.isAllSpaces(updateInput.getDescription())) {
                throw new BadRequestException("error.notBlank");
            }

            commission.setDescription(updateInput.getDescription());
        }

        if (updateInput.getPriceType() != null) {
            commission.setPriceType(updateInput.getPriceType());
        }

        if (updateInput.getPriceValue() != null) {
            commission.setPriceValue(updateInput.getPriceValue());
        }

        if (updateInput.getCollaboratorIds().size() > 0) {
            List<Account> accountList = accountRepository.findAllByIdIn(updateInput.getCollaboratorIds());
            // filter account as collaborator and not in any commission
            accountList = accountList.stream().filter(account -> account.getCommission() == null && Utils.getRoleCodes(account.getRoles()).contains(Role.COLLABORATOR.getCode())).collect(Collectors.toList());

            if (accountList.size() > 0) {
                for (Account account: accountList) {
                    account.setCommission(commission);
                    accountToSave.add(account);
                }
            }
        }

        commissionRepository.save(commission);
        accountRepository.saveAll(accountToSave);

        return new CommissionOutputDTO(commission);
    }

    /*
    Delete a commission and account relationship related
     */
    public void delete(Long commissionId) {
        if (!userACL.isSuperAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }

        Commission commission = Utils.requireExists(commissionRepository.findById(commissionId), "error.dataNotFound");
        List<Account> accountList = accountRepository.findAllByIdInCommission(commissionId);
        accountList = accountList.stream().filter(account -> Utils.getRoleCodes(account.getRoles()).contains(Role.COLLABORATOR.getCode())).collect(Collectors.toList());
        List<Account> accountToSave = new ArrayList<>();

        if (accountList.size() > 0) {
            for (Account account: accountList) {
                account.setCommission(null);
                accountToSave.add(account);
            }
            accountRepository.saveAll(accountToSave);
        }

        commissionRepository.delete(commission);
    }

    /*
    Remove a collaborator from commission
     */
    public void removeCollaboratorFromCommission(Long collaboratorId, Long commissionId) {
        if (!userACL.isSuperAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Commission commission = Utils.requireExists(commissionRepository.findById(commissionId), "error.dataNotFound");
        Account account = Utils.requireExists(accountRepository.findById(collaboratorId), "error.dataNotFound");

        if (account.getCommission() != null && !Objects.equals(account.getCommission().getId(), commission.getId())) {
            throw new AccessForbiddenException("error.accountNotBelongToCommission");
        }

        if (account.getCommission() != null) {
            account.setCommission(null);
            accountRepository.save(account);
        }
    }

    /*
    Get account collaborator in
     */
    public Page<AccountResponse> getAccountNotInCommission(CommissionFilter commissionFilter) {
        Pageable pageable = baseFilterSpecs.page(commissionFilter);
        return accountRepository.findAllAccountCommissionNull(pageable, commissionFilter.getKeyword()).map(this::convertToResponse);
    }

    private AccountResponse convertToResponse(Account account) {
        AccountResponse res = BeanUtil.copyProperties(account, AccountResponse.class);
        res.setRoles(Utils.getRoleCodes(account.getRoles()));
        return res;
    }

    public Page<CommissionOutputDTO> findAll(CommissionFilter commissionFilter) {
        if (!userACL.isSuperAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Pageable pageable = baseFilterSpecs.page(commissionFilter);
        Specification<Commission> spec = baseFilterSpecs.search(commissionFilter);
        Page<Commission> commissionPage = commissionRepository.findAll(spec, pageable);
        return commissionPage.map(CommissionOutputDTO::new);
    }

    /*
    Find one commission detail
     */
    public CommissionOutputDTO findOne(Long commissionId) {
        Commission commission = Utils.requireExists(commissionRepository.findById(commissionId), "error.dataNotFound");
        return new CommissionOutputDTO(commission);
    }

    /*
    Get collaborator in a commission
    */
    public Page<AccountResponse> getCollaboratorInCommission(CommissionFilter commissionFilter, Long commissionId) {
        if (!userACL.isSuperAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Pageable pageable = baseFilterSpecs.page(commissionFilter);
        Commission commission = Utils.requireExists(commissionRepository.findById(commissionId), "error.dataNotFound");

        return accountRepository.findAllAccountInCommission(pageable, commission.getId(), commissionFilter.getKeyword()).map(this::convertToResponse);
    }
}
