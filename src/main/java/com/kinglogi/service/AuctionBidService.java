package com.kinglogi.service;

import com.kinglogi.dto.filter.AuctionBidFilter;
import com.kinglogi.dto.response.AuctionBidResponse;

import java.util.List;

public interface AuctionBidService {

    List<AuctionBidResponse> getAuctionBidsByLoggedInUser(AuctionBidFilter filter);

    List<AuctionBidResponse> getAuctionBids(AuctionBidFilter filter);

    AuctionBidResponse getDetail(Long auctionId);
}
