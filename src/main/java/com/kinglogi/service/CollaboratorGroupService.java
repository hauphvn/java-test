package com.kinglogi.service;

import com.kinglogi.dto.collaborator_group.CollaboratorGroupRequest;
import com.kinglogi.dto.collaborator_group.CollaboratorGroupResponse;
import com.kinglogi.service.base.CrudService;

public interface CollaboratorGroupService extends CrudService<CollaboratorGroupRequest, CollaboratorGroupResponse, Long> {
}
