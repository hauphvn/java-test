package com.kinglogi.service;

import com.kinglogi.dto.promotion.PromotionRequest;
import com.kinglogi.dto.promotion.PromotionResponse;
import com.kinglogi.service.base.CrudService;

public interface PromotionService extends CrudService<PromotionRequest, PromotionResponse, Long> {
}
