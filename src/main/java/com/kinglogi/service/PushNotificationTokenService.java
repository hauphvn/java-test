package com.kinglogi.service;

import com.kinglogi.dto.UserToken;
import com.kinglogi.dto.request.SubscribePushNotificationRequest;

import java.util.List;

public interface PushNotificationTokenService {

    boolean subscribe(SubscribePushNotificationRequest request);

    List<UserToken> getTokensByAccountId(Long accountId);

    List<UserToken> getTokensByRoles(String ...roles);

    List<UserToken> getTokensByAccountIds(List<Long> accountIds);
}
