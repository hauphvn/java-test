package com.kinglogi;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import java.time.LocalDateTime;

@SpringBootApplication
@Slf4j
public class KinglogiApiApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(KinglogiApiApplication.class, args);
		log.info("Server started at " + LocalDateTime.now());
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(KinglogiApiApplication.class);
	}

}
