package com.kinglogi.service;

import com.kinglogi.KinglogiApiApplication;
import com.kinglogi.dto.MailData;
import com.kinglogi.dto.mail.MailInfo;
import com.kinglogi.dto.mail.SimpleMailInfo;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.TimeZone;

@RunWith(SpringRunner.class)
@SpringBootTest
@SpringJUnitWebConfig(KinglogiApiApplication.class)
public class EmailServiceTest {

    @Autowired
    private EmailService emailService;

    @Value("classpath:images/logo.png")
    private Resource resourceFile;

    @Autowired
    protected WebApplicationContext webAppContext;

    @BeforeEach
    public void setup() throws Exception {
        MockMvcBuilders.webAppContextSetup(webAppContext).build();// Standalone context
    }

    @Test
    @Ignore
    public void sendSimpleEmail() throws IOException {
        SimpleMailInfo mailInfo = new SimpleMailInfo();
        mailInfo.addTo("gpcodervn@gmail.com");
        mailInfo.setContent("This is an email content");
        mailInfo.setSubject("Test send mail from pic system");
        boolean result = emailService.send(mailInfo);
        Assert.assertTrue(result);
    }

    @Test
    @Ignore
    public void sendMailWithAttachment() throws IOException {
        MailData data = new MailData();
        data.setName("Giang Phan - PIC");
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        data.setSubscriptionDate(df.format(new Date()));
        data.setHobbies(Arrays.asList("Cinema", "Sports", "Music"));
        data.setImageResourceName(resourceFile.getFile().getName());

        MailInfo mailInfo = new MailInfo();
        mailInfo.setFrom("ptgiang56@gmail.com");
        mailInfo.setReplyTo("ptgiang56it@gmail.com");
        mailInfo.addTo("gpcodervn@gmail.com");
        mailInfo.setContent("This is an email content");
        mailInfo.setSubject("Test send mail from pic system");
        mailInfo.addAttachment(resourceFile.getFile().getName(), resourceFile.getFile(), false, true);
        mailInfo.setHtml(true);
        boolean result = emailService.send(mailInfo, "html/mail-template-forgot-password.tpl.html", data);
        Assert.assertTrue(result);
    }

}
