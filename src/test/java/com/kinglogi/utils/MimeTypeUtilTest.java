package com.kinglogi.utils;

import org.junit.Assert;
import org.junit.Test;

public class MimeTypeUtilTest {

    @Test
    public void getMimeTypeTest() {
        Assert.assertEquals(MimeTypeUtil.DEFAULT_MIME_TYPE, MimeTypeUtil.getMimeType(null));
        Assert.assertEquals(MimeTypeUtil.DEFAULT_MIME_TYPE, MimeTypeUtil.getMimeType(" "));
        Assert.assertEquals(MimeTypeUtil.DEFAULT_MIME_TYPE, MimeTypeUtil.getMimeType("."));
        Assert.assertEquals("application/zip", MimeTypeUtil.getMimeType("filename.zip"));
        Assert.assertEquals("application/zip", MimeTypeUtil.getMimeType(".zip"));
        Assert.assertEquals("application/zip", MimeTypeUtil.getMimeType("zip"));
        Assert.assertEquals(MimeTypeUtil.DEFAULT_MIME_TYPE, MimeTypeUtil.getMimeType("unkonwn.filename"));
    }
}
