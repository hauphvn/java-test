package com.kinglogi.vnpay.provider;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kinglogi.vnpay.VnPayRequest;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class VnPayProviderTest {

    @Test
    public void convertToMap_ShouldConvertWithKeepAlphabetOrdering() {
        VnPayRequest request = new VnPayRequest();
        request.setOrderInfo("Rent a car");
        request.setAmount(100l);
        request.setCommand("pay");

        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = mapper.convertValue(request, new TypeReference<LinkedHashMap<String, Object>>() { });

        List<String> keys = new ArrayList<>(map.keySet());

        // Should ignore null field values
        Assert.assertEquals(3, keys.size());
        Assert.assertFalse(map.keySet().contains("vnp_Version"));

        // Should convert and keep alphabet ordering
        Assert.assertTrue(keys.get(0).contains("vnp_Amount"));
        Assert.assertTrue(keys.get(1).contains("vnp_Command"));
        Assert.assertTrue(keys.get(2).contains("vnp_OrderInfo"));
    }
}
